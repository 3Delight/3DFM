set(XML_FILES
	LEareaLightTemplate.xml
	LEdirectionalLightTemplate.xml
	LEpointLightTemplate.xml
	LEspotLightTemplate.xml
	LEvolumeLightTemplate.xml)

install(FILES ${XML_FILES} DESTINATION 2018/templates)
