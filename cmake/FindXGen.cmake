set(VERSION ${XGen_FIND_VERSION})

# Locate Maya XGen include directory.
find_path(XGen${VERSION}_INCLUDE_DIR
	NAMES XGen/XgSplineAPI.h xgen/src/xgsculptcore/api/XgSplineAPI.h
	HINTS $ENV{HOME_3DELIGHT}/building/mayalibs/$ENV{PLAT}/maya${VERSION}/xgen/include
		/Applications/Autodesk/maya${VERSION}/plug-ins/xgen/include
		/usr/autodesk/maya${VERSION}/plug-ins/xgen/include
		"C:/Program Files/Autodesk/Maya${VERSION}/plug-ins/xgen/include"
	NO_DEFAULT_PATH)
set(XGen${VERSION}_INCLUDE_DIRS "${XGen${VERSION}_INCLUDE_DIR}")

# Locate Maya XGen library.
find_library(XGen${VERSION}_LIBRARY
	NAMES AdskXGen
		libAdskXGen # For Maya 2018 on windows which has a misnamed library.
	HINTS $ENV{HOME_3DELIGHT}/building/mayalibs/$ENV{PLAT}/maya${VERSION}/xgen/lib
		/Applications/Autodesk/maya${VERSION}/plug-ins/xgen/lib
		/usr/autodesk/maya${VERSION}/plug-ins/xgen/lib
		"C:/Program Files/Autodesk/Maya${VERSION}/plug-ins/xgen/lib"
	NO_DEFAULT_PATH)
set(XGen${VERSION}_LIBRARIES "${XGen${VERSION}_LIBRARY}")

# Provide target.
find_package_handle_standard_args("XGen"
	REQUIRED_VARS XGen${VERSION}_INCLUDE_DIR XGen${VERSION}_LIBRARY)

if(XGen_FOUND AND NOT TARGET XGen${VERSION}::Devkit)
	add_library(XGen${VERSION}::Devkit INTERFACE IMPORTED)
	set_target_properties(XGen${VERSION}::Devkit PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES "${XGen${VERSION}_INCLUDE_DIRS}"
		INTERFACE_LINK_LIBRARIES "${XGen${VERSION}_LIBRARIES}")
endif()
