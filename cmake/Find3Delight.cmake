# Locate 3Delight root directory.
find_path(3Delight_ROOT_DIR
	NAMES bin/renderdl bin/renderdl.exe
	HINTS ENV DELIGHT
	NO_DEFAULT_PATH
	REQUIRED)

# Locate 3Delight include directory.
find_path(3Delight_INCLUDE_DIR
	NAMES delight.h
	HINTS ${3Delight_ROOT_DIR}/include
		${3Delight_ROOT_DIR}/../include
	NO_DEFAULT_PATH
	REQUIRED)

# Locate 3Delight library.
find_library(3Delight_LIBRARY
	NAMES 3delight
	HINTS ${3Delight_ROOT_DIR}/lib
	NO_DEFAULT_PATH
	REQUIRED)

# Create target.
find_package_handle_standard_args("3Delight"
	REQUIRED_VARS 3Delight_INCLUDE_DIR 3Delight_LIBRARY)

if(3Delight_FOUND AND NOT TARGET 3Delight::3Delight)
	add_library(3Delight::3Delight INTERFACE IMPORTED)
	target_include_directories(3Delight::3Delight
		INTERFACE "${3Delight_INCLUDE_DIR}")
	target_link_libraries(3Delight::3Delight
		INTERFACE "${3Delight_LIBRARY}")
endif()
