#include "dlOpenVDBShader.h"

#include <maya/MFnStringData.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFloatVector.h>
#include <maya/MRampAttribute.h>

#include "DL_autoLoadOSL.h"
#include "DL_utils.h"

MObject dlOpenVDBShader::m_scatteringDensity;
MObject dlOpenVDBShader::m_scatteringColor;
MObject dlOpenVDBShader::m_scatteringAnisotropy;
MObject dlOpenVDBShader::m_multipleScattering;
MObject dlOpenVDBShader::m_multipleScatteringIntensity;
MObject dlOpenVDBShader::m_densityRemapEnable;
MObject dlOpenVDBShader::m_densityRemapRange;
MObject dlOpenVDBShader::m_densityRemapRangeStart;
MObject dlOpenVDBShader::m_densityRemapRangeEnd;
MObject dlOpenVDBShader::m_densityRemapCurve;

MObject dlOpenVDBShader::m_transparencyColor;
MObject dlOpenVDBShader::m_transparencyScale;

MObject dlOpenVDBShader::m_incandescence;

MObject dlOpenVDBShader::m_emissionIntensityScale;
MObject dlOpenVDBShader::m_emissionIntensityGridEnable;
MObject dlOpenVDBShader::m_emissionIntensityRange;
MObject dlOpenVDBShader::m_emissionIntensityRangeStart;
MObject dlOpenVDBShader::m_emissionIntensityRangeEnd;
MObject dlOpenVDBShader::m_emissionIntensityCurve;

MObject dlOpenVDBShader::m_blackbodyIntensity;
MObject dlOpenVDBShader::m_blackbodyMode;
MObject dlOpenVDBShader::m_blackbodyKelvin;
MObject dlOpenVDBShader::m_blackbodyTint;
MObject dlOpenVDBShader::m_blackbodyRange;
MObject dlOpenVDBShader::m_blackbodyRangeStart;
MObject dlOpenVDBShader::m_blackbodyRangeEnd;
MObject dlOpenVDBShader::m_blackbodyTemperatureCurve;

MObject dlOpenVDBShader::m_emissionRampIntensity;
MObject dlOpenVDBShader::m_emissionRampTint;
MObject dlOpenVDBShader::m_emissionRampRangeStart;
MObject dlOpenVDBShader::m_emissionRampRangeEnd;
MObject dlOpenVDBShader::m_emissionRampRange;
MObject dlOpenVDBShader::m_emissionRampColorCurve;

MObject dlOpenVDBShader::m_emissionGridIntensity;
MObject dlOpenVDBShader::m_emissionGridTint;

MObject dlOpenVDBShader::m_scatteringColorCorrectGamma;
MObject dlOpenVDBShader::m_scatteringColorCorrectHueShift;
MObject dlOpenVDBShader::m_scatteringColorCorrectSaturation;
MObject dlOpenVDBShader::m_scatteringColorCorrectVibrance;
MObject dlOpenVDBShader::m_scatteringColorCorrectContrast;
MObject dlOpenVDBShader::m_scatteringColorCorrectContrastPivot;
MObject dlOpenVDBShader::m_scatteringColorCorrectGain;
MObject dlOpenVDBShader::m_scatteringColorCorrectOffset;

MObject dlOpenVDBShader::m_outColor;
MObject dlOpenVDBShader::m_outTransparency;

void* dlOpenVDBShader::creator()
{
	return new dlOpenVDBShader;
}

MStatus dlOpenVDBShader::initialize()
{
	using namespace Utilities;

	addAttribute(DL_OSLShadingNode::CreateShaderFilenameAttribute("vdbVolume"));

	MFnNumericAttribute numAttrFn;

	// Density & Scattering attributes
	m_scatteringDensity = numAttrFn.create(
		"scatteringDensity",
		"scatDens",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Scattering Density" );
	addAttribute( m_scatteringDensity );

	m_scatteringColor = numAttrFn.createColor(
		"scatteringColor",
		"scatCol" );
	numAttrFn.setDefault( 1.0, 1.0, 1.0 );
	numAttrFn.setNiceNameOverride( "Scattering Color" );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_scatteringColor );

	m_scatteringAnisotropy = numAttrFn.create(
		"scatteringAnisotropy",
		"scatAniso",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Scattering Anisotropy" );
	numAttrFn.setMin( -1.0 );
	numAttrFn.setMax( 1.0 );
	addAttribute( m_scatteringAnisotropy );

	m_multipleScattering = numAttrFn.create(
		"multipleScattering",
		"multScat",
		MFnNumericData::kBoolean );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Multiple Scattering Enable" );
	numAttrFn.setDefault( true );
	addAttribute( m_multipleScattering );

	m_multipleScatteringIntensity = numAttrFn.create(
		"multipleScatteringIntensity",
		"msInt",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Multiple Scattering" );
	numAttrFn.setMin( 0.0 );
	numAttrFn.setMax( 1.0 );
	addAttribute( m_multipleScatteringIntensity );

	MObject superReflective = numAttrFn.create(
		"superReflective",
		"superReflective",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Super Reflective" );
	numAttrFn.setMin( 0.0 );
	numAttrFn.setMax( 1.0 );
	addAttribute( superReflective );

	// Density remap attributes
	m_densityRemapEnable = numAttrFn.create(
		"densityRemapEnable",
		"drEnb",
		MFnNumericData::kBoolean );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Enable Density Ramp" );
	numAttrFn.setDefault( false );
	addAttribute( m_densityRemapEnable );

	m_densityRemapRangeStart = numAttrFn.create(
		"densityRemapRangeStart",
		"drRgS",
		MFnNumericData::kDouble,
		0.0 );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Density Range Start" );
	addAttribute( m_densityRemapRangeStart );

	m_densityRemapRangeEnd = numAttrFn.create(
		"densityRemapRangeEnd",
		"drRgE",
		MFnNumericData::kDouble,
		1.0 );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Density Range End" );
	addAttribute( m_densityRemapRangeEnd );

	m_densityRemapRange = numAttrFn.create(
		"densityRemapRange",
		"drRg",
		m_densityRemapRangeStart,
		m_densityRemapRangeEnd );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Density Range" );
	addAttribute( m_densityRemapRange );

	m_densityRemapCurve = MRampAttribute::createCurveRamp(
		"densityRemapCurve",
		"drRmp" );
	addAttribute( m_densityRemapCurve );

	// transparency attributes
	m_transparencyColor = numAttrFn.createColor(
		"transparencyColor",
		"transpCol" );
	numAttrFn.setDefault( 0.0, 0.0, 0.0 );
	numAttrFn.setNiceNameOverride( "Transparency Color" );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_transparencyColor );

	m_transparencyScale = numAttrFn.create(
		"transparencyScale",
		"transpSc",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Transparency Scale" );
	numAttrFn.setMin( 0.0 );
	numAttrFn.setSoftMax( 4.0 );
	addAttribute( m_transparencyScale );

	MObject shadowDensity = numAttrFn.create(
		"shadowDensity", "shadowDensity", MFnNumericData::kFloat);
	makeShaderInputAttribute(numAttrFn);
	numAttrFn.setNiceNameOverride("Shadow Density");
	numAttrFn.setDefault(1.0f);
	numAttrFn.setMin(0.0f);
	numAttrFn.setSoftMax(2.0f);
	numAttrFn.setConnectable(false);
	addAttribute(shadowDensity);

	MObject shadowDensityTint = numAttrFn.createColor(
		"shadowDensityTint", "shadowDensityTint");
	makeShaderInputAttribute(numAttrFn);
	numAttrFn.setNiceNameOverride("Shadow Density Tint");
	numAttrFn.setDefault(1.0f, 1.0f, 1.0f);
	numAttrFn.setConnectable(false);
	addAttribute(shadowDensityTint);

	// incandescence attributes
	m_incandescence = numAttrFn.createColor(
		"incandescence",
		"incan" );
	numAttrFn.setDefault( 1.0, 1.0, 1.0 );
	numAttrFn.setHidden( true );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_incandescence );

	// emission intensity attributes
	m_emissionIntensityScale = numAttrFn.create(
		"emissionIntensityScale",
		"emIntSc",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Intensity Scale" );
	numAttrFn.setMin( 0.0 );
	numAttrFn.setSoftMax( 4.0 );
	addAttribute( m_emissionIntensityScale );

	MObject emissionSingleScatter = numAttrFn.create(
		"emissionSingleScatter", "emissionSingleScatter",
		MFnNumericData::kBoolean);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Visible To Single Scatter Volumes" );
	numAttrFn.setDefault( false );
	addAttribute( emissionSingleScatter );

	m_emissionIntensityGridEnable = numAttrFn.create(
		"emissionIntensityGridEnable",
		"emIntEnb",
		MFnNumericData::kBoolean );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Enable Emission Intensity Grid" );
	numAttrFn.setDefault( false );
	addAttribute( m_emissionIntensityGridEnable );

	m_emissionIntensityRangeStart = numAttrFn.create(
		"emissionIntensityRangeStart",
		"emIntRgS",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Intensity Range Start" );
	addAttribute( m_emissionIntensityRangeStart );

	m_emissionIntensityRangeEnd = numAttrFn.create(
		"emissionIntensityRangeEnd",
		"emIntRgE",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Intensity Range End" );
	addAttribute( m_emissionIntensityRangeEnd );

	m_emissionIntensityRange = numAttrFn.create(
		"emissionIntensityRange",
		"emIntRg",
		m_emissionIntensityRangeStart,
		m_emissionIntensityRangeEnd );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Intensity Range" );
	addAttribute( m_emissionIntensityRange );

	m_emissionIntensityCurve = MRampAttribute::createCurveRamp(
		"emissionIntensityCurve",
		"emIntRmp");
	addAttribute( m_emissionIntensityCurve	);

	// blackbody attributes
	m_blackbodyIntensity = numAttrFn.create(
		"blackbodyIntensity",
		"bbInt",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Intensity" );
	addAttribute( m_blackbodyIntensity );

	m_blackbodyMode = numAttrFn.create(
		"blackbodyMode",
		"bbMode",
		MFnNumericData::kDouble,
		2.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Intensity Mode" );
	addAttribute( m_blackbodyMode );

	m_blackbodyKelvin = numAttrFn.create(
		"blackbodyKelvin",
		"bbKelvin",
		MFnNumericData::kDouble,
		5000.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Kelvin" );
	numAttrFn.setMin( 0 );
	numAttrFn.setSoftMin( 1500.0 );
	numAttrFn.setSoftMax( 10000.0 );
	addAttribute( m_blackbodyKelvin );

	m_blackbodyTint = numAttrFn.createColor(
		"blackbodyTint",
		"bbTint" );
		numAttrFn.setDefault( 1.0, 1.0, 1.0 );
		numAttrFn.setNiceNameOverride( "Blackbody Tint" );
		makeShaderInputAttribute( numAttrFn );
		addAttribute( m_blackbodyTint );

	m_blackbodyRangeStart = numAttrFn.create(
		"blackbodyRangeStart",
		"bbTempRgS",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Temperature Range Start" );
	addAttribute( m_blackbodyRangeStart );

	m_blackbodyRangeEnd = numAttrFn.create(
		"blackbodyRangeEnd",
		"bbTempRgE",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Temperature Range End" );
	addAttribute( m_blackbodyRangeEnd );

	m_blackbodyRange = numAttrFn.create(
		"blackbodyRange",
		"bbTempRg",
		m_blackbodyRangeStart,
		m_blackbodyRangeEnd );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Temperature Range" );
	addAttribute( m_blackbodyRange );

	m_blackbodyTemperatureCurve = MRampAttribute::createCurveRamp(
		"blackbodyTemperatureCurve",
		"bbTempC");
	addAttribute( m_blackbodyTemperatureCurve	);

	// emission ramp attributes
	m_emissionRampIntensity = numAttrFn.create(
		"emissionRampIntensity",
		"emRampInt",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Ramp Intensity" );
	addAttribute( m_emissionRampIntensity );

	m_emissionRampTint = numAttrFn.createColor(
		"emissionRampTint",
		"emRampTint" );
		numAttrFn.setDefault( 1.0, 1.0, 1.0 );
		numAttrFn.setNiceNameOverride( "Emission Ramp Tint" );
		makeShaderInputAttribute( numAttrFn );
		addAttribute( m_emissionRampTint );

	m_emissionRampRangeStart = numAttrFn.create(
		"emissionRampRangeStart",
		"emRampTempRgS",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Ramp Temperature Range Start" );
	addAttribute( m_emissionRampRangeStart );

	m_emissionRampRangeEnd = numAttrFn.create(
		"emissionRampRangeEnd",
		"emRampTempRgE",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Ramp Temperature Range End" );
	addAttribute( m_emissionRampRangeEnd );

	m_emissionRampRange = numAttrFn.create(
		"emissionRampRange",
		"emRampTempRg",
		m_emissionRampRangeStart,
		m_emissionRampRangeEnd );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Ramp Temperature Range" );
	addAttribute( m_emissionRampRange );

	m_emissionRampColorCurve = MRampAttribute::createColorRamp(
		"emissionRampColorCurve",
		"emRampC");
	addAttribute( m_emissionRampColorCurve );

	m_outColor = numAttrFn.createColor( "outColor", "oc" );
	makeOutputAttribute( numAttrFn );
	addAttribute( m_outColor );

	m_outTransparency = numAttrFn.createColor( "m_outTransparency", "ot" );
	makeOutputAttribute( numAttrFn );
	addAttribute( m_outTransparency );

	// Emission Grid attributes
	m_emissionGridIntensity = numAttrFn.create(
		"emissionGridIntensity",
		"emGridInt",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Grid Intensity" );
	addAttribute( m_emissionGridIntensity );

	m_emissionGridTint = numAttrFn.createColor(
		"emissionGridTint",
		"emGridTint" );
	numAttrFn.setDefault( 1.0, 1.0, 1.0 );
	numAttrFn.setNiceNameOverride( "Emission Grid Tint" );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_emissionGridTint );

	// Emission contribution attributes
	const char *contribution_names[][2] =
	{
		{"camera_contribution", "Camera"},
		{"diffuse_contribution", "Diffuse"},
		{"subsurface_contribution", "Subsurface"},
		{"reflection_contribution", "Reflection"},
		{"refraction_contribution", "Refraction"},
		{"hair_contribution", "Hair"},
	};
	for( const auto &cn : contribution_names )
	{
		MObject contrib = numAttrFn.create(
			cn[0], cn[0],
			MFnNumericData::kDouble,
			1.0);
		makeShaderInputAttribute( numAttrFn );
		numAttrFn.setNiceNameOverride( cn[1] );
		numAttrFn.setMin( 0.0 );
		numAttrFn.setSoftMax( 1.0 );
		addAttribute( contrib );
	}

	// color correct attributes
	m_scatteringColorCorrectGamma = numAttrFn.create(
		"scatteringColorCorrectGamma", "scatteringCCG", MFnNumericData::kDouble, 1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Gamma" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 5 );
	addAttribute( m_scatteringColorCorrectGamma );

	m_scatteringColorCorrectHueShift = numAttrFn.create(
		"scatteringColorCorrectHueShift", "scatteringCCHS", MFnNumericData::kDouble, 0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Hue Shift" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 1 );
	addAttribute( m_scatteringColorCorrectHueShift );

	m_scatteringColorCorrectSaturation = numAttrFn.create(
		"scatteringColorCorrectSaturation", "scatteringCCS", MFnNumericData::kDouble, 1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Saturation" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 5 );
	addAttribute( m_scatteringColorCorrectSaturation );

	m_scatteringColorCorrectVibrance = numAttrFn.create(
		"scatteringColorCorrectVibrance", "scatteringCCV", MFnNumericData::kDouble, 1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Vibrance" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 5 );
	addAttribute( m_scatteringColorCorrectVibrance );

	m_scatteringColorCorrectContrast = numAttrFn.create(
		"scatteringColorCorrectContrast", "scatteringCCC", MFnNumericData::kDouble, 1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Contrast" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 5 );
	addAttribute( m_scatteringColorCorrectContrast );

	m_scatteringColorCorrectContrastPivot = numAttrFn.create(
		"scatteringColorCorrectContrastPivot", "scatteringCCCP", MFnNumericData::kDouble, 0.18);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Contrast" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 1 );
	addAttribute( m_scatteringColorCorrectContrastPivot );

	m_scatteringColorCorrectGain = numAttrFn.createColor(
		"scatteringColorCorrectGain", "scatteringCCGain" );
	numAttrFn.setDefault( 1.0, 1.0, 1.0 );
	numAttrFn.setNiceNameOverride( "Gain" );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_scatteringColorCorrectGain );

	m_scatteringColorCorrectOffset = numAttrFn.createColor(
		"scatteringColorCorrectOffset", "scatteringCCO" );
	numAttrFn.setDefault( 0.0, 0.0, 0.0 );
	numAttrFn.setNiceNameOverride( "Offset" );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_scatteringColorCorrectOffset );

	// Density color ramp attributes
	MObject densityColorRampEnable = numAttrFn.create(
		"densityColorRampEnable",
		"dcrEnb",
		MFnNumericData::kBoolean);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Enable Density Color Ramp" );
	numAttrFn.setDefault( false );
	addAttribute( densityColorRampEnable );

	AddRampRange(
		"densityColorRampRange",
		"dcrRg",
		"Density Range");

	MObject densityColorRampCurve = MRampAttribute::createColorRamp(
		"densityColorRampCurve",
		"dcrC");
	addAttribute(densityColorRampCurve);

	// Specular attributes
	MObject specularIntensity = numAttrFn.create(
		"specularIntensity", "specularIntensity", MFnNumericData::kFloat);
	makeShaderInputAttribute(numAttrFn);
	numAttrFn.setNiceNameOverride("Specular Intensity");
	numAttrFn.setDefault(0.0f);
	numAttrFn.setMin(0.0f);
	numAttrFn.setSoftMax(10.0f);
	addAttribute(specularIntensity);

	MObject specularRoughness = numAttrFn.create(
		"specularRoughness", "specularRoughness", MFnNumericData::kFloat);
	makeShaderInputAttribute(numAttrFn);
	numAttrFn.setNiceNameOverride("Specular Roughness");
	numAttrFn.setDefault(0.0f);
	numAttrFn.setMin(0.0f);
	numAttrFn.setMax(1.0f);
	addAttribute(specularRoughness);

	MObject specularGradientSmoothing = numAttrFn.create(
		"specularGradientSmoothing", "specularGradientSmoothing",
		MFnNumericData::kInt);
	makeShaderInputAttribute(numAttrFn);
	numAttrFn.setNiceNameOverride("Specular Gradient Smoothing");
	numAttrFn.setDefault(0);
	numAttrFn.setMin(0);
	numAttrFn.setMax(10);
	addAttribute(specularGradientSmoothing);

	return MStatus::kSuccess;
}

void dlOpenVDBShader::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlOpenVDBShader::compute( const MPlug& i_plug, MDataBlock& i_dataBlock )
{
	if ((i_plug != m_outColor) && (i_plug.parent() != m_outColor))
	{
		return MS::kUnknownParameter;
	}

	/* Just transfer s_color to s_outColor */
	MFloatVector& color =
		i_dataBlock.inputValue( m_scatteringColor ).asFloatVector();

	/* set ouput color attribute */
	MDataHandle outColorHandle = i_dataBlock.outputValue( m_outColor );
	MFloatVector& outColor = outColorHandle.asFloatVector();
	outColor = color;
	outColorHandle.setClean();

	return MStatus::kSuccess;
}

void dlOpenVDBShader::AddRampRange(
	MString i_longName,
	MString i_shortName,
	MString i_niceName)
{
	MFnNumericAttribute numAttrFn;

	MObject rangeStart = numAttrFn.create(
		i_longName + "Start",
		i_shortName + "S",
		MFnNumericData::kDouble,
		0.0);
	Utilities::makeShaderInputAttribute(numAttrFn);
	numAttrFn.setNiceNameOverride(i_niceName + " Start" );
	addAttribute(rangeStart);

	MObject rangeEnd = numAttrFn.create(
		i_longName + "End",
		i_shortName + "E",
		MFnNumericData::kDouble,
		1.0);
	Utilities::makeShaderInputAttribute(numAttrFn);
	numAttrFn.setNiceNameOverride(i_niceName + " End" );
	addAttribute(rangeEnd);

	MObject range = numAttrFn.create(
		i_longName,
		i_shortName,
		rangeStart,
		rangeEnd);
	Utilities::makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride(i_niceName);
	addAttribute(range);
}
