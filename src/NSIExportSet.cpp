#include "NSIExportSet.h"

#include "DelegateTable.h"
#include "DL_utils.h"
#include "MU_typeIds.h"
#include "NSIExportPriorities.h"
#include "NSIExportUtilities.h"
#include "RenderPassInterface.h"

#include <maya/MDagPath.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnSet.h>
#include <maya/MItDependencyGraph.h>
#include <maya/MItSelectionList.h>
#include <maya/MPlug.h>
#include <maya/MSelectionList.h>

#include <assert.h>

NSIExportSet::NSIExportSet(
    const RenderPassInterface &i_render_pass,
	MFnDependencyNode &i_dag,
	MObject &i_object,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate(i_dag, i_object, i_context),
	m_render_pass(i_render_pass)
{
}


/**
	\brief Iterate through all the sets and connect the members.
*/
void NSIExportSet::Connect( const DelegateTable *i_dag_hash )
{
	assert( Object().hasFn(MFn::kSet) );

	MFnSet set( Object() );

	MSelectionList list;
	MStatus status = set.getMembers( list, false /* don't flatten*/ );

	if( status != MStatus::kSuccess )
		return;

	NSI::Context nsi( m_nsi );
	MItSelectionList iter( list );
	for ( ; !iter.isDone(); iter.next() )
	{
		MDagPath item;
		MObject component;
		iter.getDagPath( item, component );

		MFnDagNode node( item, &status );

		if( status != MStatus::kSuccess )
			continue;

		MString source_node = NSIHandle( node, m_live );

		if( i_dag_hash && !i_dag_hash->contains(source_node) )
		{
			continue;
		}

		nsi.Connect(
			source_node.asChar(), "",
			Handle(), "members" );
	}

	nsi.Connect(
		AttributesHandle().asChar(), "",
		NSIHandle( Object(), m_live ).asChar(), "geometryattributes" );
}

void NSIExportSet::Create()
{
	NSICreate( m_nsi, m_nsi_handle, "set", 0, 0x0 );
	NSICreate(
		m_nsi,
		AttributesHandle().asChar(),
		"attributes",
		0, 0x0 );
}

void NSIExportSet::SetAttributes( void )
{
	MFnSet setFn( Object() );
	if( setFn.typeId() != MU_typeIds::DL_SET )
		return;

	bool AOA = AreOverridesApplied();

	NSI::Context nsi( m_nsi );

	int compositingMode = getAttrInt(Object(), "compositingMode", 0);
	SetNSIAttributeOverride(
		nsi, "matte",
		AOA && getAttrBool(Object(), "enableCompositingMode", false),
		compositingMode == 1,
		NSI_MATTE_SET_PRIORITY);

	SetNSIAttributeOverride(
		nsi, "prelit",
		AOA && getAttrBool(Object(), "enableCompositingMode", false),
		compositingMode == 2,
		NSI_PRELIT_SET_PRIORITY);

	SetNSIAttributeOverride(
		nsi, "visibility.camera",
		AOA && getAttrBool(Object(), "enableCameraVisibility", false),
		getAttrInt(Object(), "cameraVisibility", 1),
		NSI_VISIBILITY_SET_PRIORITY);

	SetNSIAttributeOverride(
		nsi, "visibility.diffuse",
		AOA && getAttrBool(Object(), "enableDiffuseVisibility", false),
		getAttrInt(Object(), "diffuseVisibility", 1),
		NSI_VISIBILITY_SET_PRIORITY);

	SetNSIAttributeOverride(
		nsi, "visibility.reflection",
		AOA && getAttrBool(Object(), "enableReflectionVisibility", false),
		getAttrInt(Object(), "reflectionVisibility", 1),
		NSI_VISIBILITY_SET_PRIORITY);

	SetNSIAttributeOverride(
		nsi, "visibility.refraction",
		AOA && getAttrBool(Object(), "enableRefractionVisibility", false),
		getAttrInt(Object(), "refractionVisibility", 1),
		NSI_VISIBILITY_SET_PRIORITY);

	SetNSIAttributeOverride(
		nsi, "visibility.shadow",
		AOA && getAttrBool(Object(), "enableShadowVisibility", false),
		getAttrInt(Object(), "shadowVisibility", 1),
		NSI_VISIBILITY_SET_PRIORITY);

	if( getAttrBool(Object(), "restrictSubsurfaceVisibility", false) )
	{
		nsi.Connect(
			Handle(), "",
			AttributesHandle().asChar(), "visibility.set.subsurface");
	}
	else if( !m_first_setattributes )
	{
		nsi.Disconnect(
			Handle(), "",
			AttributesHandle().asChar(), "visibility.set.subsurface");
	}

	m_first_setattributes = false;
}

void NSIExportSet::SetAttributesAtTime(
	double i_time, bool i_no_motion )
{
}

void NSIExportSet::AttributeChanged(
	const DelegateTable *i_dag_hash,
	MNodeMessage::AttributeMessage i_msg,
	MPlug& i_plug,
	MPlug& i_other_plug)
{
	// 4d8a41fba83eab72afb6837c0a603d0c8f8fad1c
	i_plug.asString();

	if( i_msg & MNodeMessage::kAttributeSet )
	{
		SetAttributes();
	}
	if( i_msg & MNodeMessage::kConnectionMade )
	{
		NSI::Context nsi(m_nsi);
		MString member_handle = NSIHandle(i_other_plug.node(), m_live);
		if( i_dag_hash->contains(member_handle) )
		{
			nsi.Connect(member_handle.asChar(), "", Handle(), "members");
		}
	}
	if( i_msg & MNodeMessage::kConnectionBroken )
	{
		NSI::Context nsi(m_nsi);
		MString member_handle = NSIHandle(i_other_plug.node(), m_live);
		if( i_dag_hash->contains(member_handle) )
		{
			nsi.Disconnect(member_handle.asChar(), "", Handle(), "members");
		}
	}
}

void NSIExportSet::SetNSIAttributeOverride(
	NSI::Context &i_nsi,
	const char *i_attribute_name,
	bool i_enabled,
	int i_value,
	int i_priority)
{
	MString attr_handle = AttributesHandle();
	std::string priority_name = i_attribute_name;
	priority_name += ".priority";
	if( i_enabled )
	{
		i_nsi.SetAttribute(
			attr_handle.asChar(), (
				NSI::IntegerArg(i_attribute_name, i_value),
				NSI::IntegerArg(priority_name, i_priority)));

	}
	else if( !m_first_setattributes )
	{
		i_nsi.DeleteAttribute(attr_handle.asChar(), i_attribute_name);
		i_nsi.DeleteAttribute(attr_handle.asChar(), priority_name);
	}
}

/**
	\returns
		true if the visibility overrides of this set should be applied.

	This can be disabled by the "Override dlSets" control of the render
	settings, which specifies that only some of the dlSet apply.
*/
bool NSIExportSet::AreOverridesApplied()
{
	/* See if there is an override set. If not, all sets are used. */
	MObject override_set_obj = m_render_pass.OverrideDlSets();
	if( !override_set_obj.hasFn(MFn::kSet) )
		return true;

	/* Check if we're the override set. */
	MObject obj = Object();
	if( obj == override_set_obj )
		return true;

	/* Check if we're indirectly inside the override set. */
	MItDependencyGraph itDepGraph(
		obj, MFn::kSet, MItDependencyGraph::kDownstream);
	for ( ;!itDepGraph.isDone(); itDepGraph.next() )
	{
		if( itDepGraph.currentItem() == override_set_obj )
			return true;
	}
	return false;
}
