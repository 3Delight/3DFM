#include "NSIExportIncandescenceLight.h"

#if MAYA_API_VERSION >= 201500

#include "DelegateTable.h"
#include "DL_utils.h"
#include "MU_typeIds.h"
#include "OSLUtils.h"
#include "ShaderDataCache.h"
#include "NSIExportPriorities.h"

#include <nsi.hpp>

#include <maya/MColor.h>
#include <maya/MDagPath.h>
#include <maya/MDataHandle.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnSet.h>
#include <maya/MItDag.h>
#include <maya/MObjectArray.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MSelectionList.h>
#include <maya/MString.h>
#include <maya/MGlobal.h>

#include <math.h>

/**
	We start by deconstrucing what have been created becauser we
	need an attributes set as parent and not a transform.

	The exporter, by default, will construct a tree like this:

	incandecenceLight1
		|
		--- incandecenceLight1|Shape

	All the light-linking operations are done on the parent transform (by
	connecting an attribute to it) and since we use an NSI set for the leaf
	Shape things do not work (can't transition an Attribute to a set through
	an intermeiate transform, doesn't make sense in an NSI world).

	We proceede by substituing ourselves to the parent. It means we delete it
	and create a set in its place. That way, any attribute node connection
	on the parent will be done directly on the set.

	We still create a shape node (a dangling transform) so that we don't
	get error during scene export.
*/
NSIExportIncandescenceLight::NSIExportIncandescenceLight(
		MFnDagNode &dag,
		const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( dag, i_context )
{
	assert( dag.typeId() == MU_typeIds::DL_INCANDESCENCELIGHT );

	NSI::Context nsi(m_nsi);

	MFnDagNode node( Object() );
	MDagPath path;
	node.getPath( path );
	path.pop();
	MFnDagNode transform( path );

	m_set_handle = NSIExportDelegate::NSIHandle(transform, m_live);

	/* This is a transform now, we will change it to a NSI set in \see Create */
	nsi.Delete( m_set_handle.asChar() );
}

NSIExportIncandescenceLight::~NSIExportIncandescenceLight()
{
}

const char* NSIExportIncandescenceLight::NSINodeType() const
{
	return "dlIncandescenceLightShape";
}

void NSIExportIncandescenceLight::Create()
{
	NSI::Context nsi(m_nsi);

	/*
		We need a transform for the general mechanics of the exporter. There
		are connections related to object visibility that will be made to this.
		Not important in the case of this light.

		NOTE: This sis the ONLY place where you should be using Handle().
	*/
	nsi.Create( Handle(), "transform" );

	/* Needed for multi-light layer */
	nsi.Create( m_set_handle.asChar(), "set" );

	/* Needed for light linking of controlled objects */
	MString attributes_handle = AttributesHandle();
	nsi.Create( attributes_handle.asChar(), "attributes" );

	nsi.Connect(
		attributes_handle.asChar(), "",
		m_set_handle.asChar(), "geometryattributes" );

	/*
		All our lights are created invisible to all ray types. Their membership
		status in the defaultLightSet will override this visibility. The case
		of the incandescent light is different as it doesn't have "camera"
		visibility.

		\see NSIExportDefaultLightSet
		\see NSIExportLight
	*/
	nsi.SetAttribute( attributes_handle.asChar(),
		(
			NSI::IntegerArg("visibility.diffuse", 0),
			NSI::IntegerArg("visibility.diffuse.priority", NSI_LIGHT_PRIORITY),
			NSI::IntegerArg("visibility.specular", 0),
			NSI::IntegerArg("visibility.specular.priority", NSI_LIGHT_PRIORITY),
			NSI::IntegerArg("visibility.reflection", 0),
			NSI::IntegerArg("visibility.reflection.priority", NSI_LIGHT_PRIORITY),
			NSI::IntegerArg("visibility.refraction", 0),
			NSI::IntegerArg("visibility.refraction.priority", NSI_LIGHT_PRIORITY),
			NSI::IntegerArg("visibility.shadow", 0),
			NSI::IntegerArg("visibility.shadow.priority", NSI_LIGHT_PRIORITY)
		) );
}

/**
	This method is meant to be used as is for Live modifictions. It's a bit
	ham-fisted because it sets all attributes as soon as one is modified but
	it is robust.

	\ref m_current_multipliers
*/
void NSIExportIncandescenceLight::SetAttributes()
{
	if( m_current_multipliers.size() )
	{
		/*
			We are in Live mode because SetAttributes can't be called twice
			otherwise.

			Before setting a new state we need to reset the current state
			to a neutral position. This means:

			1- All multipliers are 1.
			2- The lightset is empty.
		*/

		/* This will take care of shader parameter "incandescence_multiplier" */
		float black[3] = { 1.f, 1.f, 1.f };
		NSI::Context nsi( m_nsi );

		for( const MString &handle : m_current_multipliers )
		{
			nsi.SetAttribute(
				handle.asChar(),
				NSI::ColorArg("incandescence_multiplier", black));
		}

		/* This will take care of the Multi-Light output */
		nsi.Disconnect( NSI_ALL_NODES, "", m_set_handle.asChar(), "objects" );

		m_current_multipliers.clear();

		/*
			Since the _3delight_geometry could have been chanfed. We
			connect everything again. Note that passing 0x0 to connect
			here is not an error as ALL objects are exported in live
			mode.
		*/
		Connect( 0x0 );
	}
}

void NSIExportIncandescenceLight::SetAttributesAtTime(
	double i_time,
	bool i_no_motion )
{
}

void NSIExportIncandescenceLight::Connect( const DelegateTable *i_dag_hash )
{
	float incandescenceColor[3];
	ComputeOutputColor(incandescenceColor);

	ScanAttrGeometry(
		&NSIExportIncandescenceLight::SetAttributeForShader,
		i_dag_hash, incandescenceColor );

	float dummyColor[3];
	ScanAttrGeometry(
		&NSIExportIncandescenceLight::ConnectToObjects, i_dag_hash, dummyColor);
}

void NSIExportIncandescenceLight::ScanAttrGeometry(
	Fct i_fct, const DelegateTable *i_dag, float i_color[])
{
	MFnDependencyNode dep_node( Object() );
	MPlug plugGeometry = dep_node.findPlug("_3delight_geometry", true);

	assert( plugGeometry.isArray() );

	for (unsigned i = 0; i < plugGeometry.numElements(); i++)
	{
		MPlug single = plugGeometry[i];

		MPlugArray destinations;
		single.connectedTo(destinations, true, true);
		for (unsigned j = 0; j < destinations.length(); j++)
		{
			MStatus status;
			MFnDagNode dagNode(destinations[j].node(), &status);
			if (status == MS::kSuccess)
			{
				MString destination_node = NSIHandle(dagNode, m_live);
					MDagPath dagPath;
				dagNode.getPath(dagPath);
				(this->*i_fct)(dagPath, i_dag, i_color);
			}
			else if( destinations[j].node().hasFn(MFn::kSet) )
			{
				MFnSet set( destinations[j].node() );

				/*
					The getMembers bellow doesn't recurse inside the transforms
					inside the set. But it isn't a problem since the callee, in
					this case:

					1. Needs to find the Shading Groups and this can be done
					   through getConnectedSetsAndMembers.

					   \see SetAttributeForShader

					2. Needs to connect objects to an NSI set and this can be
					   done using the head transform.

					   \see ConnectToObjects
				*/
				MSelectionList members;
				set.getMembers(members, true);
				for (unsigned k = 0; k < members.length(); k++)
				{
					MDagPath dagPath;
					status = members.getDagPath(k, dagPath);
					if (status == MS::kSuccess)
					{
						(this->*i_fct)(dagPath, i_dag, i_color);
					}
				}
			}
		}
	}
}

void NSIExportIncandescenceLight::ComputeOutputColor(float o_color[])
{
	MColor inColor;
	::getAttrColor(Object(), "colorLight", inColor);
	float intensity = ::getAttrDouble(Object(), "intensity", 1.0);
	float exposure = ::getAttrDouble(Object(), "exposure", 1.0);

	o_color[0] = inColor[0] * intensity * exp2f(exposure);
	o_color[1] = inColor[1] * intensity * exp2f(exposure);
	o_color[2] = inColor[2] * intensity * exp2f(exposure);
}

void NSIExportIncandescenceLight::SetAttributeForShader(
	const MDagPath& i_dagPath,
	const DelegateTable *i_dag,
	float i_color[])
{
	MItDag dagIter;
	for (dagIter.reset(i_dagPath); !dagIter.isDone(); dagIter.next())
	{
		MDagPath path;
		dagIter.getPath(path);
		MStatus status;
		MFnDagNode dagNode(path, &status);
		if (status != MS::kSuccess)
		{
			continue;
		}

		if( i_dag &&
			!i_dag->contains(NSIExportDelegate::NSIHandle(dagNode, m_live)) )
		{
			/* We don't export this object, probably because it's invisible.*/
			continue;
		}

		MObjectArray sets, comps;
		unsigned int instanceNumber = path.instanceNumber();
		dagNode.getConnectedSetsAndMembers(instanceNumber, sets, comps, false);

		// Search for shading group
		for (unsigned k = 0; k < sets.length(); k++)
		{
			if (!sets[k].hasFn(MFn::kShadingEngine))
			{
				continue;
			}
			MFnSet set(sets[k], &status);
			if (status != MS::kSuccess)
			{
				continue;
			}
			MObject shaderObj;
			// Try volume shader first, then surface if there is no volume.
			if (GetShader("_3delight_volumeShader", set, shaderObj) ||
			    GetShader("volumeShader", set, shaderObj) ||
			    GetShader("_3delight_surfaceShader", set, shaderObj) ||
			    GetShader("surfaceShader", set, shaderObj))
			{
				MFnDependencyNode shader( shaderObj, &status );
				SetIncandescence( dagNode, shader, i_color );
			}
		}
	}
}

void NSIExportIncandescenceLight::ConnectToObjects(
	const MDagPath& i_dagPath,
	const DelegateTable *i_dag,
	float i_color[])
{
	MFnDagNode dagFn( i_dagPath.node() );
	MString handle = NSIExportDelegate::NSIHandle( dagFn, m_live );
	if( i_dag && !i_dag->contains(handle) )
	{
		/* We don't export this object, probably because it's invisible. */
		return;
	}

	NSI::Context nsi( m_nsi );
	nsi.Connect( handle.asChar(), "", m_set_handle.asChar(), "objects" );
}

bool NSIExportIncandescenceLight::GetShader(
	const char* i_type,
	const MFnSet& i_set,
	MObject& o_shader) const
{
	MStatus status;
	MPlug shaderPlug = i_set.findPlug(i_type, true, &status);
	if (!shaderPlug.isNull())
	{
		MPlugArray destinations;
		shaderPlug.connectedTo(destinations, true, false);
		if (destinations.length() > 0)
		{
			o_shader = destinations[0].node();
		}
	}
	return !o_shader.isNull();
}

/**
	\brief Do the necessary connections to multiply the incandescene of i_shader
	by i_color.

	In order for this to work, we need a "incandescence_multiplier" plug to
	witch we can connect modify with the color and intensity specified in
	this light source.

	We only support shaders with "incandescence_multiplier" defined and we also
	try to help the user in case of improper setup.
*/
void NSIExportIncandescenceLight::SetIncandescence(
	const MFnDagNode& i_dagNode,
	const MFnDependencyNode& i_shader,
	float i_color[] )
{
	const ShaderData *shader_data =
		ShaderDataCache::Instance()->Add( i_shader.object() );

	if( !shader_data )
	{
		assert( false );
		return;
	}

	const char *incandescence_multiplier = "incandescence_multiplier";
	if( !shader_data->GetParameterData(
		incandescence_multiplier, NSIExportUtilities::eInput) )
	{
		MGlobal::displayError(
			"3Delight for Maya: shader " + i_shader.name() +
			" does not support incandescence light functionality." );
		return;
	}

	MString shaderName =
		NSIExportDelegate::NSIShaderHandle(i_shader.object(), m_live);

	NSI::Context nsi( m_nsi );
	nsi.SetAttribute(
		shaderName.asChar(),
		NSI::ColorArg(incandescence_multiplier, i_color));

	m_current_multipliers.push_back( shaderName );
	return;
}

#endif
