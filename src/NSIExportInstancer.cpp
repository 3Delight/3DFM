
#include "NSIExportInstancer.h"
#include "NSIExportTransform.h"

#include <maya/MFnInstancer.h>
#include <maya/MStatus.h>
#include <maya/MDagPath.h>
#include <maya/MGlobal.h>

#include <assert.h>
#include <nsi.hpp>

/*
	\brief A special ctor for the instancer because we need the MDagPath

	Trying to do the following :

		MFnInstancer instancer( m_object_handle.object() );

	Will "work" until you try to call allInstances(), at which time Maya will
	not process the call. Note that calling particleCount() will return the
	good number of particles, strangely. I am not sure why all this mess but
	I have no time to investiage further. In our previouew RenderMan export we
	also used a MDagPath.
*/
NSIExportInstancer::NSIExportInstancer(
	MDagPath &i_path,
	MFnDagNode &i_dag,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( i_dag, i_context ),
	m_path(i_path)
{
	m_transform = new NSIExportTransform( i_dag, i_context );

	m_instance_handle = m_nsi_handle;
	m_instance_handle += "|instance";
}

NSIExportInstancer::~NSIExportInstancer()
{
	delete m_transform;
}

/**
	\brief A no-op method because everyhing is done in the mesh delegate
*/
const char *NSIExportInstancer::NSINodeType( void ) const
{
	return "transform";
}

/**
	\brief Crate main instancer transform
*/
void NSIExportInstancer::Create( void )
{
	assert( m_transform );

	/*
		Creating a transform for the entire instance.
	*/
	m_transform->Create();

	NSI::Context nsi( m_nsi );
	nsi.Create( m_instance_handle, "instances" );
	nsi.Connect( m_instance_handle, "", m_nsi_handle, "objects" );
}

/**
	\brief Export matrices and also make connections to instanced DAG locations.

	NOTE: As a stop-gap measure tu support appearing and disappearing particles,
	we re-create the the entire tree here. NSI allows this so let's push the
	limits of sanity.
*/
void NSIExportInstancer::SetAttributesAtTime( double i_time, bool i_no_motion )
{
	assert( m_transform );
	m_transform->SetAttributesAtTime( i_time, i_no_motion );

	MFnInstancer instancer( m_path );
	int m_num_particles = instancer.particleCount();

	if( m_num_particles == 0 )
		return;

	NSI::Context nsi( m_nsi );

	MMatrix particleMatrix;
	MDagPathArray particlePaths;

	MMatrixArray nsi_transforms;

	std::vector<MString> paths;
	std::vector<int> model_indices;

	for( int p=0; p<m_num_particles; p++ )
	{
		/*
			Create a transform for each particle and connect it to the
			instancer main transform. Set the matrix to the value
			provided by Maya for this particle position.
		*/
		int numInstances =
			instancer.instancesForParticle( p, particlePaths, particleMatrix );

		for( int i = 0; i < numInstances; i++ )
		{
			const MDagPath& instancedPath = particlePaths[i];
			MMatrix instancedPathMatrix = instancedPath.inclusiveMatrix();
			MMatrix finalMatrixForPath = instancedPathMatrix * particleMatrix;

			nsi_transforms.append( finalMatrixForPath );

			MFnDagNode node( instancedPath );
			MString path = NSIInstanceHandle( node, m_live );

			/* Find this path in already output paths. */
			int index=-1;
			for( int i=0; i<paths.size(); i++ )
			{
				if( paths[i] == path )
				{
					index = i;
					break;
				}
			}

			if( index == -1 )
			{
				/* new path */
				model_indices.push_back( paths.size() );
				paths.push_back( path );
			}
			else
				model_indices.push_back( index );
		}
	}

	nsi.Disconnect( ".all", "", m_instance_handle, "sourcemodels" );

	for( int i=0; i<paths.size(); i++ )
	{
		nsi.Connect(
			paths[i].asChar(), "", m_instance_handle, "sourcemodels",
			NSI::IntegerArg("index", i) );
	}

	int nsi_particles = nsi_transforms.length();
	nsi.SetAttributeAtTime( m_instance_handle, i_time,
	(
		 *NSI::Argument("modelindices").SetType(NSITypeInteger)
		   	->SetCount(nsi_particles)
			->SetValuePointer(&model_indices[0]),
		*NSI::Argument("transformationmatrices" ).SetType( NSITypeDoubleMatrix )
			->SetCount(nsi_particles )
			->SetValuePointer(&nsi_transforms[0])
	) );
}

/**
	\sa SetAttributesAtTime

	We only output the main instancer transform here and even that should be a
	no-op. But we do it in case things change in the future.
*/
void NSIExportInstancer::SetAttributes( void )
{
	m_transform->SetAttributes();
}
