#ifndef __NSIExportDelegate_h
#define __NSIExportDelegate_h

#include <maya/MObjectHandle.h>
#include <maya/MFnDagNode.h>
#include <maya/MString.h>
#include <maya/MMessage.h>

#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include <vector>

#include <nsi.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MFnDagNode;
class MFnDependencyNode;
#endif
class DelegateTable;

/**
	\brief An interface of an NSI exporter for a specific maya Object

	For example, one can have an exported for meshes, lights, cameras ...
*/
class NSIExportDelegate
{
public:
	/**
		\brief Holds various information related to the export.

		This struct was created to gather the delegate's constructor parameters,
		mostly to avoid having to change the signature of all delegate classes
		each time a new parameter is introduced.
	*/
	struct Context
	{
		Context(NSIContext_t i_nsi, bool i_live)
		:	m_nsi(i_nsi),
			m_live_render(i_live)
		{
		}

		NSIContext_t m_nsi;
		bool m_live_render;
	};

protected:
	NSIExportDelegate( MFnDagNode &, MObject &, const Context& i_context );

public:

	/**
		\brief Ctor for DAG exporters such as meshes and instancers
	*/
	NSIExportDelegate( MFnDagNode &, const Context& i_context );

	/**
		\brief Ctor for DAG exporters as above but with customized handle.
	*/
	NSIExportDelegate( MFnDagNode &, const Context& i_context, const MString & );

	/**
		\brief ctor for Dependency nodes such as sets and materials (shading
		groups)
	*/
	NSIExportDelegate( MFnDependencyNode &, MObject &, const Context& i_context );

	/**
		\brief
	*/
	NSIExportDelegate( MObject& i_object, const Context& i_context );

	virtual ~NSIExportDelegate();

	/**
		\brief Set total number of motion samples needed for this exporter
	*/
	void SetMotionSamples( int i_samples )
	{
		assert( i_samples>0 );
		m_motion_samples = i_samples;
	}
	int MotionSamples( void ) const { return m_motion_samples; }

	/**
		\brief Creates the NSI node for this delegate
	*/
	virtual void Create() = 0;

	/**
		\brief Sets the "nicename" attribute for the associated node.

		\param i_referenced_file
			The referenced file from which the node was imported, if applicable.
			Ideally, this parameter wouldn't be needed, but we haven't found a
			way to get this information directly from the node, yet.

		This would normally be private and called from SetAttributes, if it
		hadn't been designed as a pure virtual function (which is not called
		from the subclasses).
	*/
	void SetNiceName(const MString& i_referenced_file = MString());

	/**
		\brief Sets motion-less attributes
	*/
	virtual void SetAttributes( void ) = 0;

	/**
		\brief Set attributes that depend on time

		\param i_time
			The time at which to output the attributes

		\param i_no_motion
			There are is actually no motion blur in the render, the delegate
			can output attributes without relying on i_time
	*/
	virtual void SetAttributesAtTime(
		double i_time,
		bool i_no_motion ) = 0;

	/**
		\brief Make any needed connections that must be done _after_ all the
		Create()s of the scene export.

		\param i_da_hash
			A hash table containing a Handle() -> Delegate lookup. This can
			be used to assess if an object is *really* part of the rendered
			scene. Even though an object is part of, e.g., a Maya set it doesn't
			mean that we are rendering it. This can happen when using the
			"Scene Elements" feature with a different objects/lights to render
			set. In short, do not connect to elements that are not part of the
			hash table if you want to avoid render error message (although the
			image will still be good if you do useless connections)
	*/
	virtual void Connect( const DelegateTable *i_dag_hash ) {}

	/**
		\brief Delete all NSI associated nodes

		Note that this doesn't delete the class itself, it concerns _only_ NSI
		nodes.
	*/
	virtual void Delete( bool i_recursive = false );

	/**
		\brief Any processing which must be done after all the
		SetAttributesAtTime
	*/
	virtual void Finalize() {}

	/**
		\brief Return true if this object is deformed.
	*/
	virtual bool IsDeformed( void ) const { return false; }

	/**
		\brief Register callbacks specific to this exporter

		\return true if the delegate registers it's own callbacks, false if
		it lets the exported deal with it.
	*/
	virtual bool RegisterCallbacks( void ) { return false; }

	/**
		\brief Specifies that this delegate is a member of some DlSet.
	*/
	virtual void MemberOfSet( MObject & ) { }

	/*
		--- Utilities
	*/

	/**
		\brief Returns true if the Maya object associated with this delegate
		is usable. Do not call anyt of the Set* methods if this this not true
	*/
	bool IsValid( void ) const { return m_object_handle.isAlive(); }

	/**
		\brief Returns the NSI handle of this delegate
	*/
	const char *Handle( void ) const
	{
		return m_nsi_handle;
	}

	/**
		\brief Rename the NSI handle of this delegate
	*/
	void RenameHandle( const char* i_new_name );

	/**
		\brief Set the visibility state of this delegate.

		Invisible delegates have their SetAttributes(), SetAttributesAtTime(),
		Connect() and Finalize() calls delayed until they become visible in
		IPR. For other types of export, they are not created at all.
	*/
	void SetVisible(bool v) { m_visible = v; }
	/** \returns The visibility state of this delegate. */
	bool Visible() const { return m_visible; }

	/**
		\brief Call this before having the delegate do SetAttributes during Node
			Dirty events.

		Returns true if the delegate is ready to process a node dirty event.

		This is to avoid re-entrant NodeDirty handling in a delegate.

		This may happen for instance with particle systems, where the defaul
		NodeDirtyCB will call SetAttributes, which will attempt to retrieve the
		attribute values. That in turn will trigger a dynamic system evaluation,
		which provokes a node dirty event.
	*/
	bool NodeDirtyBegin();

	/**
		\brief Call this to signal the end of a Node Dirty event processing for this
			delegate.
	*/
	void NodeDirtyEnd();

	/**
		For all NSIHandle variations: calling the MFnDagNode version will
		return the same value a the MObject version, except when producing
		"readable" NSI handles, which are based on the full path name.
	*/

	/**
		\brief The NSI handle of a DAG object.
	*/
	static MString NSIHandle( const MFnDagNode &, bool i_live );

	/**
		\brief The NSI handle of an object.
	*/
	static MString NSIHandle( const MObject &, bool i_live );

	/**
		\brief The NSI handle of the NSI attribute node related to a Maya object.
	*/
	static MString NSIAttributesHandle( const MObject &, bool i_live );

	/**
		\brief The NSI handle of the NSI attribute node related to a DAG object.
	*/
	static MString NSIAttributesHandle( const MFnDagNode &, bool i_live );

	static MString NSIAttributeOverridesHandle( const MFnDagNode &, bool i_live );
	/**
		\brief The NSI handle of the NSI shader node related to a Maya object.
	*/
	static MString NSIShaderHandle( MObject, bool i_live );

	/**
		\brief The NSI handle of the NSI transform node related to the specific
		instance designated by the specified DAG object.

		\param i_dag_node
			A geometric DAG node of a specific instance.

		Returns an empty string if:
			- i_node does not refer to a specific instance, or
			- i_node is not a geometric object.

		Constructing a MFnDagNOde from a MDagPath will refer to a specific instance.
		Constructing a MFnDagNode from a MObject will not. (AFAIK)
	*/
	static MString NSIInstanceHandle( const MFnDagNode& i_dag_node, bool i_live );

	/**
		\brief The NSI handle of the NSI transform node related to the specific
		instance designated by a DAG node and its parent path.

		\param i_shape
			A DAG geometric object. It does not need to refer to a specific instance

		\param i_parent
			A DAG path to the specific i_shape instance's parent.

		Returns an empty string if i_shape is not a geometric DAG object
	*/
	static MString NSIInstanceHandle( MObject i_shape, const MDagPath& i_parent, bool i_live );

	/**
		\brief Rreturns the handle to attributes of this node.
	*/
	MString AttributesHandle() const
	{
		return NSIAttributesHandle( Object(), m_live );
	}

	/**
		\brief return a reference to this MObject
	*/
	const MObject &Object( void ) const
	{
		assert( IsValid() );
		return m_object_handle.objectRef();
	}

	NSIContext_t NSIContext( void ) const
	{
		return m_nsi;
	}

	/**
		\brief Adds a Maya callback ID associated with this delegate
	*/
	void AddCallbackId( MCallbackId id )
	{
		m_ids.push_back( id );
	}

protected:
	void RemoveCallbacks( void );

protected:
	const char *m_nsi_handle;
	NSIContext_t m_nsi;
	int m_motion_samples;

	std::vector<MCallbackId> m_ids;

private:
	MObjectHandle m_object_handle;

	/// Set & Queried by NodeDirtyBegin() / End()
	bool m_nodeDirtyInProgress;

	bool m_visible{true};

protected:
	/**
		Indicates whether we're in an IPR/Live Render or not. Used to generate
		NSI handles in the proper format.
	*/
	const bool m_live;
};

#endif
