#ifndef __NSIExportImagePlane_h
#define __NSIExportImagePlane_h

#include "NSIExportDelegate.h"
#include "NSIExportShader.h"

#include <maya/MDagPath.h>

class NSIExportImagePlane : public NSIExportDelegate
{
public:
	NSIExportImagePlane(
		MFnDagNode &i_dag,
		const NSIExportDelegate::Context &i_context );
	~NSIExportImagePlane();

	void Create() override;
	void SetAttributes() override;
	void SetAttributesAtTime(
		double i_time,
		bool i_no_motion ) override;
	void Delete( bool i_recursive ) override;

private:
	std::string GeoHandle() const;

private:
	/* Export delegate for the image plane shader. */
	NSIExportShader m_shader_delegate;
	/* Path to this image plane. */
	MDagPath m_dag_path;
};

#endif
