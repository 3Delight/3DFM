#include "NSIExportShadingGroup.h"

#include <assert.h>
#include <math.h>

#include <nsi.hpp>

#include <maya/MDagPath.h>
#include <maya/MDagPathArray.h>
#include <maya/MDataHandle.h>
#include <maya/MFnSet.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MGlobal.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItSelectionList.h>
#include <maya/MObjectSetMessage.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MTimerMessage.h>
#include <maya/MSelectionList.h>

#include "DL_utils.h"
#include "NSIExport.h"
#include "NSIExportPriorities.h"
#include "NSIExportShader.h"
#include "NSIExportUtilities.h"

#include "OSLUtils.h"

using namespace NSIExportUtilities;

struct NSIExportShadingGroup::ShaderDesc
{
	/* The name of the plug on the maya shadingEngine. */
	const char *m_plug_name;
	/* The name of the 3Delight specific plug on the maya shadingEngine. */
	const char *m_3delight_plug_name;
	/* The NSI attribute this connects to on geometryattributes. */
	const char *m_nsi_type;
	/* Optional. For shaders which go through an extra terminal node. */
	/* The handle suffix of that node. */
	const char *m_shading_engine_suffix;
	/* The input to connect to on that node. */
	const char *m_shading_engine_input;
};

const NSIExportShadingGroup::ShaderDesc NSIExportShadingGroup::s_shaders[] =
{
	{"surfaceShader", "_3delight_surfaceShader", "surfaceshader",
		"|surface", "i_surface"},
	{"displacementShader", "_3delight_displacementShader", "displacementshader",
		"|displacement", "i_displacement"},
	{"volumeShader", "_3delight_volumeShader", "volumeshader",
		nullptr, nullptr},
};

NSIExportShadingGroup::NSIExportShadingGroup(
	MFnDependencyNode &i_dep,
	MObject &i_object,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate(i_dep, i_object, i_context)
{
}

NSIExportShadingGroup::~NSIExportShadingGroup()
{
}

/**
	\brief Creates the attribute nodes to which the materials will be connected
	\sa Connect
*/
void NSIExportShadingGroup::Create( void )
{
	NSI::Context nsi( m_nsi );

	nsi.Create( m_nsi_handle, "attributes" );
	nsi.Create( SurfaceSGHandle().asChar(), "shader" );
	nsi.Create( DisplacementSGHandle().asChar(), "shader" );
}

/**
	\brief Iterate through all the members and connect the attributes
	to them

	Note that this goes differently than a set where we connect members
	to a transform, the direction is reversed: we connect an attribute
	to each member.

	\sa NSIExportShader
*/
void NSIExportShadingGroup::Connect( const DelegateTable *i_dag_hash )
{
	assert( Object().hasFn(MFn::kSet) );

	MFnSet set( Object() );

	MSelectionList list;
	MStatus status = set.getMembers( list, false /* don't flatten */ );
	if( status != MStatus::kSuccess )
		return;

	/*
		Connect the shading networks to the SurfaceSG / DisplacementSG shaders,
		and connect these shaders to the proper attribute on m_nsi_handle.
		For the volume shaders, there is no SG shader so we connect directly.
	*/
	for( const ShaderDesc &desc : s_shaders )
	{
		MPlug shader;
		if( HasShader(desc.m_3delight_plug_name, &shader) ||
		    HasShader(desc.m_plug_name, &shader) )
		{
			UpdateShaderConnection(desc, shader, true);
		}

	}

	/*
		Handle the shading group membership
	*/
	MItSelectionList iter( list );
	for ( ; !iter.isDone(); iter.next() )
	{
		MDagPath item;
		MObject component;
		iter.getDagPath( item, component );

		MFnDagNode node( item, &status );
		if( status != MStatus::kSuccess )
			continue;

		// Make sure the connected DAG node has a delegate
		MString destination_node = NSIHandle(node, m_live);
		if( i_dag_hash && !i_dag_hash->contains(destination_node) )
		{
			continue;
		}

		// Connect the shading group to the instance transform
		MString shadedObjHandle = NSIInstanceHandle(node, m_live);
		assert( shadedObjHandle.length() > 0 );
		if( shadedObjHandle.length() == 0 )
		{
			shadedObjHandle = destination_node;
		}

		ConnectToGeo( node, component, shadedObjHandle );
	}
}

void NSIExportShadingGroup::ConnectToGeo(
	MFnDagNode &node, MObject &component, MString &shadedObjHandle ) const
{
	MString surfaceSG_handle = SurfaceSGHandle();
	NSI::Context nsi( m_nsi );
	if( node.object().hasFn(MFn::kMesh) )
	{
		bool enable_override = false;
		MPlug override_plug =	node.findPlug( "_3delight_isOverrideVolume", true );
		override_plug.getValue( enable_override );

		bool enable_shader_override = false;
		MPlug enable_shader_override_plug =
			node.findPlug( "_3delight_enableSurfaceShaderOverride", true );
		enable_shader_override_plug.getValue( enable_shader_override );

		if( enable_override )
		{
			nsi.Connect(
					NSIExport::TransparentSurfaceHandle(), "",
					shadedObjHandle.asChar(), "geometryattributes" );

			if( enable_shader_override )
			{
				NSI::ArgumentList argList;
				argList.Add(new NSI::IntegerArg(
					"priority", NSI_SPATIAL_OVERRIDE_PRIORITY));
				nsi.Connect(
						surfaceSG_handle.asChar(), "",
						NSIAttributeOverridesHandle( node, m_live ).asChar(),
						"surfaceshader",
						argList );
			}
		}
		else if( component.isNull() )
		{
			/*
			   The assignment involves the whole object. Connect the shader's
			   attribute node to the mesh's parent transform.
			 */
			nsi.Connect(
					Handle(), "",
					shadedObjHandle.asChar(), "geometryattributes" );
		}
		else
		{
			/*
			   The selection list item has a non-null component, from
			   which we can extract the mesh faces involved in the
			   assignment. Retrieve the faces and place them in a faceset
			   NSI node, which is then connected between the mesh node and
			   the attribute node used for the shader.
			 */

			// Gather the face indices in a integer array
			MDagPath path;
			node.getPath( path );
			MItMeshPolygon faceIt( path, component );
			std::vector<int> faces;
			faces.reserve( faceIt.count() );

			for( faceIt.reset() ; !faceIt.isDone() ; faceIt.next() )
			{
				faces.push_back( faceIt.index() );
			}

			// Create the faceset NSI node and set its faces attribute
			MString faceset_handle = shadedObjHandle + "|" + Handle() + "|faceset";
			nsi.Create( faceset_handle.asChar(), "faceset" );

			NSI::ArgumentList argList;
			argList.Add( NSI::Argument::New( "faces" )
					->SetType( NSITypeInteger )
					->SetCount( faceIt.count() )
					->SetValuePointer( &faces[0] ) );

			NSI::IntegersArg facesArg( "faces", &faces[0], faces.size() );
			nsi.SetAttribute( faceset_handle.asChar(), argList );

			MString destination_node = NSIHandle(node, m_live);

			/*
			   Connect the faceset node to the mesh node.
			   Note that doing this prevents per-instance, per-face shader
			   assignments, which NSI provides no way to achieve at this time.
			 */
			nsi.Connect(
					faceset_handle.asChar(), "",
					destination_node.asChar(), "facesets" );

			// Connect the shader's attribute node to the faceset node
			nsi.Connect(
					Handle(), "",
					faceset_handle.asChar(), "geometryattributes" );
		}
	}
	else if( node.object().hasFn(MFn::kGeometric) )
	{
		nsi.Connect(
			Handle(), "",
			shadedObjHandle.asChar(), "geometryattributes" );
	}
}

const char *NSIExportShadingGroup::NSINodeType( void ) const
{
	return "attributes";
}

/**
	We chose this moment to create the shading networks. We can't do this in
	create because it won't work in IPR: Create is called only once at the
	beginning of the export.

	IPR notes: when a new material is created, we get called but the actual
	shaders are not yet connected to the set which means that nothing will be
	done here. But shortly after, the membership of the set will changed and we
	will catch that in ReconnectMaterial(), at which time we will call this
	function again and everything will fall into place.

	\sa ReconnectMaterial()
*/
void NSIExportShadingGroup::SetAttributes( void )
{
	NSI::Context nsi( m_nsi );

	MString shader_path = OSLUtils::GetFullpathname( "shadingEngine_surface" );
	nsi.SetAttribute(
		SurfaceSGHandle().asChar(),
		NSI::CStringPArg( "shaderfilename", shader_path.asChar() ) );

	shader_path = OSLUtils::GetFullpathname( "shadingEngine_displacement" );
	nsi.SetAttribute(
		DisplacementSGHandle().asChar(),
		NSI::CStringPArg( "shaderfilename", shader_path.asChar() ) );
}

/**
*/
void NSIExportShadingGroup::SetAttributesAtTime(
	double i_time, bool i_no_motion )
{
}

/**
	\brief Register all the callbacks needed for this shading group.

	We closely follow the DL_nodeWatch mechanism so that would be a good read.
	Also, we try to follow membership changes for material re-assignments.
	(FIXME: seems to half-work only).
*/
bool NSIExportShadingGroup::RegisterCallbacks( void )
{
	MStatus status;

	MObject obj = Object();
	MCallbackId id = MNodeMessage::addAttributeChangedCallback(
		obj, ShadingGroupConnectionChangedCB, this );
	AddCallbackId( id );

	return true;
}

void NSIExportShadingGroup::SetDisplacementBound( MObject obj )
{
	bool has_bump_plug;
	float as_bump = getAttrBool(
		obj, "_3delight_renderAsBump", false, has_bump_plug );

	if( as_bump )
	{
		/* Not issueing a bound will render as bump */
		return;
	}

	bool has_bound_plug;
	float curr_bound = getAttrDouble(
		obj, "_3delight_displacementBound", 0.0, has_bound_plug );

	if( !has_bound_plug )
		return;

	if( curr_bound != 0.0f )
		curr_bound *= ::fabs( getAttrDouble(obj, "scale", 1.0) );

	int vector_encoding = getAttrInt( obj, "vectorEncoding", 1 );
	if( vector_encoding == 1 )
		curr_bound *= 0.5f;

	int vector_space = getAttrInt( obj, "vectorSpace", 1 );
	const char* bound_space = vector_space == 0 ? "world" : "object";

	NSI::Context nsi( m_nsi );
	nsi.SetAttribute(
		Handle(),
		(
			NSI::FloatArg("displacementbound", curr_bound),
			NSI::CStringPArg("displacementboundspace", bound_space)
		) );
}

bool NSIExportShadingGroup::HasShader(
	const MString& plug_name,
	MPlug* o_plug ) const
{
	MFnDependencyNode dep( Object() );

	MPlug plug = dep.findPlug( plug_name, true );

	MPlugArray plugs;
	plug.connectedTo( plugs, true, false );

	if( plugs.length() == 1 )
	{
		if( o_plug )
			*o_plug = plugs[0];

		return true;
	}

	return false;
}

/**
	\brief Deal with connections and disconnections to and form this shading
	group.

	The connections we are interested in are connections to
	surfaceShader/displacementShader to signify that a new
	shading network is applied to this shading group.
*/
void NSIExportShadingGroup::ShadingGroupConnectionChangedCB(
	MNodeMessage::AttributeMessage i_msg,
	MPlug &i_plug,
	MPlug &i_otherPlug,
	void *i_data )
{
	NSIExportShadingGroup *delegate = (NSIExportShadingGroup *)i_data;
	delegate->ShadingGroupConnectionChangedCB( i_msg, i_plug, i_otherPlug );
}

void NSIExportShadingGroup::ShadingGroupConnectionChangedCB(
	MNodeMessage::AttributeMessage i_msg,
	MPlug &i_plug,
	MPlug &i_otherPlug )
{
	if( !(i_msg &
		(MNodeMessage::kConnectionMade | MNodeMessage::kConnectionBroken)) )
	{
		return;
	}

	MString plug_name = NSIExportUtilities::AttributeName( i_plug );
	NSI::Context nsi( m_nsi );
	MObject obj  = i_otherPlug.node();
	if( plug_name == "dagSetMembers" ) //&& obj.hasFn(MFn::kGeometric) )
	{
		if( i_msg & MNodeMessage::kConnectionBroken )
		{
			/*
				Don't do a Disconnect( ".all" ) here as this will force a
				render refresh for unrealted renders. For example, when using
				HyperShade panel, just clicking on a material icon will invoqu
				callbacks on shading groups and if you have an IPR session going
				somwhere else it will get re-rendered too even if there are no
				related changes.

				Also node how we call NSIInstanceHandle() here .vs the one in
				\ref Connect(). We need to get the parent oursleves here because
				we don't have the complete dag path to the instnace which is
				returned by set.getMembers() in connect.
			*/
			MFnDagNode node(obj);
			MObject parent = node.parent(0);
			MDagPath parent_path;
			MFnDagNode(parent).getPath( parent_path );

			MString shadedObjHandle = NSIInstanceHandle(obj, parent_path, m_live);
			assert( shadedObjHandle.length() > 0 );
			if( shadedObjHandle.length() == 0 )
			{
				MString destination_node = NSIHandle(node, true);
				shadedObjHandle = destination_node;
			}

			nsi.Disconnect( m_nsi_handle, "", shadedObjHandle.asChar(),
				"geometryattributes" );
		}
		else if( i_msg & MNodeMessage::kConnectionMade )
		{
			/*
				Here we don't try to be fency and reconnect everything. 3Delight
				will not trigger a render if nothing changed.

				The reason we do this is because there could be things to
				NSICreate (namely facesets).
			*/
			Connect( 0x0 );
		}
		nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
		return;
	}

	for( const ShaderDesc &desc : s_shaders )
	{
		bool is_3delight_plug = plug_name == desc.m_3delight_plug_name;
		if( plug_name == desc.m_plug_name || is_3delight_plug )
		{
			bool connect = i_msg & MNodeMessage::kConnectionMade;
			MPlug other_shader;

			if( !is_3delight_plug &&
				HasShader(desc.m_3delight_plug_name, &other_shader) )
			{
				/* Do nothing as the 3Delight specific plug is used instead. */
				continue;
			}

			if( is_3delight_plug && connect &&
			    HasShader(desc.m_plug_name, &other_shader) )
			{
				/* Disconnect regular shader before connecting 3Delight one. */
				UpdateShaderConnection(desc, other_shader, false);
			}

			/* Do what we're actually supposed to do here. */
			UpdateShaderConnection(desc, i_otherPlug, connect);

			if( is_3delight_plug && !connect &&
			    HasShader(desc.m_plug_name, &other_shader) )
			{
				/* Connect regular shader after disconnecting 3Delight one. */
				UpdateShaderConnection(desc, other_shader, true);
			}
		}
	}

	// Other cases involve membership changes, which are handled by another event
	nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
}



MString NSIExportShadingGroup::SurfaceSGHandle() const
{
	return SGHandle("|surface");
}

MString NSIExportShadingGroup::DisplacementSGHandle() const
{
	return SGHandle("|displacement");
}

MString NSIExportShadingGroup::SGHandle(const char *i_suffix) const
{
	return MString(m_nsi_handle) + i_suffix;
}

/**
	\param i_desc
		Description of the shader to update.
	\param i_shader_plug
		Output plug of the shader node to [dis]connect to this group.
	\param i_connect
		Connect if true, disconnect otherwise.
*/
void NSIExportShadingGroup::UpdateShaderConnection(
	const ShaderDesc &i_desc,
	MPlug &i_shader_plug,
	bool i_connect)
{
	NSI::Context nsi( m_nsi );

	MString shader_handle = NSIShaderHandle(i_shader_plug.node(), m_live);
	if( i_desc.m_shading_engine_suffix )
	{
		/* Check that the connected node is supported. */
		MString out_param = GetShaderParameterName(i_shader_plug, eOutput);
		if( out_param.length() == 0 )
			return;
		MString sg_handle = SGHandle(i_desc.m_shading_engine_suffix);
		/*
			shader output is connected to shadingEngine_xx's input and
			shadingEngine to the geometryattributes.
		*/
		if( i_connect )
		{
			nsi.Connect(
				shader_handle.asChar(), out_param.asChar(),
				sg_handle.asChar(), i_desc.m_shading_engine_input);
			nsi.Connect(
				sg_handle.asChar(), "",
				Handle(), i_desc.m_nsi_type);

			if( 0 == strcmp(i_desc.m_nsi_type, "displacementshader") )
			{
				SetDisplacementBound(i_shader_plug.node());
			}
		}
		else
		{
			nsi.Disconnect(
				shader_handle.asChar(), out_param.asChar(),
				sg_handle.asChar(), i_desc.m_shading_engine_input);
			nsi.Disconnect(
				sg_handle.asChar(), "",
				Handle(), i_desc.m_nsi_type);
		}
	}
	else
	{
		/* Shader is connected directly to geometryattributes. */
		if( i_connect )
		{
			nsi.Connect(
				shader_handle.asChar(), "",
				Handle(), i_desc.m_nsi_type);
		}
		else
		{
			nsi.Disconnect(
				shader_handle.asChar(), "",
				Handle(), i_desc.m_nsi_type);
		}
	}
}
