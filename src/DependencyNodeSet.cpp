#include "DependencyNodeSet.h"

#include <maya/MDagPath.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnSet.h>
#include <maya/MIntArray.h>
#include <maya/MIteratorType.h>
#include <maya/MItDag.h>
#include <maya/MItSelectionList.h>
#include <maya/MSelectionList.h>
#include <maya/MUuid.h>

/**
	\returns
		A string which uniquely identifies the node. This is not meant to be
		human readable.
*/
std::string DependencyNodeSet::NodeID(const MObject &i_node)
{
	MStatus status;
	MFnDependencyNode depFn(i_node, &status);
	if( !status )
		return {};

	MString ns = depFn.parentNamespace();
	int ns_length;
	const char *ns_chars = ns.asChar(ns_length);

	unsigned char uuid[16];
	depFn.uuid().get(uuid);

	std::string id;
	id.reserve(ns_length + 16);
	id.assign(ns_chars, ns_length);
	id.append(reinterpret_cast<char*>(uuid), 16);
	return id;
}

DependencyNodeSet::DependencyNodeSet()
{
}

DependencyNodeSet::~DependencyNodeSet()
{
}

/**
	Makes the set empty.
*/
void DependencyNodeSet::Clear()
{
	m_nodes.clear();
}

/**
	\returns
		true if the given dependency node is part of this set.
*/
bool DependencyNodeSet::Contains(const MObject &i_node) const
{
	/* Quick check to avoid generating the node id. */
	if( m_nodes.empty() )
		return false;

	return m_nodes.count(NodeID(i_node)) != 0;
}

/**
	Adds the given dependency node to this set.
*/
void DependencyNodeSet::AddNode(const MObject &i_node)
{
	std::string id = NodeID(i_node);
	if( !id.empty() )
	{
		m_nodes.emplace(id);
	}
}

/**
	For all the dag nodes contained in the flattened set, add them and their
	children to this set.

	Only nodes with types in i_types or MFn::kTransform are added.

	If i_with_parents is true, also add parent nodes all the way to but
	excluding the root node.
*/
void DependencyNodeSet::AddSetDagNodes(
	const MObject &i_set,
	const std::vector<int> &i_types,
	bool i_with_parents)
{
	MStatus status;
	MFnSet set_fn(i_set, &status);
	if( !status )
		return;

	MSelectionList list;
	status = set_fn.getMembers(list, true);
	if( !status )
		return;

	MIntArray types;
	types.append(MFn::kTransform);
	for( int t : i_types )
	{
		types.append(t);
	}
	MIteratorType it_filter;
	it_filter.setFilterList(types);
	MItDag dag_it(it_filter);

	for( MItSelectionList it(list); !it.isDone(); it.next() )
	{
		MDagPath dag_path;
		if( !it.getDagPath(dag_path) )
			continue;

		if( i_with_parents )
		{
			MDagPath parent_path = dag_path;
			for( unsigned n = dag_path.length(); n > 1; --n )
			{
				parent_path.pop();
				AddNode(parent_path.node());
			}
		}
		/* Iterate over all children to add them. */
		MObject node = dag_path.node();
		dag_it.reset(it_filter, &node, &dag_path);
		dag_it.traverseUnderWorld(true);
		for( ; !dag_it.isDone(); dag_it.next() )
		{
			AddNode(dag_it.currentItem());
		}
	}
}
