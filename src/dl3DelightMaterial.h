#ifndef __dl3DelightMaterial_H__
#define __dl3DelightMaterial_H__

#include <maya/MPxNode.h>

class dl3DelightMaterial : public MPxNode
{
public:
	static void* creator();
	static MStatus initialize();

	virtual void postConstructor();
	virtual MStatus compute( const MPlug&, MDataBlock& );

private:
	static MObject s_coating_on;
	static MObject s_coating_transmittance;
	static MObject s_coating_reflectivity;
	static MObject s_coating_roughness;
	static MObject s_coating_thickness;

	static MObject s_color;
	static MObject s_transparency;
	static MObject s_diffuse_roughness;
	static MObject s_sss_on;
	static MObject s_sss_transmittance;
	static MObject s_sss_scale;
	static MObject s_sss_ior;
	static MObject s_sss_merge_set;
	static MObject s_sss_dominant_material;
	static MObject s_sss_double_sided;

	static MObject s_reflect_color;
	static MObject s_reflect_reflectivity;
	static MObject s_reflect_roughness;
	static MObject s_reflect_anisotropy;
	static MObject s_reflect_anisotropy_direction;

	static MObject s_incandescence;
	static MObject s_incandescence_intensity;

	static MObject s_bump_layer;
	static MObject s_normalCamera;

	static MObject s_outColor;
	static MObject s_outTransparency;
};

#endif

