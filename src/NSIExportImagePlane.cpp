#include "NSIExportImagePlane.h"

#include <nsi.hpp>

#include <maya/MPlug.h>

NSIExportImagePlane::NSIExportImagePlane(
	MFnDagNode &i_dag,
	const NSIExportDelegate::Context &i_context )
:
	NSIExportDelegate( i_dag, i_context ),
	m_shader_delegate( i_dag.object(), i_context ),
	m_dag_path( i_dag.dagPath() )
{
}

NSIExportImagePlane::~NSIExportImagePlane()
{
}

void NSIExportImagePlane::Create()
{
	NSI::Context nsi(m_nsi);

	nsi.Create( Handle(), "transform" );
	const std::string &geo_handle = GeoHandle();
	nsi.Create( geo_handle, "plane" );
	nsi.Create( AttributesHandle().asChar(), "attributes" );
	m_shader_delegate.Create();

	/* We can connect all these nodes together right now as we know they've all
	   been created. */
	nsi.Connect( geo_handle, "", Handle(), "objects" );
	nsi.Connect(
		AttributesHandle().asChar(), "", Handle(), "geometryattributes" );
	nsi.Connect(
		m_shader_delegate.Handle(), "",
		AttributesHandle().asChar(), "surfaceshader" );
}

void NSIExportImagePlane::SetAttributes()
{
	/*
		Note: if the image sequence support of the shader code turns out to be
		insufficient, we could use MRenderUtil::exactImagePlaneFileName().
	*/
	m_shader_delegate.SetAttributes();

	NSI::Context nsi(m_nsi);

	/* Use depth attribute to place the plane. */
	MFnDependencyNode dep_fn(Object());
	MPlug depth_plug = dep_fn.findPlug("depth", true);
	MStatus status;
	double Z = depth_plug.asDouble(&status);
	if( !status )
	{
		Z = 100.0;
	}

	double plane_transform[16] =
	{
		1.0, 0.0, 0.0, 0.0,
		0.0, 1.0, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.0, 0.0, -Z, 1.0
	};
	nsi.SetAttribute( Handle(),
		NSI::DoubleMatrixArg("transformationmatrix", plane_transform) );

	/*
		Make it visible only to camera rays.
		This could eventually be an option.
	*/
	nsi.SetAttribute( AttributesHandle().asChar(), (
		NSI::IntegerArg("visibility", 0),
		NSI::IntegerArg("visibility.camera", 1)) );

	/*
		Find the camera we're parented to and export its handle as a parameter
		of the shader.
	*/
	MDagPath camera_path = m_dag_path;
	while( camera_path.length() != 0 )
	{
		if( camera_path.hasFn( MFn::kCamera ) )
		{
			/* This is the camera. */
			MString handle = NSIHandle( camera_path, m_live );
			nsi.SetAttribute( m_shader_delegate.Handle(),
				NSI::StringArg("parentCamera", handle.asChar()) );
			break;
		}
		camera_path.pop();
	}
}

void NSIExportImagePlane::SetAttributesAtTime(
	double i_time,
	bool i_no_motion )
{
	m_shader_delegate.SetAttributesAtTime( i_time, i_no_motion );
}

void NSIExportImagePlane::Delete( bool i_recursive )
{
	NSI::Context nsi(m_nsi);
	nsi.Delete( GeoHandle() );
	nsi.Delete( AttributesHandle().asChar() );
	m_shader_delegate.Delete( i_recursive );
	NSIExportDelegate::Delete( i_recursive );
}

/*
	Returns a handle for our plane geometry node.
	Handle() is used for a transform we need to place the plane.
*/
std::string NSIExportImagePlane::GeoHandle() const
{
	return std::string(Handle()) + "|geo";
}
