/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#include "RenderPassInterface.h"
#include "NSIExportUtilities.h"

#include <maya/MArrayDataHandle.h>
#include <maya/MDataHandle.h>
#include <maya/MDagPath.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MGlobal.h>
#include <maya/MIntArray.h>
#include <maya/MObjectArray.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <math.h>
#include <assert.h>

#include "MU_typeIds.h"

RenderPassInterface::RenderPassInterface(
	MObject& i_renderPassNode,
	bool i_canApplyOverrides) :
	m_object_handle(i_renderPassNode),
	m_canApplyOverrides(i_canApplyOverrides)
{
	assert( isValid() );
}

RenderPassInterface::~RenderPassInterface()
{
}

bool
RenderPassInterface::isValid() const
{
	MFnDependencyNode depNode( m_object_handle.object() );
	return ( !m_object_handle.object().isNull() &&
		( depNode.typeId() == MU_typeIds::DL_RENDERSETTINGS ||
		  depNode.typeId() == MU_typeIds::DL_VIEWPORTRENDERSETTINGS ) );
}

MString
RenderPassInterface::name() const
{
	MFnDependencyNode depNode( m_object_handle.object() );
	return depNode.name();
}

/**
	\brief Return camera(s) to render.

	We either return one camera or a pair of cameras from a stereo rig. We do
	that by comparing paths. FIXME: instances could fuck up this comparison ?
*/
std::vector<MObject> RenderPassInterface::cameras() const
{
	MObject object = getConnectedNode(MString("camera"));
	std::vector<MObject> cameras;

	if( object.hasFn(MFn::kTransform) )
	{
		/* A stereo rig! Get all the cameras underneath */
		MIntArray types; types.append( MFn::kCamera );
		MFnDagNode root(object);

		MDagPath root_path;
		root.getPath(root_path);

		NSIExportUtilities::FlattenMayaTree( root, types, cameras );

		/* remove center camera, we only render left and right */
		std::vector<MObject> final;
		for( size_t i=0; i<cameras.size(); i++ )
		{
			MDagPath camera_path;
			MDagPath::getAPathTo( cameras[i], camera_path );

			camera_path.pop();
			if( camera_path == root_path )
				continue;
			final.push_back( cameras[i] );
		}
		cameras.swap(final);
	}
	else
	{
		cameras.push_back( object );
	}
	return cameras;
}

MObject RenderPassInterface::ObjectsToRender( void ) const
{
	return getConnectedNode( "objectsToRender" );
}

MObject RenderPassInterface::LightsToRender( void ) const
{
	return getConnectedNode( "lightsToRender" );
}

MObject RenderPassInterface::MatteObjects( void ) const
{
	return getConnectedNode( "matteObjects" );
}

MObject RenderPassInterface::ForceObjects(void) const
{
	return getConnectedNode( "forceObjects" );
}

MObject RenderPassInterface::ExcludeObjects(void) const
{
	return getConnectedNode( "excludeObjects" );
}

MObject RenderPassInterface::OverrideDlSets(void) const
{
	return getConnectedNode("overrideDlSets");
}

MObject RenderPassInterface::ForceLights(void) const
{
	return getConnectedNode("forceLights");
}

MObject RenderPassInterface::ExcludeLights(void) const
{
	return getConnectedNode("excludeLights");
}

namespace
{
struct SetDesc
{
	const char *name;
	bool is_light;
}
s_object_sets[] =
{
	{"objectsToRender", false},
	{"lightsToRender", true},
	{"forceObjects", false},
	{"excludeObjects", false},
	{"forceLights", true},
	{"excludeLights", true}
};
}
int RenderPassInterface::NumObjectSets() const
{
	return std::size(s_object_sets);
}

int RenderPassInterface::WhichObjectSet(const MString &name) const
{
	for( int i = 0; i < std::size(s_object_sets); ++i )
	{
		if( name == s_object_sets[i].name )
			return i;
	}
	return -1;
}

MObject RenderPassInterface::GetObjectSet(int idx, bool &o_is_light) const
{
	o_is_light = false;
	if( idx < 0 || idx >= std::size(s_object_sets) )
		return {};
	o_is_light = s_object_sets[idx].is_light;
	return getConnectedNode(s_object_sets[idx].name);
}

MObject RenderPassInterface::Environment() const
{
	return getConnectedNode(MString("environment"));
}

MObject RenderPassInterface::Atmosphere() const
{
	return getConnectedNode(MString("atmosphere"));
}

void
RenderPassInterface::framesToRender(MDoubleArray& o_frames) const
{
	o_frames.clear();

	for(double i = StartFrame(); i <= EndFrame(); i += Increment())
	{
		o_frames.append(i);
	}
}

RenderPassInterface::eRenderMode RenderPassInterface::RenderMode( void ) const
{
	int render_mode = 0;
	getAttrValue(MString("renderMode"), render_mode );
	return (eRenderMode) render_mode;
}

bool
RenderPassInterface::EnableMotionBlur() const
{
	int enable = 1;
	getAttrValue(MString("enableMotionBlur"), enable);
	return enable && !DisableMotionBlur();
}

/**
	We don't use the progressive mode when no framebuffer is enabled.
*/
bool RenderPassInterface::IsProgressive( void ) const
{
	int progressive =
		MGlobal::optionVarIntValue( "_3delight_progressive_render" );

	if( !progressive )
		return false;

	MIntArray layer_indices;
	layerIndices( layer_indices );
	for( int i = 0; i < layer_indices.length(); i++ )
	{
		int index = layer_indices[ i ];
		if( layerOutput( index ) && layerFramebufferOutput(true) )
			return true;
	}

	return false;
}

int RenderPassInterface::ShadingSamples( void ) const
{
	int shading_samples = 100;
	getAttrValue( MString("shadingSamples"), shading_samples );
	return shading_samples;
}

int RenderPassInterface::PixelSamples( void ) const
{
	int pixel_samples = 4;
	getAttrValue( MString("pixelSamples"), pixel_samples );
	return pixel_samples;
}

int RenderPassInterface::PixelFilterIndex(void) const
{
	int pixel_filter_idx = 0;
	getAttrValue(MString("pixelFilter"), pixel_filter_idx);
	return pixel_filter_idx;
}

int RenderPassInterface::VolumeSamples( void ) const
{
	int volume_samples = 16;
	getAttrValue( MString("volumeSamples"), volume_samples );
	return volume_samples;
}

bool RenderPassInterface::EnableClamping(void) const
{
	int enable_clamping = 0;
	getAttrValue(MString("enableClamping"), enable_clamping);
	return enable_clamping;
}

double RenderPassInterface::ClampingValue(void) const
{
	double clamping_value = 2;
	getAttrValue(MString("clampingValue"), clamping_value);
	return clamping_value;
}

double RenderPassInterface::FilterWidth( void ) const
{
	double filter_width = 2;
	getAttrValue( MString("filterWidth"), filter_width );
	return filter_width;
}

/**
	Makure sure to synchronize with RS_getAllPixelFilters in
	mel/dlRenderSettings.mel
*/
const char * RenderPassInterface::PixelFilter( void ) const
{
	int pixel_filter_number = PixelFilterIndex();

	const char *filters[] =
	{
		"blackman-harris",
		"blackman-harris",
		"mitchell",
		"catmull-rom",
		"sinc",
		"box",
		"triangle",
		"gaussian"
	};

	if( pixel_filter_number < 0 )
		pixel_filter_number = 0;
	else if(pixel_filter_number>7 )
		pixel_filter_number = 7;

	return filters[pixel_filter_number];
}

int RenderPassInterface::MaxRayDepth( MString &type ) const
{
	assert(
		type == "Reflection" ||
		type == "Refraction" ||
		type == "Diffuse" ||
		type == "Specular" ||
		type == "Hair" );

	MString attribute_name = MString("max") + type + MString("Depth");
	int depth = 3;
	getAttrValue( attribute_name, depth );
	return depth;
}

double RenderPassInterface::MaxDistance( void ) const
{
	double max_distance;
	getAttrValue( MString("maxDistance"), max_distance );
	return max_distance;
}

int RenderPassInterface::ResolutionX( void ) const
{
	int xres = 960;
	getAttrValue( MString("resolutionX"), xres );
	return xres / ResolutionReduceFactor();
}

int RenderPassInterface::ResolutionY( void ) const
{
	int yres = 540;
	getAttrValue( MString("resolutionY"), yres );
	return yres / ResolutionReduceFactor();
}

double RenderPassInterface::PixelAspectRatio( void ) const
{
	double pixel_aspect_ratio = 1.f;
	getAttrValue( MString("pixelAspectRatio"), pixel_aspect_ratio );
	return pixel_aspect_ratio;
}

void RenderPassInterface::Crop(
	float o_top_left[2], float o_bottom_right[2] ) const
{
	double minX=0, minY=0;
	double maxX=1, maxY=1;

	int use_crop = false;
	getAttrValue( MString("useCropWindow"), use_crop );

	if( use_crop )
	{
		getAttrValue( MString("cropMinX"), minX );
		getAttrValue( MString("cropMinY"), minY );
		getAttrValue( MString("cropMaxX"), maxX );
		getAttrValue( MString("cropMaxY"), maxY );
	}

	o_top_left[0] = minX; o_top_left[1] = minY;
	o_bottom_right[0] = maxX; o_bottom_right[1] = maxY;
}

bool RenderPassInterface::UseNetworkCache( void ) const
{
	int use_network_cache = false;
	getAttrValue( "useNetworkCache", use_network_cache );
	return use_network_cache;
}

bool RenderPassInterface::RenderCurrentFrameOnly( void ) const
{
	int render_sequence = true;
	getAttrValue( MString("isRenderingSequence"), render_sequence);
	return !render_sequence;
}

bool RenderPassInterface::ArchiveCurrentFrameOnly(void) const
{
	int archive_current_frame = false;
	getAttrValue(MString("archiveCurrentFrameOnly"), archive_current_frame);
	return archive_current_frame;
}

double RenderPassInterface::StartFrame( void ) const
{
	double start_frame = 1;
	getAttrValue( MString("startFrame"), start_frame );
	return start_frame;
}

double RenderPassInterface::Increment( void ) const
{
	double increment = 1;
	getAttrValue( MString("increment"), increment );
	return increment;
}

double RenderPassInterface::EndFrame( void ) const
{
	double end_frame = 1;
	getAttrValue( MString("endFrame"), end_frame );
	return end_frame;
}

MString
RenderPassInterface::expandedNSIFilename() const
{
	MString cmd("RS_getExpandedNSIFilename \"" + name() + "\"");
	return MGlobal::executeCommandStringResult(cmd);
}

void
RenderPassInterface::layerIndices(MIntArray& o_indices) const
{
	o_indices.clear();

	MStatus status;

	// Fetch the layersOrder attribute
	MFnDependencyNode depNode( m_object_handle.object() );
	MPlug layersOrderPlug =
		depNode.findPlug(MString("layersOrder"), true, &status);

	assert(status == MStatus::kSuccess);

	// This attribute is a Int32Array, so we need to use MDataHandle to retrieve
	// the values.
	//
	MDataHandle data = layersOrderPlug.asMDataHandle();
	MFnIntArrayData arrayData(data.data());
	o_indices = arrayData.array();

	layersOrderPlug.destructHandle(data);
}

MString
RenderPassInterface::layerFilename(int i_index) const
{
	MString filename;
	if( !LayerUsesDefaultFilename( i_index ) )
	{
		getAttrValue(MString("layerFilenames"), i_index, filename);
	}
	else
	{
		getAttrValue("layerDefaultFilename", filename);
	}

	return filename;
}

bool RenderPassInterface::LayerUsesDefaultFilename(int i_index) const
{
	MString filename;
	getAttrValue(MString("layerFilenames"), i_index, filename);

	if(filename == MString("<default>"))
	{
		return true;
	}

	return false;
}

MString
RenderPassInterface::layerDriver(int i_index) const
{
	MString driver;
	getAttrValue(MString("layerDrivers"), i_index, driver);

	if(driver.length() == 0)
	{
		getAttrValue(MString("layerDefaultDriver"), driver);
	}
	
	return driver;
}

void
RenderPassInterface::layerVariable(
	int i_index,
	MString& o_variable_source,
	MString& o_layer_type,
	int& o_with_alpha,
	MString& o_nsi_name,
	MString& o_token,
	MString& o_label,
	MString* o_description)const
{
	/*
		Retrieve the layer variable output data from the node connected to 
		layerAOVNodes, if any.
	*/
	MPlug aovNodePlug;
	if(findPlugElement(MString("layerAOVNodes"), i_index, aovNodePlug))
	{
		MObject aovNode = getConnectedNode(aovNodePlug);
		if(!aovNode.isNull())
		{
			MFnDependencyNode aovNodeDepFn(aovNode);

			MString plugName("variableName");

			MPlug plug = aovNodeDepFn.findPlug(plugName, true);
			plug.getValue(o_nsi_name);
			if(o_nsi_name.length() == 0)
			{
				o_nsi_name = aovNodeDepFn.name();
			}

			plugName = "type";
			plug = aovNodeDepFn.findPlug(plugName, true);
			int typeEnumVal = 0;
			plug.getValue(typeEnumVal);
			switch(typeEnumVal)
			{
				default: case 0: o_layer_type = "scalar"; break;
				case 1: o_layer_type = "color"; break;
				case 2: o_layer_type = "vector"; break;
				case 3: o_layer_type = "quad"; break;
			}

			plugName = "source";
			plug = aovNodeDepFn.findPlug(plugName, true);
			plug.getValue(o_variable_source);

			// No node data for these yet.
			o_with_alpha = 0;
			o_token = o_nsi_name;
			o_label = o_nsi_name;

			if(o_description)
			{
				*o_description = o_variable_source + "|" +
					o_layer_type + "|" +
					o_with_alpha + "|" +
					o_nsi_name + "|" +
					o_nsi_name + "|" +
					o_nsi_name + "|" +
					o_nsi_name;
			}
			return;			
		}
	}

	MString variable;
	getAttrValue(MString("layerOutputVariables"), i_index, variable);

	variableFields(
		variable,
		o_variable_source,
		o_layer_type,
		o_with_alpha,
		o_nsi_name,
		o_token,
		o_label);

	if(o_description)
	{
		*o_description = variable;
	}
}

void
RenderPassInterface::layerLightCategories(
	MStringArray& o_categories,
	MObjectArray& o_objCategories) const
{
	// FIXME: this is fetching light groups; MEL has procs to convert light groups
	// to categories and vice versa.
	//
	o_categories.clear();
	o_objCategories.clear();

	// Replicate RS_getLightGroups behaviour, which always starts with a "" group
	// that represents all lights.
	//
	o_categories.append("");
	o_objCategories.append(MObject());
	
	MString attrName("lightGroups");

	MStatus status;
	MFnDependencyNode depNode( m_object_handle.object() );
	MPlug plug = depNode.findPlug(attrName, true, &status);
	assert(status == MStatus::kSuccess);

	for(int i = 0; i < plug.numElements(); i++)
	{
		MPlug elemPlug = plug.elementByPhysicalIndex(i);

		// Not using getConnectedNode here because we may need to inspect the source
		// plug name for the "environment" and "incandescence" special cases.
		//
		MPlugArray sourcePlugs;
		elemPlug.connectedTo(sourcePlugs, true, false, &status);

		if(sourcePlugs.length() < 1)
		{
			continue;
		}

		MPlug groupPlug = sourcePlugs[0];
		MObject groupNode = groupPlug.node();
		MFnDependencyNode groupNodeDefFn( groupNode );

		if(groupNode.hasFn(MFn::kDagNode))
		{
			// Connected DAG node (normally, this should be a light shape). Set
			// the category to the light's full path name.

			MFnDagNode light(groupNode);
			MString category = light.fullPathName();
			/* If connected to an alternate attribute, append it to light
			   category name. */
			MFnAttribute groupAttr(groupPlug.attribute());
			MString attrName(groupAttr.name());
			if( attrName != "usedBy3dfm" )
			{
				category += "." + attrName;
			}
			o_categories.append(category);

			if( light.typeId() == MU_typeIds::DL_INCANDESCENCELIGHT )
			{
				/*
					\see NSIExortIncandescenceLight. We must point to parent
					node instead.
				*/
				MDagPath path; light.getPath( path );
				path.pop();
				MFnDagNode parent( path );
				o_objCategories.append(parent.object());
			}
			else
			{
				o_objCategories.append(groupNode);
			}
		}
		else if(groupNode.hasFn(MFn::kSet) || 
			groupNodeDefFn.typeName() == MString( "lightGroup" ) )
		{
			// Connected light group. We only use light groups for multi-light aov.
			o_categories.append(groupNodeDefFn.name());
			o_objCategories.append(groupNode);
		}
		else
		{
			// Special case for the environment entry, which is
			// connection to some hidden render pass attributes.
			// FIXME: is this good ? shouldn't we use non-shortned name ?
			MString partialPlugName = groupPlug.partialName();
			if(partialPlugName == "lightGroup_environment")
			{
				MObject envObj( Environment() );
				if( !envObj.isNull() )
				{
					MFnDagNode env( Environment(), &status );
					if( status == MStatus::kSuccess )
					{
						o_categories.append( env.fullPathName() );
						o_objCategories.append(envObj);
					}
				}
			}
		}
	}
}

bool
RenderPassInterface::layerOutput(int i_index) const
{
	int enable = 1;
	getAttrValue("layerOutput", i_index, enable);

	return enable == 1;
}

bool
RenderPassInterface::layerMultiLight(int i_index) const
{
	int enable = 1;
	getAttrValue("layerMultiLight", i_index, enable);

	return enable == 1;
}

bool
RenderPassInterface::layerFramebufferOutput(bool ipr) const
{
	int enable = 0;
	getAttrValue("outputOptionsDefault", enable);

	return enable == e_idisplay || enable == e_imageAndDisplay || ipr;
}

bool
RenderPassInterface::layerFileOutput() const
{
	int enable = 0;
	getAttrValue("outputOptionsDefault", enable);

	return enable == e_image || enable == e_imageAndDisplay;
}

bool
RenderPassInterface::layerNSIOutput() const
{
	int enable = 0;
	getAttrValue("outputOptionsDefault", enable);

	return enable == e_nsi;
}

bool
RenderPassInterface::layerJpegOutput() const
{
	int enable = false;
	getAttrValue("createJpegCopy", enable);

	return enable;
}

void RenderPassInterface::LayerEffectiveFileOutput(
	int i_index,
	eFileOutputMode i_mode,
	bool& o_fileOutput,
	bool& o_jpegOutput,
	bool ipr) const
{
	if( i_mode == e_disabled || ipr)
	{
		o_fileOutput = false;
		o_jpegOutput = false;
		return;
	}


	bool file_toggle = layerFileOutput();
	bool jpeg_toggle = layerJpegOutput() && file_toggle;

	assert( i_mode == e_enabled);

	o_fileOutput = file_toggle;
	o_jpegOutput = jpeg_toggle;
}

MString RenderPassInterface::layerScalarFormat(int i_index) const
{
	MString format;
	getAttrValue(MString("layerScalarFormats"), i_index, format);

	if( format.length() == 0 )
	{
		getAttrValue(MString("layerDefaultScalarFormat"), format);
	}
	return format;
}

bool RenderPassInterface::EnableSpeedBoost( void ) const
{
	if( !m_canApplyOverrides || !RenderCurrentFrameOnly() )
		return false;

	int speed_boost = 0;
	getAttrValue( "enableSpeedBoost",speed_boost );
	return speed_boost;
}

bool RenderPassInterface::DisableMotionBlur() const
{
	int disable = 0;
	if( EnableSpeedBoost() )
	{
		getAttrValue(MString("disableMotionBlur"), disable);
	}
	return disable;
}

bool RenderPassInterface::DisableDepthOfField() const
{
	int disable = 0;
	if( EnableSpeedBoost() )
	{
		getAttrValue(MString("disableDepthOfField"), disable);
	}
	return disable;
}

bool RenderPassInterface::DisableDisplacement() const
{
	int disable = 0;
	if ( EnableSpeedBoost() )
	{
		getAttrValue(MString("disableDisplacement"), disable);
	}
	return disable;
}

bool RenderPassInterface::DisableSubsurface() const
{
	int disable = 0;
	if( EnableSpeedBoost() )
	{
		getAttrValue(MString("disableSubsurface"), disable);
	}
	return disable;
}

bool RenderPassInterface::DisableAtmosphere() const
{
	int disable = 0;
	if( EnableSpeedBoost() )
	{
		getAttrValue(MString("disableAtmosphere"), disable);
	}
	return disable;
}

bool RenderPassInterface::DisableMultipleScattering() const
{
	int disable = 0;
	if( EnableSpeedBoost() )
	{
		getAttrValue(MString("disableMultipleScattering"), disable);
	}
	return disable;
}

bool RenderPassInterface::DisableExtraImageLayers() const
{
	int disable = 0;
	if( EnableSpeedBoost() )
	{
		getAttrValue(MString("disableExtraImageLayers"), disable);
	}
	return disable;
}

int RenderPassInterface::ResolutionReduceFactor() const
{
	static int reduceFactors[4] = {1, 2, 4, 8};
	int resolution = 0;
	if( EnableSpeedBoost() )
	{
		getAttrValue(MString("resolutionMultiplier"), resolution);
		if( resolution<0 )
			resolution = 0;
		else if( resolution>3 )
			resolution = 3;
    }
	return reduceFactors[resolution];
}

double RenderPassInterface::SamplingReduceFactor() const
{
	static double reduceFactors[5] = {1, 0.25, 0.1, 0.04, 0.01};
	int sampling = 0;
	if( EnableSpeedBoost() )
	{
		getAttrValue(MString("samplingMultiplier"), sampling);
		if( sampling < 0 )
			sampling = 0;
		else if( sampling > 4 )
			sampling = 4;
	}
	return reduceFactors[sampling];
}

bool RenderPassInterface::IsolateSelection()const
{
	int isolate = 0;
	getAttrValue("isolateSelection", isolate);

	return isolate;
}

MObject
RenderPassInterface::getConnectedNode(MString i_attrName) const
{
	MStatus status;

	MFnDependencyNode depNode( m_object_handle.object() );
	MPlug attrPlug = depNode.findPlug(i_attrName, true, &status);
	if (attrPlug.isNull())
	{
		return MObject::kNullObj;
	}
	assert(status == MStatus::kSuccess);

	return getConnectedNode(attrPlug);
}

MObject
RenderPassInterface::getConnectedNode(MPlug& i_plug) const
{
	if(!i_plug.isConnected())
	{
		return MObject();
	}

	MStatus status;

	MPlugArray sourcePlugs;
	i_plug.connectedTo(sourcePlugs, true, false, &status);
	assert(status == MStatus::kSuccess);

	if(sourcePlugs.length() < 1)
	{
		return MObject();
	}

	return sourcePlugs[0].node();
}

bool RenderPassInterface::findPlugElement(
	MString i_name, int i_index, MPlug& o_plug) const
{
	MStatus status;
	MFnDependencyNode depNode( m_object_handle.object() );

	MPlug attrArrayPlug = depNode.findPlug(i_name, true, &status);

	if(status != MStatus::kSuccess)
	{
		return false;
	}

	o_plug = attrArrayPlug.elementByLogicalIndex(i_index);
	return true;
}

bool
RenderPassInterface::getAttrValue(
	MString i_attrName, double& o_value) const
{
	MStatus status;
	MFnDependencyNode depNode( m_object_handle.object() );

	MPlug attrPlug = depNode.findPlug(i_attrName, true, &status);

	if(status != MStatus::kSuccess)
		return false;

	return attrPlug.getValue(o_value) == MStatus::kSuccess;
}

bool
RenderPassInterface::getAttrValue(MString i_attrName, int& o_value) const
{
	MStatus status;
	MFnDependencyNode depNode( m_object_handle.object() );

	MPlug attrPlug = depNode.findPlug(i_attrName, true, &status);

	if(status != MStatus::kSuccess)
		return false;

	return attrPlug.getValue(o_value) == MStatus::kSuccess;
}

bool
RenderPassInterface::getAttrValue(
	MString i_attrName, MString& o_value) const
{
	MStatus status;
	MFnDependencyNode depNode( m_object_handle.object() );

	MPlug attrPlug = depNode.findPlug(i_attrName, true, &status);

	if(status != MStatus::kSuccess)
		return false;

	return attrPlug.getValue(o_value) == MStatus::kSuccess;
}

bool
RenderPassInterface::getAttrValue(
	MString i_attrName, int i_index, double& o_value ) const
{
	MPlug plug;

	if(!findPlugElement(i_attrName, i_index, plug))
		return false;

	return plug.getValue(o_value) == MStatus::kSuccess;
}

bool
RenderPassInterface::getAttrValue(
	MString i_attrName, int i_index, int& o_value ) const
{
	MPlug plug;
	
	if(!findPlugElement(i_attrName, i_index, plug))
		return false;

	return plug.getValue(o_value) == MStatus::kSuccess;
}

bool
RenderPassInterface::getAttrValue(
	MString i_attrName, int i_index, MString& o_value ) const
{
	MPlug plug;
	
	if(!findPlugElement(i_attrName, i_index, plug))
		return false;

	return plug.getValue(o_value) == MStatus::kSuccess;
}

void RenderPassInterface::variableFields(
	const MString& i_description,
	MString& o_variable_source,
	MString& o_layer_type,
	int& o_with_alpha,
	MString& o_nsi_name,
	MString& o_token,
	MString& o_label)
{
	MStringArray tokens;
	i_description.split('|', tokens);
	if(tokens.length() >= 6)
	{
		o_variable_source = tokens[0];
		o_layer_type = tokens[1];
		o_with_alpha = tokens[2].isInt() ? tokens[2].asInt() : 0;
		o_nsi_name = tokens[3];
		o_token = tokens[4];
		o_label = tokens[5];
	}
	else
	{
		// Same as DOV_defaultAov()
		o_variable_source = "shader";
		o_layer_type = "color";
		o_with_alpha = 1;
		o_nsi_name = "Ci";
		o_token = "rgba";
		o_label = "RGBA (color + alpha)";
	}
}

