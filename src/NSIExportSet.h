#ifndef __NSIExportSet_h
#define __NSIExportSet_h

#include "NSIExportDelegate.h"
#include "RenderPassInterface.h"

#include <maya/MNodeMessage.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MPlug;
#endif

#include <nsi.hpp>

class RenderPassInterface;

/**
	\sa NSIExportDelegate
*/
class NSIExportSet : public NSIExportDelegate
{
public:
	NSIExportSet(
		const RenderPassInterface &,
		MFnDependencyNode &,
		MObject &,
		const NSIExportDelegate::Context& i_context );

	virtual void Create();
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );
	virtual void Connect( const DelegateTable * );

	void AttributeChanged(
		const DelegateTable *i_dag_hash,
		MNodeMessage::AttributeMessage i_msg,
		MPlug& i_plug,
		MPlug& i_other_plug);

private:
	void SetNSIAttributeOverride(
		NSI::Context &i_nsi,
		const char *i_attribute_name,
		bool i_enabled,
		int i_value,
		int i_priority);

	bool m_first_setattributes{true};

	bool AreOverridesApplied();

	const RenderPassInterface &m_render_pass;
};
#endif
