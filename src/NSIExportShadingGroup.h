#ifndef __NSIExportShadingGroup_h
#define __NSIExportShadingGroup_h

#include "NSIExportDelegate.h"

#include <nsi.hpp>
#include <list>

#include <maya/MNodeMessage.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MPlug;
class MObject;
#endif

/**
	\sa NSIExportDelegate

	TODO: We should use faceset nodes to do per-face assignments. As of now
	the python code does this using some switches.
*/
class NSIExportShadingGroup : public NSIExportDelegate
{
public:
	NSIExportShadingGroup(
		MFnDependencyNode &,
		MObject &,
		const NSIExportDelegate::Context& i_context );

	~NSIExportShadingGroup();

	virtual const char *NSINodeType( void ) const;
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );
	virtual void Connect( const DelegateTable*  );
	virtual void Create( void );

	virtual bool RegisterCallbacks( void );

	/**
		\brief A callback for IPR connection changes to this shading group.
	*/
	static void ShadingGroupConnectionChangedCB(
		MNodeMessage::AttributeMessage i_msg, MPlug &, MPlug &, void *);
	void ShadingGroupConnectionChangedCB(
		MNodeMessage::AttributeMessage i_msg, MPlug &, MPlug &);

	/**
		\brief This method expects a displacementShader node and sets the
		displacement bound necessary.
	*/
	void SetDisplacementBound( MObject );

	/**
		\brief Returns the NSI handle of the node that uses the
		shadingEngine_surface shader.
	*/
	MString SurfaceSGHandle() const;

	/**
		\brief Returns the NSI handle of the node that uses the
		shadingEngine_displacement shader.
	*/
	MString DisplacementSGHandle() const;

private:
	void ConnectToGeo( MFnDagNode &node, MObject &component, MString &shadedObjHandle ) const;

	/**
		\brief Returns true if the shading group has a node connected to the
		specified plug name.
	*/
	bool HasShader( const MString& plug_name,	MPlug* o_plug ) const;

	MString SGHandle(const char *i_suffix) const;

	struct ShaderDesc;
	static const ShaderDesc s_shaders[];

	void UpdateShaderConnection(
		const ShaderDesc &i_desc,
		MPlug &i_shader_plug,
		bool i_connect);
};
#endif
