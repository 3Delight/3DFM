/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#include "NSIExportCmd.h"

#include <maya/MArgList.h>
#include <maya/MArgDatabase.h>
#include <maya/MAnimControl.h>
#include <maya/MComputation.h>
#include <maya/MDGMessage.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MGlobal.h>
#include <maya/MSceneMessage.h>
#include <maya/MSelectionList.h>
#include <maya/MStringArray.h>
#include <maya/MSyntax.h>

#include <list>
#include <algorithm>

#include <assert.h>

#include "DL_errors.h"
#include "NSIExport.h"
#include "MU_typeIds.h"
#include "ShaderDataCache.h"
#include "dlViewport.h"

struct RenderPass
{
	RenderPass( NSIExport *i_exporter, const MObject &i_object )
	:
		m_exporter(i_exporter), m_object(i_object)
	{
		bool is_sequence = false;
		MFnDependencyNode depNode(m_object);
		MPlug attrPlug = depNode.findPlug(MString("isRenderingSequence"), true);
		attrPlug.getValue(is_sequence);

		/*
			exportOperation value of 3 is for sequence rendering and 1 is for
			one layer rendering. We set them according to the pressed button.
			Value 2 is for IPR rendering and we set it on NSIRender.mel file.
		*/
		if (is_sequence)
			SetExportOpOnRenderSettings(3);
		else
			SetExportOpOnRenderSettings(1);
	}

	/* The UI monitors this render settings attribute */
	void SetExportOpOnRenderSettings( int i_value )
	{
		MFnDependencyNode depFn( m_object );
		MPlug exportOpPlug = depFn.findPlug( MString( "exportOperation" ), true );
		exportOpPlug.setValue( i_value );
	}

	void Destroy( void )
	{
		MMessage::removeCallback( m_id );
		m_exporter->StopRender();
		delete m_exporter;

		SetExportOpOnRenderSettings( 0 );
	}

	NSIExport *m_exporter;
	MObject m_object;
	MCallbackId m_id;
};

/**
	A list of active exporters. We allow many renders at the same
	time with this system.
*/
std::list< RenderPass > m_active_exporters;
MString m_renderEndedCB;

/* Singleton instance of the RenderPass only for viewport render. */
std::unique_ptr< RenderPass > m_vp_exporter;

/**
	Ids for the callbacks that we listen too in addition to the per-pass
	callback
*/
static MCallbackId sg_unload_plugin_callback_id = -1;
static MCallbackId sg_maya_exiting_callback_id = -1;
static MCallbackId sg_before_new_callback_id = -1;
static MCallbackId sg_before_open_callback_id = -1;

bool NSIExportCmd::isUndoable() const
{
	return false;
}

void* NSIExportCmd::creator()
{
	return new NSIExportCmd;
}
/**
	\brief Register the syntax for the NSIExport command.

	The "-mode" flag is required. It specifies the export mode. Available values
	are "interactive", "live", "live_in_vp2", "batch", "export" and "archive".

	The "-s" flag stops any render.
*/
MSyntax NSIExportCmd::newSyntax()
{
	MSyntax syntax;

	syntax.addFlag( "-rs", "-renderSettings", MSyntax::kString );
	syntax.addFlag( "-m", "-mode", MSyntax::kString );
	syntax.addFlag( "-s", "-stop" );
	syntax.addFlag( "-p", "-panel", MSyntax::kString );
	syntax.addFlag( "-ecb", "-renderEndedCB", MSyntax::kString );
	syntax.addFlag( "-c", "-cloud" );

	// Require that a single object could be received in a selection list.
	syntax.setObjectType(MSyntax::kSelectionList, 0, 1);
	syntax.enableQuery(false);
	syntax.enableEdit(false);

	return syntax;
}

/**
	Note that this command is a Maya command and this means that it cannot
	be executed in parallel.

	FIXME: this is the worst spaghetti in a loooong time.
*/
MStatus NSIExportCmd::doIt( const MArgList& i_args )
{
	// Build the arg database.
	MStatus status = MStatus::kFailure;
	MArgDatabase argDB( syntax(), i_args, &status );

	if( status != MStatus::kSuccess )
		return status;

	// Enforce -mode specification and validate its value.
	NSIExport::ExportMode mode = NSIExport::e_invalid;

	if( argDB.isFlagSet( "-mode" ) )
	{
		MString mode_str;
		argDB.getFlagArgument( "-mode", 0, mode_str );

		if( mode_str == "interactive" )
		{
			mode = NSIExport::e_interactive;
		}
		else if( mode_str == "live" )
		{
			mode = NSIExport::e_live;
		}
		else if ( mode_str == "live_in_vp2" )
		{
			mode = NSIExport::e_live_in_vp2;
		}
		else if( mode_str == "batch" )
		{
			mode = NSIExport::e_maya_batch;
		}
		else if( mode_str == "export" )
		{
			mode = NSIExport::e_export_scene;
		}
		else if( mode_str == "archive" )
		{
			mode = NSIExport::e_export_archive;
		}
	}

	bool stop_render = argDB.isFlagSet( "-s" );
	bool endCB = argDB.isFlagSet( "-ecb" );

	if( mode == NSIExport::e_invalid && !stop_render && !endCB )
	{
		MString error = _3DFM_ERR_PREFIX( NSIEXPORT_CMD_STR ) +
			MString( "one of -mode <mode>, -renderEndedCB or -stop are required. " ) +
			MString( "<mode> is one of interactive, live, batch, export or archive." );

		MGlobal::displayError( error );
		return MStatus::kInvalidParameter;
	}

	if( endCB )
	{
		return argDB.getFlagArgument( "-ecb", 0, m_renderEndedCB );
	}

	// Get the render settings object
	MObject pass_object;
	status = MStatus::kFailure;
	if( argDB.isFlagSet( "-renderSettings" ) )
	{
		MString settings;
		argDB.getFlagArgument( "-renderSettings", 0, settings );
		MSelectionList list;
		status = list.add(settings);
		if(status == MStatus::kSuccess)
		{
			status = list.getDependNode(0, pass_object);
		}
	}

	bool render_settings_specified = status == MStatus::kSuccess;

	if( render_settings_specified )
	{
		MFnDependencyNode depFn( pass_object );

		if( depFn.typeId() != MU_typeIds::DL_RENDERSETTINGS &&
			depFn.typeId() != MU_typeIds::DL_VIEWPORTRENDERSETTINGS )
		{
			MString error = _3DFM_ERR_PREFIX( NSIEXPORT_CMD_STR ) + depFn.name() +
				MString( " is neither dlRenderSettings nor dlViewportRenderSettings node.");

			MGlobal::displayError( error );
			return MStatus::kInvalidParameter;
		}
	}
	else
	{
		if( stop_render )
		{
			// No render settings node specified, so stop all ongoing renderings.
			StopAllRenders();
			return MStatus::kSuccess;
		}

		MString error = _3DFM_ERR_PREFIX( NSIEXPORT_CMD_STR ) +
			MString( "Invalid object specified as a dlRenderSettings.");

		MGlobal::displayError( error );
		return status;
	}

	// Get the modelPanel, usually used by live in vp2.
	//
	MString panel;
	if( argDB.isFlagSet( "-p" ) )
	{
		status = argDB.getFlagArgument( "-p", 0, panel );
		if( status != MS::kSuccess )
		{
			MString error = _3DFM_ERR_PREFIX( NSIEXPORT_CMD_STR ) +
				MString( "Start or stop live in VP2 requires the model panel" );

			MGlobal::displayError( error );
			return status;
		}
	}

	/* Stop the render override on the panel. */
	if( stop_render && panel.length() && m_vp_exporter.get() )
	{
		/* Disable the NSI render override here just in case. */
		M3dView view;
		CHECK_MSTATUS( M3dView::getM3dViewFromModelPanel( panel, view ) );
		view.setRenderOverrideName( "" );

		/* Delete the render screen and output layer. */
		NSIViewportExport *raw_exporter =
			dynamic_cast< NSIViewportExport * >( m_vp_exporter->m_exporter );
		raw_exporter->DeleteRenderOutput( panel );

		/* Stop the updating on the panel. */
		dlViewportRenderOverride::s_instance->StopRender( panel );
	}

	bool is_ipr = mode == NSIExport::e_live;
	bool is_vp = mode == NSIExport::e_live_in_vp2;
	bool is_interactive = mode == NSIExport::e_interactive;


	if( is_vp && m_vp_exporter.get() )
	{
		StopRender( pass_object );
		if(stop_render)
		{
			return MStatus::kSuccess;
		}
	}
	else if( (is_ipr || is_interactive) && RenderPassActive(pass_object) )
	{
		MString error = _3DFM_ERR_PREFIX( NSIEXPORT_CMD_STR ) +
			MString("there is already an ongoing rendering using " ) +
			MFnDependencyNode( pass_object ).name() +
			MString( ". Please abort it prior launching a new rendering.") ;

		MGlobal::displayError( error );
		return MStatus::kFailure;
	}

	if( stop_render )
	{
		StopRender( pass_object );
		return MStatus::kSuccess;
	}

	/*
		If we're exporting a selection, retrieve it and send it to
		NSIExport::Export.
		We support only a single-object selection at the moment.
	*/
	MSelectionList selection_list;
	argDB.getObjects( selection_list );
	assert( selection_list.length() <= 1 );
	MDagPath root_path;
	if( !selection_list.isEmpty() )
	{
		MObject root_node;
		selection_list.getDagPath( 0, root_path, root_node );
	}

	if( is_vp )
	{
		NSIViewportExport *exporter = 0x0;
		if( m_vp_exporter.get() )
		{
			// m_vp_exporter->m_exporter is always set to a NSIViewportExport*
			exporter =
				dynamic_cast< NSIViewportExport * >( m_vp_exporter->m_exporter );
		}
		else
		{
			exporter = new NSIViewportExport(
				pass_object, true /* can apply overrides */ );

			exporter->Export( mode, root_path );

			m_vp_exporter.reset( new RenderPass( exporter, pass_object ) ) ;
		}

		exporter->CreateRenderOutput( panel );

		dlViewportRenderOverride::s_instance->StartRender( panel, exporter );
	}
	else
	{
		assert( mode != NSIExport::e_live_in_vp2 );
		MComputation* computation = 0x0;

		bool enableOverrides;

		if( mode != NSIExport::e_maya_batch )
		{
			computation = new MComputation;
			computation->beginComputation(
				true, /* show progress bar */
				true, /* isInterruptable */
				true /* use wait cursor */ );

			computation->setProgressRange( 0, 100 );

			enableOverrides = true;
		}
		else
		{
			/* Disable the "speed boost" in batch render only */
			enableOverrides = false;
		}

		NSIExport *exporter = new NSIExport(
			pass_object,
			enableOverrides,
			computation );

		exporter->Export( mode, root_path );

		if( is_ipr || is_interactive )
		{
			RegisterExporter( exporter, pass_object );
		}
		else
			delete exporter;

		if( computation )
		{
			computation->endComputation();
			delete computation;
			computation = 0x0;
		}
	}

	status = MStatus::kSuccess;

	return status;
}

/**
	\brief Returns true if the given render-pass is already active.
*/
bool NSIExportCmd::RenderPassActive( const MObject &i_object )
{
	for( auto it = m_active_exporters.cbegin();
	     it != m_active_exporters.cend(); ++it )
	{
		const RenderPass &p = *it;
		if( p.m_object == i_object )
			return true;
	}

	/* Maybe this is the global viewport render pass. */
	if( m_vp_exporter.get() && m_vp_exporter->m_object == i_object)
	{
		return true;
	}

	return false;
}

/**
	\brief Called when a render pass is being deleted.
*/
void NodeRemovedCallback( MObject &i_pass, void *i_data )
{
	/* We reset the all viewport rendering before then destroy. */
	dlViewportRenderOverride::s_instance->StopAllRenders();

	NSIExportCmd::StopRender( i_pass );
}

/**
	\brief Called at Maya exit.
*/
void stopRenders( void * )
{
	/* Just remove all exporters, this will also remove this callback. */
	for( auto it = m_active_exporters.begin(); it != m_active_exporters.end(); )
	{
		it->Destroy();
		it = m_active_exporters.erase( it );
	}

	/* Disable the NSI render override and destroy the only instance. */
	dlViewportRenderOverride::s_instance->StopAllRenders();

	if( m_vp_exporter.get() )
	{
		m_vp_exporter->Destroy();
		m_vp_exporter.reset();
	}
}

void removeCallbacks( void )
{
	MMessage::removeCallback( sg_unload_plugin_callback_id );
	MMessage::removeCallback( sg_maya_exiting_callback_id );
	MMessage::removeCallback( sg_before_open_callback_id );
	MMessage::removeCallback( sg_before_new_callback_id );
	sg_unload_plugin_callback_id = -1;
	sg_maya_exiting_callback_id = -1;
	sg_before_new_callback_id = -1;
	sg_before_open_callback_id = -1;
}

/**
	\brief Stop all IPR sessions when unloading a plugin.
*/
void pluginUnloadCallback(
	const MStringArray &parameters, void *)
{
	assert( m_active_exporters.size() > 0 );
	stopRenders( 0x0 );
	removeCallbacks( );
}


/**
	\brief Regiters this exporter in our list of active exporters.

	We must spy on the associated Render Pass, so that we stop the render if it
	dispears. Also, as soon as we have one render going, we need to make sure to
	stop it in some dangerous circumstances: plug-in unload, new scene, etc ...
*/
void NSIExportCmd::RegisterExporter(
	NSIExport *i_exporter, const MObject &i_pass )
{
	if( RenderPassActive(i_pass) )
	{
		assert( false );
		return;
	}

	RenderPass pass(i_exporter, i_pass);

	pass.m_id = MDGMessage::addNodeRemovedCallback(
		&NodeRemovedCallback, "dlRenderSettings" );

	m_active_exporters.push_back( pass );

	if( m_active_exporters.size() == 1 )
	{
		sg_unload_plugin_callback_id = MSceneMessage::addStringArrayCallback(
			MSceneMessage::kBeforePluginUnload, pluginUnloadCallback );

		sg_maya_exiting_callback_id =
			MSceneMessage::addCallback(MSceneMessage::kMayaExiting,stopRenders);

		/*
			Scene open and new will also stop any pending renders. FIXME: should
			we use "AfterOpen" ?
		*/
		sg_before_new_callback_id =
			MSceneMessage::addCallback(MSceneMessage::kBeforeNew, stopRenders );
		sg_before_open_callback_id =
			MSceneMessage::addCallback(MSceneMessage::kBeforeOpen, stopRenders );
	}
}

static void stopOneRender( RenderPass& i_pass )
{
	i_pass.Destroy();

	if( m_renderEndedCB.length() > 0 )
	{
		MFnDependencyNode depFn( i_pass.m_object );
		MString cmd = m_renderEndedCB + " \"" +  depFn.name() + "\"";
		MGlobal::executeCommandOnIdle( cmd );
	}
}

/**
	\brief Stops an active render
*/
void NSIExportCmd::StopRender( const MObject &i_pass )
{
	for( auto it = m_active_exporters.begin();
	     it != m_active_exporters.end(); ++it )
	{
		RenderPass &p = *it;

		if( p.m_object == i_pass )
		{
			stopOneRender( p );
			m_active_exporters.erase( it );
			break;
		}
	}

	if( m_active_exporters.size() == 0 )
	{
		removeCallbacks();
		ShaderDataCache::Instance()->DeleteInstance();
	}

	/* Clean the viewport rendering context only there is no view is using it. */
	if( m_vp_exporter.get() && m_vp_exporter->m_object == i_pass )
	{
		MStringArray render_panels;
		dlViewportRenderOverride::s_instance->GetRenderPanels( render_panels );

		if( !render_panels.length() )
		{
			m_vp_exporter->Destroy();
			m_vp_exporter.reset();
		}
	}
}

/**
	\brief Stops all active renders
*/
void NSIExportCmd::StopAllRenders()
{
	for( auto it = m_active_exporters.begin();
	     it != m_active_exporters.end(); ++it )
	{
		RenderPass &p = *it;
		stopOneRender( p );
	}

	removeCallbacks();
	m_active_exporters.clear();

	/* Reset the all panels and destroy the context. */
	dlViewportRenderOverride::s_instance->StopAllRenders();

	if( m_vp_exporter.get() )
	{
		m_vp_exporter->Destroy();
		m_vp_exporter.reset();
	}
}
