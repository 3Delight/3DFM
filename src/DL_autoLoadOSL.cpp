/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "DL_autoLoadOSL.h"

#include "MU_typeIds.h"

#include <filesystem>
#include <stdio.h>
#include <string.h>

#define MNoVersionString
#define MNoPluginEntry

#include <maya/MDGModifier.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnPlugin.h>
#include <maya/MFnStringData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MGlobal.h>
#include <maya/MMessage.h>
#include <maya/MNodeMessage.h>
#include <maya/MObjectArray.h>
#include <maya/MPlugArray.h>
#include <maya/MRampAttribute.h>
#include "OSLUtils.h"

#include "DL_utils.h"

/* Cache of all the loaded shaders to avoid loading OSL shaders each time the
	 node created */
std::map<std::string, DlShaderInfo*> s_shaders;

class StringMetadata
{
public:
	enum eDataIndex
	{
		attrName,
		label,
		type,
		group,
		options,
		widget,
		niceName,
		relatedToWidget,
		numDataTypes
	};

	StringMetadata();
	~StringMetadata();

	void Clear();
	const MString& GetString( eDataIndex i_index );
	void SetString( eDataIndex i_index, const char* i_string );

private:
	MString** m_data;
	MString m_emptyString;
};

StringMetadata::StringMetadata() :
	m_data(NULL),
	m_emptyString("")
{
	m_data = new MString*[numDataTypes];
	for(int i = 0; i < numDataTypes; i++ )
	{
		m_data[i] = NULL;
	}
}

StringMetadata::~StringMetadata()
{
	Clear();
	delete[] m_data;
}

void StringMetadata::Clear()
{
	for(int i = 0; i < numDataTypes; i++)
	{
		delete m_data[i];
		m_data[i] = NULL;
	}
}

const MString& StringMetadata::GetString( eDataIndex i_index )
{
	if(i_index < numDataTypes && m_data[i_index])
	{
		return *m_data[i_index];
	}

	return m_emptyString;
}

void StringMetadata::SetString( eDataIndex i_index,	const char* i_string )
{
	if( i_index == numDataTypes )
	{
		return;
	}

	if(m_data[i_index])
	{
		delete m_data[i_index];
	}

	m_data[i_index] = new MString(i_string);
}

static void ParseStringMetadata(
	const DlShaderInfo::Parameter* i_parameter,
	StringMetadata& o_data )
{
	o_data.Clear();

	/*
		Remember that "maya_attribute" was encountered in order to skip following
		"attribute" - i.e. "maya_attribute" overrides "attribute".
	*/
	bool maya_attribute_found = false;

	for (unsigned int j=0;  j<i_parameter->metadata.size(); j++)
	{
		const DlShaderInfo::Parameter& meta = i_parameter->metadata[j];

		// Skip non-string meta data
		if( !meta.type.IsOneString() || meta.sdefault.size() <= 0 )
			continue;

		StringMetadata::eDataIndex index = StringMetadata::numDataTypes;

		if( meta.name == "maya_attribute" ||
			( !maya_attribute_found && meta.name == "attribute") )
		{
			index = StringMetadata::attrName;
			maya_attribute_found = meta.name == "maya_attribute";
		}
		else if(meta.name == "label")
		{
			index = StringMetadata::label;
		}
		else if(meta.name == "maya_type" || meta.name == "type" )
		{
			index = StringMetadata::type;
		}
		else if(meta.name == "page")
		{
			index = StringMetadata::group;
		}
		else if(meta.name == "options")
		{
			index = StringMetadata::options;
		}
		else if(meta.name == "widget")
		{
			index = StringMetadata::widget;
		}
		else if(meta.name == "niceName")
		{
			index = StringMetadata::niceName;
		}
		else if(meta.name == "related_to_widget")
		{
			index = StringMetadata::relatedToWidget;
		}

		if(index != StringMetadata::numDataTypes)
		{
			o_data.SetString(index, meta.sdefault[0].c_str());
		}
	}
}

struct NumericMetadata
{
	union NumValue
	{
		float f;
		int i;
	};

	bool m_hasMin;
	NumValue m_min;

	bool m_hasMax;
	NumValue m_max;

	bool m_hasSoftMin;
	NumValue m_softMin;

	bool m_hasSoftMax;
	NumValue m_softMax;

	int m_hidden;
};

static void ParseNumericMetadata(
		const DlShaderInfo::Parameter* i_parameter,
		NumericMetadata& o_data )
{
	o_data.m_hasMin = o_data.m_hasMax = false;
	o_data.m_hasSoftMin = o_data.m_hasSoftMax = false;
	o_data.m_hidden = 0;

	for (unsigned int j=0;  j<i_parameter->metadata.size(); j++)
	{
		const DlShaderInfo::Parameter& meta = i_parameter->metadata[j];

		if(!o_data.m_hasMin && meta.name == "min")
		{
			if( meta.type.IsOneFloat() )
			{
				o_data.m_hasMin = true;
				o_data.m_min.f = meta.fdefault[0];
			}
			else if( meta.type.IsOneInteger() )
			{
				o_data.m_hasMin = true;
				o_data.m_min.i = meta.idefault[0];
			}
		}
		else if(!o_data.m_hasMax && meta.name == "max")
		{
			if( meta.type.IsOneFloat() )
			{
				o_data.m_hasMax = true;
				o_data.m_max.f = meta.fdefault[0];
			}
			else if( meta.type.IsOneInteger() )
			{
				o_data.m_hasMax = true;
				o_data.m_max.i = meta.idefault[0];
			}
		}
		else if(!o_data.m_hasSoftMin && meta.name == "slidermin")
		{
			if( meta.type.IsOneFloat() )
			{
				o_data.m_hasSoftMin = true;
				o_data.m_softMin.f = meta.fdefault[0];
			}
			else if( meta.type.IsOneInteger() )
			{
				o_data.m_hasSoftMin = true;
				o_data.m_softMin.i = meta.idefault[0];
			}
		}
		else if(!o_data.m_hasSoftMax && meta.name == "slidermax")
		{
			if( meta.type.IsOneFloat() )
			{
				o_data.m_hasSoftMax = true;
				o_data.m_softMax.f = meta.fdefault[0];
			}
			else if( meta.type.IsOneInteger() )
			{
				o_data.m_hasSoftMax = true;
				o_data.m_softMax.i = meta.idefault[0];
			}
		}
		else if(meta.name == "hidden")
		{
			if( meta.type.IsOneInteger() )
			{
				o_data.m_hidden = meta.idefault[0];
			}
		}
	}
}

void DL_OSLShadingNode::CreateAttributesFromShaderParameters(
	DlShaderInfo *i_shader,
	MObject* i_object,
	MObjectArray* o_objects,
	MStringArray* o_objectNames )
{
	/*
		The two use cases for this procedure are:
		- Add the shader parameters as dynamic attributes on i_object;
		- Return the MObjects of created attributes in o_objects, suitable for
		  static attributes;
	*/
	if( !i_shader )
	{
		return;
	}
	MDGModifier dgModifier;

	using namespace Utilities;

	for( int i=0; i<i_shader->nparams(); i++)
	{
		const DlShaderInfo::Parameter* parameter = i_shader->getparam(i);

		StringMetadata meta;

		ParseStringMetadata(parameter, meta);

		// String metadata values that may be altered here
		MString attrName(meta.GetString(StringMetadata::attrName));
		MString type(meta.GetString(StringMetadata::type));

		// String metadata that will be used as-is
		const MString& label(meta.GetString(StringMetadata::label));
		const MString& options(meta.GetString(StringMetadata::options));
		const MString& widget(meta.GetString(StringMetadata::widget));
		const MString& niceName(meta.GetString(StringMetadata::niceName));
		const MString& relatedToWidget(
			meta.GetString(StringMetadata::relatedToWidget));

		if( attrName == "*none" )
		{
			continue;
		}

		if( relatedToWidget.length() > 0 &&
			( relatedToWidget == "maya_colorRamp" ||
				relatedToWidget == "maya_floatRamp") )
		{
			/*
				Skip any params that are related to a ramp widget. Their attribute is
				created when we reach the attribute that has the ramp widget.
			*/
			continue;
		}

		NumericMetadata data;
		ParseNumericMetadata( parameter, data );

		// Map some widget types to attribute types, when no attr type is specified
		if( type.length() == 0 && widget.length() > 0 )
		{
			if( widget == "mapper" )
				type = "enum";
			else if( widget == "checkBox" )
				type = "bool";
		}

		/* If there is no attribute in metadata, use the current parameter name*/
		if( !attrName.length() )
		{
			attrName = parameter->name.c_str();
		}

		/* Create a parameter depending on the type. */

		/*
			Trap the ramp attributes first, so that they are not handled as array
			attributes.
		*/
		bool isColorRamp = widget == "maya_colorRamp";
		bool isFloatRamp = widget == "maya_floatRamp";
		if( (isColorRamp || isFloatRamp) )
		{
			MString rampAttrName = attrName;
			int index = rampAttrName.index('.');
			if( index >= 0 )
			{
				rampAttrName = attrName.substring( 0, index - 1 );
			}

			MObject attribute;

			if( isColorRamp)
			{
				attribute = MRampAttribute::createColorRamp(rampAttrName, rampAttrName);
			}
			else
			{
				attribute = MRampAttribute::createCurveRamp(rampAttrName, rampAttrName);
			}

			if( o_objects != 0x0 )
			{
				o_objects->append( attribute );
			}
			if( o_objectNames )
			{
				o_objectNames->append( attrName );
			}

			if( i_object != 0x0 )
			{
				dgModifier.addAttribute( *i_object, attribute );
				dgModifier.doIt();
			}

			continue;
		}


		if( parameter->isclosure ||
		    parameter->type.IsOneColor() ||
		    parameter->type.IsOnePoint() ||
		    parameter->type.IsOneVector() ||
		    parameter->type.IsOneNormal() )
		{
			MFnNumericAttribute nAttr;
			MObject attribute;
			bool is_color = false;

			if( parameter->type.IsOneColor() ||
				parameter->isclosure )
			{
				attribute = nAttr.createColor( attrName, attrName );
				is_color = true;
			}
			else
			{
				attribute = nAttr.createPoint( attrName, attrName );
			}

			if( !parameter->isoutput )
			{
				makeShaderInputAttribute( nAttr );

				if( parameter->fdefault.size() == 3 )
				{
					nAttr.setDefault(
						parameter->fdefault[0],
						parameter->fdefault[1],
						parameter->fdefault[2]);
				}
			}
			else
			{
				makeOutputAttribute( nAttr );
			}

			if( niceName.length() > 0 )
			{
				if( is_color )
				{
					Utilities::setNiceNameColor( nAttr, niceName );
				}
				else
				{
					Utilities::setNiceNamePoint( nAttr, niceName );
				}
			}

			nAttr.setHidden( data.m_hidden > 0 );

			if( o_objects != 0x0 )
			{
				o_objects->append( attribute );
			}
			if( o_objectNames )
			{
				o_objectNames->append( attrName );
			}

			if( i_object != 0x0 )
			{
				dgModifier.addAttribute( *i_object, attribute );
				dgModifier.doIt();
			}
		}
		else if(parameter->type.elementtype == NSITypeFloat &&
			parameter->type.arraylen == 2 )
		{
			// UVs
			MFnNumericAttribute nAttr;

			MObject attributeu =
			nAttr.create(attrName + "U", attrName + "U", MFnNumericData::kFloat);

			if( parameter->isoutput )
			{
				makeOutputAttribute( nAttr );
			}
			else
			{
				makeShaderInputAttribute( nAttr );
			}

			nAttr.setHidden( data.m_hidden > 0 );
			if( niceName.length() > 0 )
			{
				nAttr.setNiceNameOverride( niceName );
			}

			MObject attributev =
			nAttr.create(attrName + "V", attrName + "V", MFnNumericData::kFloat);

			if( parameter->isoutput )
			{
				makeOutputAttribute( nAttr );
			}
			else
			{
				makeShaderInputAttribute( nAttr );
			}

			nAttr.setHidden( data.m_hidden > 0 );
			if( niceName.length() > 0 )
			{
				nAttr.setNiceNameOverride( niceName );
			}

			MObject attribute =
			nAttr.create(attrName, attrName, attributeu, attributev);

			if( parameter->isoutput )
			{
				makeOutputAttribute( nAttr );
			}
			else
			{
				makeShaderInputAttribute( nAttr );
			}

			nAttr.setHidden( data.m_hidden > 0 );
			if( niceName.length() > 0 )
			{
				nAttr.setNiceNameOverride( niceName );
			}

			if( o_objects != 0x0 )
			{
				o_objects->append( attributeu );
				o_objects->append( attributev );
				o_objects->append( attribute );
			}
			if( o_objectNames )
			{
				o_objectNames->append( attrName + "U" );
				o_objectNames->append( attrName + "V" );
				o_objectNames->append( attrName );
			}

			if( i_object != 0x0 )
			{
				dgModifier.addAttribute( *i_object, attribute );
				dgModifier.doIt();
			}
		}
		else if( parameter->type.IsOneFloat() )
		{
			// float
			MFnNumericAttribute nAttr;

			MObject attribute = nAttr.create(
				attrName, attrName,
				MFnNumericData::kFloat );

			if( !parameter->isoutput )
			{
				makeShaderInputAttribute( nAttr );
				nAttr.setDefault(
					parameter->fdefault[0]);
			}
			else
			{
				makeOutputAttribute( nAttr );
			}

			if( data.m_hasMin )
			{
				nAttr.setMin( data.m_min.f );
			}

			if( data.m_hasMax )
			{
				nAttr.setMax( data.m_max.f );
			}

			if( data.m_hasSoftMin )
			{
				nAttr.setSoftMin( data.m_softMin.f );
			}

			if( data.m_hasSoftMax )
			{
				nAttr.setSoftMax( data.m_softMax.f );
			}

			nAttr.setHidden( data.m_hidden > 0 );

			if( niceName.length() > 0 )
			{
				nAttr.setNiceNameOverride( niceName );
			}

			if( o_objects != 0x0 )
			{
				o_objects->append( attribute );
			}
			if( o_objectNames )
			{
				o_objectNames->append( attrName );
			}

			if( i_object != 0x0 )
			{
				dgModifier.addAttribute( *i_object, attribute );
				dgModifier.doIt();
			}
		}
		else if( parameter->type.IsOneMatrix() )
		{
			MFnMatrixAttribute matrixAttrFn;
			MObject attribute = matrixAttrFn.create( attrName, attrName );

			if( !parameter->isoutput )
			{
				makeShaderInputAttribute( matrixAttrFn );
				if( parameter->fdefault.size() == 16 )
				{
					const float (*values)[4] = (float (*)[4])&(parameter->fdefault[0]);
					MFloatMatrix mat(values);
					matrixAttrFn.setDefault( mat );
				}
			}
			else
			{
				makeOutputAttribute( matrixAttrFn );
			}

			matrixAttrFn.setHidden( data.m_hidden > 0 );

			if( o_objects != 0x0 )
			{
				o_objects->append( attribute );
			}
			if( o_objectNames )
			{
				o_objectNames->append( attrName );
			}

			if( i_object != 0x0 )
			{
				dgModifier.addAttribute( *i_object, attribute );
				dgModifier.doIt();
			}
		}
		else if( parameter->type.IsOneInteger() )
		{
			MObject attribute;

			if( type == "enum" )
			{
				MFnEnumAttribute enumAttr;
				attribute = enumAttr.create( attrName, attrName );

				if( !parameter->isoutput )
				{
					makeShaderInputAttribute( enumAttr );
					enumAttr.setDefault( parameter->idefault[0] );
				}
				else
				{
					makeOutputAttribute( enumAttr );
				}

				if( niceName.length() > 0 )
				{
					enumAttr.setNiceNameOverride( niceName );
				}

				/*
					Simply flag the attr as hidden when relevant, but the whole enum
					names / values definition is pointless when hidden > 0.
				*/
				enumAttr.setHidden( data.m_hidden > 0 );

				// Looking for 'name:value|' in the value of "options"
				const char* enums = options.asChar();

				// The start & end of a enum name
				int start = 0, end = 0;

				// The value of an enum
				int value = -1;

				for(int z = 0; z < options.length(); z++ )
				{
					// Check if we are at the end of a 'name:value' token
					if( enums[z] == '|' || z + 1 == options.length() )
					{
						// Check if a value was specified using ':x'
						if( end == start )
						{
							end = enums[z] == '|' ? z - 1 : z;
							value++;
						}
						else
						{
							int valueEnd = enums[z] == '|' ? z - 1 : z;
							MString valueStr = options.substring( end + 2, valueEnd );
							value = atoi( valueStr.asChar() );
						}

						// Add enum field to attr
						MString fieldName = options.substring( start, end );
						enumAttr.addField( fieldName, value );

						start = end = z + 1;
					}
					else if( enums[z] == ':' && z > 0 )
					{
						end = z - 1;
					}
				}
			}
			else
			{
				MFnNumericAttribute numAttr;
				MFnNumericData::Type dataType;

				if( type == "bool" )
				{
					dataType = MFnNumericData::kBoolean;
				}
				else
				{
					dataType = MFnNumericData::kInt;
				}

				attribute = numAttr.create( attrName, attrName, dataType );

				if( !parameter->isoutput )
				{
					makeShaderInputAttribute( numAttr );
					numAttr.setDefault( parameter->idefault[0]);
				}
				else
				{
					makeOutputAttribute( numAttr );
				}

				if( dataType == MFnNumericData::kInt )
				{
					if( data.m_hasMin )
					{
						numAttr.setMin( data.m_min.i );
					}

					if( data.m_hasMax )
					{
						numAttr.setMax( data.m_max.i );
					}

					if( data.m_hasSoftMin )
					{
						numAttr.setSoftMin( data.m_softMin.i );
					}

					if( data.m_hasSoftMax )
					{
						numAttr.setSoftMax( data.m_softMax.i );
					}
				}

				if( niceName.length() > 0 )
				{
					numAttr.setNiceNameOverride( niceName );
				}

				numAttr.setHidden( data.m_hidden > 0 );
			}

			if( o_objects != 0x0 )
			{
				o_objects->append( attribute );
			}
			if( o_objectNames )
			{
				o_objectNames->append( attrName );
			}

			if( i_object != 0x0 )
			{
				dgModifier.addAttribute( *i_object, attribute );
				dgModifier.doIt();
			}
		}
		else if( parameter->type.IsOneString() )
		{
			/*
				This should be moved outside of the parameter type checks if message
				attributes are meaningful for other shader parameter types.
			*/
			if( type == "message" )
			{
				MFnMessageAttribute mAttr;

				MObject attribute = mAttr.create( attrName, attrName );

				if( niceName.length() > 0 )
				{
					mAttr.setNiceNameOverride( niceName );
				}

				mAttr.setHidden( data.m_hidden > 0 );

				if( o_objects != 0x0 )
				{
					o_objects->append( attribute );
				}
				if( o_objectNames )
				{
					o_objectNames->append( attrName );
				}

				if( i_object != 0x0 )
				{
					dgModifier.addAttribute( *i_object, attribute );
					dgModifier.doIt();
				}
			}
			else
			{
 				MFnStringData stringData;
 				MObject defaultValueObj =
 					stringData.create(parameter->sdefault[0].c_str());

				MFnTypedAttribute tAttr;

				MObject attribute = tAttr.create(
					attrName, attrName,
					MFnData::kString,
					defaultValueObj );

				if( !parameter->isoutput )
				{
					tAttr.setStorable(true);
					tAttr.setKeyable(false);
				}
				else
				{
					makeOutputAttribute( tAttr );
				}

				if( niceName.length() > 0 )
				{
					tAttr.setNiceNameOverride( niceName );
				}

				tAttr.setHidden( data.m_hidden > 0 );

				MFnStringData defaultData;
				MString defaultValue( parameter->sdefault[0].c_str() );
				defaultData.set( defaultValue );

				if( o_objects != 0x0 )
				{
					o_objects->append( attribute );
				}
				if( o_objectNames )
				{
					o_objectNames->append( attrName );
				}

				if( i_object != 0x0 )
				{
					dgModifier.addAttribute( *i_object, attribute );
					dgModifier.doIt();

					 /*
					 	The default for the string attribute does not "stick" after a save
					 	and reload; explicitly set it via its MPlug.
					 */
					//MPlug plug( *i_object, attribute );
					//plug.setValue( parameter->sdefault[0].c_str() );
				}
			}
		}
	}
}

/*
	This creates a shaderFilename string attribute with the provided default
	value. Add this as a static attribute and the shader data cache will use it
	to figure out the OSL shader file to open for a given shading node.
	See ShaderData::CacheShaderData()
*/
MObject DL_OSLShadingNode::CreateShaderFilenameAttribute(
	const MString &i_name )
{
	MFnStringData defaultShaderFilenameData;
	MObject defaultShaderFilename = defaultShaderFilenameData.create( i_name );

	MFnTypedAttribute typedAttrFn;
	MObject shaderFilename =
		typedAttrFn.create( "shaderFilename", "file", MFnData::kString );
	typedAttrFn.setConnectable( false );
	typedAttrFn.setHidden( true );
	typedAttrFn.setDefault( defaultShaderFilename );
	return shaderFilename;
}

bool DL_OSLShadingNode::FindAttribute(
	const MString &i_name,
	MObject &o_object,
	const MObjectArray &i_objects,
	const MStringArray &i_objectNames)
{
	for( unsigned i = 0; i < i_objectNames.length(); ++i )
	{
		if( i_objectNames[i] == i_name && i < i_objects.length() )
		{
			o_object = i_objects[i];
			return true;
		}
	}
	return false;
}

void DL_OSLShadingNode::DefineShaderNiceName(
	const MString& i_name,
	const MString& i_niceName )
{
	if( i_niceName.length() > 0 )
	{
		MString key = MString( "n_" ) + i_name + ".niceName";

		MString cmd;
		cmd = MString("if (!`displayString -exists \"") + key + "\"` ) { " +
			"displayString -value \"" +  i_niceName + "\" \"" + key + "\"; }";

		MGlobal::executeCommand( cmd );
	}
}

bool DL_OSLShadingNode::ReloadShader( const MString& i_nodeTypeName )
{
	DlShaderInfo *info;
	MString shaderPath = OSLUtils::OpenShader( i_nodeTypeName, info );

	if( shaderPath == "" )
	{
		return false;
	}

	// Update the cached query object
	std::string shaderName = i_nodeTypeName.asChar();
	s_shaders[ shaderName ] = info;

	// Update the AE template
	bool generateAETemplate = true;

	for( const auto &meta : info->metadata() )
	{
		if( meta.name == "maya_generateAETemplate" && meta.idefault.size() > 0)
		{
			generateAETemplate = meta.idefault[0];
			break;
		}
	}

	if( generateAETemplate )
	{
		MGlobal::executeCommand( AETemplate(info) );
		MGlobal::executeCommand( "refreshEditorTemplates" );
	}
	else
	{
		MString procName = "AE" + i_nodeTypeName + "Template";
		MString cmd =
			"if( exists(\"" + procName + "\") ) { source \"" + procName + "\"; " +
			"refreshEditorTemplates; } ";

		MGlobal::executeCommand( cmd );
	}

	return true;
}

void* DL_OSLShadingNode::creator()
{
	DL_OSLShadingNode*  new_node;

	new_node = new DL_OSLShadingNode();

	return new_node;
}

MStatus DL_OSLShadingNode::initialize()
{
	/* Empty. We init all the parameters in postConstructor */
	return MStatus::kSuccess;
}

void DL_OSLShadingNode::postConstructor()
{
	MObject obj = thisMObject();

	auto it = s_shaders.find( typeName().asChar() );

	if (it == s_shaders.end())
	{
		/* There is no such shader in the cache. It can happen if the user saved the
			 scene and then removed the shader from the shader directory. */
		return;
	}

	CreateAttributesFromShaderParameters( it->second, &obj, nullptr, nullptr );

	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus DL_OSLShadingNode::compute(
		const MPlug& i_plug,
		MDataBlock& i_block )
{
	return MS::kSuccess;
}

MString DL_OSLShadingNode::AETemplate(DlShaderInfo *i_shader)
{
	MString typeName = i_shader->shadername().c_str();
	MString body = "global proc AE" + typeName + "Template(string $node) {\n";
	MString footer;
	MString initProc = "";

	body += "editorTemplate -beginScrollLayout;\n";

	MString last_group;

	for( int i=0; i<i_shader->nparams(); i++)
	{
		const DlShaderInfo::Parameter* parameter = i_shader->getparam(i);

		if( parameter->isoutput )
		{
			continue;
		}

		StringMetadata meta;
		ParseStringMetadata(parameter, meta);

		MString attrName(meta.GetString(StringMetadata::attrName));
		MString group(meta.GetString(StringMetadata::group));

		const MString& label(meta.GetString(StringMetadata::label));
		const MString& widget(meta.GetString(StringMetadata::widget));


		/* If there is no attribute metadata, use the current parameter name*/
		if( !attrName.length() )
		{
			attrName = parameter->name.c_str();
		}

		/* Check if the parameter shouln't be displayed in the UI. */
		NumericMetadata data;
		ParseNumericMetadata( parameter, data );

		int hidden = data.m_hidden;

		// Matrix attributes are never shown
		if( parameter->type.IsOneMatrix() )
		{
			hidden = 1;
		}

		// "null" widgets are never shown
		if( widget == "null" )
		{
			hidden = 1;
		}

		if( hidden )
		{
			footer += "editorTemplate -suppress \"" + attrName + "\";\n";
			continue;
		}

		if( !group.length() )
		{
			group = "Parameters";
		}

		/* Check if we need to create a new group */
		if( last_group != group )
		{
			if( last_group.length() )
			{
				/* If there was another group, we have to close it */
				body += "editorTemplate -endLayout;\n";
			}

			body +=
				"editorTemplate -beginLayout \"" +
				group +
				"\" -collapse false;\n";

			last_group = group;
		}

		if( widget.length() > 0 && widget == "navigation" )
		{
			body += "AE_addNavigationControlWithLabel ";
			body += "\"" + attrName + "\" ";
			body += "\"" + label + "\" ;\n";
		}
		else if( widget.length() > 0 && (widget == "maya_colorRamp" ||
			widget == "maya_floatRamp") )
		{
			MStringArray tokens;
			attrName.split('.', tokens);

			body += "AEaddRampControl ";
			body += "\"" + tokens[0] + "\";\n";

			if( widget == "maya_floatRamp" )
			{
				/*
					Default float ramp points: 0.0 to 1.0 linear interpolation (1).
				*/
				initProc += MString("DL_setOneFloatRampPoint( $node, " ) +
					MString( "\"" + tokens[0] + "\", 0, 0.0, 1, 0.0 );\n" );
				initProc += MString( "DL_setOneFloatRampPoint( $node, " ) +
					MString( "\"" + tokens[0] + "\", 1, 1.0, 1, 1.0 );\n" );
			}
			else
			{
				/*
					Default color ramp points: black to white, spline interpolation (3).
				*/
				initProc += MString( "DL_setOneColorRampPoint( $node, " ) +
					MString( "\"" + tokens[0] + "\", 0, 0.0, 3, 0.0, 0.0, 0.0 );\n" );
				initProc += MString( "DL_setOneColorRampPoint( $node, " ) +
					MString( "\"" + tokens[0] + "\", 1, 1.0, 3, 1.0, 1.0, 1.0 );\n" );
			}
		}
		else if( widget == "aovGroup" )
		{
			body += "AE_addAOVGroup(\"" + attrName + "\");\n";
		}
		else if( widget == "filename" )
		{
			body += "AE_addFileControlWithLabel(\"" + attrName + "\",\""
				+ label + "\");\n";
		}
		else
		{
			body += "editorTemplate -addDynamicControl ";

			if( label.length() > 0 )
			{
				body += "-label \"" + label + "\" ";
			}

			body += "\"" + attrName + "\";\n";
		}
	}

	body += "editorTemplate -addExtraControls  -extraControlsLabel \"\";\n";

	body += footer;

	body += "editorTemplate -suppress caching;\n";
	body += "editorTemplate -suppress nodeState;\n";

	body += "editorTemplate -suppress usedBy3dfm;\n";
	body += "editorTemplate -suppress version;\n";
	body += "editorTemplate -suppress frozen;\n";

	body += "editorTemplate -endLayout;\n";
	body += "editorTemplate -endScrollLayout;\n";

	body += "}\n";

	if( initProc.length() > 0 )
	{
		initProc = MString("global proc " + typeName + "Init( string $node )\n") +
			MString("{\n") + initProc + MString("}\n\n");

		body = initProc + body;
	}

	return body;
}

void DL_OSLShadingNode::RegisterSingleOSLShader(
	MFnPlugin &i_plugin, bool i_isUserPath, const char* i_path)
{
	/* Check if the filename is not too short */
	size_t path_len = strlen(i_path);
	if( path_len < 5 )
	{
		return;
	}

	/* If it does not end in .oso, do not autoload it. */
	if( 0 != strcmp(i_path + path_len - 4, ".oso") )
	{
		return;
	}

	DlShaderInfo *shader = DlGetShaderInfo(i_path);
	if( !shader )
	{
		/* It's not a shader. */
		return;
	}

	MString shaderName( shader->shadername().c_str() );

	// Don't attempt to regsiter a node type name that already exists
	if( MFnPlugin::isNodeRegistered( shaderName ) )
	{
		if( i_isUserPath )
		{
			MString msg = ("3Delight for Maya: Skipping duplicate '");
			msg += shaderName;
			msg += MString("' from '") + MString(i_path) + MString("'");

			MGlobal::displayInfo(msg);
		}

		return;
	}

	s_shaders.insert(
			std::pair<std::string, DlShaderInfo*>(
				shader->shadername().c_str(), shader));

	/* Get maya type & typeID */
	MString maya_type;
	MString maya_typeID;
	MString classification;
	MString niceName;

	bool generateAETemplate = true;

	for( const auto &meta : shader->metadata() )
	{
		if( meta.name == "tags")
		{
			// TODO :Multiple tags should be supported (similar to classifications)
			if( meta.sdefault.size()>0)
			{
				maya_type = meta.sdefault[0].c_str();
			}
		}
		else if( meta.name == "maya_typeID" && meta.sdefault.size() > 0)
		{
			maya_typeID = meta.sdefault[0].c_str();
		}
		else if( meta.name == "maya_classification" && meta.sdefault.size() > 0)
		{
			classification = meta.sdefault[0].c_str();
		}
		else if( meta.name == "maya_generateAETemplate" && meta.idefault.size() > 0)
		{
			generateAETemplate = meta.idefault[0];
		}
		else if( meta.name == "niceName" && meta.sdefault.size() > 0 )
		{
			niceName = meta.sdefault[0].c_str();
		}
	}

	/* Use the provided typeID to register the node, or generate one.

		 FIXME: the automatically generated type ID should be a hash of the type
		 name. The available ID range for internal custom nodes is 0x0 to 0x7ffff;
		 maybe we should use this, leaving the first and last 128 IDs for actual
		 studio use. And provide a clear warning that the generated ID may clash
		 with other generated ones.*/

	// This is what is done by the MtypeId(uint, uint) constructor. This is wrong
	// (see issue 6226), but is kept for now to provide a migration path for
	// existing scenes.
	//
	unsigned int id = MU_typeIds::DL_3DELIGHTOSL << 8 | (s_shaders.size() & 0xff);

	if(maya_typeID.length() > 0)
	{
		int annotation_id = 0;
		if(sscanf(maya_typeID.asChar(), "%x", &annotation_id) == 1)
		{
			id = annotation_id;
		}
	}
	else if( !i_isUserPath )
	{
		// Skip the shader since no type ID was provided
		return;
	}

	bool isSurface = false;

	bool rawClassification = classification.indexW("RAW=") == 0;
	if( rawClassification )
	{
		classification = classification.substringW(
			4, classification.numChars() - 1);
	}
	else if( classification.length() > 0 )
	{
		const MString dlPrefix(":rendernode/dl/shader/");

		if( classification == "texture/2d" )
		{
			classification = MString(":texture/2d") + dlPrefix + classification;
		}
		else if( classification == "surface" )
		{
			classification = MString(":shader/surface") + dlPrefix + classification;
			isSurface = true;
		}
		else if( classification == "displacement" )
		{
			classification = MString(":shader/displacement") + dlPrefix + classification;
		}
		else
		{
			/*
				Adding the "shader" classification allows the node type to be shown by
				the hypershade's node editor tab menu
			*/
			classification = dlPrefix + classification + ":shader";
		}
	}
	else if( maya_type.length() > 0 )
	{
		/* Maya classification is based on the type of the shader. */
		/* TODO: We need to add a swatch here. */
		MString shaderSurfaceNoSwatchClass;

		if( maya_type == "texture" )
		{
			/* We should decide if we want Maya to connect place2dTexture to the node.
				 Maya links place2dTexture to "uv" and "uvFilterSize" attributes of the
				 shader (see renderCreateNode.mel). So basically we can check if there are
				 such attributes, Maya should connect place2dTexture. */
			bool uv = false;
			bool uvFilterSize = false;
			for( int i=0; i<shader->nparams(); i++)
			{
				const DlShaderInfo::Parameter* parameter = shader->getparam(i);

				StringMetadata meta;
				ParseStringMetadata( parameter, meta );
				MString attrName(meta.GetString(StringMetadata::attrName));

				/* If there is no attribute metadata, use the current parameter name */
				if( !attrName.length() )
				{
					attrName = parameter->name.c_str();
				}

				if( attrName == "uv" )
				{
					uv = true;
				}

				if( attrName == "uvFilterSize" )
				{
					uvFilterSize = true;
				}
			}

			if( uv && uvFilterSize )
			{
				/* The classification that allows Maya to automatically create
					 place2dtexture node. */
				shaderSurfaceNoSwatchClass +=
					MString( ":shader" ) +
					":rendernode/dl/shader/texture/2d";
			}
			else
			{
				shaderSurfaceNoSwatchClass +=
					MString( ":shader" ) +
					":rendernode/dl/shader/texture";
			}
		}
		else if( maya_type == "lens" )
		{
			shaderSurfaceNoSwatchClass +=
				MString( ":shader" ) +
				":rendernode/dl/shader/lens";
		}
		else
		{
			shaderSurfaceNoSwatchClass +=
				MString( ":shader/surface" ) +
				":rendernode/dl/shader/surface";

			isSurface = true;
		}

		classification = shaderSurfaceNoSwatchClass;
	}

	// Add the proper Viewport shader classification
	MString vpPartialClassification( ":drawdb/shader/surface/dl/");
	MString vpSurface = vpPartialClassification + "VPSurfaceShader";
	MString vpShader = vpPartialClassification + "VPShader";

	if( !rawClassification )
	{
		if( isSurface )
		{
			classification += vpSurface;
		}
		else
		{
			classification += vpShader;
		}
	}

	MString msg("3Delight for Maya: Registering type '");
	msg += shaderName + MString("' ");

	if(maya_typeID.length() > 0)
	{
		msg += "(" + maya_typeID + ") ";
	}

	msg += MString("from '") + MString(i_path) + MString("'");

	MGlobal::displayInfo(msg);

	i_plugin.registerNode(
		shaderName,
		MTypeId(id),
		DL_OSLShadingNode::creator,
		DL_OSLShadingNode::initialize,
		MPxNode::kDependNode,
		&classification );

	if( generateAETemplate )
	{
		MGlobal::executeCommand( AETemplate(shader) );
	}

	DefineShaderNiceName( shaderName, niceName );
}

MStatus DL_OSLShadingNode::RegisterOSLShaders( MFnPlugin& i_plugin )
{
	MStringArray paths = OSLUtils::GetBuiltInSearchPaths();
	std::error_code ec;

	for( unsigned int i = 0; i < paths.length(); i++ )
	{
		for( const auto &entry :
			std::filesystem::directory_iterator{paths[i].asChar(), ec} )
		{
			RegisterSingleOSLShader(
				i_plugin, false, entry.path().string().c_str());
		}
	}

	paths.clear();
	paths = OSLUtils::GetUserSearchPaths();
	for( unsigned int i = 0; i < paths.length(); i++ )
	{
		for( const auto &entry :
			std::filesystem::directory_iterator{paths[i].asChar(), ec} )
		{
			RegisterSingleOSLShader(
				i_plugin, true, entry.path().string().c_str());
		}
	}

	return MStatus::kSuccess;
}
