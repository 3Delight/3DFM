#include "dl3DelightMaterial.h"

#include <maya/MFloatVector.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>

#include "DL_utils.h"

MObject dl3DelightMaterial::s_coating_on;
MObject dl3DelightMaterial::s_coating_transmittance;
MObject dl3DelightMaterial::s_coating_reflectivity;
MObject dl3DelightMaterial::s_coating_roughness;
MObject dl3DelightMaterial::s_coating_thickness;

MObject dl3DelightMaterial::s_color;
MObject dl3DelightMaterial::s_transparency;
MObject dl3DelightMaterial::s_diffuse_roughness;
MObject dl3DelightMaterial::s_sss_on;
MObject dl3DelightMaterial::s_sss_transmittance;
MObject dl3DelightMaterial::s_sss_scale;
MObject dl3DelightMaterial::s_sss_ior;
MObject dl3DelightMaterial::s_sss_merge_set;
MObject dl3DelightMaterial::s_sss_dominant_material;
MObject dl3DelightMaterial::s_sss_double_sided;

MObject dl3DelightMaterial::s_reflect_color;
MObject dl3DelightMaterial::s_reflect_reflectivity;
MObject dl3DelightMaterial::s_reflect_roughness;
MObject dl3DelightMaterial::s_reflect_anisotropy;
MObject dl3DelightMaterial::s_reflect_anisotropy_direction;

MObject dl3DelightMaterial::s_incandescence;
MObject dl3DelightMaterial::s_incandescence_intensity;

MObject dl3DelightMaterial::s_bump_layer;
MObject dl3DelightMaterial::s_normalCamera;

MObject dl3DelightMaterial::s_outColor;
MObject dl3DelightMaterial::s_outTransparency;

void* dl3DelightMaterial::creator()
{
	return new dl3DelightMaterial();
}

MStatus dl3DelightMaterial::initialize()
{
	MFnNumericAttribute numAttr;
	MFnEnumAttribute enumAttr;

	// Coating layer attributes
	s_coating_on = numAttr.create(
		"coating_on",
		"coating_on",
		MFnNumericData::kBoolean,
		0);
	numAttr.setNiceNameOverride( "On" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute(s_coating_on);

	s_coating_transmittance = numAttr.createColor(
		"coating_transmittance",
		"coating_transmittance" );
	numAttr.setDefault( 1.0, 0.5, 0.1 );
	Utilities::setNiceNameColor(numAttr, "Coating Color");
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_coating_transmittance );

	s_coating_reflectivity = numAttr.create(
		"coating_reflectivity",
		"coating_reflectivity",
		MFnNumericData::kFloat,
		0.3);
	numAttr.setMin(0.0);
	numAttr.setMax(1.0);
	numAttr.setSoftMin(0.0);
	numAttr.setSoftMax(1.0);
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_coating_reflectivity );

	s_coating_roughness = numAttr.create(
		"coating_roughness",
		"coating_roughness",
		MFnNumericData::kFloat,
		0.0);
	numAttr.setMin(0.0);
	numAttr.setMax(1.0);
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_coating_roughness );

	s_coating_thickness = numAttr.create(
		"coating_thickness",
		"coating_thickness",
		MFnNumericData::kFloat,
		0.01);
	numAttr.setMin(0.0);
	numAttr.setMax(100.0);
	numAttr.setSoftMin(0.0);
	numAttr.setSoftMax(1.0);
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_coating_thickness );

	// Base Layer: Diffuse and Subsurface attributes
	s_color = numAttr.createColor(
		"color",
		"c" );
	numAttr.setDefault( 0.6, 0.6, 0.6 );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_color );

	s_transparency = numAttr.createColor(
		"transparency",
		"transparency" );
	numAttr.setDefault( 0.0, 0.0, 0.0 );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_transparency );

	s_diffuse_roughness = numAttr.create(
		"diffuse_roughness",
		"diffuse_roughness",
		MFnNumericData::kFloat,
		0.3);
	numAttr.setMin(0.0);
	numAttr.setMax(1.0);
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_diffuse_roughness );

	s_sss_on = numAttr.create(
		"sss_on",
		"sss_on",
		MFnNumericData::kBoolean,
		0);
	numAttr.setNiceNameOverride( "Subsurface" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute(s_sss_on);

	s_sss_transmittance = numAttr.createColor(
		"sss_transmittance",
		"sss_transmittance" );
	numAttr.setDefault( 0.79, 0.59, 0.29 );
	Utilities::setNiceNamePoint(numAttr, "Transmittance");
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_sss_transmittance );

	s_sss_scale = numAttr.create(
		"sss_scale",
		"sss_scale",
		MFnNumericData::kFloat,
		0.1);
	numAttr.setMin(0.0);
	numAttr.setMax(100.0);
	numAttr.setSoftMin(0.0);
	numAttr.setSoftMax(2.0);
	numAttr.setNiceNameOverride( "Model Scale" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_sss_scale );

	s_sss_ior = numAttr.create(
		"sss_ior",
		"sss_ior",
		MFnNumericData::kFloat,
		1.6);
	numAttr.setMin(0.0);
	numAttr.setMax(100.0);
	numAttr.setSoftMin(1.0);
	numAttr.setSoftMax(5.0);
	numAttr.setNiceNameOverride( "Subsurface IOR" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_sss_ior );

	MFnTypedAttribute typedAttr;
	s_sss_merge_set = typedAttr.create(
		"sss_merge_set",
		"sss_merge_set",
		MFnData::kString);
	numAttr.setNiceNameOverride( "Subsurface Merge Set" );
	Utilities::makeShaderInputAttribute(typedAttr);
	addAttribute( s_sss_merge_set );

	s_sss_dominant_material = numAttr.create(
		"sss_dominant_material",
		"sss_dominant_material",
		MFnNumericData::kBoolean,
		0);
	numAttr.setNiceNameOverride( "Dominant Material" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute(s_sss_dominant_material);

	s_sss_double_sided = numAttr.create(
		"sss_double_sided",
		"sss_double_sided",
		MFnNumericData::kBoolean,
		0);
	numAttr.setNiceNameOverride( "Duble-Sided" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute(s_sss_double_sided);

	// Base Layer: Reflection attributes
	s_reflect_color = numAttr.createColor(
		"reflect_color",
		"reflect_color" );
	numAttr.setDefault( 1.0, 1.0, 1.0 );
	Utilities::setNiceNameColor(numAttr, "Reflection Color");
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_reflect_color );

	s_reflect_reflectivity = numAttr.create(
		"reflect_reflectivity",
		"reflect_reflectivity",
		MFnNumericData::kFloat,
		0.05);
	numAttr.setMin(0.0);
	numAttr.setMax(1.0);
	numAttr.setSoftMin(0.0);
	numAttr.setSoftMax(1.0);
	numAttr.setNiceNameOverride( "Reflectivity" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_reflect_reflectivity );

	s_reflect_roughness = numAttr.create(
		"reflect_roughness",
		"reflect_roughness",
		MFnNumericData::kFloat,
		0.3);
	numAttr.setMin(0.0);
	numAttr.setMax(1.0);
	numAttr.setNiceNameOverride( "Reflection Roughness" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_reflect_roughness );

	s_reflect_anisotropy = numAttr.create(
		"reflect_anisotropy",
		"reflect_anisotropy",
		MFnNumericData::kFloat,
		0.0);
	numAttr.setMin(-1.0);
	numAttr.setMax(1.0);
	numAttr.setNiceNameOverride( "Anisotropy" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_reflect_anisotropy );

	s_reflect_anisotropy_direction = numAttr.createColor(
		"reflect_anisotropy_direction",
		"reflect_anisotropy_direction" );
	numAttr.setDefault( 0.5, 1.0, 0.0 );
	Utilities::setNiceNameColor(numAttr, "Anisotropy Direction");
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_reflect_anisotropy_direction );

	// Incandescence attributes
	s_incandescence = numAttr.createColor(
		"incandescence",
		"incandescence" );
	numAttr.setDefault( 0.0, 0.0, 0.0 );
	Utilities::setNiceNameColor(numAttr, "Incandescence Color");
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_incandescence );

	s_incandescence_intensity = numAttr.create(
		"incandescence_intensity",
		"incandescence_intensity",
		MFnNumericData::kFloat,
		1.0);
	numAttr.setMin(0.0);
	numAttr.setSoftMin(0.0);
	numAttr.setSoftMax(10.0);
	numAttr.setNiceNameOverride( "Intensity" );
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_incandescence_intensity );

	// Bump attributes
	s_bump_layer = enumAttr.create(
		"bump_layer",
		"bump_layer");
	enumAttr.addField("Both Layers", 0);
	enumAttr.addField("Coating Layer", 1);
	enumAttr.addField("Base Layer", 2);
	Utilities::makeShaderInputAttribute(enumAttr);
	addAttribute(s_bump_layer);

	s_normalCamera = numAttr.createPoint(
		"normalCamera",
		"normalCamera" );
	numAttr.setDefault( 0.0, 0.0, 0.0 );
	Utilities::setNiceNamePoint(numAttr, "Bump Mapping");
	Utilities::makeShaderInputAttribute(numAttr);
	addAttribute( s_normalCamera );

	// Output attributes
	s_outColor = numAttr.createColor( "outColor", "oc" );
	Utilities::makeOutputAttribute(numAttr);
	addAttribute( s_outColor );

	s_outTransparency = numAttr.createColor( "outTransparency", "ot" );
	Utilities::makeOutputAttribute(numAttr);
	addAttribute( s_outTransparency );

	attributeAffects( s_color, s_outColor );

	return MStatus::kSuccess;
}

void dl3DelightMaterial::postConstructor()
{
	MFnDependencyNode dep_node(thisMObject());

	/* Set the default node name */
	MString defaultname = typeName() + "#";

	/* Save the digit in the node name */
	if( isdigit( defaultname.asChar()[0] ) )
	{
		defaultname = "_" + defaultname;
	}

	dep_node.setName(defaultname);

	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dl3DelightMaterial::compute(
    const MPlug& i_plug,
    MDataBlock& i_block )
{
	if ((i_plug != s_outColor) && (i_plug.parent() != s_outColor))
	{
		return MS::kUnknownParameter;
	}

	/* Just transfer s_color to s_outColor */
	MFloatVector& color = i_block.inputValue( s_color ).asFloatVector();

	/* set ouput color attribute */
  	MDataHandle outColorHandle = i_block.outputValue( s_outColor );
	MFloatVector& outColor = outColorHandle.asFloatVector();
	outColor = color;
	outColorHandle.setClean();

	return MS::kSuccess;
}
