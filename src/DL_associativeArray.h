#ifndef __DL_ASSOCIATIVEARRAY_H__
#define __DL_ASSOCIATIVEARRAY_H__

#include <maya/MPxCommand.h>

class DL_stringIntMap;

class DL_associativeArray : public MPxCommand
{
public:
  virtual MStatus       doIt(const MArgList& args);

  virtual bool          isUndoable() const
                        { return false; }

  static void*          creator()
                        { return new DL_associativeArray; }

  static MSyntax        newSyntax();

  // Returns a reference to the object with all the stored maps.
  static DL_stringIntMap& getMaps();
private:
};

#endif
