#include "NSIExportTransform.h"

#include <maya/MFnTransform.h>
#include <maya/MFnMatrixData.h>
#include <maya/MMatrix.h>
#include <maya/MPlug.h>

#include <assert.h>

void NSIExportTransform::Create()
{
	NSICreate( m_nsi, m_nsi_handle, "transform", 0, 0x0 );

	/*
		Specify an "assetname" attribute for objects that have a namespace
		different that their parent's. This will set a uniform "id.asset" AOV
		for all objects from the same namespace.
	*/
	MString ns = MFnDependencyNode(Object()).parentNamespace();
	if(ns.length() > 0)
	{
		MObject parent = MFnDagNode(Object()).parent(0);
		assert(!parent.isNull());
		MString parent_ns = MFnDependencyNode(parent).parentNamespace();
		if(ns != parent_ns)
		{
			NSICreate(m_nsi, AttributesHandle().asChar(), "attributes", 0, nullptr);
			NSIConnect(m_nsi, AttributesHandle().asChar(), "", m_nsi_handle, "geometryattributes", 0, nullptr);

			const char* name = ns.asChar();
			NSIParam_t assetname;
			assetname.name = "assetname";
			assetname.data = &name;
			assetname.type = NSITypeString;
			assetname.count = 1;
			assetname.flags = 0;
			NSISetAttribute(m_nsi, AttributesHandle().asChar(), 1, &assetname);
		}
	}
}

/**
	All transform attributes are time-dependent.
*/
void NSIExportTransform::SetAttributes( void )
{
	assert( IsValid() );
	assert( Object().hasFn(MFn::kTransform) );

	return;
}

void NSIExportTransform::SetAttributesAtTime(
	double i_time, bool i_no_motion )
{
	assert( IsValid() );
	assert( Object().hasFn(MFn::kTransform) );

	MStatus status = MStatus::kSuccess;
	MFnTransform trs( Object(), &status);

	if( status != MStatus::kSuccess )
	{
		assert( false );
		return;
	}

	MMatrix matrix = trs.transformation( &status ).asMatrix();

	MFnDependencyNode dep_node( Object() );
	auto offset_plug = dep_node.findPlug("offsetParentMatrix", true, &status);
	if( status == MStatus::kSuccess )
	{
		MFnMatrixData offsetParentMatrix( offset_plug.asMObject() );
		matrix = matrix * offsetParentMatrix.matrix();
	}

	NSIParam_t transformParam;
	transformParam.name = "transformationmatrix";
	transformParam.data = matrix.matrix;
	transformParam.type = NSITypeDoubleMatrix;
	transformParam.count = 1;
	transformParam.flags = 0;

	if( i_no_motion )
		NSISetAttribute( m_nsi, m_nsi_handle, 1, &transformParam );
	else
		NSISetAttributeAtTime(
			m_nsi, m_nsi_handle, i_time, 1, &transformParam );

	return;
}
