#include <assert.h>
#include <stdio.h>

#include <maya/MFileIO.h>
#include <maya/MFloatVector.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnParticleSystem.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MGlobal.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MSelectionList.h>
#include <maya/MString.h>
#include <maya/MCommandResult.h>

#ifdef _WIN32
#define snprintf _snprintf
#endif

#include "DL_utils.h"
#include "DL_renderState.h"
#include "delight.h"

#include <filesystem>

namespace Utilities
{
//
// Notes on the "default attribute attributes"
//
// setKeyable should be overidden on a per-attribute basis; generally we want it
// on shading nodes, and we don't want it on shapes because apparently using the
// 's' shortcut to key shape attributes will also key these.
// The Maya Shape attributes are generally non-keyable by default. This can be
// changed on a given node in the Channel Box editor (Edit -> Channel Control)
//
// Since some users really like the channel box editor, the attributes will be
// visibile in it even though they are not "keyable".
//
void makeInputAttribute(MFnAttribute& i_attrFn, bool i_keyable)
{
  // Keep the list of set() calls in sync in makeInputOutput and makeOutput.
  i_attrFn.setKeyable(i_keyable);
  i_attrFn.setChannelBox(!i_keyable);
  i_attrFn.setStorable(true);
  i_attrFn.setReadable(false);
  i_attrFn.setWritable(true);
  i_attrFn.setConnectable(true);
}

void makeShaderInputAttribute(MFnAttribute& i_attrFn)
{
  // Keep the list of set() calls in sync in makeInputOutput and makeOutput.
  i_attrFn.setKeyable(true);
  /*
    This is a stupid value for setChannelBox. The doc says that it is ignored
    when the attribute is keyable, but setting it to true will produce gadgets
    with a light gray background, as if they were not keyable.
  */
  i_attrFn.setChannelBox(false);
  i_attrFn.setStorable(true);
  i_attrFn.setReadable(true);
  i_attrFn.setWritable(true);
  i_attrFn.setConnectable(true);
}

//
// This is the default Maya behaviour for non-array (multi) attributes.
//
void makeInputOutputAttribute(MFnAttribute& i_attrFn)
{
  // Keep the list of set() calls in sync in makeInput and makeOutput.
  i_attrFn.setKeyable(true);
  i_attrFn.setChannelBox(false);
  i_attrFn.setStorable(true);
  i_attrFn.setReadable(true);
  i_attrFn.setWritable(true);
  i_attrFn.setConnectable(true);
}

void makeOutputAttribute(MFnAttribute& i_attrFn)
{
  // Keep the list of set() calls in sync in makeInputOutput and makeInput.
  i_attrFn.setKeyable(false);
  i_attrFn.setStorable(false);
  i_attrFn.setReadable(true);
  i_attrFn.setWritable(false);
  i_attrFn.setConnectable(true);
}

void setNiceNameColor(MFnNumericAttribute& io_numAttr, const MString& i_name)
{
  io_numAttr.setNiceNameOverride( i_name );

    MObject child1 = io_numAttr.child(0);
    MFnAttribute attr1(child1);
    MString name1 = i_name + " R";
  attr1.setNiceNameOverride( name1 );

    MObject child2 = io_numAttr.child(1);
    MFnAttribute attr2(child2);
    MString name2 = i_name + " G";
  attr2.setNiceNameOverride( name2 );

    MObject child3 = io_numAttr.child(2);
    MFnAttribute attr3(child3);
    MString name3 = i_name + " B";
  attr3.setNiceNameOverride( name3 );
}

void setNiceNamePoint(MFnNumericAttribute& io_numAttr, const MString& i_name)
{
  io_numAttr.setNiceNameOverride( i_name );

    MObject child1 = io_numAttr.child(0);
    MFnAttribute attr1(child1);
    MString name1 = i_name + " X";
  attr1.setNiceNameOverride( name1 );

    MObject child2 = io_numAttr.child(1);
    MFnAttribute attr2(child2);
    MString name2 = i_name + " Y";
  attr2.setNiceNameOverride( name2 );

    MObject child3 = io_numAttr.child(2);
    MFnAttribute attr3(child3);
    MString name3 = i_name + " Z";
  attr3.setNiceNameOverride( name3 );
}

float CIEluminance( const MFloatVector& i_color )
{
  return
    i_color[0] * 0.212671 +
    i_color[1] * 0.715160 +
    i_color[2] * 0.072169;
}

std::string FrameID( double frame_num )
{
	char frameid[64];

	/* Taken from DL_utils.cpp */
	if (frame_num - floor(frame_num) < 1e-6)
	{
		snprintf(frameid, sizeof(frameid), "%04d", static_cast<int>(frame_num));
	}
	else
	{
		const char* padding = "";
		if (frame_num < 10)
			padding = "000";
		else if (frame_num < 100)
			padding = "00";
		else if (frame_num < 1000)
			padding = "0";

		snprintf(frameid, sizeof(frameid), "%s%.3f", padding, frame_num);
	}

	return std::string(frameid);
}

bool isReadingSceneFile()
{
  return (MFileIO::isImportingFile() |
    MFileIO::isOpeningFile() |
    MFileIO::isReferencingFile() |
    MFileIO::isReadingFile());
}

void GetTypeClassificationStrings(
  const MString& i_typeName,
  MStringArray& o_classificationStrings )
{
  MString classification = MFnDependencyNode::classification( i_typeName );

  MStringArray tokens;
  classification.split(':', o_classificationStrings);
}

bool HasClass(
  const MString& i_class,
  const MStringArray i_classificationStrings )
{
  for(unsigned i = 0; i < i_classificationStrings.length(); i++ )
  {
    if( !strncmp(
      i_classificationStrings[i].asChar(),
      i_class.asChar(),
      i_class.length() ) )
    {
      return true;
    }
  }

  return false;
}

}  // Utilities namespace

bool
getSourceNode(
  MObject& source_node,
  const MFnDependencyNode& dst_node_fn,
  const MString& dst_attr)
{
  MStatus found_plug;
  MPlug   dst_plug;
  bool    success;

  dst_plug = dst_node_fn.findPlug(dst_attr, true, &found_plug);

  if (found_plug)
  {
    MPlugArray  source_connections;

    // only look for connections with this plug as destination
    dst_plug.connectedTo(source_connections, true, false);

    if (source_connections.length() > 0)
    {
      // we can only have one input so we can hard-code this index
      source_node = source_connections[0].node();

      success = true;
    }
    else
      success = false;
  }
  else
  {
    success = false;
  }

  return success;
}

/**
	\brief returns the object that might hols Pref and Nref for the given
	geometry.

	\return
		true if reference node present, false otherwise.
*/
bool getReferenceObject(
	const MFnDependencyNode& i_dep_node_fn,
	MObject& o_reference_mesh )
{
	return getSourceNode(o_reference_mesh, i_dep_node_fn, "referenceObject");
}

bool
getAttrBool(
  const MObject& node,
  const MString& attr_name,
  bool default_value,
  bool& attr_found)
{
  bool value;
  attr_found = false;

  MStatus status = MStatus::kFailure;

  if (!node.isNull())
  {
    MFnDependencyNode node_fn(node);
    MPlug plug;

    plug = node_fn.findPlug(attr_name, true);

    if (!plug.isNull())
    {
      status = plug.getValue(value);
      attr_found = true;
    }
  }

  if (status != MStatus::kSuccess)
  {
    value = default_value;
  }

  return value;
}

bool
getAttrBool(
  const MObject& node,
  const MString& attr_name,
  bool default_value)
{
  bool dummy;
  return getAttrBool(node, attr_name, default_value, dummy);
}

int
getAttrInt(
  const MObject& node,
  const MString& attr_name,
  int default_value,
  bool& attr_found)
{
  int value;
  attr_found = false;

  MStatus status = MStatus::kFailure;

  if (!node.isNull())
  {
    MFnDependencyNode node_fn(node);
    MPlug plug;

    plug = node_fn.findPlug(attr_name, true);

    if (!plug.isNull())
    {
      status = plug.getValue(value);
      attr_found = true;
    }
  }

  if (status != MStatus::kSuccess)
  {
    value = default_value;
  }

  return value;
}

int
getAttrInt(
  const MObject& node,
  const MString& attr_name,
  int default_value)
{
  bool dummy;
  return getAttrInt(node, attr_name, default_value, dummy);
}

double
getAttrDouble(
  const MObject& node,
  const MString& attr_name,
  double default_value,
  bool& attr_found)
{
  double value;
  attr_found = false;

  MStatus status = MStatus::kFailure;

  if (!node.isNull())
  {
    MFnDependencyNode node_fn(node);
    MPlug plug;

    plug = node_fn.findPlug(attr_name, true);

    if (!plug.isNull())
    {
      status = plug.getValue(value);
      attr_found = true;
    }
  }

  if (status != MStatus::kSuccess)
  {
    value = default_value;
  }

  return value;
}

double
getAttrDouble(
  const MObject& node,
  const MString& attr_name,
  double default_value)
{
  bool dummy;
  return getAttrDouble(node, attr_name, default_value, dummy);
}

static bool isAttrOfType(
  const MObject& node,
  const MString& attr_name,
  MFnData::Type i_type)
{
  MStatus status = MStatus::kFailure;
  bool is_correct_type = false;

  if (!node.isNull())
  {
    MFnDependencyNode node_fn(node);
    MObject attr;

    attr = node_fn.attribute(attr_name, &status);

    if (status)
    {
      MFnAttribute attr_fn(attr);
      is_correct_type = attr_fn.accepts(i_type);
    }
  }

  return is_correct_type;
}

bool
isAttrNumeric(const MObject& node, const MString& attr_name)
{
  // Note: it looks like MFnData::kString attributes may return
  // true for here, while a numeric attr seems to return false
  // when passed ti isAttrString.
  //
  return isAttrOfType(node, attr_name, MFnData::kNumeric);
}

bool
isAttrString(const MObject& node, const MString& attr_name)
{
  return isAttrOfType(node, attr_name, MFnData::kString);
}

MString
getAttrString(
  const MObject& node,
  const MString& attr_name,
  const MString& default_value,
  bool& attr_found)
{
  MString value;
  attr_found = false;

  MStatus status = MStatus::kFailure;

  if (!node.isNull())
  {
    MFnDependencyNode node_fn(node);
    MPlug plug;

    plug = node_fn.findPlug(attr_name, true);

    if (!plug.isNull())
    {
      status = plug.getValue(value);
      attr_found = true;
    }
  }

  if (status != MStatus::kSuccess)
  {
    value = default_value;
  }

  return value;
}

MString
getAttrString(
  const MObject& node,
  const MString& attr_name,
  const MString& default_value)
{
  bool dummy;
  return getAttrString(node, attr_name, default_value, dummy);
}

bool
getAttrIntArray(
  const MObject& node,
  const MString& attr_name,
  MIntArray& value)
{
  MStatus status = MStatus::kFailure;

  value.clear();

  if (!node.isNull())
  {
    MFnDependencyNode node_fn(node);
    MPlug plug = node_fn.findPlug(attr_name, true);

    if (!plug.isNull())
    {
      MObject plug_value;
      plug.getValue(plug_value);

      MFnIntArrayData array(plug_value, &status);

      if (status == MStatus::kSuccess)
      {
        array.copyTo(value);
      }
    }
  }

  return status == MStatus::kSuccess;
}


bool
getAttrDoubleArray(
  const MObject& node,
  const MString& attr_name,
  MDoubleArray& value)
{
  MStatus status = MStatus::kFailure;

  value.clear();

  if (!node.isNull())
  {
    MFnDependencyNode node_fn(node);
    MPlug plug = node_fn.findPlug(attr_name, true);

    if (!plug.isNull())
    {
      MObject plug_value;
      plug.getValue(plug_value);

      MFnDoubleArrayData array(plug_value, &status);

      if (status == MStatus::kSuccess)
      {
        array.copyTo(value);
      }
    }
  }

  return status == MStatus::kSuccess;
}

bool
getAttrVectorArray(
  const MObject& node,
  const MString& attr_name,
  MVectorArray& value)
{
  MStatus status = MStatus::kFailure;

  value.clear();

  if (!node.isNull())
  {
    MFnDependencyNode node_fn(node);
    MPlug plug = node_fn.findPlug(attr_name, true);

    if (!plug.isNull())
    {
      MObject plug_value;
      plug.getValue(plug_value);

      MFnVectorArrayData array(plug_value, &status);

      if (status == MStatus::kSuccess)
      {
        array.copyTo(value);
      }
    }
  }

  return status == MStatus::kSuccess;
}

bool
getAttrColor(
  const MObject& node,
  const MString& attr_name,
  MColor& value)
{
  if (!node.isNull())
  {
    MFnDependencyNode node_fn(node);
    MPlug plug = node_fn.findPlug(attr_name, true);

    if (!plug.isNull() && plug.isCompound() && plug.numChildren() >= 3)
    {
      double r = 0.0;
      double g = 0.0;
      double b = 0.0;
      double a = 1.0;

      plug.child(0).getValue(r);
      plug.child(1).getValue(g);
      plug.child(2).getValue(b);

      if(plug.numChildren() > 3)
      {
        plug.child(3).getValue(a);
      }
      value = MColor(r, g, b, a);
      return true;
    }
  }

  return false;
}

bool
getAttrFloat3(
  const MObject& node,
  const MString& attr_name,
  MFloatArray& value)
{
  if (!node.isNull())
  {
    MFnDependencyNode node_fn(node);
    MPlug plug = node_fn.findPlug(attr_name, true);

    if (!plug.isNull() && plug.isCompound() && plug.numChildren() >= 3)
    {
      double x = 0.0;
      double y = 0.0;
      double z = 0.0;

      plug.child(0).getValue(x);
      plug.child(1).getValue(y);
      plug.child(2).getValue(z);

      value.setLength(3);
      value[0] = x;
      value[1] = y;
      value[2] = z;

      return true;
    }
  }

  return false;
}

void
normalizeKnotValues(
  MDoubleArray& knot_vector,
  double min,
  double max)
{
  double normalizer = 1.0 / (max - min);

  for (unsigned knot_num = 0; knot_num < knot_vector.length(); ++knot_num)
  {
    knot_vector[knot_num] = (knot_vector[knot_num]-min) * normalizer;
  }
}

// the "current" uv set (if any) will NOT be in the list of extra uv sets
void
getAllUVsetNames(
  MString& current_uv_set,
  MStringArray& extra_uv_sets,
  const MFnMesh& mesh_fn)
{
  mesh_fn.getUVSetNames(extra_uv_sets);

  mesh_fn.getCurrentUVSetName(current_uv_set);

  // remove the current UV set from the array
  if (current_uv_set != "")
  {
    for (unsigned i = 0; i < extra_uv_sets.length(); ++i)
    {
      if (extra_uv_sets[i] == current_uv_set)
      {
        extra_uv_sets.remove(i);
        break;
      }
    }
  }
  // else do nothing
}

void
fillFrameString(char* frame_string,
                unsigned frame_string_length,
                int num_int_digits,
                float frame_num)
{
  if (num_int_digits > 0)
  {

    char format[12];

    if (frame_num - floor(frame_num) < 1e-6)
    {
      // Produce a format string of "%0Yd", where Y = num_int_digits.
      snprintf( format, 12, "%%0%dd", num_int_digits );

      snprintf(frame_string, frame_string_length-1, format,
          static_cast<int>(frame_num));
    }
    else
    {
      /*
        The desired format string here is "%0Yd.3f", where
        Y = num_int_digits + 1 (for '.') + 3 ( num digits after the decimal).
      */
      snprintf( format, 12, "%%0%d.3f", num_int_digits + 4 );

      snprintf(frame_string, frame_string_length-1, format, frame_num);
    }
  }
  else
  {
    if (frame_num - floor(frame_num) < 1e-6)
      snprintf(frame_string, frame_string_length-1, "%.0f", frame_num);
    else
      snprintf(frame_string, frame_string_length-1, "%.3f", frame_num);
  }

  // just in case...
  frame_string[frame_string_length-1] = '\0';
}

void
parseFrameExpression(MString& frame_string,
                     const MString& expression,
                     float frame_num)
{
  if (expression != "")
  {
    const char* expression_chars;
    int         i = 0;
    float       computed_frame = 0;
    int         pad_frame = 0;
    bool        expression_is_valid;

    expression_chars = expression.asChar();

    if (expression_chars[i] == '#' || expression_chars[i] == '@')
    {
      if (expression_chars[i++] == '#')
        pad_frame = 4;
      else
      {
        pad_frame = 1;
        while( expression_chars[i] == '@')
        {
          pad_frame++;
          i++;
        }
      }

      // check for an operator and operand
      if (expression.length() > i)
      {
        char  expression_operator = expression_chars[i];

        if (   expression_operator == '+'
            || expression_operator == '-'
            || expression_operator == '*'
            || expression_operator == '/'
            || expression_operator == '%')
        {
          MString operand_string;

          operand_string = expression.substring(i + 1, expression.length()-1);

          if (operand_string.isInt())
          {
            int operand = operand_string.asInt();

            switch(expression_operator)
            {
              case '+':
                computed_frame = frame_num + operand;
                break;
              case '-':
                computed_frame = frame_num - operand;
                break;
              case '*':
                computed_frame = frame_num * operand;
                break;
              case '/':
                computed_frame = frame_num / operand;
                break;
              case '%':
                computed_frame = static_cast<int>(frame_num) % operand;
                break;
            }

            expression_is_valid = 1;
          }
          else
            expression_is_valid = 0;
        }
        else
          expression_is_valid = 0;
      }
      else
      {
        computed_frame = frame_num;

        expression_is_valid = 1;
      }

      if (expression_is_valid)
      {
        char  frame_chars[128];

        fillFrameString(frame_chars, 128, pad_frame, computed_frame);
        frame_string.set(frame_chars);
      }
      // else do nothing
    }
    // else expression isn't valid.. do nothing
  }
  // else expression is empty.. do nothing
}

void
expandEnvVars(char* dest, unsigned dest_len, const char* src, float frame_num)
{
  expandEnvVars(dest, dest_len, src, true, frame_num);
}

void
expandEnvVars(char* dest, unsigned dest_len, const char* src,
              bool do_frame_seq, float frame_num)
{
  // TODO: support ~username
  unsigned  dest_pos = 0;
  unsigned  src_pos = 0;

  while (src[src_pos] != '\0' && dest_pos < dest_len-1)
  {
    if ( (src[src_pos] == '#' || src[src_pos] == '@' || src[src_pos] == '{')
          && do_frame_seq )
    {
      MString   expression;

      if (src[src_pos] == '#' || src[src_pos] == '@')
      {

        if (src[src_pos] == '#')
        {
          expression = "#";
          src_pos++;
        }
        else
        {
          do
          {
            expression += "@";
          }
          while( src[++src_pos] == '@' );
        }
      }
      else
      {
        unsigned  expression_start;
        unsigned  expression_end;

        src_pos++;
        expression_start = src_pos;

        // process the expression in between the braces.. make sure we don't run
        // past the end of the string (when there is no terminating brace)
        while (src[src_pos] != '}' && src[src_pos] != '\0')
          src_pos++;

        expression_end = src_pos-1;

        expression.set(&src[expression_start],
                       expression_end-expression_start+1);

        // don't forget to eat the closing '}'
        if (src[src_pos] == '}')
          src_pos++;
      }

      MString   frame_string;
      unsigned  string_pos;

      parseFrameExpression(frame_string, expression, frame_num);

      // now that we have our frame string we need to append it to our
      // destination string
      string_pos = 0;

      while(string_pos < frame_string.length() && dest_pos < dest_len-1)
      {
        dest[dest_pos] = frame_string.asChar()[string_pos];
        dest_pos++;
        string_pos++;
      }
    }
    else if (
      src[src_pos] == '$' ||
      src[src_pos] == '~' ||
      src[src_pos] == '%' ||
      src[src_pos] == '`')
    {
      char first_char = src[src_pos];
      unsigned saved_src_pos = src_pos;

      // only look for a variable if the $, % or ~ is the first character of
      // the string OR the previous character isn't a \ (ie if we're on the
      // second character and the first character is a \ then don't look for a
      // variable). Same goes for `, except that we look for a command in that
      // case.
      if (src_pos == 0 || src[src_pos-1] != '\\')
      {
        MString var_name;
        char*   curr_env_val;

        if ((src[src_pos] == '%' || src[src_pos] == '`') ||
            (src[src_pos] == '$' && src[src_pos+1] == '{'))
        {
          unsigned  var_start;
          unsigned  var_end;

          char close_char = first_char;
          if( first_char == '$' )
          {
            // ${VAR}: extra ++ for the { and look for } at the end.
            close_char = '}';
            ++src_pos;
          }

          src_pos++;
          var_start = src_pos;

          // process the expression in between the percents or backquotes.
          // Make sure we don't run past the end of the string (when there is no
          // terminating character)
          while (src[src_pos] != close_char && src[src_pos] != '\0')
            src_pos++;

          var_end = src_pos-1;

          var_name.set(&src[var_start], var_end-var_start+1);

          // don't forget to eat the closing character
          if (src[src_pos] == close_char)
            src_pos++;
        }
        else if (src[src_pos] == '$')
        {
          unsigned  var_start;
          unsigned  var_end;

          src_pos++;
          var_start = src_pos;

          while (isalnum(src[src_pos]) || src[src_pos] == '_')
            src_pos++;

          var_end = src_pos-1;

          var_name.set(&src[var_start], var_end-var_start+1);
        }
        else // ~
        {
          // Don't expand ~ if it is followed by something else than a
          // directory separator or null character - on Windows we may receive
          // a truncated dir name like MAXIME~1 that we must not expand.
          //
          if (src[src_pos + 1] == '/' ||
            src[src_pos + 1] == '\\' ||
            src[src_pos + 1] == '\0')
          {
            var_name = "HOME";
          }
          src_pos++;
        }

        if (first_char == '`')
        {
          curr_env_val =
            const_cast<char*>(getStringCommandResult(var_name).asChar());
        }
        else
        {
          curr_env_val = getenv(var_name.asChar());
        }

        if (curr_env_val)
        {
          unsigned  val_pos;

          val_pos = 0;

          // loop until we hit the end of the environment variable's value or
          // the end of the destination string
          while(curr_env_val[val_pos] != '\0' && dest_pos < dest_len-1)
          {
            dest[dest_pos] = curr_env_val[val_pos];

            if(dest[dest_pos] == '\\')
            {
              dest[dest_pos] = '/';
            }

            dest_pos++;
            val_pos++;
          }
        }
        else
        {
          /* No value. Copy the lookup to output unchanged. */
          while( saved_src_pos != src_pos && dest_pos < dest_len-1 )
          {
            dest[dest_pos++] = src[saved_src_pos++];
          }
        }
      }
      else // the $ or ~ was escaped.. don't expand
      {
        // we need to remove the \ from the destination string
        dest_pos--;

        // then write the $
        dest[dest_pos] = src[src_pos];
        dest_pos++;
        src_pos++;
      }
    }
    else
    {
      dest[dest_pos] = src[src_pos];

      if(dest[dest_pos] == '\\')
      {
        dest[dest_pos] = '/';
      }

      dest_pos++;
      src_pos++;
    }
  }

  // don't forget to terminate the string
  dest[dest_pos] = '\0';
}

MString Utilities::expandFToken( const MString& i_filename, float frame_number )
{
  if( i_filename.length() < 3 )
    return i_filename;

  const char* filename = i_filename.asChar();
  int token_start_index = -1;
  for( int i = 0; i <= i_filename.length() - 3; i++ )
  {
    if( filename[ i ] == '<' &&
      filename[ i + 1 ] == 'f' &&
      filename[ i + 2 ] == '>')
    {
      token_start_index = i;
    }
  }

  if( token_start_index < 0 )
    return i_filename;

  MString filename_pre("");
  if(token_start_index > 0)
  {
    filename_pre = i_filename.substring( 0, token_start_index - 1 );
  }

  MString filename_post = i_filename.substring(
    token_start_index + 3, i_filename.length() );

  char frame_chars[128];

  MString expandedFilename;

  for( int i = 1; i <= 8; i++ )
  {
    fillFrameString( frame_chars, 128, i, frame_number );

    MString curr_file = filename_pre + frame_chars + filename_post;

	std::error_code ec;
    if( std::filesystem::exists(curr_file.asChar(), ec) )
    {
      expandedFilename = curr_file;
      break;
    }

    if( i == 1 )
    {
      // Not a result that exists
      expandedFilename = curr_file;
    }
  }

  return expandedFilename;
}

MString
getStringCommandResult(const MString cmd)
{
  MCommandResult cmd_result;
  if (MGlobal::executeCommand(cmd, cmd_result) != MStatus::kSuccess)
  {
    // Don't need to post error message as the command engin will do it
    // automatically.
    //
    return MString("");
  }

  MString result;

  if (cmd_result.resultType() == MCommandResult::kInt)
  {
    int res;
    cmd_result.getResult(res);
    result += res;
  }
  else if (cmd_result.resultType() == MCommandResult::kIntArray)
  {
    MIntArray res;
    cmd_result.getResult(res);

    result += "[";
    for( unsigned i = 0; i < res.length(); ++i )
    {
      if( i != 0 )
        result += ", ";

      result += res[i];
    }
    result += "]";
  }
  else if (cmd_result.resultType() == MCommandResult::kDouble)
  {
    double res;
    cmd_result.getResult(res);
    result += res;
  }
  else if (cmd_result.resultType() == MCommandResult::kDoubleArray)
  {
    MDoubleArray res;
    cmd_result.getResult(res);

    result += "[";
    for( unsigned i = 0; i < res.length(); ++i )
    {
      if( i != 0 )
        result += ", ";

      result += res[i];
    }
    result += "]";
  }
  else if (cmd_result.resultType() == MCommandResult::kString)
  {
    MString res;
    cmd_result.getResult(res);
    result = res;

  }
  else if (cmd_result.resultType() == MCommandResult::kStringArray)
  {
    MStringArray res;
    cmd_result.getResult(res);

    result += "[";
    for( unsigned i = 0; i < res.length(); ++i )
    {
      if( i != 0 )
        result += ", ";

      result += res[i];
    }
    result += "]";
  }
  else if (cmd_result.resultType() == MCommandResult::kVector)
  {
    MVector res;

    result += "[";
    result += res[0];
    result += ", ";
    result += res[1];
    result += ", ";
    result += res[2];
    result += "]";
  }
  else if (cmd_result.resultType() == MCommandResult::kVectorArray)
  {
    MVectorArray res;
    cmd_result.getResult(res);

    result += "[";
    for( unsigned i = 0; i < res.length(); ++i )
    {
      // NOTE: Maya adds a newline here but I don't think it's good for us.
      if( i != 0 )
        result += ", ";

      result += i;
      result += ": [";
      result += res[i][0];
      result += ", ";
      result += res[i][1];
      result += ", ";
      result += res[i][2];
      result += "]";
    }
    result += "]";
  }

  return result;
}

static MString
getFilenameForShape(MString i_node_full_path, MString i_workspacePathCmd)
{
  MString render_pass = DL_renderState::getRenderState().getRenderNode();
  MString filename;
  MString cmd = "DL_hashLongFilename(DL_expandString(\"<shape_name>.#\", \""
    + i_node_full_path + "\", \""
    + render_pass + "\"))";
  MGlobal::executeCommand(cmd, filename);

  cmd = "DL_createDir(DL_getExpandedAbsoluteOutputPath(";
  cmd += MString("\"\", \"\", \"");
  cmd += render_pass + MString("\",");
  cmd += i_workspacePathCmd + MString("))");

  MString file_path;
  MGlobal::executeCommand(cmd, file_path);

  return file_path + "/" + filename;
}
MString
getFluidFilename(MString i_node_full_path)
{
  return getFilenameForShape(i_node_full_path, MString("DL_getFluidsPath()"));
}

MString
getFurFilename(MString i_node_full_path)
{
  return getFilenameForShape(i_node_full_path, MString("DL_getFurFilesPath()"));
}

/* Portable sleep with milisecond precision. */
void
SleepMilliseconds( unsigned i_ms )
{
#ifdef _WIN32
  Sleep( i_ms );
#else
  struct timespec sleep_time;
  sleep_time.tv_sec = i_ms / 1000;
  sleep_time.tv_nsec = (i_ms % 1000) * 1000000;
  nanosleep( &sleep_time, NULL );
#endif
}

double TimeUnitToFPS(MTime::Unit i_unit)
{
  switch (i_unit)
  {
    case MTime::kHours:
      return 1.0/3600.0;
    case MTime::kMinutes:
      return 1.0/60.0;
    case MTime::kSeconds:
      return 1.0;
    case MTime::kMilliseconds:
      return 1000.0;
    case MTime::kGames:
      return 15.0;
    case MTime::kFilm:
      return 24.0;
    case MTime::kPALFrame:
      return 25.0;
    case MTime::kNTSCFrame:
      return 30.0;
    case MTime::kShowScan:
      return 48.0;
    case MTime::kPALField:
      return 50.0;
    case MTime::kNTSCField:
      return 60.0;
    case MTime::k2FPS:
      return 2.0;
    case MTime::k3FPS:
      return 3.0;
    case MTime::k4FPS:
      return 4.0;
    case MTime::k5FPS:
      return 5.0;
    case MTime::k6FPS:
      return 6.0;
    case MTime::k8FPS:
      return 8.0;
    case MTime::k10FPS:
      return 10.0;
    case MTime::k12FPS:
      return 12.0;
    case MTime::k16FPS:
      return 16.0;
    case MTime::k20FPS:
      return 20.0;
    case MTime::k40FPS:
      return 40.0;
    case MTime::k75FPS:
      return 75.0;
    case MTime::k80FPS:
      return 80.0;
    case MTime::k100FPS:
      return 100.0;
    case MTime::k120FPS:
      return 120.0;
    case MTime::k125FPS:
      return 125.0;
    case MTime::k150FPS:
      return 150.0;
    case MTime::k200FPS:
      return 200.0;
    case MTime::k240FPS:
      return 240.0;
    case MTime::k250FPS:
      return 250.0;
    case MTime::k300FPS:
      return 300.0;
    case MTime::k375FPS:
      return 375.0;
    case MTime::k400FPS:
      return 400.0;
    case MTime::k500FPS:
      return 500.0;
    case MTime::k600FPS:
      return 600.0;
    case MTime::k750FPS:
      return 750.0;
    case MTime::k1200FPS:
      return 1200.0;
    case MTime::k1500FPS:
      return 1500.0;
    case MTime::k2000FPS:
      return 2000.0;
    case MTime::k3000FPS:
      return 3000.0;
    case MTime::k6000FPS:
      return 6000.0;
    default:
      return 24.0;
  };
}

/* Checks if the particle system connected to the instancer is cached. */
bool IsPartsysCached(const MObject& i_instancer_node)
{
  MStatus stat;

  MFnDependencyNode dep_node;
  dep_node.setObject(i_instancer_node);

  /* Get the particle system connected to the instancer node. */
  MPlug inputPoints_plug = dep_node.findPlug("inputPoints", true, &stat);
  if (stat == MS::kSuccess)
  {
    MPlugArray connected_plugs;
    inputPoints_plug.connectedTo(connected_plugs, true, false, &stat);
    if (stat == MS::kSuccess)
    {
      if (connected_plugs.length() > 0)
      {
        MFnDependencyNode particle_fn( connected_plugs[0].node() );

        /* Get the cache */
        MObject cache;
        if (getSourceNode(cache, particle_fn, "cacheArrayData"))
        {
          MFnDependencyNode dep_cache;
          dep_cache.setObject(cache);

          /* Check the "enable" attribute of the cache. */
          return getAttrBool(cache, "enable", false);
        }
      }
    }
  }

  return false;
}

MString getDelightPath()
{
  MString delightPath( DlGetInstallRoot() );
  return getFixedPath(delightPath);
}

static MString s_delight_maya_path;

const MString& getDelightMayaPath()
{
  return s_delight_maya_path;
}

void setDelightMayaPath(const MString &i_path)
{
  s_delight_maya_path = getFixedPath(i_path);
}

MString getFixedPath(MString i_path)
{
#ifdef _WIN32
  i_path.substitute(MString("\\"), MString("/"));
#endif

  return i_path;
}
