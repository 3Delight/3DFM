#ifndef __NSIExportPriorities_h
#define __NSIExportPriorities_h

/**
	Light linking priorities
*/
/** Defines the priority set by the lights themsleves */
#define NSI_LIGHT_PRIORITY 1
/** Priority set by defaultLightSet. It is used when linking to that set too */
#define NSI_LIGHTSET_PRIORITY 2
/** Priority when linking a shding group to a light. */
#define NSI_OBJECTSET_PRIORTY 3
/**
	Priority set by a direct link to a light shape. This overrides any other
	visibility setting
*/
#define NSI_LIGHTLINK_PRIORITY 4


/**
	Matte priorities
*/
/** Priority for the matte attribute defined on the object. */
#define NSI_MATTE_OBJ_PRIORITY 0
/** Priority for the matte attribute defined through a dlSet. */
#define NSI_MATTE_SET_PRIORITY 1
/** Priority for the "matte objects" set selected on render settings. */
#define NSI_MATTE_RENDERSETTINGS_PRIORITY 3


/**
	Prelit priorities
*/
/** Priority for the prelit attribute defined on an object */
#define NSI_PRELIT_OBJ_PRIORITY NSI_MATTE_OBJ_PRIORITY
/** Priority for the prelit attribute defined through a dlSet. */
#define NSI_PRELIT_SET_PRIORITY NSI_MATTE_SET_PRIORITY


/**
	Visibility priorities.
*/
/** Priority for visibility defined on an object. */
#define NSI_VISIBILITY_OBJ_PRIORITY 0
/** Priority for visibility defined through a dlSet. */
#define NSI_VISIBILITY_SET_PRIORITY 2


/** Priority for all attributes set through spatial overrides. */
#define NSI_SPATIAL_OVERRIDE_PRIORITY 10

#endif
