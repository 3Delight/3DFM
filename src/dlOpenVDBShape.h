/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#ifndef __dlOpenVDBShape_h__
#define __dlOpenVDBShape_h__

#include <maya/MEventMessage.h>
#include <maya/MPxGeometryOverride.h>
#include <maya/MPxSurfaceShape.h>

#include "DL_fileSequence.h"
#include <vector>

/*
 * Represents Maya objects to read and render OpenVDB files
 */

class dlOpenVDBShapeGeo;

class dlOpenVDBShape : public MPxSurfaceShape
{
public:
	dlOpenVDBShape();
	virtual ~dlOpenVDBShape();

	virtual void postConstructor();
	virtual MStatus compute(const MPlug& i_plug, MDataBlock& i_data);
	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const;

	virtual MStatus setDependentsDirty(const MPlug& plug, MPlugArray& affected);
	virtual MSelectionMask getShapeSelectionMask() const;
	const dlOpenVDBShapeGeo* getCurrentGeo() const;

	static void* creator();
	static MStatus initialize();

	static MString drawDbClassification;
	static MString drawRegistrantId;

	void updateGeo() const;

private:

	// Input attributes
	static MObject m_time;

	static DL_fileSequence m_fileSequence;
	static MObject m_filename;

	static MObject m_smokeGrid;
	static MObject m_smokeColorGrid;
	static MObject m_temperatureGrid;
	static MObject m_emissionIntenistyGrid;
	static MObject m_emissionTintGrid;
	static MObject m_velocityGrid;
	static MObject m_velocityScale;

	static MObject m_drawPoints;

	static MObject m_visibilityDiffuse;

	// Output attributes
	static MObject m_bboxMin;
	static MObject m_bboxMax;
	static MObject m_outFilename;

	/* Pointer because it's necessary to change the content in const methods.
	 * For example for automatic reload the point array. */
	dlOpenVDBShapeGeo *m_shape_geo;
};

// Geometry Override
//

class dlOpenVDBShapeGeometryOverride : public MHWRender::MPxGeometryOverride
{
public:
	static MHWRender::MPxGeometryOverride* Creator(const MObject& obj);

	virtual ~dlOpenVDBShapeGeometryOverride();

	// Returns the draw API supported by this override.
	virtual MHWRender::DrawAPI supportedDrawAPIs() const;

	virtual void updateDG();
	virtual bool isIndexingDirty(const MHWRender::MRenderItem& item);
	virtual bool isStreamDirty(const MHWRender::MVertexBufferDescriptor& desc);
	virtual void updateRenderItems(const MDagPath& path, MHWRender::MRenderItemList& list);
	virtual void populateGeometry(
		const MHWRender::MGeometryRequirements& requirements,
		const MHWRender::MRenderItemList& renderItems,
		MHWRender::MGeometry& data);
	virtual void cleanUp();

private:
	dlOpenVDBShapeGeometryOverride(const MObject& obj);

	static void timeChangedCB(void* data);

	dlOpenVDBShape* m_shape;

	MCallbackId m_cb_id;

	bool m_bbox_dirty;
	bool m_points_dirty;

	double m_minX, m_minY, m_minZ;
	double m_maxX, m_maxY, m_maxZ;
	double m_scale;

	bool m_display_points;

	MString m_current_file;
	MString m_current_grid;
};

#endif
