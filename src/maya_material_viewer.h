/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#ifndef __maya_material_viewer_h
#define __maya_material_viewer_h

// Register the display driver with 3Delight
void RegisterMayaMaterialViewer();

#endif
