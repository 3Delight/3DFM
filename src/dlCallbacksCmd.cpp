#include "dlCallbacksCmd.h"

#include <maya/MSyntax.h>
#include <maya/MArgParser.h>

#include <list> 

#include "DL_errors.h"

/*
	Basic data structure to hold callback data.

	Consider using more efficient data structure in the eventuality we support
	many hooks or expect many callback registration.
*/
struct Callback
{
	MString m_hook;
	MString m_owner;
	MString m_command;
};

static std::list< Callback > sg_callbackRegistry;

MSyntax dlCallbacksCmd::newSyntax()
{
	MSyntax syntax;

	// The syntax is derived from the Maya 'callbacks' command.
	syntax.addFlag( "-ac", "-addCallback", MSyntax::kString );
	syntax.addFlag( "-cc", "-clearCallbacks" );
	syntax.addFlag( "-ec", "-executeCallbacks" );
	syntax.addFlag( "-esa", "-executeStringArg", MSyntax::kString );
	syntax.addFlag( "-h", "-hook", MSyntax::kString );
	syntax.addFlag( "-o", "-owner", MSyntax::kString );
	syntax.addFlag( "-rc", "-removeCallback", MSyntax::kString );

	syntax.enableQuery( false );
	syntax.enableEdit( false );

	return syntax;
}

MStatus dlCallbacksCmd::doIt( const MArgList& i_argList )
{
	MStatus status;
	MArgParser argParser( syntax(), i_argList, &status );

	if( status != MStatus::kSuccess )
	{
		return status;
	}

	// Validate that a valid flag combination was provided
	bool isAddCallbackSet = argParser.isFlagSet( "-addCallback" );
	bool isClearCallbacksSet = argParser.isFlagSet( "-clearCallbacks" );
	bool isExecuteCallbacksSet = argParser.isFlagSet( "-executeCallbacks" );
	bool isHookSet = argParser.isFlagSet( "-hook" );
	bool isOwnerSet = argParser.isFlagSet( "-owner" );
	bool isRemoveCallbackSet = argParser.isFlagSet( "-removeCallback" );

	if( (int)isAddCallbackSet + (int)isClearCallbacksSet + 
		(int)isExecuteCallbacksSet + (int)isRemoveCallbackSet != 1 )
	{
		MString error = _3DFM_ERR_PREFIX( DLCALLBACKSCMD_STR ) +
			MString( "Must specify precisely one of -addCallback, " ) +
			MString( "-clearCallbacks, -executeCallbacks or " ) +
			MString( "-removeCallback.");

		displayError( error );
		return MStatus::kFailure;
	}

	MString hook;
	if( isHookSet )
	{
		argParser.getFlagArgument( "-hook", 0, hook );
	}

	MString owner;
	if( isOwnerSet )
	{
		argParser.getFlagArgument( "-owner", 0, owner );
	}

	if( isAddCallbackSet )
	{
		if( hook.length() == 0 || owner.length() == 0 )
		{
			MString error = _3DFM_ERR_PREFIX( DLCALLBACKSCMD_STR ) +
				MString( "-addCallback requires proper values for -hook and -owner ") +
				MString( "flags." );

			displayError( error );
			return MStatus::kFailure;			
		}

		MString command;
		argParser.getFlagArgument( "-addCallback", 0, command );

		Callback cb;
		cb.m_hook = hook;
		cb.m_owner = owner;
		cb.m_command = command;

		sg_callbackRegistry.push_back( cb );
	}
	else if( isClearCallbacksSet )
	{
		if( owner.length() == 0 )
		{
			MString error = _3DFM_ERR_PREFIX( DLCALLBACKSCMD_STR ) +
				MString( "-clearCallbacks requires proper value for -owner flag.");

			displayError( error );
			return MStatus::kFailure;
		}

		// remove all entries of owner in all hook tables
		std::list< Callback >::iterator	it( sg_callbackRegistry.begin() );
		while( it != sg_callbackRegistry.end() )
		{
			if( it->m_owner == owner )
			{
				it = sg_callbackRegistry.erase( it );
			}
			else
			{
				++it;
			}
		}
	}
	else if( isExecuteCallbacksSet )
	{
		if( hook.length() == 0 )
		{
			MString error = _3DFM_ERR_PREFIX( DLCALLBACKSCMD_STR ) +
				MString( "-executeCallbacks requires proper value for -hook flag.");

			displayError( error );
			return MStatus::kFailure;			
		}

		MString stringArg;
		if( argParser.isFlagSet( "-executeStringArg" ) )
		{
			argParser.getFlagArgument( "-executeStringArg", 0, stringArg );
		}

		// execute command for any onwer for hook, adding stringArg.
		std::list< Callback >::iterator	it( sg_callbackRegistry.begin() );
		for( ; it != sg_callbackRegistry.end(); ++it )
		{
			if( it->m_hook == hook )
			{
				MString cmd = it->m_command;
				if( stringArg.length() > 0 )
				{
					cmd += MString(" ") + stringArg;
				}

				MGlobal::executeCommand( cmd );
			}
		}
	}
	else if( isRemoveCallbackSet )
	{
		if( hook.length() == 0 || owner.length() == 0 )
		{
			MString error = _3DFM_ERR_PREFIX( DLCALLBACKSCMD_STR ) +
				MString( "-removeCallback requires proper values for -hook and " ) + 
				MString( "-owner flags." );

			displayError( error );
			return MStatus::kFailure;			
		}

		MString command;
		argParser.getFlagArgument( "-removeCallback", 0, command );

		// remove callbacks that match hook, owner and command.
		std::list< Callback >::iterator	it( sg_callbackRegistry.begin() );
		while( it != sg_callbackRegistry.end() )
		{
			if( it->m_owner == owner && 
				it->m_hook == hook && 
				it->m_command == command )
			{
				it = sg_callbackRegistry.erase( it );
			}
			else
			{
				++it;
			}
		}
	}

	return MStatus::kSuccess;
}
