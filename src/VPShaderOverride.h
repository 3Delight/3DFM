/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#ifndef __VPShaderOverride__
#define __VPShaderOverride__

#include <maya/MPxShadingNodeOverride.h>

class VPShaderOverride : public MHWRender::MPxShadingNodeOverride
{
public:
	static MHWRender::MPxShadingNodeOverride* creator(
		const MObject& i_obj );

	virtual ~VPShaderOverride();

	virtual MHWRender::DrawAPI supportedDrawAPIs() const;
	virtual MString fragmentName() const;

	private:
	VPShaderOverride( const MObject& i_obj );

	MString m_fragmentName;
};

#endif
