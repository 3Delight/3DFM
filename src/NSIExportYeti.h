#ifndef __NSIExportYeti_h
#define __NSIExportYeti_h

#include "NSIExportDelegate.h"

/**
	\brief Export delegate for Yeti fur.

	This calls the MEL command provided by Yeti for our old RenderMan export
	system : pgYetiRenderCommand.

	It appears to support our API fairly well. For example:

	pgYetiRenderCommand -a -sampleTime 1 "pgYetiMaya1|pgYetiMaya1Shape";
	pgYetiRenderCommand -a -sampleTime 1.2 "pgYetiMaya1|pgYetiMaya1Shape";
	pgYetiRenderCommand -a -sampleTime 1.4 "pgYetiMaya1|pgYetiMaya1Shape";
	pgYetiRenderCommand -list;
	pgYetiRenderCommand -emit "|pgYetiMaya1|pgYetiMaya1Shape";
	pgYetiRenderCommand -remove "|pgYetiMaya1|pgYetiMaya1Shape";

	The emit will just export a RiProcedural with parameters for the required
	sample times. We use a RifFilter to grab that and write an NSI command
	instead.
*/
class NSIExportYeti : public NSIExportDelegate
{
public:
	static const char* MayaDAGNodeName() { return "pgYetiMaya"; }

	NSIExportYeti(
		MFnDagNode &i_dagNode,
		const NSIExportDelegate::Context& i_context );

	virtual void Create() override;

	virtual void SetAttributes() override;

	virtual void SetAttributesAtTime(
		double i_time,
		bool i_no_motion ) override;

	virtual bool IsDeformed() const override;

	virtual bool RegisterCallbacks() override;

	virtual void Finalize() override;

private:
	MString m_dagPath;
};

#endif
