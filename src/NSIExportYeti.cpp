#include "NSIExportYeti.h"

#include <maya/MAnimControl.h>
#include <maya/MGlobal.h>

#include "nsi.hpp"

NSIExportYeti::NSIExportYeti(
	MFnDagNode &i_dagNode,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( i_dagNode, i_context ),
	m_dagPath( i_dagNode.fullPathName() )
{
}


void NSIExportYeti::Create()
{
	/* Just build a transform node that the procedural will connect to. */
	NSICreate( m_nsi, m_nsi_handle, "transform", 0, 0x0 );
}


void NSIExportYeti::SetAttributes()
{
}


void NSIExportYeti::SetAttributesAtTime(
	double i_time,
	bool i_no_motion )
{
	/*
		Yeti requires global time values for motion blur to work. It will end
		up producing motion samples relative to the (rounded) frame however.
	*/
	double global_time = MAnimControl::currentTime().as(MTime::uiUnit());
	double frame = global_time - i_time;

	MString command = "pgYetiRenderCommand -a -sampleTime ";
	command += global_time;
	command += " -frame ";
	command += frame;
	command += " ";
	command += m_dagPath;
	MGlobal::executeCommand( command );
}


/**
	Always consider Yeti node to be deformed. Otherwise, fur will not track a
	moving object.
*/
bool NSIExportYeti::IsDeformed() const
{
	return true;
}

/**
	Do not attempt any kind of IPR on Yeti nodes.
*/
bool NSIExportYeti::RegisterCallbacks()
{
	return true;
}

void NSIExportYeti::Finalize()
{
	MString command = "pgYetiRenderCommand -prm -emit ";
	command += m_dagPath;
	MString result = MGlobal::executeCommandStringResult( command );

	NSI::Context nsi( m_nsi );

	nsi.Evaluate( (
		NSI::StringArg( "type", "RiProcDynamicLoad" ),
		NSI::StringArg( "filename", "pgYetiDLRender" ),
		NSI::StringArg( "parameters", result.asChar() ),
		NSI::StringArg( "transform", m_nsi_handle )
	) );

	/* Flush item from the command's cache. */
	command = "pgYetiRenderCommand -remove ";
	command += m_dagPath;
	MGlobal::executeCommand( command );
}
