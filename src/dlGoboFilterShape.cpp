/*
  Copyright (c) 2017 The 3Delight Team.
*/

#include "dlGoboFilterShape.h"
#include "DL_autoLoadOSL.h"
#include "DL_errors.h"
#include "DL_utils.h"

#include "MU_typeIds.h"

#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MRampAttribute.h>

MTypeId dlGoboFilterShape::id( MU_typeIds::DL_GOBOFILTER );

MObject dlGoboFilterShape::s_map;
MObject dlGoboFilterShape::s_density;
MObject dlGoboFilterShape::s_invert;
MObject dlGoboFilterShape::s_useFilterCoordinateSystem;
MObject dlGoboFilterShape::s_scale;
MObject dlGoboFilterShape::s_scaleS;
MObject dlGoboFilterShape::s_scaleT;
MObject dlGoboFilterShape::s_offset;
MObject dlGoboFilterShape::s_offsetS;
MObject dlGoboFilterShape::s_offsetT;
MObject dlGoboFilterShape::s_sWrapMode;
MObject dlGoboFilterShape::s_tWrapMode;
MObject dlGoboFilterShape::s_filter_output;

dlGoboFilterShape::dlGoboFilterShape()
{
}

dlGoboFilterShape::~dlGoboFilterShape()
{
}

void dlGoboFilterShape::postConstructor()
{
  setExistWithoutInConnections(true);
  setExistWithoutOutConnections(true);
}

MStatus dlGoboFilterShape::compute(
  const MPlug& /*plug*/,
  MDataBlock& /*datablock*/ )
{
  return MS::kUnknownParameter;
}

void* dlGoboFilterShape::creator()
{
  return new dlGoboFilterShape();
}

MStatus dlGoboFilterShape::initialize()
{
  MFnCompoundAttribute compAttr;
  MFnEnumAttribute enumAttr;
  MFnNumericAttribute numAttr;
  MFnTypedAttribute typedAttr;

  addAttribute(DL_OSLShadingNode::CreateShaderFilenameAttribute(
    "dlGoboFilter"));

  s_map = typedAttr.create("textureName", "tn", MFnData::kString);
  typedAttr.setNiceNameOverride("Map");
  Utilities::makeInputAttribute(typedAttr, true);
  addAttribute(s_map);

  MObject texturePattern = numAttr.createColor(
    "texturePattern",
    "texturePattern");
  numAttr.setDefault(1.0, 1.0, 1.0);
  Utilities::setNiceNameColor(numAttr, "Texture");
  Utilities::makeShaderInputAttribute(numAttr);
  addAttribute(texturePattern);

  s_density = numAttr.create("density", "d", MFnNumericData::kFloat, 1.0f);
  numAttr.setMin( 0.0f );
  numAttr.setSoftMax( 1.0f );
  Utilities::makeInputAttribute(numAttr, true);
  addAttribute(s_density);

  s_invert = numAttr.create("invert", "i", MFnNumericData::kBoolean, 0);
  Utilities::makeInputAttribute(numAttr, true);
  addAttribute(s_invert);

  s_useFilterCoordinateSystem = numAttr.create("useFilterCoordinateSystem", "ufc", MFnNumericData::kBoolean, 1);
  Utilities::makeInputAttribute(numAttr, true);
  addAttribute(s_useFilterCoordinateSystem);

  s_scaleS = numAttr.create("scaleS", "ss", MFnNumericData::kFloat, 1.0f);
  numAttr.setNiceNameOverride("Scale S");
  numAttr.setMin( 0.0f );
  Utilities::makeInputAttribute(numAttr, true);
  addAttribute(s_scaleS);
  s_scaleT = numAttr.create("scaleT", "st", MFnNumericData::kFloat, 1.0f);
  numAttr.setNiceNameOverride("Scale T");
  numAttr.setMin( 0.0f );
  Utilities::makeInputAttribute(numAttr, true);
  addAttribute(s_scaleT);
  s_scale = numAttr.create("scale", "s", s_scaleS, s_scaleT);
  Utilities::makeInputAttribute(numAttr, true);
  addAttribute(s_scale);

  s_offsetS = numAttr.create("offsetS", "os", MFnNumericData::kFloat, 0.0f);
  numAttr.setNiceNameOverride("Offset S");
  Utilities::makeInputAttribute(numAttr, true);
  addAttribute(s_offsetS);
  s_offsetT = numAttr.create("offsetT", "ot", MFnNumericData::kFloat, 0.0f);
  numAttr.setNiceNameOverride("Offset T");
  Utilities::makeInputAttribute(numAttr, true);
  addAttribute(s_offsetT);
  s_offset = numAttr.create("offset", "o", s_offsetS, s_offsetT);
  Utilities::makeInputAttribute(numAttr, true);
  addAttribute(s_offset);

  MFnStringData stringFn;
  MObject defaultText = stringFn.create("clamp");
  s_sWrapMode = typedAttr.create("swrap", "swm", MFnData::kString, defaultText);
  Utilities::makeInputAttribute(typedAttr, true);
  addAttribute(s_sWrapMode);

  s_tWrapMode = typedAttr.create("twrap", "twm", MFnData::kString, defaultText);
  Utilities::makeInputAttribute(typedAttr, true);
  addAttribute(s_tWrapMode);

  s_filter_output = numAttr.createColor( "filter_output", "fo" );
  Utilities::makeOutputAttribute(numAttr);
  addAttribute(s_filter_output);

  attributeAffects( s_map, s_filter_output );
  attributeAffects( s_density, s_filter_output );
  attributeAffects( s_invert, s_filter_output );
  attributeAffects( s_useFilterCoordinateSystem, s_filter_output );
  attributeAffects( s_scaleS, s_filter_output );
  attributeAffects( s_scaleT, s_filter_output );
  attributeAffects( s_scale, s_filter_output );
  attributeAffects( s_offsetS, s_filter_output );
  attributeAffects( s_offsetT, s_filter_output );
  attributeAffects( s_offset, s_filter_output );
  attributeAffects( s_sWrapMode, s_filter_output );
  attributeAffects( s_tWrapMode, s_filter_output );

  return MS::kSuccess;
}
