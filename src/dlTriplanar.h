#ifndef __dlTriplanar_H__
#define __dlTriplanar_H__

#include <maya/MPxNode.h>

class dlTriplanar : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
  virtual MStatus compute( const MPlug& i_plug, MDataBlock& i_dataBlock);

private:
	static MObject s_colorTexture;
	static MObject s_floatTexture;
	static MObject s_heightTexture;
	static MObject s_outColor;
	static MObject s_outAlpha;
	static MObject s_outFloat;
	static MObject s_outHeight;
};

#endif
