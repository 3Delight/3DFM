#ifndef __DependencyNodeSet_h
#define __DependencyNodeSet_h

#include <maya/MObject.h>

#include <string>
#include <unordered_set>
#include <vector>

/**
	A set of dependency nodes which does not exist in the scene and has
	efficient lookups.
*/
class DependencyNodeSet
{
public:
	static std::string NodeID(const MObject &i_node);

	DependencyNodeSet();
	~DependencyNodeSet();

	void Clear();
	bool Contains(const MObject &i_node) const;
	void AddNode(const MObject &i_node);

	void AddSetDagNodes(
		const MObject &i_set,
		const std::vector<int> &i_types,
		bool i_with_parents);

private:
	std::unordered_set<std::string> m_nodes;
};

#endif
