/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#include "DL_extensionAttrRegistry.h"

#include <maya/MDGModifier.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MNodeClass.h>

#include <assert.h>

#include "DL_errors.h"
#include "DL_utils.h"

using namespace Utilities;

DL_extensionAttrRegistry* DL_extensionAttrRegistry::m_instance = NULL;

DL_extensionAttrRegistry&
DL_extensionAttrRegistry::getInstance()
{
	if(m_instance == NULL)
	{
		m_instance = new DL_extensionAttrRegistry();
	}

	return *m_instance;
}

void
DL_extensionAttrRegistry::deleteInstance()
{
	delete m_instance;
	m_instance = NULL;
}

DL_extensionAttrRegistry::DL_extensionAttrRegistry()
{
}

DL_extensionAttrRegistry::~DL_extensionAttrRegistry()
{
	unregisterAll();
}

void
DL_extensionAttrRegistry::setPlugin(const MObject& i_plugin)
{
	if(i_plugin.hasFn(MFn::kPlugin))
	{
		m_plugin = i_plugin;
	}
}

MStatus
DL_extensionAttrRegistry::registerAll()
{
	MStatus status =
		addDisplacementShaderAttrGroup(MString("displacementShader"));
	RETURN_ON_FAILED_MSTATUS(status);

	status = addBump2dAttrGroup(MString("bump2d"));
	RETURN_ON_FAILED_MSTATUS(status);

	status = addMeshAttrGroup(MString("mesh"));
	RETURN_ON_FAILED_MSTATUS(status);

	status = addAttributeOverrideVolumeAttrGroup("mesh");
	RETURN_ON_FAILED_MSTATUS(status);

	status = addCameraAttrGroup(MString("camera"));
	RETURN_ON_FAILED_MSTATUS(status);

	// Light types on which light extension attributes are added.
	//
	MStringArray types;
	types.append("ambientLight");
	types.append("areaLight");
	types.append("directionalLight");
	types.append("pointLight");
	types.append("spotLight");
	// For volumeLight, see below

	for(unsigned int i = 0; i < types.length(); i++)
	{
		status = addLightAttrGroup(types[i]);
		RETURN_ON_FAILED_MSTATUS(status);
	}

	//
	// volumeLight derives from pointLight. Adding extension attributes
	// to pointLight nodes will also add them to volumeLight nodes; trying to
	// re-add them to volumeLight will cause Maya to hang / crash.
	//
	// Add an empty light extension attribute group for a volumeLight type id.
	// hasAttributeGroup() will return true, and unregisterAll will not attempt
	// to remove extension attributes that never were added on this type.
	//
	MNodeClass volumeLightClass("volumeLight");
	HandleList* dummyHandleList;
	status = addAttrGroupForNodeType(
		volumeLightClass, getLightGroupName(), dummyHandleList);

	status = addAreaLightAttrGroup(MString("areaLight"));
	RETURN_ON_FAILED_MSTATUS(status);

	status = addDirectionalAttributesGroup("directionalLight");
	RETURN_ON_FAILED_MSTATUS(status);

	status = addSpotAndPointLightsAttributesGroup("spotLight");
	RETURN_ON_FAILED_MSTATUS(status);

	status = addSpotAndPointLightsAttributesGroup("pointLight");
	RETURN_ON_FAILED_MSTATUS(status);

	// Types that need the geometric attributes.
	// This should consist of all renderable geometric types.
	//
	types.clear();
	types.append("mesh");
	types.append("nurbsSurface");
	types.append("pfxHair");
	types.append("stroke");
	types.append("particle");
	// ? instancer ?
	for(unsigned int i = 0; i < types.length(); i++)
	{
		status = addVisibilityAttrGroup(types[i]);
		RETURN_ON_FAILED_MSTATUS(status);

		status = addMotionBlurGroup(types[i]);
		RETURN_ON_FAILED_MSTATUS(status);
	}

	status = addCurvesGroup("pfxHair");
	RETURN_ON_FAILED_MSTATUS(status);

	status = addImagePlaneGroup("imagePlane");
	RETURN_ON_FAILED_MSTATUS(status);

	status = addShadingEngineGroup("shadingEngine");
	RETURN_ON_FAILED_MSTATUS(status);

	status = addMotionBlurGroup("transform");
	RETURN_ON_FAILED_MSTATUS(status);

	/*
		FIXME: uncomment when supported in 3delight
	status = addMotionBlurGroup("camera");
	RETURN_ON_FAILED_MSTATUS(status);
	*/

	return status;
}

MStatus
DL_extensionAttrRegistry::unregisterAll()
{
	MDGModifier mod;
	MStatus status;

	AttrRegistry::iterator regItr;
	for(regItr = m_registry.begin(); regItr != m_registry.end(); regItr++)
	{
		MTypeId currTypeId((*regItr).first);
		MNodeClass nodeClass(currTypeId);

		AttrGroupList& groupList((*regItr).second);
		for(int i = 0; i < groupList.size(); i++)
		{
			HandleList& attrList(groupList[i].second);
			for(int j = 0; j < attrList.size(); j++)
			{
				status = removeExtensionAttribute(attrList[j].object(), mod, nodeClass);
				RETURN_ON_FAILED_MSTATUS(status);
			}
		}
	}

	m_registry.clear();

	return status;
}

MStringArray
DL_extensionAttrRegistry::getAttributeGroupNames()
{
	MStringArray nameList;
	nameList.append(getDisplacementShaderGroupName());
	nameList.append(getBump2dGroupName());
	nameList.append(getMeshGroupName());
	nameList.append(getCameraGroupName());
	nameList.append(getLightGroupName());
	nameList.append(getAreaLightGroupName());
	nameList.append(getDirectionalLightGroupName());
	nameList.append(getVisibilityGroupName());
	nameList.append(getMotionBlurGroupName());
	nameList.append(getCurvesGroupName());
	nameList.append(getImagePlaneGroupName());
	nameList.append(getShadingEngineGroupName());

	return nameList;
}

MStringArray
DL_extensionAttrRegistry::getAttributeGroupNames(const MString& i_nodeType)
{
	MStringArray groups;

	MNodeClass nodeClass(i_nodeType);
	unsigned int typeId = nodeClass.typeId().id();

	AttrRegistry::iterator regItr(m_registry.find(typeId));
	if(regItr == m_registry.end())
	{
		return groups;
	}

	AttrGroupList& groupList = (*regItr).second;
	for(int i = 0; i < groupList.size(); i++)
	{
		groups.append(groupList[i].first);
	}

	return groups;
}

MStatus
DL_extensionAttrRegistry::addAttributeGroup(
	const MString& i_nodeType,
	const MString& i_groupName)
{
	if(i_groupName == getDisplacementShaderGroupName())
	{
		return addDisplacementShaderAttrGroup(i_nodeType);
	}
	else if(i_groupName == getBump2dGroupName())
	{
		return addBump2dAttrGroup(i_nodeType);
	}
	else if(i_groupName == getMeshGroupName())
	{
		return addMeshAttrGroup(i_nodeType);
	}
	else if(i_groupName == getCameraGroupName())
	{
		return addCameraAttrGroup(i_nodeType);
	}
	else if(i_groupName == getLightGroupName())
	{
		return addLightAttrGroup(i_nodeType);
	}
	else if(i_groupName == getAreaLightGroupName())
	{
		return addAreaLightAttrGroup(i_nodeType);
	}
	else if(i_groupName == getDirectionalLightGroupName() )
	{
		return addDirectionalAttributesGroup(i_nodeType);
	}
	else if(i_groupName == getVisibilityGroupName())
	{
		return addVisibilityAttrGroup(i_nodeType);
	}
	else if(i_groupName == getMotionBlurGroupName())
	{
		return addMotionBlurGroup(i_nodeType);
	}
	else if(i_groupName == getCurvesGroupName())
	{
		return addCurvesGroup(i_nodeType);
	}
	else if(i_groupName == getImagePlaneGroupName())
	{
		return addImagePlaneGroup(i_nodeType);
	}
	else if(i_groupName == getShadingEngineGroupName())
	{
		return addShadingEngineGroup(i_nodeType);
	}

	return MStatus::kUnknownParameter;
}

MStatus
DL_extensionAttrRegistry::removeAttributeGroup(
	const MString& i_nodeType,
	const MString& i_groupName)
{
	MNodeClass nodeClass(i_nodeType);
	unsigned int typeId = nodeClass.typeId().id();

	if(typeId == 0)
	{
		return MStatus::kInvalidParameter;
	}

	AttrRegistry::iterator regItr(m_registry.find(typeId));
	if(regItr == m_registry.end())
	{
		return MStatus::kFailure;
	}

	AttrGroupList& groupList = (*regItr).second;
	AttrGroupList::iterator listItr;
	for(listItr = groupList.begin(); listItr != groupList.end(); listItr++)
	{
		if((*listItr).first == i_groupName)
		{
			MDGModifier dgMod;
			HandleList& attrHandles((*listItr).second);

			for(int j = 0; j < attrHandles.size(); j++)
			{
				removeExtensionAttribute(attrHandles[j].object(), dgMod, nodeClass);
			}

			groupList.erase(listItr);

			if(groupList.empty())
			{
				m_registry.erase(regItr);
			}

			return MStatus::kSuccess;
		}
	}

	return MStatus::kFailure;
}

MStatus
DL_extensionAttrRegistry::addDisplacementShaderAttrGroup(
	const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status = addAttrGroupForNodeType(
		nodeClass,
		getDisplacementShaderGroupName(),
		attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	// Create the base objects for extension attributes definition
	MDGModifier dgMod;

	// Add the render as bump attribute.
	MString attrName("_3delight_renderAsBump");
	MFnNumericAttribute numAttrFn;
	MObject renderAsBumpAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Render as Bump"));
	makeInputOutputAttribute(numAttrFn);
	numAttrFn.setKeyable(true);

	status =
		addExtensionAttribute(renderAsBumpAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);


	// Add the displacement bound attribute.
	attrName = MString("_3delight_displacementBound");
	MObject displBoundAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kDouble,
		1.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Displacement Bound"));
	numAttrFn.setMin(0.0);
	numAttrFn.setSoftMax(1.0);
	makeInputOutputAttribute(numAttrFn);
	numAttrFn.setKeyable(true);

	status =
		addExtensionAttribute(displBoundAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	// Add the displacement vertex attribute.
	attrName = MString("_3delight_displacementVertex");

	MObject displVertexAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Displace Vertices Only"));
	makeInputOutputAttribute(numAttrFn);
	numAttrFn.setKeyable(true);

	status =
		addExtensionAttribute(displVertexAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	// Add the displacement level attribute.
	attrName = MString("_3delight_displacementLevel");

	MObject displLevelAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kLong,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Subdivision Steps"));
	numAttrFn.setDefault(0);
	numAttrFn.setMin(0);
	numAttrFn.setMax(99);
	numAttrFn.setSoftMax(6);
	makeInputOutputAttribute(numAttrFn);
	numAttrFn.setKeyable(true);

	status =
		addExtensionAttribute(displLevelAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	// Add the UV coordinate attributes

	attrName = MString("_3delight_uCoord");
	MObject uCoordAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kDouble,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);
	numAttrFn.setNiceNameOverride(MString("U Coord"));

	attrName = MString("_3delight_vCoord");
	MObject vCoordAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kDouble,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);
	numAttrFn.setNiceNameOverride(MString("V Coord"));

	attrName = MString("_3delight_uvCoord");
	MObject uvCoordAttr = numAttrFn.create(
		attrName,
		attrName,
		uCoordAttr,
		vCoordAttr,
		MObject::kNullObj,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);
	numAttrFn.setNiceNameOverride(MString("UV Coord"));
	makeShaderInputAttribute(numAttrFn);

	status = addExtensionAttribute(uvCoordAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	return status;
}

MStatus
DL_extensionAttrRegistry::addBump2dAttrGroup(
	const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status = addAttrGroupForNodeType(
		nodeClass,
		getBump2dGroupName(),
		attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	// Create the base objects for extension attributes definition
	MDGModifier dgMod;

	// Add the UV coordinate attributes

	MFnNumericAttribute numAttrFn;

	MString attrName = MString("_3delight_uCoord");
	MObject uCoordAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kDouble,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);
	numAttrFn.setNiceNameOverride(MString("U Coord"));

	attrName = MString("_3delight_vCoord");
	MObject vCoordAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kDouble,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);
	numAttrFn.setNiceNameOverride(MString("V Coord"));

	attrName = MString("_3delight_uvCoord");
	MObject uvCoordAttr = numAttrFn.create(
		attrName,
		attrName,
		uCoordAttr,
		vCoordAttr,
		MObject::kNullObj,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);
	numAttrFn.setNiceNameOverride(MString("UV Coord"));
	makeShaderInputAttribute(numAttrFn);

	status = addExtensionAttribute(uvCoordAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	return status;
}

MStatus
DL_extensionAttrRegistry::addMeshAttrGroup(const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status =
		addAttrGroupForNodeType(nodeClass, getMeshGroupName(), attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	// Create the base objects for extension attributes definition
	MDGModifier dgMod;

	// Add the poly as subd attribute.
	MString attrName("_3delight_poly_as_subd");

	MFnNumericAttribute numAttrFn;
	MObject polyAsSubdAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(
		MString("Render Mesh as a Subdivision Surface"));

	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(polyAsSubdAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	return status;
}

MStatus
DL_extensionAttrRegistry::addCameraAttrGroup(const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status =
		addAttrGroupForNodeType(nodeClass, getCameraGroupName(), attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	// Create the base objects for extension attributes definition
	MDGModifier dgMod;

	// Add the projection type attribute.
	MString attrName("_3delight_projectionType");

	MFnEnumAttribute enumAttrFn;
	MObject projectionTypeAttr = enumAttrFn.create(
		attrName,
		attrName,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	enumAttrFn.addField(MString("Default"), 0);
	enumAttrFn.addField(MString("Cylindrical"), 1);
	enumAttrFn.addField(MString("Fisheye stereographic"), 2);
	enumAttrFn.addField(MString("Fisheye equidistant"), 3);
	enumAttrFn.addField(MString("Fisheye equisolid"), 4);
	enumAttrFn.addField(MString("Fisheye orthographic"), 5);
	enumAttrFn.addField(MString("Spherical"), 6);

	enumAttrFn.setNiceNameOverride(MString("Projection Type"));
	makeInputOutputAttribute(enumAttrFn);

	status =
		addExtensionAttribute(projectionTypeAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	// Add the field of view attribute.
	attrName = MString("_3delight_fov");

	MFnNumericAttribute numAttrFn;
	MObject fieldOfViewAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kFloat,
		180.0f,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Auxiliary Field of View"));
	numAttrFn.setMin(0.0f);
	numAttrFn.setMax(360.0f);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(fieldOfViewAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	/* Add the shutter position */
	{
		attrName = MString("_3delight_shutter_position");
		MFnEnumAttribute enumAttr;
		MObject obj = enumAttr.create( attrName, attrName, 0.0f, &status);
		enumAttr.setNiceNameOverride(MString("Shutter Position"));
		enumAttr.addField(MString("Center on Frame"), 0);
		enumAttr.addField(MString("Start on Frame"), 1);
		enumAttr.addField(MString("End on Frame"), 2);
		addExtensionAttribute(obj, *attrHandles, dgMod, nodeClass);
	}

	/* Add the shutter open efficiency attribute */
	{
		attrName = MString("_3delight_shutter_open_efficiency");
		MFnNumericAttribute numAttrFn;
		MObject obj = numAttrFn.create(
			attrName, attrName, MFnNumericData::kFloat, 1.0f, &status);
		numAttrFn.setNiceNameOverride(MString("Shutter Opening Efficiency"));
		numAttrFn.setMin(0.0f);
		numAttrFn.setMax(1.0f);
		makeInputOutputAttribute(numAttrFn);
		numAttrFn.setHidden(true);
		addExtensionAttribute(obj, *attrHandles, dgMod, nodeClass);
	}

	/* Add shutter close efficiency */
	{
		attrName = MString("_3delight_shutter_close_efficiency");
		MFnNumericAttribute numAttrFn;
		MObject obj = numAttrFn.create(
			attrName, attrName, MFnNumericData::kFloat, 1.0, &status);
		numAttrFn.setNiceNameOverride(MString("Shutter Closing Efficiency"));
		numAttrFn.setMin(0.0f); numAttrFn.setMax(1.0f);
		makeInputOutputAttribute(numAttrFn);
		numAttrFn.setHidden(true);
		addExtensionAttribute(obj, *attrHandles, dgMod, nodeClass);
	}

	/* Add the aperture toggle for finite number of blades */
	{
		attrName = MString("_3delight_use_aperture");
		MObject obj = numAttrFn.create(
			attrName, attrName, MFnNumericData::kBoolean, 0.0);
		numAttrFn.setNiceNameOverride(MString("Use Finite Number of Blades"));
		numAttrFn.setDefault(false);
		makeInputOutputAttribute(numAttrFn);
		addExtensionAttribute(obj, *attrHandles, dgMod, nodeClass);
	}

	/* Number of blades ... */
	{
		MString attrName("_3delight_number_of_blades");
		MFnNumericAttribute numAttrFn;
		MObject obj = numAttrFn.create(
			attrName, attrName, MFnNumericData::kInt, 0);
		numAttrFn.setNiceNameOverride(MString("Number of Blades"));
		numAttrFn.setDefault(5);
		numAttrFn.setMin(3);
		makeInputOutputAttribute(numAttrFn);
		addExtensionAttribute(obj, *attrHandles, dgMod, nodeClass);
	}

	/* Add aperture rotation */
	{
		attrName = MString("_3delight_aperture_rotation");
		MFnNumericAttribute numAttrFn;
		MObject obj = numAttrFn.create(
			attrName, attrName, MFnNumericData::kFloat, 0, &status);
		numAttrFn.setNiceNameOverride(MString("Rotation"));
		numAttrFn.setMin(0.0f); numAttrFn.setMax(360.0f);
		makeInputOutputAttribute(numAttrFn);
		addExtensionAttribute(obj, *attrHandles, dgMod, nodeClass);
	}


	// Add the lens distortion attributes
	{
		// Add Distortion Type
		attrName = MString("_3delight_distortion_type");

		MObject distortionTypeAttr = enumAttrFn.create(
			attrName,
			attrName,
			0.0,
			&status);
		RETURN_ON_FAILED_MSTATUS(status);

		enumAttrFn.addField(MString("None"), 0);
		enumAttrFn.addField(MString("Barrel"), 1);
		enumAttrFn.addField(MString("Pincushion"), 2);

		enumAttrFn.setNiceNameOverride(MString("Distortion Type"));
		makeInputOutputAttribute(enumAttrFn);

		status =
			addExtensionAttribute(distortionTypeAttr, *attrHandles, dgMod, nodeClass);
		RETURN_ON_FAILED_MSTATUS(status);

		// Add Distortion Intensity
		attrName = MString("_3delight_distortion_intensity");

		MObject distortionIntensityAttr = numAttrFn.create(
			attrName,
			attrName,
			MFnNumericData::kFloat,
			0.0f,
			&status);
		RETURN_ON_FAILED_MSTATUS(status);

		numAttrFn.setNiceNameOverride(MString("Distortion Intensity"));
		numAttrFn.setMin(0.0f);
		numAttrFn.setSoftMax(1.0f);
		makeInputOutputAttribute(numAttrFn);

		status =
			addExtensionAttribute(distortionIntensityAttr, *attrHandles, dgMod, nodeClass);
		RETURN_ON_FAILED_MSTATUS(status);
	}

	return status;
}

MStatus
DL_extensionAttrRegistry::addLightAttrGroup(const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status =
		addAttrGroupForNodeType(nodeClass, getLightGroupName(),	attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	// Create the base objects for extension attributes definition
	MDGModifier dgMod;

	const char *contributionNames[][2] =
	{
		{"_3delight_diffuseContribution", "Diffuse"},
		{"_3delight_subsurfaceContribution", "Subsurface"},
		{"_3delight_reflectionContribution", "Reflection"},
		{"_3delight_refractionContribution", "Refraction"},
		{"_3delight_hairContribution", "Hair"},
		{"_3delight_volumeContribution", "Volume"},
	};

	MString attrName;
	MFnNumericAttribute numAttrFn;
	// Add the contribution attributes.
	for( const auto &cname : contributionNames )
	{
		attrName = MString(cname[0]);

		MObject diffuseContrAttr = numAttrFn.create(
			attrName,
			attrName,
			MFnNumericData::kFloat,
			1.0,
			&status);
		RETURN_ON_FAILED_MSTATUS(status);

		numAttrFn.setNiceNameOverride(MString(cname[1]));
		numAttrFn.setDefault(1.0);
		numAttrFn.setMin(0.0);
		numAttrFn.setSoftMax(10.0);
		makeInputOutputAttribute(numAttrFn);

		status =
			addExtensionAttribute(diffuseContrAttr, *attrHandles, dgMod, nodeClass);
		RETURN_ON_FAILED_MSTATUS(status);
	}

	/* emission type */
	if( i_nodeType != "ambientLight" )
	{
		attrName = MString("_3delight_emissionType");
		MFnEnumAttribute enumAttr;
		MObject obj = enumAttr.create(attrName, attrName, 0, &status);
		RETURN_ON_FAILED_MSTATUS(status);
		enumAttr.setNiceNameOverride(MString("Emission Type"));
		enumAttr.addField(MString("All"), 0);
		enumAttr.addField(MString("Toon"), 1);
		enumAttr.addField(MString("Not Toon"), 2);
		status = addExtensionAttribute(obj, *attrHandles, dgMod, nodeClass);
		RETURN_ON_FAILED_MSTATUS(status);
	}

	/* -- exposure */
	attrName = MString("_3delight_light_exposure");
	MObject light_exposure = numAttrFn.create(
		attrName, attrName, MFnNumericData::kFloat, 0.0, &status);

	if( status != MStatus::kSuccess )
		return status;

	numAttrFn.setNiceNameOverride(MString("Exposure"));
	numAttrFn.setDefault(0.0);
	numAttrFn.setSoftMin(-5.0);
	numAttrFn.setSoftMax(5.0);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(
			light_exposure, *attrHandles, dgMod, nodeClass);

	if( status != MStatus::kSuccess )
		return status;

	/* Light spread below exists only for area lights and spot lights */
	if( i_nodeType == "directionalLight" || i_nodeType == "pointLight" )
	{
		return status;
	}

	/* --- light spread */
	attrName = MString("_3delight_light_spread");
	MObject light_spread = numAttrFn.create(
		attrName, attrName, MFnNumericData::kFloat, 1.0, &status);

	if( status != MStatus::kSuccess )
		return status;

	numAttrFn.setNiceNameOverride(MString("Spread"));
	numAttrFn.setDefault(1.0);
	numAttrFn.setMin(0.0);
	numAttrFn.setMax(1.0);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(
			light_spread, *attrHandles, dgMod, nodeClass);

	return status;
}

MStatus DL_extensionAttrRegistry::addDirectionalAttributesGroup(
	const MString &i_nodeType )
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;
	status = addAttrGroupForNodeType(
		nodeClass,
		getDirectionalLightGroupName(),
		attrHandles);

	if( status != MStatus::kSuccess )
		return status;

	MDGModifier dgMod;

	MString attrName("_3delight_angular_size");
	MFnNumericAttribute numAttrFn;
	MObject angular_size = numAttrFn.create(
		attrName, attrName, MFnNumericData::kFloat, 0.0, &status);

	if( status != MStatus::kSuccess )
		return status;

	/* Sync with Katana name too */
	numAttrFn.setNiceNameOverride(MString("Angular Diameter"));
	numAttrFn.setMin(0.0);
	numAttrFn.setMax(360.0);
	makeInputOutputAttribute(numAttrFn);

	status = addExtensionAttribute(
			angular_size, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	// Add the prelit attribute
	attrName = MString("_3delight_prelit");

	MObject prelitAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Prelit"));
	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(prelitAttr, *attrHandles, dgMod, nodeClass);

	// Add the visibility attribute
	attrName = MString("_3delight_arealight_visibility");

	MObject visibilityAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Visible to Camera"));
	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(visibilityAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	return status;
}

MStatus DL_extensionAttrRegistry::addSpotAndPointLightsAttributesGroup(
	const MString& i_nodeType )
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status = addAttrGroupForNodeType(
		nodeClass,
		getSpotAndPointLightsGroupName(),
		attrHandles);

	if( status != MStatus::kSuccess )
		return status;

	MString name("_3delight_radius");
	MFnNumericAttribute attribute;
	MObject angular_size = attribute.create(
		name, name, MFnNumericData::kFloat, 0.001f, &status);

	if( status != MStatus::kSuccess )
		return status;

	attribute.setNiceNameOverride(MString("Radius"));
	attribute.setMin( 0.001f );
	attribute.setSoftMax( 1.0f );
	makeInputOutputAttribute(attribute);

	MDGModifier dgMod;
	return addExtensionAttribute(
			angular_size, *attrHandles, dgMod, nodeClass);
}

MStatus
DL_extensionAttrRegistry::addAreaLightAttrGroup(const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status = addAttrGroupForNodeType(
		nodeClass,
		getAreaLightGroupName(),
		attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	// Create the base objects for extension attributes definition
	MDGModifier dgMod;

	// Add the geometry objects attribute
	MString attrName("_3delight_geometry");

	MFnMessageAttribute msgAttrFn;
	MObject geometryAttr = msgAttrFn.create(attrName, attrName, &status);
	RETURN_ON_FAILED_MSTATUS(status);

	msgAttrFn.setNiceNameOverride(MString("Geometry"));
	msgAttrFn.setArray(true);
	// The attribute must be non-readable for indexMatters(false) to be
	// effective.
	//
	makeInputAttribute(msgAttrFn);
	msgAttrFn.setIndexMatters(false);


	status = addExtensionAttribute(geometryAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	// Add the normalize intensity contribution attribute
	attrName = MString("_3delight_normalize_arealight_intensity");

	MFnNumericAttribute numAttrFn;
	MObject normIntensityAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Normalize Intensity by Area"));
	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(normIntensityAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	// Add the visibility attribute
	attrName = MString("_3delight_arealight_visibility");

	MObject visibilityAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Visible to Camera"));
	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(visibilityAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	// Add the prelit attribute
	attrName = MString("_3delight_prelit");

	MObject prelitAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Prelit"));
	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(prelitAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	// Add the two sided attribute
	attrName = MString("_3delight_arealight_twosided");

	MObject twoSidedAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Two Sided"));
	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(twoSidedAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	/* Default area light shape */
	{
		MString name = "_3delight_default_arealight_shape";

		MFnEnumAttribute deformation;
		MObject object = deformation.create( name, name, 0/*default*/, &status);

		assert( status == MStatus::kSuccess );

		deformation.setNiceNameOverride("Shape");

		deformation.addField( "Square", 0 );
		deformation.addField( "Disk", 1 );
		deformation.addField( "Sphere", 2 );
		deformation.addField( "Cylinder", 3 );

		makeInputOutputAttribute( deformation );

		status =
			addExtensionAttribute(object, *attrHandles, dgMod, nodeClass);
	}

	return status;
}

MStatus
DL_extensionAttrRegistry::addVisibilityAttrGroup(const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status = addAttrGroupForNodeType(
		nodeClass,
		getVisibilityGroupName(),
		attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	MDGModifier dgMod;

	/*
		Add the visible to diffuse  attribute.
		The dlOpenVDBShape defines its own version of this attribute. Both must
		be defined with exactly the same properties.
	*/
	MString attrName("_3delight_visibilityDiffuse");

	MFnNumericAttribute numAttrFn;
	MObject visibilityDiffuseAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Visible in Diffuse"));
	numAttrFn.setDefault(true);
	makeInputOutputAttribute(numAttrFn);

	status = addExtensionAttribute(
		visibilityDiffuseAttr,
		*attrHandles,
		dgMod,
		nodeClass);

	RETURN_ON_FAILED_MSTATUS(status);

	// Add the (obsolete) matte attribute
	attrName = MString("_3delight_matte");

	MObject normIntensityAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Object is Matte"));
	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(normIntensityAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	// Add the (obsolete) prelit attribute
	attrName = MString("_3delight_prelit");

	MObject prelitAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Prelit"));
	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	// Add the compositing mode attribute
	status = addExtensionAttribute(prelitAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	attrName = "_3delight_compositingMode";

	MFnEnumAttribute compositing;
	MObject compositingAttr =
		compositing.create( attrName, attrName, 0, &status);

	assert( status == MStatus::kSuccess );

	compositing.setNiceNameOverride("Compositing");

	compositing.addField( "Regular", 0 );
	compositing.addField( "Matte", 1 );
	compositing.addField( "Prelit", 2 );

	makeInputOutputAttribute( compositing );

	status =
		addExtensionAttribute(compositingAttr, *attrHandles, dgMod, nodeClass);

	return status;
}

MStatus
DL_extensionAttrRegistry::addMotionBlurGroup(const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status = addAttrGroupForNodeType(
		nodeClass,
		getMotionBlurGroupName(),
		attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	MDGModifier dgMod;

	// Add the additional motion samples attribute
	MString attrName("_3delight_additional_motion_samples");

	MFnNumericAttribute numAttrFn;
	MObject addlSamplesAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kInt,
		0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Additional Samples"));
	numAttrFn.setDefault(0);
	numAttrFn.setMin(0);
	makeInputOutputAttribute(numAttrFn);
	numAttrFn.setKeyable(false);

	status =
		addExtensionAttribute(addlSamplesAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	/* Deformation Motion Blur */
	if( i_nodeType != "transform" )
	{
		MString name = "_3delight_deformation_motion_blur";

		MFnEnumAttribute deformation;
		MObject object = deformation.create( name, name, 0/*default*/, &status);

		assert( status == MStatus::kSuccess );

		deformation.setNiceNameOverride("Deformation Motion Blur");

		deformation.addField( "On (if detected)", 0 );
		deformation.addField( "On (always)", 1 );
		deformation.addField( "Off", 2);

		makeInputOutputAttribute( deformation );

		status =
			addExtensionAttribute(object, *attrHandles, dgMod, nodeClass);
	}

	return status;
}

MStatus
DL_extensionAttrRegistry::addCurvesGroup(const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status = addAttrGroupForNodeType(
		nodeClass,
		getCurvesGroupName(),
		attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	MDGModifier dgMod;

	// Add the smooth curves attribute
	MString attrName("_3delight_smooth_curves");

	MFnNumericAttribute numAttrFn;
	MObject smoothCurvesAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Smooth Curves"));
	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(smoothCurvesAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	return status;
}

MStatus
DL_extensionAttrRegistry::addImagePlaneGroup(const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status = addAttrGroupForNodeType(
		nodeClass,
		getImagePlaneGroupName(),
		attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	MDGModifier dgMod;

	// Add the write to RGB attribute
	MString attrName("_3delight_write_rgb");

	MFnNumericAttribute numAttrFn;
	MObject writeRGBAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Write to RGB"));
	numAttrFn.setDefault(true);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(writeRGBAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	// Add the write to alpha attribute
	attrName = "_3delight_write_alpha";

	MObject writeAlphaAttr = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(MString("Write to Alpha"));
	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(writeAlphaAttr, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);

	return status;
}

MStatus
DL_extensionAttrRegistry::addShadingEngineGroup(const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status = addAttrGroupForNodeType(
		nodeClass,
		getShadingEngineGroupName(),
		attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	MDGModifier dgMod;

	/*
		Nice names were chosen mainly for the Hypershade node view. They fit
		and are easy to tell apart from the default maya plugs.
	*/
	std::pair<MString, MString> names[] =
	{
		{ "_3delight_surfaceShader", "3Delight Surface" },
		{ "_3delight_displacementShader", "3Delight Displacement" },
		{ "_3delight_volumeShader", "3Delight Volume" },
	};
	MFnTypedAttribute typedAttrFn;
	for( const auto &n : names )
	{
		MObject attr = typedAttrFn.create(
			n.first, n.first, MFnData::kAny, MObject::kNullObj, &status);
		RETURN_ON_FAILED_MSTATUS(status);

		typedAttrFn.setNiceNameOverride(n.second);
		typedAttrFn.setKeyable(true);
		typedAttrFn.setChannelBox(false);
		typedAttrFn.setReadable(false);
		typedAttrFn.setStorable(false);
		typedAttrFn.setConnectable(true);
		typedAttrFn.setUsedAsColor(true);

		status = addExtensionAttribute(attr, *attrHandles, dgMod, nodeClass);
		RETURN_ON_FAILED_MSTATUS(status);
	}

	return status;
}

MStatus
DL_extensionAttrRegistry::addAttributeOverrideVolumeAttrGroup(
	const MString& i_nodeType)
{
	MStatus status;
	MNodeClass nodeClass(i_nodeType);
	HandleList* attrHandles = NULL;

	status = addAttrGroupForNodeType(
		nodeClass,
		getAttributeOverrideVolumeGroupName(),
		attrHandles);
	RETURN_ON_FAILED_MSTATUS(status);

	// Create the base objects for extension attributes definition
	MDGModifier dgMod;


	// Render as attribute override volume attr
	MString attrName("_3delight_isOverrideVolume");

	MFnNumericAttribute numAttrFn;
	MObject attrOverrideVol = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		0.0,
		&status);
	RETURN_ON_FAILED_MSTATUS(status);

	numAttrFn.setNiceNameOverride(
		MString("Use as Spatial Overrides"));

	numAttrFn.setDefault(false);
	makeInputOutputAttribute(numAttrFn);

	status =
		addExtensionAttribute(attrOverrideVol, *attrHandles, dgMod, nodeClass);
	RETURN_ON_FAILED_MSTATUS(status);


	// compositing mode override & its enable attr
	attrName = MString("_3delight_enableCompositingModeOverride");

	MObject enableCompositingMode = numAttrFn.create(
		attrName,
		attrName,
		MFnNumericData::kBoolean,
		false);

	numAttrFn.setNiceNameOverride(MString("Override Compositing Mode"));
	Utilities::makeInputOutputAttribute(numAttrFn);
	status = addExtensionAttribute(
		enableCompositingMode, *attrHandles, dgMod, nodeClass);

	MFnEnumAttribute enumAttrFn;
	attrName = MString("_3delight_compositingModeOverride");
	MObject compositingMode = enumAttrFn.create( attrName, attrName, 0 );

	enumAttrFn.setNiceNameOverride( MString( "Compositing" ) );

	enumAttrFn.addField( "Regular", 0 );
	enumAttrFn.addField( "Matte", 1 );
	enumAttrFn.addField( "Prelit", 2 );

	Utilities::makeInputOutputAttribute( enumAttrFn );
	status = addExtensionAttribute(
		compositingMode, *attrHandles, dgMod, nodeClass);


	// Visible to camera override & its enable attr
	attrName = MString("_3delight_enableVisibilityCameraOverride");
	MObject enableVisibilityCamera =
		numAttrFn.create( attrName, attrName, MFnNumericData::kBoolean, false);

	numAttrFn.setNiceNameOverride(MString("Override Visible to Camera"));
	Utilities::makeInputOutputAttribute(numAttrFn);

	status = addExtensionAttribute(
		enableVisibilityCamera, *attrHandles, dgMod, nodeClass);

	attrName = MString("_3delight_visibilityCameraOverride");
	MObject visibilityCamera =
		numAttrFn.create( attrName, attrName, MFnNumericData::kBoolean, false);

	numAttrFn.setNiceNameOverride(MString("Visible to Camera"));
	Utilities::makeInputOutputAttribute(numAttrFn);

	status = addExtensionAttribute(
		visibilityCamera, *attrHandles, dgMod, nodeClass);


	// Visible in diffuse override & its enable attr
	attrName = MString("_3delight_enableVisibilityDiffuseOverride");
	MObject enableVisibilityDiffuse =
		numAttrFn.create( attrName, attrName, MFnNumericData::kBoolean, false);

	numAttrFn.setNiceNameOverride(MString("Override Visible in Diffuse"));
	Utilities::makeInputOutputAttribute(numAttrFn);

	status = addExtensionAttribute(
		enableVisibilityDiffuse, *attrHandles, dgMod, nodeClass);

	attrName = MString("_3delight_visibilityDiffuseOverride");
	MObject visibilityDiffuse =
		numAttrFn.create( attrName, attrName, MFnNumericData::kBoolean, false);

	numAttrFn.setNiceNameOverride(MString("Visible in Diffuse"));
	Utilities::makeInputOutputAttribute(numAttrFn);

	status = addExtensionAttribute(
		visibilityDiffuse, *attrHandles, dgMod, nodeClass);


	// Visible in reflections override & its enable attr
	attrName = MString("_3delight_enableVisibilityReflectionsOverride");
	MObject enableVisibilityReflection =
		numAttrFn.create( attrName, attrName, MFnNumericData::kBoolean, false);

	numAttrFn.setNiceNameOverride(MString("Override Visible in Reflections"));
	Utilities::makeInputOutputAttribute(numAttrFn);

	status = addExtensionAttribute(
		enableVisibilityReflection, *attrHandles, dgMod, nodeClass);

	attrName = MString("_3delight_visibilityReflectionsOverride");
	MObject visibilityReflection =
		numAttrFn.create( attrName, attrName, MFnNumericData::kBoolean, false);

	numAttrFn.setNiceNameOverride(MString("Visible in Reflections"));
	Utilities::makeInputOutputAttribute(numAttrFn);

	status = addExtensionAttribute(
		visibilityReflection, *attrHandles, dgMod, nodeClass);


	// Visible in refractions override & its enable attr
	attrName = MString("_3delight_enableVisibilityRefractionsOverride");
	MObject enableVisibilityRefractions =
		numAttrFn.create( attrName, attrName, MFnNumericData::kBoolean, false);

	numAttrFn.setNiceNameOverride(MString("Override Visible in Refractions"));
	Utilities::makeInputOutputAttribute(numAttrFn);

	status = addExtensionAttribute(
		enableVisibilityRefractions, *attrHandles, dgMod, nodeClass);

	attrName = MString("_3delight_visibilityRefractionsOverride");
	MObject visibilityRefractions =
		numAttrFn.create( attrName, attrName, MFnNumericData::kBoolean, false);

	numAttrFn.setNiceNameOverride(MString("Visible in Refractions"));
	Utilities::makeInputOutputAttribute(numAttrFn);

	status = addExtensionAttribute(
		visibilityRefractions, *attrHandles, dgMod, nodeClass);


	// Surface shader override
	attrName = MString("_3delight_enableSurfaceShaderOverride");
	MObject enableSurfaceShader =
		numAttrFn.create( attrName, attrName, MFnNumericData::kBoolean, false);

	numAttrFn.setNiceNameOverride(MString("Override Surface Shader"));
	Utilities::makeInputOutputAttribute(numAttrFn);

	status = addExtensionAttribute(
		enableSurfaceShader, *attrHandles, dgMod, nodeClass);

	RETURN_ON_FAILED_MSTATUS(status);

	return status;
}

MStatus
DL_extensionAttrRegistry::addAttrGroupForNodeType(
	MNodeClass& i_nodeClass,
	const MString& i_groupName,
	HandleList*& o_attrHandles)
{
	unsigned int typeId = i_nodeClass.typeId().id();

	if(typeId == 0)
	{
		return MStatus::kInvalidParameter;
	}

	std::pair<unsigned int, AttrGroupList> newRegItem;
	newRegItem.first = typeId;

	std::pair<AttrRegistry::iterator, bool> insertRetVal =
		m_registry.insert(newRegItem);

	bool isNewRegItem = insertRetVal.second;
	AttrGroupList& groupList((*insertRetVal.first).second);

	if(!isNewRegItem && hasAttributeGroup(groupList, i_groupName))
	{
		// i_groupName already exists in the attribute group list for this node
		// type.
		//
		o_attrHandles = NULL;
		return MStatus::kFailure;
	}

	AttrGroup newGroup;
	newGroup.first = i_groupName;

	groupList.push_back(newGroup);

	o_attrHandles = &groupList.back().second;

	return MStatus::kSuccess;
}

MString
DL_extensionAttrRegistry::getDisplacementShaderGroupName()
{
	return MString("displacementShader");
}

MString
DL_extensionAttrRegistry::getBump2dGroupName()
{
	return MString("bump2d");
}

MString
DL_extensionAttrRegistry::getMeshGroupName()
{
	return MString("mesh");
}

MString
DL_extensionAttrRegistry::getCameraGroupName()
{
	return MString("camera");
}

MString
DL_extensionAttrRegistry::getLightGroupName()
{
	return MString("light");
}

MString
DL_extensionAttrRegistry::getAreaLightGroupName()
{
	return MString("areaLight");
}

MString
DL_extensionAttrRegistry::getDirectionalLightGroupName()
{
	return "directionalLight";
}

MString
DL_extensionAttrRegistry::getSpotAndPointLightsGroupName()
{
	return "spotAndPointLights";
}

MString
DL_extensionAttrRegistry::getVisibilityGroupName()
{
	return MString("visibility");
}

MString
DL_extensionAttrRegistry::getMotionBlurGroupName()
{
	return MString("motionBlur");
}

MString
DL_extensionAttrRegistry::getCurvesGroupName()
{
	return MString("curves");
}

MString
DL_extensionAttrRegistry::getImagePlaneGroupName()
{
	return MString("imagePlane");
}

MString
DL_extensionAttrRegistry::getShadingEngineGroupName()
{
	return MString("shadingEngine");
}

MString
DL_extensionAttrRegistry::getAttributeOverrideVolumeGroupName()
{
	return MString("attributeOverrideVolume");
}

bool
DL_extensionAttrRegistry::isPluginValid()
{
	return !m_plugin.object().isNull();
}

MStatus
DL_extensionAttrRegistry::addExtensionAttribute(
	MObject& i_attr,
	HandleList& o_attrHandles,
	MDGModifier& i_dgMod,
	MNodeClass& i_nodeClass)
{
	MStatus status = i_dgMod.addExtensionAttribute(i_nodeClass, i_attr);
	RETURN_ON_FAILED_MSTATUS(status);

	if(isPluginValid())
	{
		status = i_dgMod.linkExtensionAttributeToPlugin(m_plugin.object(), i_attr);
		RETURN_ON_FAILED_MSTATUS(status);
	}

	o_attrHandles.push_back(MObjectHandle(i_attr));
	return status;
}

MStatus
DL_extensionAttrRegistry::removeExtensionAttribute(
	MObject i_attr,
	MDGModifier& i_dgMod,
	MNodeClass& i_nodeClass)
{
	MStatus status;

	if(isPluginValid())
	{
		status =
			i_dgMod.unlinkExtensionAttributeFromPlugin(m_plugin.object(), i_attr);
		RETURN_ON_FAILED_MSTATUS(status);
	}

	status = i_dgMod.removeExtensionAttribute(i_nodeClass, i_attr);
	return status;
}

bool
DL_extensionAttrRegistry::attributeExists(
	const MString& i_attrName,
	HandleList& i_attrHandles)
{
	bool attrFound = false;
	HandleList::iterator itr;

	for(itr = i_attrHandles.begin(); itr != i_attrHandles.end(); itr++)
	{
		MFnAttribute attrFn((*itr).object());
		if(attrFn.name() == i_attrName)
		{
			attrFound = true;
			break;
		}
	}

	return attrFound;
}

bool
DL_extensionAttrRegistry::hasAttributeGroup(
	AttrGroupList& i_groupList,
	const MString& i_groupName)
{
	for(int i = 0; i < i_groupList.size(); i++)
	{
		if(i_groupList[i].first == i_groupName)
		{
			return true;
		}
	}

	return false;
}
