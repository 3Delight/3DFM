#include <stdlib.h>
#include <stdio.h>

#include <maya/MDagMessage.h>
#include <maya/MDGMessage.h>
#include <maya/MDrawRegistry.h>
#include <maya/MGlobal.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnPlugin.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MIteratorType.h>
#include <maya/MModelMessage.h>
#include <maya/MPlugArray.h>
#include <maya/MSceneMessage.h>
#include <maya/MSelectionList.h>
#include <maya/MSwatchRenderRegister.h>
#include <maya/MViewport2Renderer.h>

#include "DL_plugin.h"

#include "DL_about.h"
#include "DL_associativeArray.h"
#include "DL_autoLoadOSL.h"
#include "DL_genericShadingNode.h"
#include "DL_externalCommandServer.h"
#include "DL_existsCmd.h"
#include "DL_extensionAttrCmd.h"
#include "DL_extensionAttrRegistry.h"
#include "DL_expandVariables.h"
#include "DL_hash.h"
#include "DL_optionMenuCmd.h"
#include "DL_packageSpecific.h"
#include "DL_processingPanel.h"
#include "DL_renderStateCmd.h"
#include "DL_stringArrayCmd.h"
#include "DL_stringOp.h"
#include "DL_utils.h"
#include "DL_version.h"
#include "dl3DelightMaterial.h"
#include "dlAOV.h"
#include "dlBoxNoise.h"
#include "dlCallbacksCmd.h"
#include "dlCarPaint.h"
#include "dlColorBlend.h"
#include "dlDecayFilterShape.h"
#include "dlEnvironmentShape.h"
#include "dlGlass.h"
#include "dlGoboFilterShape.h"
#include "dlHairAndFur.h"
#include "dlIncandescenceLightShape.h"
#include "dlLayeredMaterial.h"
#include "dlMetal.h"
#include "dlPrincipled.h"
#include "dlStandard.h"
#include "dlSubstance.h"
#include "dlTexture.h"
#include "dlThin.h"
#include "dlTriplanar.h"
#include "dlOpenVDBShader.h"
#include "dlOpenVDBShape.h"
#include "dlOslUtilsCmd.h"
#include "dlRenderGlobals.h"
#include "dlRenderSettings.h"
#include "dlSet.h"
#include "dlSkin.h"
#include "dlSky.h"
#include "dlSolidRamp.h"
#include "dlStandInNode.h"
#include "dlToon.h"
#include "dlToonGlass.h"
#include "dlWorleyNoise.h"
#include "dlViewport.h"
#include "MaterialViewer.h"
#include "MU_typeIds.h"
#include "maya_render_view.h"
#include "maya_material_viewer.h"
#include "NSIExportCmd.h"
#include "ShaderFragmentCmd.h"
#include "SwatchRender.h"
#include "VPFragmentRegistry.h"
#include "VPShaderOverride.h"
#include "VPSurfaceShaderOverride.h"
#include "VPUtilities.h"

#include "OpenVDBCache.h"

/* 3Delight header to get version number. */
#include "delight.h"

_3DL_ALWAYS_EXPORT MStatus initializePlugin(MObject);
_3DL_ALWAYS_EXPORT MStatus uninitializePlugin(MObject);

static MCallbackIdArray s_callbackIds;
static bool s_sceneOpenInProgress;
static MSelectionList* s_exportCallbacksSelection;

MString DL_plugin::m_pluginName("");

/*
  Strip a single component off a path (either unix or windows). We cut before
  the last slash or backslash found.
*/
static MString CutLastPathElement( const MString i_path )
{
#if MAYA_API_VERSION >= 850
  int pos_slash = i_path.rindexW( (wchar_t)'/' );
  int pos_bslash = i_path.rindexW( (wchar_t)'\\' );
#else
  int pos_slash = i_path.rindex( '/' );
  int pos_bslash = i_path.rindex( '\\' );
#endif

  int pos = pos_slash > pos_bslash ? pos_slash : pos_bslash;

  if( pos == -1 )
	  return i_path;

#if MAYA_API_VERSION >= 850
  return i_path.substringW( 0, pos - 1 );
#else
  return i_path.substring( 0, pos - 1 );
#endif
}

/*
  This sets the environment variables required by 3Delight if they're not
  already set. For the 3Delight for Maya package, they are always redefined
	here. The affected variables are:
  - DELIGHT
  - PATH (we add $DELIGHT/bin to it if DELIGHT wasn't set)

  NOTES
  - This assumes the plugin was loaded from platform-specific paths.
*/
static void Set3DelightEnv( MFnPlugin &plugin )
{
  if( getenv( "DELIGHT" ) && !DL_packageSpecific::isPackage3dfm() )
	  return;

  MString plugin_path = plugin.loadPath();
  // We want to strip "/maya/<version>/plug-ins" off the path. This seems like
  // a not-too-ugly way.
  MString delight = CutLastPathElement( plugin_path );
  delight = CutLastPathElement( delight );
  delight = CutLastPathElement( delight );

  MString delight_var = "DELIGHT=" + delight;
  putenv( strdup(delight_var.asChar()) );

#ifdef _WIN32
  // The strangeness of env vars on Windows:
  // - Using the libc putenv() or the win32 SetEnvironmentVariable() functions
  //   appear to succeed, but calling the MEL getenv proc will fail to see the
  //   modification. The MEL putenv sets something that works for both the MEL
  //   getenv command and the win32 GetEnvironmentVariable. So calling it along
  //   with the libc putenv cover all bases.
  //
  MString cmd("putenv \"DELIGHT\" \"");
  cmd += delight + MString("\"");
  MGlobal::executeCommand(cmd);
#endif

  MString path_var_value = delight + "/bin";

  if( getenv( "PATH" ) )
  {
#ifndef _WIN32
    path_var_value += ":";
#else
    path_var_value += ";";
#endif
    path_var_value += getenv( "PATH" );
  }

  MString path_var = "PATH=" + path_var_value;
  putenv( strdup(path_var.asChar()) );
#ifdef _WIN32
  cmd = MString("putenv \"PATH\" \"") + path_var_value + MString("\"");
  MGlobal::executeCommand(cmd);
#endif
}

static void SetDelightMayaPath( MFnPlugin &plugin )
{
  /*
    This should be "$DELIGHT/maya/<version>/plug-ins". Strip back to
    "$DELIGHT/maya". The reason for working from the plugin path is so it also
    works for a plugin built outside the 3Delight install.
  */
  MString path = plugin.loadPath();
  path = CutLastPathElement( path );
  path = CutLastPathElement( path );
  setDelightMayaPath( path );
}

//
// Adds path to 3Delight's pyhton folder to PYTHONPATH env var.
// This assumes that DELIGHT has a proper value defined.
//
static void
SetPythonPath()
{
  MString delightPath = getDelightPath();
  MString pythonPath(delightPath + "/python");

  MString cmd("import sys; sys.path.append(\"" + pythonPath + "\")");
  MGlobal::executePythonCommand(cmd);
}

static void
beforeOpenCallback(void*)
{
  s_sceneOpenInProgress = true;
}

static void
afterOpenCallback(void*)
{
  s_sceneOpenInProgress = false;
  MGlobal::executeCommand("delightUpdateOldNodes");
}

static void
afterReferenceCallback(void*)
{
  if(!s_sceneOpenInProgress)
    MGlobal::executeCommand("delightUpdateOldNodes");
}

static void
afterImportCallback(void*)
{
  MGlobal::executeCommand("delightAfterImportCallback");
}

static void onXGenPluginLoad()
{
#if MAYA_API_VERSION >= 201700
  // Load 3dfmXGen if it is not already loaded
  MObject exporter = MFnPlugin::findPlugin( MString( "3dfmXGen" ) );
  if( exporter.isNull() )
  {
    MGlobal::executeCommand( MString( "loadPlugin \"3dfmXGen\" " ) );
  }

  /*
    Add extension attributes to some XGen shapes. This could be done in the
    3dfmXGen plug-in itself, with the added complexity of watching for 3dfm
    plug-in load event and executing the command only when it is loaded.

    It's much simpler to take care of that here.
  */
  DL_extensionAttrRegistry::getInstance().addAttributeGroup(
    "xgmSplineDescription",
    "motionBlur" );
  DL_extensionAttrRegistry::getInstance().addAttributeGroup(
    "xgmSplineDescription",
    "curves" );
#endif
}

static void afterPluginLoad( const MStringArray& i_pluginInfo, void* )
{
  if( i_pluginInfo.length() < 2 )
  {
    return;
  }

  if( i_pluginInfo[1] == MString("xgenToolkit") )
  {
    onXGenPluginLoad();
  }
}

static void beforePluginUnload( const MStringArray& i_pluginInfo, void* )
{
  if( i_pluginInfo.length() < 1 )
  {
    return;
  }

  if( i_pluginInfo[0] == MString("xgenToolkit") )
  {
#if MAYA_API_VERSION >= 201700
    DL_extensionAttrRegistry::getInstance().removeAttributeGroup(
      "xgmSplineDescription",
      "motionBlur" );
#endif
  }
}

static void loadCommonFragments()
{
  MStringArray fragments;
  fragments.append("dlGGX");
  fragments.append("dlGGXConductor");
  fragments.append("dlOrenNayar");
  fragments.append("dlFresnel");
  fragments.append("dlSpecLevelToF0");
  fragments.append("dlMetalEdgeColor");
  fragments.append("dlCombiner");

  MStringArray fragmentGraphs;

#if MAYA_API_VERSION >= 201700
  VPFragmentRegistry::Instance()->Add( fragments, fragmentGraphs );
#endif
}

MStatus
initializePlugin( MObject obj )
{
  MStatus result;

  MFnPlugin plugin(
		obj,
		"DNA research",
		DL_get3dfmVersionNumberString(),
		"Any");

  DL_plugin::setName(plugin.name());

  Set3DelightEnv( plugin );
  SetDelightMayaPath( plugin );
  SetPythonPath();

  DL_extensionAttrRegistry::getInstance().setPlugin(obj);

  // Register the display drivers
  RegisterMayaRenderView();
  RegisterMayaMaterialViewer();

	DL_packageSpecific::set3dfmRibOption();

  // Required classification for a light to be recognized by the light linker &
  // lnode type to be returned by `listNodeTypes light`.
  //
  MString light_classification("light");

  // Classification to be listed under 3Delight -> Light in the Hypershade
  MString dl_light_classification("rendernode/dl/light");

  /*
    Basic classification for 3Delight nodes. This allows us to define an
    initialization procedure that gets called when the node is created in the
    Hypershade or the Node Editor.
  */
  MString basicClass( "rendernode/dl" );
  MString openVDBShape_classification = dlOpenVDBShape::drawDbClassification;
  openVDBShape_classification += ":" + basicClass;

  MString env_classification = dlEnvironmentShape::drawDbClassification;
  env_classification += ":" + light_classification +
    ":" + dl_light_classification;

  result = plugin.registerShape( "dlEnvironmentShape",
                                dlEnvironmentShape::id,
                                dlEnvironmentShape::creator,
                                dlEnvironmentShape::initialize,
                                nullptr, /* legacy viewport is unsupported */
                                &env_classification );
  CHECK_MSTATUS(result);

  result = MHWRender::MDrawRegistry::registerDrawOverrideCreator(
    dlEnvironmentShape::drawDbClassification,
    dlEnvironmentShape::drawRegistrantId,
    dlEnvironmentShapeDrawOverride::creator);
  CHECK_MSTATUS(result);

  result =
    MHWRender::MDrawRegistry::registerGeometryOverrideCreator(
        dlOpenVDBShape::drawDbClassification,
        dlOpenVDBShape::drawRegistrantId,
        dlOpenVDBShapeGeometryOverride::Creator);
  CHECK_MSTATUS(result);

  const MString delightShaderSwatchClass("swatch/delightShaderSwatchRender");


  //
  // Classification strings building blocks
  //

  // Viewport 2.0 shader classifications
  MString shaderDrawDBClassPrefix = MString( "drawdb/shader/surface/dl/");
  MString vpSurfaceShaderClass = shaderDrawDBClassPrefix + "VPSurfaceShader";
  MString vpShaderClass = shaderDrawDBClassPrefix + "VPShader";

  // The order of the various classification seem to matter; placing the swatch
  // render classification first will make Maya not render any swatch in the
  // hypershade. "shader/surface" must be the first classification.
  //
  // Using one of the built-in Maya classifications also has the benefit of
  // allowing the Node Editor creation system (when pressing tab) to offer the
  // node type.
  //
  const MString shaderSurfaceWithSwatchClass(
      /* This classification is very important. This means that render node can
       * be assigned to the surface directly. All mia_* shaders contain this
       * classification. Unfortunately it makes the node listed at Maya/Surface
       * group of the Hypershade. */
      MString("shader/surface:") +
      /* Register swatch renderer */
      delightShaderSwatchClass );

  // This older classification string is still useful when we want an icon
  // instead of a swatch render in the hypershade.
  //
  const MString shaderSurfaceNoSwatchClass(
      delightShaderSwatchClass + MString(":") +
      "shader/surface:" );

  // This class gets listed in 3Delight->Surface in the Hypershade
  const MString dlSurfaceClass(":rendernode/dl/shader/surface");

  // This class is for former 3Delight surface shaders
  const MString dlDeprecatedSurfaceClass(
    ":rendernode/dl/shader/deprecated/surface");

  // Swatches in Hypershade on by default for 2015, off by default for older
  // versions.
  //
  MString swatchesEnvVar(getenv("_3DFM_HYPERSHADE_SWATCHES"));
  MString shaderSurfaceClass;

#if MAYA_API_VERSION >= 201500
  shaderSurfaceClass = shaderSurfaceWithSwatchClass;

  if (swatchesEnvVar == MString("0"))
  {
    shaderSurfaceClass = shaderSurfaceNoSwatchClass;
  }
#else
  shaderSurfaceClass = shaderSurfaceNoSwatchClass;

  if (swatchesEnvVar == MString("1"))
  {
    shaderSurfaceClass = shaderSurfaceWithSwatchClass;
  }
#endif

  // Because the swatch classification is first, Maya will show an icon in the
  // Hypershade instead of a rendered swatch. In this case, this is the desired
  // behaviour. The default icon is a vector-based "sheet". Remplace with a
  // 256x256 or 512x512 <nodetype>.png or <nodetype>.svg file.
  //
  const MString shaderTextureClass(
      /* Register swatch renderer */
      delightShaderSwatchClass +
      /* The node is listed at Maya/2D Texture group */
      ":texture/2d");

  /*
    This class indicates that creating the shader should not be created with
    a connected shading group. Currently supported by the volume class.
  */
  const MString dlNoShadingGroupClass(":noShadingGroup");

  // This class gets listed in 3Delight->2D Textures in the Hypershade
  const MString dl2dTextureClass(":rendernode/dl/shader/texture/2d");

  // This class gets listed in 3Delight->2D Textures in the Hypershade
  const MString dl3dTextureClass(":rendernode/dl/shader/texture/3d");

  // This class gets list in 3Delight->Utility in the HyperShade
  const MString dlUtilityClass("rendernode/dl/shader/utility:utility/general");

  /*
    This class gets listed in 3Delight->Volume in the HyperShade. The
    /shader/volume class allows the shader to be assignable and the Material
    Viewer will adjust accordingly.
  */
  const MString dlVolumeClass(
    "/shader/volume:rendernode/dl/shader/volume" );

  // Define atmosphere shaders as a specialization of the volume class.
  const MString dlAtmosphereClass = dlVolumeClass + "/atmosphere";

  // For our atmosphere shader, we also don't want a shading group for it.
  const MString dlAtmosphereClassNoSG =
    dlAtmosphereClass + dlNoShadingGroupClass;

  /*
    Providing "shader" as a classification allows the Node Editor panel of
    the Hypershade to list that type in its "tab" menu
  */

  // This class gets list in 3Delight->Light Filters in the HyperShade
  MString dlLightFiltersClass("shader:rendernode/dl/shader/lightFilters");

  // This class gets listed in 3Delight->Environment in the Hypershade
  MString dlEnvironmentClass("shader:rendernode/dl/shader/environment");

  // Class for deprecated texture nodes
  const MString dlDeprecatedTextureClass(
    ":rendernode/dl/deprecated/texture");

  //
  // Complete classification strings
  //
  MString full2dTextureClass = "texture/2d" + dl2dTextureClass;
  MString full3dTextureClass = "texture/3d" + dl3dTextureClass;

  MString fullSurfaceSwatchClass =
    shaderSurfaceClass + dlSurfaceClass + ":" + vpSurfaceShaderClass;

  MString fullSurfaceNoSwatchClass =
    shaderSurfaceNoSwatchClass + dlSurfaceClass + ":" + vpSurfaceShaderClass;

  MString deprecatedFullTextureClass =
    shaderTextureClass + dlDeprecatedTextureClass;

  MString deprecatedFullSurfaceClass =
    dlDeprecatedSurfaceClass + ":" +
    vpSurfaceShaderClass + ":" +
    "shader/surface";

  MString deprecatedFullSurfaceNoSwatchClass =
    shaderSurfaceNoSwatchClass + dlDeprecatedSurfaceClass;

  // Listed in the AOV selector
  MString aovSelectorClass( "rendernode/dl/aov" );
  // Shown in the 3Delight -> AOV section of the hypershade
  MString aovShadingNodeClass( "shader:rendernode/dl/shader/aov:" );

  MString bothAovClasses = aovShadingNodeClass + ":" + aovSelectorClass;

  //
  // Component registration
  //


  /*
    Viewport shader overrides registration.
  */
  VPUtilities::RegisterVPShaderOverride();
  VPUtilities::RegisterVPSurfaceShaderOverride();

  /*
    Shading nodes registration, in alphabetical order.
  */

  result = plugin.registerNode("dl3DelightMaterial",
                                MTypeId(MU_typeIds::DL_MATERIAL3DELIGHT),
                                dl3DelightMaterial::creator,
                                dl3DelightMaterial::initialize,
                                MPxNode::kDependNode,
                                &deprecatedFullSurfaceClass);

  result = plugin.registerNode("dlAOVColor",
                                MTypeId(MU_typeIds::DL_AOVCOLOR),
                                dlAOV::creator,
                                dlAOV::initializeColor,
                                MPxNode::kDependNode,
                                &bothAovClasses);

  result = plugin.registerNode("dlAOVFloat",
                                MTypeId(MU_typeIds::DL_AOVFLOAT),
                                dlAOV::creator,
                                dlAOV::initializeFloat,
                                MPxNode::kDependNode,
                                &bothAovClasses);

  result = plugin.registerNode("dlAOVGroup",
                                MTypeId(MU_typeIds::DL_AOVGROUP),
                                dlAOVGroup::creator,
                                dlAOVGroup::initialize,
                                MPxNode::kDependNode,
                                &aovShadingNodeClass);

  result = plugin.registerNode("dlAOVPrimitiveAttribute",
                                MTypeId(MU_typeIds::DL_AOVPRIMITIVEATTRIBUTE),
                                dlAOVPrimitiveAttribute::creator,
                                dlAOVPrimitiveAttribute::initialize,
                                MPxNode::kDependNode,
                                &bothAovClasses);

  result = plugin.registerNode("dlBoxNoise",
                                MTypeId(MU_typeIds::DLBOXNOISE),
                                dlBoxNoise::creator,
                                dlBoxNoise::initialize,
                                MPxNode::kDependNode,
                                &full3dTextureClass);

  result = plugin.registerNode("dlCarPaint",
                                MTypeId(MU_typeIds::DLCARPAINT),
                                dlCarPaint::creator,
                                dlCarPaint::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceSwatchClass);

  result = plugin.registerNode("dlColorBlend",
                                MTypeId(MU_typeIds::DL_COLORBLEND),
                                dlColorBlend::creator,
                                dlColorBlend::initialize,
                                MPxNode::kDependNode,
                                &dlUtilityClass);

  result = plugin.registerNode("dlColorCorrection",
                                MTypeId(MU_typeIds::DL_COLORCORRECTION),
                                DL_genericShadingNode::creator,
                                DL_genericShadingNode::initialize,
                                MPxNode::kDependNode,
                                &dlUtilityClass);

  result = plugin.registerNode("dlColorVariation",
                                MTypeId(MU_typeIds::DL_COLORVARIATION),
                                DL_genericShadingNode::creator,
                                DL_genericShadingNode::initialize,
                                MPxNode::kDependNode,
                                &dlUtilityClass);

  result = plugin.registerNode("dlGlass",
                                MTypeId(MU_typeIds::DL_GLASS),
                                dlGlass::creator,
                                dlGlass::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceSwatchClass);

  result = plugin.registerNode("dlHairAndFur",
                                MTypeId(MU_typeIds::DL_HAIRANDFUR),
                                dlHairAndFur::creator,
                                dlHairAndFur::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceNoSwatchClass);

  result = plugin.registerNode("dlLayeredMaterial",
                                MTypeId(MU_typeIds::DL_LAYEREDMATERIAL),
                                dlLayeredMaterial::creator,
                                dlLayeredMaterial::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceSwatchClass);

  result = plugin.registerNode("dlMetal",
                                MTypeId(MU_typeIds::DL_METAL),
                                dlMetal::creator,
                                dlMetal::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceSwatchClass);

  result = plugin.registerNode("dlOpenVDBShader",
                                MTypeId(MU_typeIds::DLOPENVDBSHADER),
                                dlOpenVDBShader::creator,
                                dlOpenVDBShader::initialize,
                                MPxNode::kDependNode,
                                &dlVolumeClass);

  result = plugin.registerNode("dlPrimitiveAttribute",
                                MTypeId( MU_typeIds::DL_PRIMITIVEATTRIBUTE),
                                DL_genericShadingNode::creator,
                                DL_genericShadingNode::initialize,
                                MPxNode::kDependNode,
                                &dlUtilityClass);

  result = plugin.registerNode("dlPrincipled",
                                MTypeId(MU_typeIds::DL_3DELIGHTPRINCIPLED),
                                dlPrincipled::creator,
                                dlPrincipled::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceSwatchClass);

  result = plugin.registerNode("dlStandard",
                                MTypeId(MU_typeIds::DLSTANDARD),
                                dlStandard::creator,
                                dlStandard::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceSwatchClass);  

  result = plugin.registerNode("dlSkin",
                                MTypeId(MU_typeIds::DL_SKIN),
                                dlSkin::creator,
                                dlSkin::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceSwatchClass);

  result = plugin.registerNode("dlSky",
                                MTypeId( MU_typeIds::DL_SKY),
                                dlSky::creator,
                                dlSky::initialize,
                                MPxNode::kDependNode,
                                &dlEnvironmentClass);

  result = plugin.registerNode("dlSolidRamp",
                                MTypeId(MU_typeIds::DLSOLIDRAMP),
                                dlSolidRamp::creator,
                                dlSolidRamp::initialize,
                                MPxNode::kDependNode,
                                &full3dTextureClass);

  result = plugin.registerNode("dlSubstance",
                                MTypeId(MU_typeIds::DL_SUBSTANCE),
                                dlSubstance::creator,
                                dlSubstance::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceSwatchClass);

  result = plugin.registerNode("dlTexture",
                                MTypeId(MU_typeIds::DLTEXTURE),
                                dlTexture::creator,
                                dlTexture::initialize,
                                MPxNode::kDependNode,
                                &full2dTextureClass);

  result = plugin.registerNode("dlThin",
                                MTypeId(MU_typeIds::DL_THIN),
                                dlThin::creator,
                                dlThin::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceSwatchClass);

  result = plugin.registerNode("dlToon",
                                MTypeId(MU_typeIds::DLTOON),
                                dlToon::creator,
                                dlToon::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceSwatchClass);

  result = plugin.registerNode("dlToonGlass",
                                MTypeId(MU_typeIds::DLTOONGLASS),
                                dlToonGlass::creator,
                                dlToonGlass::initialize,
                                MPxNode::kDependNode,
                                &fullSurfaceSwatchClass);

  result = plugin.registerNode("dlTriplanar",
                                MTypeId(MU_typeIds::DLTRIPLANAR),
                                dlTriplanar::creator,
                                dlTriplanar::initialize,
                                MPxNode::kDependNode,
                                &full3dTextureClass);

  result = plugin.registerNode("dlWorleyNoise",
                                MTypeId(MU_typeIds::DLWORLEYNOISE),
                                dlWorleyNoise::creator,
                                dlWorleyNoise::initialize,
                                MPxNode::kDependNode,
                                &full3dTextureClass);

  result = DL_OSLShadingNode::RegisterOSLShaders( plugin );

  /*
    Shapes, in alphabetical order
  */
  result = plugin.registerShape(
    "dlDecayFilterShape",
    dlDecayFilterShape::id,
    dlDecayFilterShape::creator,
    dlDecayFilterShape::initialize,
    dlDecayFilterShape::creator,
    &dlLightFiltersClass);

  result = plugin.registerShape(
    "dlGoboFilterShape",
    dlGoboFilterShape::id,
    dlGoboFilterShape::creator,
    dlGoboFilterShape::initialize,
    dlGoboFilterShape::creator,
    &dlLightFiltersClass);

  const MString incandescenceLightClassification =
    light_classification + ":" + dl_light_classification;

  result = plugin.registerShape(
    "dlIncandescenceLightShape",
    dlIncandescenceLightShape::id,
    dlIncandescenceLightShape::creator,
    dlIncandescenceLightShape::initialize,
    dlIncandescenceLightShape::creator,
    &incandescenceLightClassification);

  result = plugin.registerShape("dlOpenVDBShape",
                                MTypeId(MU_typeIds::DLOPENVDBSHAPE),
                                dlOpenVDBShape::creator,
                                dlOpenVDBShape::initialize,
                                nullptr, /* legacy viewport is unsupported */
                                &openVDBShape_classification);

  result = plugin.registerNode(
	"dlStandInNode",
	dlStandInNode::id,
	&dlStandInNode::creator,
	&dlStandInNode::initialize,
	MPxNode::kLocatorNode);

  /*
    Other dependency nodes, in alphabetical order
  */
  result = plugin.registerNode(
    "dlRenderGlobals",
    dlRenderGlobals::id,
    dlRenderGlobals::creator,
    dlRenderGlobals::initialize,
    MPxNode::kDependNode);

  result = plugin.registerNode(
    "dlRenderSettings",
    dlRenderSettings::id,
    dlRenderSettings::creator,
    dlRenderSettings::initialize,
    MPxNode::kDependNode,
    &basicClass);

  result = plugin.registerNode(
    "dlSet",
    dlSet::m_id,
    dlSet::creator,
    dlSet::initialize,
    MPxNode::kObjectSet);


  /*
    Commands, in alphabetical order
  */
  result = plugin.registerCommand(
      "_3DelightOpenVDBCache",
      OpenVDBCache::creator,
      OpenVDBCache::newSyntax );

  result = plugin.registerCommand("delightAbout",
                                  DL_about::creator,
                                  DL_about::newSyntax);

  result = plugin.registerCommand(
    DLCALLBACKSCMD_STR,
    dlCallbacksCmd::creator,
    dlCallbacksCmd::newSyntax );

  result = plugin.registerCommand(
    SHADERFRAGMENTCMD_STR,
    ShaderFragmentCmd::creator,
    ShaderFragmentCmd::newSyntax );

  result = plugin.registerCommand(
    DLOSLUTILSCMD_STR,
    dlOslUtilsCmd::creator,
    dlOslUtilsCmd::newSyntax );

  result = plugin.registerCommand("delightExists",
                                  DL_existsCmd::creator,
                                  DL_existsCmd::newSyntax);

  result = plugin.registerCommand("delightExpandVariables",
                                  DL_expandVariables::creator,
                                  DL_expandVariables::newSyntax);

  result = plugin.registerCommand(DL_EXTENSIONATTR_CMD_STR,
                                  DL_extensionAttrCmd::creator,
                                  DL_extensionAttrCmd::newSyntax);

  result = plugin.registerCommand("delightHash",
                                  DL_hash::creator,
                                  DL_hash::newSyntax);

  result = plugin.registerCommand("delightOptionMenu",
                                  DL_optionMenuCmd::creator,
                                  DL_optionMenuCmd::newSyntax);

  result = plugin.registerCommand("delightProcessingPanel",
								  DL_processingPanel::creator,
								  DL_processingPanel::newSyntax);

  result = plugin.registerCommand("delightRenderState",
                                  DL_renderStateCmd::creator,
                                  DL_renderStateCmd::newSyntax);

  result = plugin.registerCommand("delightStringArray",
                                  DL_stringArrayCmd::creator,
                                  DL_stringArrayCmd::newSyntax);

  result = plugin.registerCommand("delightStringOp",
                                  DL_stringOp::creator,
                                  DL_stringOp::newSyntax);

  result = plugin.registerCommand("delightTable",
                                  DL_associativeArray::creator,
                                  DL_associativeArray::newSyntax);

  result = plugin.registerCommand(
    NSIEXPORT_CMD_STR,
    NSIExportCmd::creator,
    NSIExportCmd::newSyntax );


  /*
    Translators
  */
  result = DL_packageSpecific::registerNSIArchiveTranslator(plugin);

  /*
    Material Viewer renderer
  */
#if MAYA_API_VERSION >= 201600
  // This avoids the following nonsensical error in batch mode:
  // Cannot find procedure "shaderBallRendererMenuUpdate".
  //
  // The error may appear when 3dfm is loaded during a scene open operation
  // (i.e. it is not auto-loaded upon Maya launch)
  //
  if( MGlobal::mayaState() == MGlobal::kInteractive )
  {
    result = plugin.registerRenderer("3Delight", MaterialViewer::creator);
  }
#endif

  /*
    Swatch renderer
  */
  result = MSwatchRenderRegister::registerSwatchRender(
    "delightShaderSwatchRender", SwatchRender::creator);


  // Register extension attributes
  DL_extensionAttrRegistry::getInstance().registerAll();

  // Extra init when XGen plug-in is already loaded
  MObject xgenPlugin = MFnPlugin::findPlugin( MString( "xgenToolkit" ) );
  if( !xgenPlugin.isNull() )
  {
    onXGenPluginLoad();
  }

  // Register startup / shutdown procedures
  result = plugin.registerUI("DL_startupUI",
                             "DL_shutdownUI",
                             "DL_startupBatch",
                             "DL_shutdownBatch");

  /*
    Source everything right now. Otherwise some callbacks which rely on MEL
    will not work correctly if opening/importing a scene is what caused the
    plugin to be loaded.
  */
  MGlobal::sourceFile( "DL_startup.mel" );

  /*
    Registers the Hypershade callbacks right away. Trying to do that in the
    procedures called by MFnPlugin::registerUI() gets the plug-in change
    callback registered too late to effectively  request an Hypershade panel
    rebuild.
  */
  MGlobal::executeCommand("DL_setupDisplayStrings; DL_HypershadeInit;");

  // Extra notes about executing MEL commands here:
  // When the plugin is loaded due to a "requires" statement in the scene file,
  // what can actually be done by an executeCommand() seems quite limited. It
  // may look like the command has ran successfully, and some things do work -
  // a MEL procedure may alter successfully a global var, return a value, and
  // execute some commands. Print statements are not executed, and some commands
  // will fail silently, aborting the procedure execution. Finally, some other
  // commands do not fail but have no effect.
  //
  s_sceneOpenInProgress = false;
  s_callbackIds.clear();

  // Update old nodes when scene is opened or references are loaded
  MStatus status;
  MSceneMessage sceneMessage;
  s_callbackIds.append(sceneMessage.addCallback(
      MSceneMessage::kBeforeOpen,
      beforeOpenCallback));
  s_callbackIds.append(sceneMessage.addCallback(
      MSceneMessage::kAfterOpen,
      afterOpenCallback));

#if MAYA_API_VERSION >= 201100
  s_callbackIds.append(sceneMessage.addCallback(
      MSceneMessage::kAfterLoadReference,
      afterReferenceCallback));
  s_callbackIds.append(sceneMessage.addCallback(
      MSceneMessage::kAfterCreateReference,
      afterReferenceCallback));
#else
  s_callbackIds.append(sceneMessage.addCallback(
      MSceneMessage::kAfterReference,
      afterReferenceCallback));
#endif

  // Get notified of Import operations
  s_callbackIds.append(sceneMessage.addCallback(
    MSceneMessage::kAfterImport,
    afterImportCallback));

  s_callbackIds.append(sceneMessage.addCallback(
    MSceneMessage::kAfterImportReference,
    afterImportCallback));

  s_exportCallbacksSelection = NULL;

  s_callbackIds.append( sceneMessage.addStringArrayCallback(
    MSceneMessage::kAfterPluginLoad,
    afterPluginLoad ) );

  s_callbackIds.append( sceneMessage.addStringArrayCallback(
    MSceneMessage::kBeforePluginUnload,
    beforePluginUnload ) );

  /* NSI viewport renderer. */
  if( MGlobal::mayaState() == MGlobal::kInteractive )
  {
    MHWRender::MRenderer *renderer = MHWRender::MRenderer::theRenderer(false);
    if (renderer)
    {
      if ( !dlViewportRenderOverride::s_instance )
      {
        dlViewportRenderOverride::s_instance =
          new dlViewportRenderOverride( dlViewportRenderOverride::s_name );
      }

      dlViewportNsiSceneRenderer::RegisterDisplayDriver();
      result = renderer->registerOverride(
        dlViewportRenderOverride::s_instance );
      CHECK_MSTATUS( result );

      plugin.registerNode(
        "dlViewportRenderSettings",
        dlViewportRenderSettings::id,
        dlViewportRenderSettings::creator,
        dlViewportRenderSettings::initialize,
        dlViewportRenderSettings::kDependNode);
    }

    loadCommonFragments();
  }

  /* We need all that crap to reliably print our version when loading. */
  MString loadingMessage( "3Delight for Maya " );
	loadingMessage += MString( DL_get3dfmVersionNumberString() )
		+ " loaded, using " + DlGetLibNameAndVersionString();

  if( MGlobal::mayaState() == MGlobal::kInteractive )
  {
    MGlobal::displayInfo( loadingMessage );
  }
  else
  {
    /* Maya outputs the log to stdout on windows but stderr on linux. */
    fprintf(
#ifdef _WIN32
      stdout,
#else
      stderr,
#endif
      "// %s\n", loadingMessage.asChar() );
  }

  return result;
}

MStatus
uninitializePlugin( MObject obj)
{
  MStatus   result;
  MFnPlugin plugin( obj );

  DL_externalCommandServer::CleanUp();

  MMessage::removeCallbacks(s_callbackIds);
  DL_extensionAttrRegistry::deleteInstance();

  /*
    Shading nodes, in alphabetical order
  */
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_MATERIAL3DELIGHT));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_AOVCOLOR));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_AOVFLOAT));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_AOVGROUP));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_AOVPRIMITIVEATTRIBUTE));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DLBOXNOISE));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DLCARPAINT));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_COLORBLEND));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_COLORCORRECTION));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_COLORVARIATION));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_GLASS));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_HAIRANDFUR));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_LAYEREDMATERIAL));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_METAL));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DLOPENVDBSHADER));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_PRIMITIVEATTRIBUTE));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_3DELIGHTPRINCIPLED));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_SKIN));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_SKY));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DLSOLIDRAMP));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_SUBSTANCE));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DL_THIN));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DLTRIPLANAR));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DLWORLEYNOISE));
  result = plugin.deregisterNode(MTypeId(MU_typeIds::DLTOON));

  /*
    Shapes, in alphabetical order
  */
  result = plugin.deregisterNode(dlDecayFilterShape::id);
  result = plugin.deregisterNode(dlGoboFilterShape::id);
  result = plugin.deregisterNode(dlIncandescenceLightShape::id);
  result = plugin.deregisterNode(MU_typeIds::DLOPENVDBSHAPE);
  result = plugin.deregisterNode(dlStandInNode::id);

  /*
    Dependency nodes, in alphabetical order
  */
  result = plugin.deregisterNode(dlRenderGlobals::id);
  result = plugin.deregisterNode(dlRenderSettings::id);
  result = plugin.deregisterNode(dlSet::m_id);

  /*
    Commands, in akphabetical order
  */
  result = plugin.deregisterCommand("delightAbout");
  result = plugin.deregisterCommand(DLCALLBACKSCMD_STR);
  result = plugin.deregisterCommand(DLOSLUTILSCMD_STR);
  result = plugin.deregisterCommand(SHADERFRAGMENTCMD_STR);
  result = plugin.deregisterCommand("delightExists");
  result = plugin.deregisterCommand("delightExpandVariables");
  result = plugin.deregisterCommand(DL_EXTENSIONATTR_CMD_STR);
  result = plugin.deregisterCommand("delightHash");
  result = plugin.deregisterCommand("delightOptionMenu");
  result = plugin.deregisterCommand("delightRenderState");
  result = plugin.deregisterCommand("delightStringArray");
  result = plugin.deregisterCommand("delightStringOp");
  result = plugin.deregisterCommand("delightTable");
  result = plugin.deregisterCommand(NSIEXPORT_CMD_STR);

  result = plugin.deregisterCommand("RiProcedural");

  /*
    Translators
  */
  result = DL_packageSpecific::deregisterNSIArchiveTranslator(plugin);

  /*
    Material viewer renderer
  */
#if MAYA_API_VERSION >= 201600
  if( MGlobal::mayaState() == MGlobal::kInteractive )
  {
    result = plugin.deregisterRenderer("3Delight");
  }
#endif

  /*
    Swatch renderer
  */
  result =
    MSwatchRenderRegister::unregisterSwatchRender("delightShaderSwatchRender");


  // Deregister draw overrides
  //
  result = MHWRender::MDrawRegistry::deregisterGeometryOverrideCreator(
      dlOpenVDBShape::drawDbClassification,
      dlOpenVDBShape::drawRegistrantId);

  MHWRender::MRenderer *renderer = MHWRender::MRenderer::theRenderer();
  if (renderer)
  {
    result = renderer->deregisterOverride( dlViewportRenderOverride::s_instance );
    delete dlViewportRenderOverride::s_instance;
    dlViewportRenderOverride::s_instance = NULL;
  }

  VPUtilities::DeregisterVPShaderOverride();
  VPUtilities::DeregisterVPSurfaceShaderOverride();

  VPFragmentRegistry::DeleteInstance();
  VPUtilities::ClearShaderMgrEffectCache();
  return result;
}


MStatus
DL_plugin::registerShape(
	MFnPlugin& i_plugin,
	const MString& i_typeName,
	const MTypeId& i_typeId,
	MCreatorFunction i_creatorFunction,
	MInitializeFunction i_initFunction,
	MCreatorFunction i_uiCreatorFunction,
	const MString* i_classification)
{
	return i_plugin.registerShape(
		i_typeName,
		i_typeId,
		i_creatorFunction,
		i_initFunction,
		i_uiCreatorFunction,
		i_classification);
}

MStatus
DL_plugin::deregisterNode(MFnPlugin& i_plugin, const MTypeId& i_typeId)
{
	return i_plugin.deregisterNode(i_typeId);
}

MStatus
DL_plugin::registerCommand(
	MFnPlugin& i_plugin,
	const MString& i_commandName,
	MCreatorFunction i_creatorFunction,
	MCreateSyntaxFunction i_createSyntaxFunction)
{
	return i_plugin.registerCommand(
    i_commandName,
    i_creatorFunction,
    i_createSyntaxFunction);
}

MStatus
DL_plugin::deregisterCommand(MFnPlugin& i_plugin,	const MString& i_commandName)
{
	return i_plugin.deregisterCommand(i_commandName);
}

MString
DL_plugin::name()
{
  return m_pluginName;
}

void
DL_plugin::setName(MString i_name)
{
  m_pluginName = i_name;
}
