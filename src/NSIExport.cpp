/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#include "NSIExport.h"

#ifndef _WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <signal.h>

#include <maya/MAnimControl.h>
#include <maya/MComputation.h>
#include <maya/MDagPath.h>
#include <maya/MDGMessage.h>
#include <maya/MDagMessage.h>
#include <maya/MGlobal.h>
#include <maya/MFloatArray.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnSet.h>
#include <maya/MSelectionList.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnMesh.h>
#include <maya/MFnCamera.h>
#include <maya/MFnReference.h>
#include <maya/MFnTransform.h>
#include <maya/MFnInstancer.h>
#include <maya/MItDag.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MItSelectionList.h>
#include <maya/MIteratorType.h>
#include <maya/MMatrix.h>
#include <maya/MNodeMessage.h>
#include <maya/MObjectArray.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MString.h>
#include <maya/MUuid.h>

#include "DL_externalCommandServer.h"
#include "DL_plugin.h"
#include "DL_renderState.h"
#include "DL_utils.h"
#include "MU_typeIds.h"
#include "NSIErrorHandler.h"
#include "NSIExportCamera.h"
#include "NSIExportCustom.h"
#include "NSIExportDefaultLightSet.h"
#include "NSIExportGoboFilter.h"
#include "NSIExportImagePlane.h"
#include "NSIExportIncandescenceLight.h"
#include "NSIExportInstancer.h"
#include "NSIExportLight.h"
#include "NSIExportLightGroup.h"
#include "NSIExportLightLink.h"
#include "NSIExportMesh.h"
#include "NSIExportNurbsSurface.h"
#include "NSIExportOpenVDB.h"
#include "NSIExportParticle.h"
#include "NSIExportPlace3dTexture.h"
#include "NSIExportPfxGeometry.h"
#include "NSIExportPriorities.h"
#include "NSIExportSet.h"
#include "NSIExportShadingGroup.h"
#include "NSIExportShader.h"
#include "NSIExportStandIn.h"
#include "NSIExportTransform.h"
#include "NSIExportUtilities.h"
#include "NSIExportYeti.h"
#include "RenderPassInterface.h"
#include "dlSet.h"
#include "dlViewport.h"

#include <assert.h>
#include <algorithm>
#include <filesystem>
#include <thread>

struct CachedExporterData
{
	NSICustomExporter* m_exporter;
	void* m_data;
};

const MCallbackId NSIExport::k_null_callback;

NSIExport::NSIExport(
	MObject &i_pass,
	bool i_canApplyOverrides,
	MComputation* i_computation )
:
	m_context( NSI_BAD_CONTEXT ),
	m_export_mode(e_invalid),
	m_isOutput(0),
	m_motion_blur(0),
	m_max_motion_steps(1),
	m_shutter_angle(2.0 * M_PI),
	m_computation( i_computation )
{
	/* Check the type id of this render pass node. */
	MFnDependencyNode render_pass(i_pass);
	if (render_pass.typeId() == MU_typeIds::DL_RENDERSETTINGS)
	{
		m_render_pass.reset(
			new RenderPassInterface(i_pass, i_canApplyOverrides) );
	}
	else if (render_pass.typeId() == MU_typeIds::DL_VIEWPORTRENDERSETTINGS)
	{
		m_render_pass.reset(
			new dlViewportRenderPass( i_pass, i_canApplyOverrides ) );
	}

	assert( m_render_pass.get() );
	if( !m_render_pass->isValid() )
	{
		MGlobal::displayError( "3Delight for Maya: invalid render pass" );
		return;
	}

	/* Get various quantities from the render pass. */
	if( m_render_pass->layerNSIOutput())
	{
		m_export_mode = e_export_scene;
	}

	m_motion_blur = m_render_pass->EnableMotionBlur();

	std::vector<MObject> cameras = m_render_pass->cameras();
	assert( cameras.size() > 0 );
	if( cameras[0].isNull() )
	{
		MGlobal::displayError( "3Delight for Maya: invalid camera" );
		return;
	}

	MFnDagNode camera( cameras[0] );
	m_shutter_angle =
		getAttrDouble( camera.object(), "shutterAngle", 2.0*M_PI );
}

NSIExport::~NSIExport()
{
}

/**
	Note that when rendering an animation we don't reset the time to
	its starting position but leave it at the last frame.
*/
void NSIExport::Export( ExportMode i_mode, const MDagPath& i_root )
{
	if( !m_render_pass->RenderCurrentFrameOnly() )
	{
		if( i_mode == e_interactive )
		{
			i_mode = e_interactive_frame_sequence;
		}
		if( i_mode == e_maya_batch )
		{
			i_mode = e_maya_batch_sequence;
		}
	}

	m_export_mode = i_mode;

	m_root = i_root;

	/*
		FIXME: Setting the current pass in the renderState allows proper frame
		number expansion in strings.
	*/
	DL_renderState::getRenderState().setRenderNode( m_render_pass->name() );
	bool standin_current_frame =
		m_export_mode == e_export_archive && m_render_pass->ArchiveCurrentFrameOnly();
	bool render_current_frame = IsLive() ||
		(m_export_mode != e_export_archive && m_render_pass->RenderCurrentFrameOnly());

	if(standin_current_frame || render_current_frame)
	{
		m_frameProgressIncrement = 100;
		MTime currentTime = MAnimControl::currentTime();
		double time = currentTime.value();

		ExportFrame( time );

		/* Put back to where we were, in case time has changed. */
		MGlobal::viewFrame( time );
	}
	else
	{
		assert(IsSequence() || i_mode == e_export_scene);
		/* In sequence render mode we setup a sequence context. */
		if( IsSequence() )
		{
			assert( m_context == NSI_BAD_CONTEXT );
			CreateSequenceContext();
		}

		double start_frame = m_render_pass->StartFrame();
		double end_frame = m_render_pass->EndFrame();

		double t;
		double increment = m_render_pass->Increment();

		/*
			Define the progress bar value range we can use for one frame
		*/
		int num_frames = ( end_frame - start_frame ) / increment + 1;
		m_frameProgressIncrement = 100 / num_frames;

		for( t=start_frame; t<=end_frame; t+=increment )
		{
			ExportFrame( t );

			SetProgressValue( t * m_frameProgressIncrement );

			if( CancelRequested() )
			{
				break;
			}
		}

		if( IsSequence() )
		{
			assert( m_sequence_context );
			m_sequence_context->RenderControl(
				NSI::StringArg("action", "endsequence") );
			/* Batch sequence waits right now. */
			if( m_export_mode == e_maya_batch_sequence )
			{
				m_sequence_context->RenderControl(
					NSI::StringArg("action", "wait") );
				m_sequence_context.reset();
			}
		}

		/* Time could have been moved around because of motion. */
		MGlobal::viewFrame( t-increment );
	}

	if( CancelRequested() )
	{
		RenderStoppedCallback(
			(void*)this, m_context, NSIRenderAborted );
	}

	if( !IsSequence() )
	{
		InitiateRender();
	}

	/* FIXME: see comment at previous renderstate call above. */
	DL_renderState::getRenderState().setRenderNode( "" );
}

void NSIExport::InitiateRender()
{

	NSI::Context nsi(m_context);

	/** For non-batch renders, use lower priority */
	if( !IsBatch() && m_export_mode != e_export_scene )
	{
		nsi.SetAttribute(
			NSI_SCENE_GLOBAL, NSI::IntegerArg("renderatlowpriority", 1) );
	}

	NSI::ArgumentList argList;
	argList.Add( new NSI::CStringPArg( "action", "start" ) );

	bool live = IsLive();
	if( live || m_export_mode == e_interactive )
	{
		if( live )
		{
			RegisterIPRCallbacks();
		}

		bool progressive = live ? 1 : m_render_pass->IsProgressive();

		argList.Add( new NSI::IntegerArg( "interactive", live ) );
		argList.Add( new NSI::IntegerArg( "progressive", progressive ) );
		argList.Add( new NSI::PointerArg(
					"stoppedcallback",
					(void*) NSIExport::RenderStoppedCallback ) );
		argList.Add( new NSI::PointerArg( "stoppedcallbackdata", this ) );
	}
	else if( IsBatch() )
	{
		/* Disable progressive in offline renders. */
		argList.Add( new NSI::IntegerArg( "progressive", 0 ) );
	}

	argList.Add( new NSI::CStringPArg( "action", "start") );

	nsi.RenderControl( argList );

	/*
		In may batch render mode, we must wait for the render to finish.
		Note that we don't issue an "wait" in e_scene_export mode as
		we that wait is issued when rendering that NSI file using rendredl.
	*/
	if( m_export_mode == e_maya_batch )
	{
		nsi.RenderControl( NSI::CStringPArg( "action", "wait" ) );
	}
}

void NSIExport::ExportFrame( double i_time )
{
	/*
		FIXME: Setting the current frame in the renderState allows proper
		frame number expansion in strings.
	*/
	DL_renderState::getRenderState().setCurrentFrame( i_time );

	MGlobal::viewFrame( i_time );

	if( CancelRequested() )
		return;

	Begin( i_time );

	if( m_export_mode != e_export_archive )
	{
		/*
			Unconsequential for render but ends up in the Activities in user's
			account (3Delight Cloud)
		*/
		ExportRenderNotes( i_time );

		NSI::Context(m_context).SetAttribute(
			NSI_SCENE_GLOBAL, NSI::IntegerArg("defaultassetdepth", 1));
	}

	/* This will be updated as we scan through the DAG */
	m_max_motion_steps = 1;

	ExportObjectAttributeNodes();
	NSIExportShader::CreateTextureUVCoordNode( m_context );
	ExportTransparentSurface();

	MIntArray filter;
	if( m_export_mode == e_export_archive )
	{
		GetArchivableObjectTypes( filter );
	}
	else
	{
		GetRenderableObjectTypes( filter );
	}

	bool cancel = false;

	m_archive_bbox.clear();
	ScanDAG( filter );

	if( CancelRequested() )
	{
		End();
		return;
	}

	/*
		This step may take a while because of the texture conversion process.
		Either fix the texture issue or check for interruption inside.
	*/
	ScanDependencyNodes();

	if( CancelRequested() )
	{
		End();
		return;
	}

	OverrideNumMotionSamples();
	RegisterDlSets();
	SetProgressValue( GetProgressValue() + m_frameProgressIncrement * 0.1 );
	SetDelegateAttributes();

	if( CancelRequested() )
	{
		End();
		return;
	}

	SetProgressValue( GetProgressValue() + m_frameProgressIncrement * 0.1 );
	SetDelegateAttributesAtTime( i_time );

	if( CancelRequested() )
	{
		End();
		return;
	}

	SetDelegateFinalized();

	NSI::Context nsi(m_context);

	/*
		The remaining stuff doesn't need to be in an archive, so we can clock
		out early if that's what we're doing.
	*/
	if(m_export_mode == e_export_archive)
	{
		if(m_root.length() > 0)
		{
			MFnDagNode root_node(m_root);
			nsi.Connect(
				NSIExportDelegate::NSIHandle(root_node, IsLive()).asChar(), "",
				NSI_SCENE_ROOT, "objects");
		}

		End();
		ExportArchiveSidecar();
		return;
	}

	IsolateSelection();

	ExportAtmosphere();
	ExportImageLayers();

	ExportMatteObjects(true);
	ExportQualityGroup();
	ExportOverridesGroup();
	ExportFrameID( i_time );

	if( m_export_mode == e_export_scene || IsSequence() )
	{
		/*
			We are writing an NSI to file. We need to put a RenderControl
			"start" in there as we will be executing renderdl on the file.
			The "wait" is inside "renderdl"
		*/
		nsi.RenderControl( NSI::CStringPArg( "action", "start") );
		End();
	}
}

/**
	\brief Sets the differents IDs needed for 3Delight Cloud.

	notes
		Is needed on 3delight.com -> 3Delight Cloud -> Activities

	frameid and project
		Both are passed back to 3Delight Display and shown in the 3Delight
		Cloud Dashboard.

	ATTENTION: do not do arbitrary cahges to formatting is this might affect
	the layout of the UI and website.
*/
void NSIExport::ExportRenderNotes( double frame_num ) const
{
	if( m_context == NSI_BAD_CONTEXT )
	{
		assert( false );
		return;
	}

	if( m_export_mode == e_export_archive )
	{
		/* Doesn't make sense in this export mode. */
		return;
	}

	MStatus status;
	MString scene_name =
		MGlobal::executeCommandStringResult("file -q -sn",false,false,&status);

	std::string frameid = Utilities::FrameID( frame_num );
	std::string project = scene_name.asChar();
	try
	{
		project = std::filesystem::path{project}.filename().string();
	}
	catch(...) {}

	NSI::Context nsi( m_context );
	nsi.SetAttribute(
		NSI_SCENE_GLOBAL,
		(
			NSI::StringArg("statistics.frameid", frameid.c_str()),
			NSI::StringArg("statistics.project", project.c_str())
		) );

	if( m_export_mode == e_export_scene )
	{
		/* renderdl will inject its own notes. */
		return;
	}

	MString notes;

	if( scene_name == "" )
	{
		notes = "No scene name";
	}

	notes += "\n";
	notes += m_render_pass->name();
	notes += "  |  ";
	notes += frameid.c_str();

	nsi.SetAttribute(
		NSI_SCENE_GLOBAL,
		(
			NSI::StringArg("statistics.notes", notes.asChar())
		) );
}

/**
*/
void NSIExport::Begin( double i_time )
{
	assert( m_context == NSI_BAD_CONTEXT );

	/*
		When exporting a sequence, the frames are tied to a sequence context.
		3Delight takes care of storing, rendering, and deleting them.
	*/
	if( IsSequence() )
	{
		assert( m_sequence_context );
		NSI::Context nsi;
		nsi.Begin( NSI::IntegerArg(
			"sequencecontext", m_sequence_context->Handle()) );
		nsi.Detach();
		m_context = nsi.Handle();
		return;
	}

	if( m_export_mode == e_maya_batch )
	{
		NSI::Context nsi;
		nsi.Begin(NSI::StringArg("software", "MAYA_BATCH"));
		nsi.Detach();
		m_context = nsi.Handle();
		return;
	}

	if( m_export_mode != e_export_scene && m_export_mode != e_export_archive )
	{
		NSIErrorHandler_t eh = NSIErrorHandlerMaya;
		static_assert(sizeof(eh) == sizeof(void*));
		NSI::Context nsi;
		nsi.Begin((
			NSI::StringArg("software", IsLive() ? "MAYA_LIVE" : "MAYA"),
			NSI::IntegerArg("readpreferences", 1),
			NSI::IntegerArg("separateprocess", UseSeparateProcess()),
			NSI::PointerArg("errorhandler", reinterpret_cast<const void*>(eh))
			));
		nsi.Detach();
		m_context = nsi.Handle();
		return;
	}

	/*
		FIXME: maybe we should do some verifications here about the path ? Is
		it writable ? Is it correct? Do we have to create the directory
		structure for it?
	*/
	MString currently_exported_file = m_render_pass->expandedNSIFilename();
	const char *output = currently_exported_file.asChar();
	const char *streamformat = getenv( "_3DFM_NSI_STREAM_FORMAT" );
	if( streamformat == 0x0 )
	{
		streamformat = "autonsi";
	}

	NSIParam_t streamParams[2];
	streamParams[0].name = "streamfilename";
	streamParams[0].data = &output;
	streamParams[0].type = NSITypeString;
	streamParams[0].count = 1;
	streamParams[0].flags = 0;

	streamParams[1].name = "streamformat";
	streamParams[1].data = &streamformat;
	streamParams[1].type = NSITypeString;
	streamParams[1].count = 1;
	streamParams[1].flags = 0;


	m_context = NSIBegin( 2, streamParams );
}

void NSIExport::FinalizeCustomExporters()
{
	// Finalize custom exporters.
	NSIUtilsImp utils(IsLive());

	for( const auto &item : m_customExporters )
	{
		CachedExporterData* expData = item.second.get();

		if( expData && expData->m_exporter )
		{
			NSICustomExporter::FinalizeFunc finalizeFunc =
				expData->m_exporter->m_finalize;

			if( finalizeFunc )
			{
				(*finalizeFunc)( m_context, utils, expData->m_data );
			}
		}
	}
}

void NSIExport::End()
{
	FinalizeCustomExporters();

	NSIEnd( m_context );
	m_context = NSI_BAD_CONTEXT;

	if( m_export_mode == e_export_scene || m_export_mode == e_export_archive )
	{
		MString msg("Scene exported to: \"");
		msg += m_render_pass->expandedNSIFilename();
		msg += "\"";

		MGlobal::displayInfo(msg);
	}

	m_delegates.Clear();
	m_exported_dag_nodes.Clear();
	m_customExporters.clear();
}

void NSIExport::ExportArchiveSidecar()const
{
	assert(m_export_mode == e_export_archive);

	MString nsi_name = m_render_pass->expandedNSIFilename();
	unsigned nsi_size = nsi_name.numChars();

	MString nsj_name;
	if(nsi_size > 4 &&
		nsi_name.substringW(nsi_size-4, nsi_size-1) == MString(".nsi"))
	{
		nsj_name = nsi_name.substringW(0, nsi_size-5);
	}
	else
	{
		nsj_name = nsi_name;
	}
	nsj_name += ".nsj";

	FILE* nsj_file = fopen(nsj_name.asChar(), "w");
	if(!nsj_file)
	{
		MGlobal::displayError(
			"3Delight for Maya: unable to open archive descriptor file '" +
			nsj_name +
			"' for output while exporting archive '" + nsi_name + "'." );
		return;
	}

	MPoint min = m_archive_bbox.min();
	MPoint max = m_archive_bbox.max();

	fprintf(nsj_file,
R"JSON({
  "boundingbox": {
    "min": [ %#.17g, %#.17g, %#.17g ],
    "max": [ %#.17g, %#.17g, %#.17g ]
  }
})JSON",
		min[0], min[1], min[2],
		max[0], max[1], max[2]);

	fclose(nsj_file);
}


/**
	\brief Scan the DAG and build our NSI export delegate list.
*/
void NSIExport::ScanDAG(
	MIntArray &i_filter,
	bool i_first_time )
{
	MStatus status( MStatus::kFailure );
#if 0
	MObject environment_object = m_render_pass->Environment();

	MString environment_handle = environment_object.isNull() ?
		"" : NSIExportDelegate::NSIHandle( environment_object, IsLive() );
#endif

	/*
		Track if a given node has been sampled at the specified time.
		This prevents outputing instances multiple times (m_isOutput).
	*/
	m_isOutput = MFnDependencyNode::allocateFlag( DL_plugin::name(), &status );

	if( status == MStatus::kFailure )
	{
		assert( false );
		return;
	}

	clearFlag(m_isOutput);

	/**
		Prepare two string sets with objects to render and lights to render.
		We will use these sets to assess if an object should be part of this
		render or no.

		\see ReIssueDAGObjects for live render mode set selection.
	*/
	bool has_objects_set;
	std::unordered_set<std::string> objects_set = FlattenObjectSet(
		m_render_pass->ObjectsToRender(), false, &has_objects_set);

	bool has_lights_set;
	std::unordered_set<std::string> lights_set = FlattenObjectSet(
		m_render_pass->LightsToRender(), true, &has_lights_set);
	if( !has_lights_set )
	{
		MObject lightsToRender( m_render_pass->LightsToRender() );
		MFnDependencyNode depFn( lightsToRender );
		if( depFn.typeName() == MString( "lightGroup" ) )
		{
			has_lights_set = true;
			NSIExportUtilities::FlattenMayaLightGroup(
				lightsToRender,
				IsLive(),
				lights_set );
		}
	}

	/* Same for the advanced control sets. */
	std::unordered_set<std::string> force_objects_set = FlattenObjectSet(
		m_render_pass->ForceObjects(), false, nullptr);
	std::unordered_set<std::string> exclude_objects_set = FlattenObjectSet(
		m_render_pass->ExcludeObjects(), false, nullptr);

	std::unordered_set<std::string> force_lights_set = FlattenObjectSet(
		m_render_pass->ForceLights(), true, nullptr);
	std::unordered_set<std::string> exclude_lights_set = FlattenObjectSet(
		m_render_pass->ExcludeLights(), true, nullptr);

	std::vector<int> object_set_types{MFn::kGeometric};
	std::vector<int> light_set_types{
		MFn::kLight, MFn::kPluginShape, MFn::kPluginLocatorNode};

	m_forced_visible_nodes.Clear();
	/*
		Force visibility of objects in the various 'force' sets. This works on
		dependency nodes only so it can force too much visibility if a group is
		invisible and only some nodes under it are in the set. Not a real
		problem so far for intended use.
	*/
	m_forced_visible_nodes.AddSetDagNodes(
		m_render_pass->ForceObjects(), object_set_types, true);
	m_forced_visible_nodes.AddSetDagNodes(
		m_render_pass->ForceLights(), light_set_types, true);

	/**
		Prepare a list of all paths leading to cameras. This is needed to
		force connection of hidden cameras.

		Why is this needed ? Because hideen maya objects are made non-renderable
		by not connecting them to NSI's root node. But cameras are an exception
		and we still need to connect them to root in order to use them in the
		display driver chain.

		\see IsObjectVisible
	*/
	std::vector<MObject> camera_shapes;
	std::unordered_set<std::string> camera_paths;
	MIntArray types; types.append(MFn::kCamera);
	MFnDagNode null_node;
	NSIExportUtilities::FlattenMayaTree( null_node, types, camera_shapes );

	for( int i=0; i<camera_shapes.size(); i++ )
	{
		assert( camera_shapes[i].hasFn(MFn::kCamera) );
		MFnDagNode node( camera_shapes[i] );
		MDagPath path; node.getPath( path );
		do
		{
			camera_paths.emplace(path.fullPathName().asChar());
		}
		while( path.pop() );
	}

	/* Track visibility when exporting for IPR. */
	unsigned invisibleParentDepth = UINT_MAX;

	/*
		Filter only the objects that we are interested in.
	*/
	MIteratorType itrFilter;
	itrFilter.setFilterList( i_filter );

	/*
		Iterate over the scene DAG.
		Note: MItDag::kBreadthFirst has a severe (about 10x) performance
		penality.
	*/
	MItDag itr( itrFilter, MItDag::kDepthFirst, &status );
	if( m_root.isValid() )
	{
		/*
			If we're not exporting from the scene root, reset the iterator.
			Calling reset() seems like the only way to tell the iterator to
			start elsewhere, as there is no corresponding constructor.
			Also, the "object" parameter seems to be required even though the
			documentation seems to indicate that only one of the second (object)
			and third (path) parameters are required. If we only pass m_root,
			the call to reset() fails and the iterator stays on the scene's
			root.
		*/
		MObject object = MFnDagNode(m_root).object();
		itr.reset( itrFilter, &object, &m_root, MItDag::kDepthFirst );
	}
	itr.traverseUnderWorld(true);
	for( ; !itr.isDone(); itr.next() )
	{
		/*
			Creating the MFnDagNode from the current DAG path instead of
			MItDag::currentItem() allows proper DAG path retrieval by
			MFnDagNode::getPath() later, which is issued to figure the current
			node's parent.
		*/
		MDagPath path;
		itr.getPath(path);

		MFnDagNode currNode(path, &status);

		bool invisible = itr.depth() > invisibleParentDepth;
		if( !invisible )
		{
			/* We moved to a potentially visible sibling, reset this. */
			invisibleParentDepth = UINT_MAX;
		}

		/*
			Skip 'default' nodes; so far they all seem irrelevant or viewport-
			specific.
		*/
		if(status != MStatus::kSuccess || currNode.isDefaultNode())
		{
			continue;
		}

		if(!IsObjectRenderable(currNode))
		{
			continue;
		}

		/*
			We can usually ignore invisible objects. But not in IPR as they may
			become visible later and we want them already connected to the
			right shader, etc. We still delay setting most attributes until
			that happens for better performance.
		*/
		if( !IsObjectVisible(currNode) &&
			camera_paths.count(path.fullPathName().asChar()) == 0 &&
			!IsObjectUsedByInstancer(currNode) )
		{
			if( IsLive() )
			{
				invisible = true;
				invisibleParentDepth =
					std::min(invisibleParentDepth, itr.depth());
			}
			else
			{
				itr.prune();
				continue;
			}
		}

		/*
			If we are re-scanning the DAG, make sure not to issue duplicate
			delegates. This should have no noticeable effect on rendering but
			will be very inefficient (twice the callbacks, twice the data,
			etc...)
		*/
		if( !i_first_time )
		{
			const MString handle =
				NSIExportDelegate::NSIHandle( currNode, IsLive() );
			if( m_delegates.ContainsDagNode(handle.asChar()) )
			{
				continue;
			}
		}

		MString nsi_handle = NSIExportDelegate::NSIHandle( currNode, IsLive() );

		/*
			Ignore objects that are not part of the scene elements.
		*/
		if( IsGeo( currNode ) )
		{
			bool ignore = false;
			if( has_objects_set
				&& objects_set.find(nsi_handle.asChar()) == objects_set.end() )
			{
				ignore = true;
			}
			if( force_objects_set.size()
				&& force_objects_set.find(nsi_handle.asChar())
					!= force_objects_set.end() )
			{
				ignore = false;
			}
			if( exclude_objects_set.find(nsi_handle.asChar())
				!= exclude_objects_set.end() )
			{
				ignore = true;
			}
			if( ignore )
			{
				continue;
			}
		}

		bool object_is_environment =
			currNode.typeId() == MU_typeIds::DL_ENVIRONMENTNODE;

		if( IsLight( currNode ) )
		{
			bool ignore = false;
			if( has_lights_set
				&& lights_set.find(nsi_handle.asChar()) == lights_set.end() )
			{
				ignore = true;
			}
			if ( force_lights_set.size()
				&& force_lights_set.find(nsi_handle.asChar())
					!= force_lights_set.end() )
			{
				ignore = false;
			}
			if( exclude_lights_set.find(nsi_handle.asChar())
				!= exclude_lights_set.end() )
			{
				ignore = true;
			}
			if( ignore )
			{
				continue;
			}
		}

		/* Record which nodes get through the above filtering so the ones which
		   don't will not get exported as dependency nodes either. */
		m_exported_dag_nodes.AddNode(currNode.object());

		if( m_export_mode == e_export_archive )
		{
			/*
				Don't include environment nodes since we don't want to archive
				any light source. This is a special case that is not trapped by
				the filter, since we want to include other plugin shapes, such
				as our Yeti node.
			*/
			if( object_is_environment )
			{
				continue;
			}

			// Update the archive's bounding box
			if( currNode.object().hasFn(MFn::kGeometric) )
			{
				MBoundingBox node_bbox = currNode.boundingBox(&status);
				if(status == MStatus::kSuccess)
				{
					/*
						Transforming by an inverse is bad : it would be better
						to compute the transform from the object to m_root.
					*/
					node_bbox.transformUsing(path.exclusiveMatrix());
					node_bbox.transformUsing(m_root.exclusiveMatrixInverse());
					m_archive_bbox.expand(node_bbox);
				}
			}
		}

		/*
			Create a transform that is used as a connection anchor point for
			instance-specific connections.
		*/
		MString instanceTransformHandle =
			NSIExportDelegate::NSIInstanceHandle( currNode, IsLive() );

		if( instanceTransformHandle.length() > 0 )
		{
			NSI::Context nsi( m_context );
			nsi.Create( instanceTransformHandle.asChar(), "transform" );
			/* Set nicename now as this has no delegate to do it later. */
			MString nn = NSIExportDelegate::NSIInstanceHandle(currNode, false);
			nsi.SetAttribute(instanceTransformHandle.asChar(),
				NSI::StringArg("nicename", nn.asChar()));
		}

		NSIExportDelegate *delegate = 0x0;
		if( CreateDelegate(path, currNode, delegate) )
		{
			itr.prune();
		}

		if( delegate )
		{
			/*
				We take this opportunity to create the NSI node and also output
				time invariants. We could do the SetAttributes() later but the
				Create() has to be done here. This is important because we
				connect to parent just below.

				The SetAttributes was moved outside here (where it was
				producing a slightly better NSI output) to after the
				RegisterDlSets call.
			*/
			delegate->Create();

			if( invisible )
			{
				/* Record this on the delegate so we can delay export. */
				delegate->SetVisible(false);
			}
			else if( !i_first_time )
			{
				/*
					In live re-export, we won't have the occasion to do the
					following operations, so to them now.
				*/
				assert( IsLive() );
				delegate->SetAttributesAtTime( 0., true );
				delegate->Connect( m_delegates.DagHashTable() );
				delegate->Finalize();
			}
		}

		/*
			Note that we still connect this graph location to it's parent even
			if the shape has already been created (delegate == null). For
			instance, this happens with instances :).

			We don't connect hidden objects, but we still connect cameras
			because we need them for the display chain.

			We don't connect environment lights that are not the selected
			environment light.

			We don't connect incandescence light since they are actually sets
			and have their own semantics.

			We don't connect a filter shader as the node is created via
			NSIExportShader.
		*/

		bool is_light_filter_node = currNode.typeId() == MU_typeIds::DL_GOBOFILTER
								 || currNode.typeId() == MU_typeIds::DL_DECAYFILTER;

		if( IsObjectVisible(currNode) ||
			camera_paths.count(path.fullPathName().asChar()) != 0 )
		{
			if( object_is_environment )
			{
				connectToParent( m_context, currNode );
			}
			else if( currNode.typeId() != MU_typeIds::DL_INCANDESCENCELIGHT
					&& !is_light_filter_node)
				connectToParent( m_context, currNode );
		}

		if( currNode.object().hasFn( MFn::kGeometric ) && !is_light_filter_node)
		{
			ConnectToObjectAttributesNodes( currNode );
		}

		/*
			This object has already been created.
		*/
		if( !delegate )
			continue;

		m_delegates.AddDagDelegate( delegate );
	}
	MFnDependencyNode::deallocateFlag(DL_plugin::name(), m_isOutput);

	m_isOutput = 0;
}

/**
	\brief Scan the dependency nodes

	Dependency nodes include things like sets and materials
*/
void NSIExport::ScanDependencyNodes()
{
	/*
		Using an itemFilter with a partial list of the dozens of MFn related to
		built-in shading nodes appears to be slightly slower than using a
		filter-less iterator, skipping almost all DAG nodes and testing for
		proper classification strings.
	*/

	m_references.clear();
	MItDependencyNodes iterator;
	for (; !iterator.isDone(); iterator.next())
	{
		MObject node = iterator.thisNode();
		if(node.hasFn(MFn::kReference))
		{
			m_references.append(node);
		}

		if( node.hasFn(MFn::kDagNode) && !m_exported_dag_nodes.Contains(node) )
		{
			/* This was hidden or not in "objects to render". */
			continue;
		}

		NSIExportDelegate* delegate = CreateDelegate( node );

		if( !delegate )
		{
			/** \ref CreateDelegate to see how this can happen */
			continue;
		}

		delegate->Create();

		m_delegates.AddDependencyDelegate( delegate );
	}
}

/**
	\brief Flatten one of the objects/lights to render sets.

	\param i_set
		The set to flatten.
	\param i_is_light
		If true, this is a lights to render set. Otherwise, objects.
	\param o_is_valid
		If not nullptr, set to true if the provided object is a valid set.
	\returns
		A set of NSI handles to contained shapes.
*/
std::unordered_set<std::string> NSIExport::FlattenObjectSet(
	MObject i_set,
	bool i_is_light,
	bool *o_is_valid)
{
	MStatus status;
	MFnSet set_fn(i_set, &status);
	if( o_is_valid )
	{
		*o_is_valid = status;
	}
	if( !status )
		return {};

	MIntArray types;
	if( i_is_light )
	{
		/* Possible function sets for light DAG nodes. */
		types.append(MFn::kLight);
		types.append(MFn::kPluginShape);
		types.append(MFn::kPluginLocatorNode);
	}
	else
	{
		types.append(MFn::kGeometric);
	}
	std::unordered_set<std::string> handles;
	NSIExportUtilities::FlattenMayaSet(set_fn, types, IsLive(), handles);
	return handles;
}

/**
	\brief Creates a delegate out of a dependency node.
*/
NSIExportDelegate* NSIExport::CreateDelegate( MObject& node )
{
	const NSIExportDelegate::Context export_context(m_context, IsLive());

	/*
		Ignore all DAG nodes except the ones that could possibly be shading
		nodes: place3DTexture and plug-in DAG nodes.
	*/
	if( node.hasFn( MFn::kDagNode ) &&
		! ( node.hasFn( MFn::kPlace3dTexture ) ||
			node.hasFn( MFn::kPluginShape ) ||
			node.hasFn( MFn::kPluginLocatorNode ) ) )
	{
		return nullptr;
	}

	NSIExportDelegate *delegate = 0x0;

	MFnDependencyNode dep_node( node );
	/*
		It is important the check kShadingEngine before kSet because the
		later contains the former.
	*/
	if( node.hasFn(MFn::kShadingEngine) )
	{
		delegate = new NSIExportShadingGroup( dep_node, node, export_context );
	}
	else if( node.hasFn(MFn::kSet) )
	{
		if( dep_node.name() == "defaultLightSet" )
		{
			delegate = new NSIExportDefaultLightSet(
				dep_node, node, export_context );
		}
		else
		{
			delegate = new NSIExportSet(
				*m_render_pass, dep_node, node, export_context );
		}
	}
	else if( node.hasFn(MFn::kLightLink) )
	{
		delegate = new NSIExportLightLink( dep_node, node, export_context );
	}
	else if( node.hasFn(MFn::kPlace3dTexture) )
	{
		delegate = new NSIExportPlace3dTexture( node, export_context );
	}
	else if( dep_node.typeName() == MString( "lightGroup" ) )
	{
		delegate = new NSIExportLightGroup( node, export_context );
	}
	else if( dep_node.typeId() == MU_typeIds::DL_GOBOFILTER )
	{
		delegate = new NSIExportGoboFilter( node, export_context );
	}
	else
	{
		if(( node.hasFn( MFn::kPluginDependNode ) ||
			node.hasFn( MFn::kPluginShape ) ||
			node.hasFn( MFn::kPluginLocatorNode ) ) )
		{
			/*
				Check for the existence of a custom delegate for the plug-in
				defined node, and use it if it exists.
			*/
			delegate = CreateCustomDelegate( node );
		}

		if( !delegate && NSIExportUtilities::IsShadingNode( node ) )
		{
			delegate = new NSIExportShader( node, export_context );
		}
	}

	return delegate;
}

/*
	\brief Generate NSI output for a specific dag node position

	\return True if pruning can can be performed on that Maya iterator.
*/
bool NSIExport::CreateDelegate(
	MDagPath &i_path,
	MFnDagNode& i_dagNode,
	NSIExportDelegate *& o_delegate,
	bool i_live )
{
	/*
		Build the appropriate delegate for this node.
	*/
	bool pruneChildren = false;
	o_delegate = 0x0;

	int num_motion_samples = m_motion_blur ? 2 : 1;

	bool is_transform = false;
	bool is_output = i_live ? false : i_dagNode.isFlagSet( m_isOutput );

	/*
		FIXME : parameter "i_live" seems redundant with already existing
		IsLive() accessor, at first, but then it's actually true only in one
		context (when called from NodeAddedCB), so it's probably not very
		reliable. So, we use the more consistent IsLive() accessor when
		initializing the delegate's export context.
	*/
	const NSIExportDelegate::Context export_context(m_context, IsLive());

	if( i_dagNode.object().hasFn(MFn::kTransform) )
	{
		if( is_output )
		{
			/*
				If this transform has already been output, its children
				have been too.  Return true to avoid processing them.
			*/
			pruneChildren = true;
		}
		else
		{
			if( i_dagNode.object().hasFn(MFn::kInstancer) )
			{
				o_delegate = new NSIExportInstancer(
					i_path, i_dagNode, export_context);
			}
			else
			{
				o_delegate = new NSIExportTransform(i_dagNode, export_context);
				is_transform =  true;
			}
		}
	}
	else if( !is_output)
	{
		if( i_dagNode.object().hasFn(MFn::kMesh) )
		{
			o_delegate = new NSIExportMesh( i_dagNode, export_context );
		}
		else if(i_dagNode.object().hasFn(MFn::kNurbsSurface) )
		{
			o_delegate = new NSIExportNurbsSurface( i_dagNode, export_context );
		}
		else if( i_dagNode.object().hasFn(MFn::kCamera) )
		{
			o_delegate =
				new NSIExportCamera( *m_render_pass, i_dagNode, export_context );
		}
		else if( i_dagNode.typeId() == MU_typeIds::DL_ENVIRONMENTNODE  ||
			i_dagNode.object().hasFn(MFn::kLight) )
		{
			o_delegate = new NSIExportLight( i_dagNode, export_context );
		}
		else if( i_dagNode.object().hasFn(MFn::kPluginShape) )
		{
			MString type_name = i_dagNode.typeName();
			if( i_dagNode.typeId().id() == NSIExportOpenVDB::MayaDAGTypeID() )
			{
				o_delegate =
					new NSIExportOpenVDB( i_dagNode.object(), export_context );
			}
			else if( i_dagNode.typeName() == NSIExportYeti::MayaDAGNodeName() )
			{
				o_delegate = new NSIExportYeti( i_dagNode, export_context );
			}
			else if( i_dagNode.typeId() == MU_typeIds::DL_INCANDESCENCELIGHT )
			{
				o_delegate =
					new NSIExportIncandescenceLight( i_dagNode, export_context );
			}
			else if( i_dagNode.typeId() == MU_typeIds::DL_DECAYFILTER ||
					i_dagNode.typeId() == MU_typeIds::DL_GOBOFILTER )
			{
				/*
					Light filters are create later via NSIExportShader.
					Don't bother to connect these kind of nodes to
					"geometryattributes".
				*/
				pruneChildren = true;
			}
			else
			{
				MObject obj( i_dagNode.object() );
				o_delegate = CreateCustomDelegate( obj );
			}
		}
		else if( i_dagNode.object().hasFn(MFn::kParticle) )
		{
			o_delegate = new NSIExportParticle(i_dagNode, export_context);
		}
		else if(
			i_dagNode.object().hasFn(MFn::kPfxGeometry) ||
			i_dagNode.object().hasFn(MFn::kPfxHair) )
		{
			o_delegate = new NSIExportPfxGeometry( i_dagNode, export_context );
		}
		else if( i_dagNode.typeId() == MU_typeIds::DL_STANDIN )
		{
			o_delegate =
				new NSIExportStandIn( i_dagNode.object(), export_context );
		}
		else if( i_dagNode.object().hasFn(MFn::kPluginLocatorNode ) )
		{
			MObject obj( i_dagNode.object() );
			o_delegate = CreateCustomDelegate( obj );
		}
		else if( i_dagNode.object().hasFn(MFn::kImagePlane) )
		{
			o_delegate = new NSIExportImagePlane( i_dagNode, export_context );
		}
	}

	if( o_delegate )
	{
		if( m_motion_blur )
		{
			int additional_samples = getAttrInt(
				i_dagNode.object(),
				"_3delight_additional_motion_samples", 0 );

			if( is_transform )
			{
				num_motion_samples += additional_samples;
			}
			else
			{
				/*
					0=detect, 1=always, 2=off
				*/
				int deformation = getAttrInt(
					i_dagNode.object(),
					"_3delight_deformation_motion_blur", 0 );

				if( deformation == 0  )
				{
					deformation = o_delegate->IsDeformed() ? 1 : 2;
				}

				if( deformation == 1 )
				{
					num_motion_samples += additional_samples;
				}
				else
				{
					assert( deformation == 2 /* off */ );
					num_motion_samples = 1;
				}
			}

			if( num_motion_samples > m_max_motion_steps )
				m_max_motion_steps = num_motion_samples;

			o_delegate->SetMotionSamples( num_motion_samples );
		}
	}
	else if( !is_output &&
			i_dagNode.typeId() != MU_typeIds::DL_DECAYFILTER &&
			i_dagNode.typeId() != MU_typeIds::DL_GOBOFILTER )
	{
		MGlobal::displayWarning( "3Delight for Maya: no DAG exporter for " +
			i_path.fullPathName() + " of type " +
			i_dagNode.object().apiTypeStr() + "." );
	}


	if( !i_live && !is_output )
	{
		i_dagNode.setFlag(m_isOutput, 1);
	}

	return pruneChildren;
}

/*
	NOTE: if a plug-in has already registered as a dag node, we don't
	re-register it as a dependency node.
*/
NSIExportDelegate* NSIExport::CreateCustomDelegate( MObject& node )
{
	MString h = NSIExportDelegate::NSIHandle( node, IsLive() );
	if( m_delegates.ContainsDagNode(h.asChar()) )
	{
		return nullptr;
	}

	// Attempt to use a custom NSI exporter.
	MString typeName = MFnDependencyNode( node ).typeName();
	CachedExporterData* expData = 0x0;

	// Check if we already figured how to handle this specific node type
	auto it = m_customExporters.find(typeName);
	if( it != m_customExporters.end() )
	{
		expData = it->second.get();
	}
	else
	{
		/*
			Try to execute a "<node_type_name>Exporter" command; if one
			exists, we expect it to return a pointer to a NSICustomExporter
			struct, formated as a string.
		*/
		MString cmd = typeName + "Exporter";
		cmd = MString( "if(`exists \"" ) + cmd + "\"` ) " + cmd;
		MStatus status;
		MString result = MGlobal::executeCommandStringResult(
			cmd, false, false, &status );

		void* ptr = 0x0;
		if( status == MStatus::kSuccess )
		{
			sscanf( result.asChar(), "%p", &ptr );
		}

		/*
			Cache the result. If the command failed, we store a null pointer
			so we know that there is nothing to be done with other nodes of
			this type.
		*/
		expData = new CachedExporterData;
		expData->m_exporter = (NSICustomExporter*)ptr;

		m_customExporters.emplace(typeName, expData);

		/*
			Call the initialize function if one is defined. This is done
			only once per custom exporter.
		*/
		if( expData->m_exporter && expData->m_exporter->m_initialize )
		{
			NSIUtilsImp utils(IsLive());

			NSICustomExporter::InitializeFunc initFunc =
				expData->m_exporter->m_initialize;

			(*initFunc)( m_context, utils, expData->m_data );
		}
	}

	/*
		Create a custom delegate
	*/
	if( expData &&
		expData->m_exporter &&
		expData->m_exporter->m_createDelegate )
	{
		return new NSIExportCustom(
			expData->m_exporter,
			node,
			NSIExportDelegate::Context(m_context, IsLive()),
			expData->m_data );
	}

	return 0x0;
}

void NSIExport::SetDelegateAttributes()
{
	m_delegates.ResetIterator();

	NSIExportDelegate* delegate = m_delegates.Next();
	while( delegate != 0x0 )
	{
		delegate->SetNiceName();
		if( delegate->Visible() )
		{
			delegate->SetAttributes();
		}
		delegate = m_delegates.Next();
	}

	/*
		Override nice names for nodes that come from a reference. The reason we
		do it as a post-process is that it's easy to list all nodes coming from
		a single reference, but we haven't found a way to do the opposite (ie :
		finding the reference an arbitrary node originates from).
	*/
	SetReferencesNiceName();
}

void NSIExport::SetDelegateAttributesAtTime( double i_time )
{
	m_delegates.ResetIterator();

	const bool live = IsLive();

	/*
		Export all time varying attributes using their delegate.

		Only one time step is exported when no motion blur is required by the
		caller or when the objects to export have a constant geometry. The
		latter case only occurs when exporting a single shape to an archive, as
		adding a transform to the mix will automatically assume the need for
		more than one time sample.
	*/
	if( !m_motion_blur || m_max_motion_steps == 1)
	{
		NSIExportDelegate *delegate;
		while( (delegate = m_delegates.Next()) )
		{
			if( !delegate->Visible() )
				continue;
			delegate->Connect( m_delegates.DagHashTable() );
			delegate->SetAttributesAtTime( 0, true /* mo motion */ );
		}
	}
	else
	{
		/*
			Build a quantized time table and sort all objects using a stupid
			bin sort!
		*/
		struct TimeSlot
		{
			double m_time;
			std::vector<NSIExportDelegate*> m_objects;
		};

		TimeSlot *tslots = new TimeSlot[ m_max_motion_steps ];
		double *time_attr = new double[m_max_motion_steps];

		double shutter_interval = m_shutter_angle / (2.0 * M_PI);
		double sample_delta = shutter_interval / double(m_max_motion_steps-1);

		std::vector<MObject> cameras = m_render_pass->cameras();
		assert( cameras.size() > 0 );

		int shutter_position = getAttrInt(
			cameras[0], "_3delight_shutter_position", 0 );
		double shutter_open_offset = -shutter_interval / 2.0; /* center */

		if( shutter_position == 1 /* start on frame */ )
		{
			shutter_open_offset = 0;
		}
		else if( shutter_position == 2 /* end on frame */ )
		{
			shutter_open_offset = -shutter_interval;
		}

		for( int i=0; i<m_max_motion_steps; i++ )
		{
			tslots[i].m_time = shutter_open_offset + double(i)*sample_delta;
			time_attr[i] = (double)(-shutter_interval / 2.0) + double(i)*sample_delta;
		}

		/*
			Dispatch each object to it's time slot. Objects that are not moving
			are exported on the spot.
		*/
		NSIExportDelegate *delegate;
		while( (delegate = m_delegates.Next()) )
		{
			if( !delegate->Visible() )
				continue;
			delegate->Connect( m_delegates.DagHashTable() );

			int num_samples = delegate->MotionSamples();
			if( num_samples == 1 )
			{
				delegate->SetAttributesAtTime( 0, true /* no motion */ );
				continue;
			}

			for( int s=0; s<num_samples; s++ )
			{
				double t = double(s)/((double)num_samples-1);
				int slot = int(t*(m_max_motion_steps-1) + 0.5);

				tslots[slot].m_objects.push_back( delegate );
			}
		}

		int stepProgressIncrement =
			m_frameProgressIncrement * 0.8 / m_max_motion_steps;

		/*
			Now, by God's will, loop through all the time samples and output
			NSI attributes.
		*/
		for( int i=0; i<m_max_motion_steps; i++ )
		{
			if( CancelRequested() )
			{
				break;
			}

			double current_time = tslots[i].m_time;

			MGlobal::viewFrame( i_time + current_time );

			for( size_t j=0; j<tslots[i].m_objects.size(); j++ )
			{
				NSIExportDelegate *d = tslots[i].m_objects[j];
				d->SetAttributesAtTime( time_attr[i], false );
			}

			SetProgressValue( GetProgressValue() + stepProgressIncrement );
		}

		delete [] tslots;
		delete [] time_attr;
	}
}

/**
	Call Finalize() on all delegates.
*/
void NSIExport::SetDelegateFinalized()
{
	m_delegates.ResetIterator();
	NSIExportDelegate *delegate;
	while( (delegate = m_delegates.Next()) )
	{
		if( !delegate->Visible() )
			continue;
		delegate->Finalize();
	}
}

NSIExportDelegate* NSIExport::GetDelegate( const MString& handle )
{
	/* Try to get the delegate directly. */
	NSIExportDelegate* delegate = m_delegates.Find( handle.asChar() );

	return delegate;
}

void NSIExport::OverrideNumMotionSamples()
{
	if( !m_motion_blur )
		return;

	/* Iterate on dep nodes of type nsiSet */
	MStatus status;
	MItDependencyNodes iterator( MFn::kSet );

	std::unordered_set<std::string> set_members;

	for (; !iterator.isDone(); iterator.next())
	{
		MObject node = iterator.thisNode();
		MFnSet setFn( node );

		if( setFn.typeId() != MU_typeIds::DL_SET )
			continue;

		/*
			Check if the set has the number of extra transform samples
			override enabled.
		*/
		bool enable_trs = false;

		MPlug enable_trs_plug = setFn.findPlug(
			"enableExtraTransformSamples", true, &status );

		if( status == MStatus::kSuccess )
		{
			enable_trs_plug.getValue( enable_trs );
		}

		/*
			Check if the set has the number of extra deform samples override
			enabled.
		*/
		bool enable_def = false;
		MPlug enable_def_plug = setFn.findPlug(
			"enableExtraDeformSamples", true, &status );

		if( status == MStatus::kSuccess )
		{
			enable_def_plug.getValue( enable_def );
		}

		if( !enable_trs && !enable_def )
			continue;

		/*
			Find the extra number of samples of the enabled overrides
		*/
		int trs_samples = 0;
		int def_samples = 0;

		if( enable_trs )
		{
			MPlug trs_samples_plug =
				setFn.findPlug( "extraTransformSamples", true, &status );

			if( status == MStatus::kSuccess )
			{
				trs_samples_plug.getValue( trs_samples );
				trs_samples += 2;
			}
		}

		if( enable_def )
		{
			MPlug def_samples_plug =
				setFn.findPlug( "extraDeformSamples", true, &status );

			if( status == MStatus::kSuccess )
			{
				def_samples_plug.getValue( def_samples );
				def_samples += 2;
			}
		}

		/*
			Iterate on set members to override their number of samples
		*/
		MSelectionList membersList;
		setFn.getMembers( membersList, true );

		MItSelectionList membersIterator( membersList );
		for( ; !membersIterator.isDone(); membersIterator.next() )
		{
			MDagPath memberPath;
			membersIterator.getDagPath( memberPath );

			/*
				Consider overriding transform samples for transform nodes, if
				the override is enabled on the set.
			*/
			bool override_trs_samples =
				enable_trs && memberPath.node().hasFn( MFn::kTransform );

			/*
				Consider overriding deform samples for geometric nodes and
				cameras, if the override is enabled on the set.
			*/
			bool override_def_samples = enable_def &&
				( memberPath.node().hasFn( MFn::kGeometric ) ||
					memberPath.node().hasFn( MFn::kCamera ) );

			if( !override_trs_samples && !override_def_samples )
				continue;

			int num_samples = override_trs_samples ? trs_samples : def_samples;
			num_samples += 2;

			if( !set_members.emplace(memberPath.fullPathName().asChar())
					.second )
			{
				MString msg( "3Delight for Maya: More than one set define the " );
				msg += ("'Additional " );

				if( override_trs_samples )
					msg += "Transformation ";
				else
					msg += "Deformation ";

				msg += "Motion Samples' override for object '";
				msg += memberPath.fullPathName();
				msg += "'. Results are unknown.";

				MGlobal::displayWarning( msg );
			}

			/*
				Find the delegate of the set member
			*/
			MFnDagNode dagNode( memberPath.node() );
			MString handle = NSIExportDelegate::NSIHandle( dagNode, IsLive() );
			NSIExportDelegate* delegate = m_delegates.Find( handle.asChar() );

			assert( delegate );
			if( !delegate )
				continue;

			/*
				For deformation blur, we still need to honour the object's
				"_3delight_deformation_motion_blur" extension attribute
			*/
			if( override_def_samples )
			{
				/*
					0=detect, 1=always, 2=off
				*/
				int deformation = getAttrInt(
					memberPath.node(),
					"_3delight_deformation_motion_blur", 2 );

				if( deformation == 0  )
				{
					deformation = delegate->IsDeformed() ? 1 : 2;
				}

				if( deformation != 1 )
				{
					continue;
				}
			}

			delegate->SetMotionSamples( num_samples );

			if( m_max_motion_steps < num_samples )
				m_max_motion_steps = num_samples;
		}
	}
}

/**
	\brief Loops through all NSI sets and register them with the contained
	delegate(s).

	This will allow the delegate to reference the NSI set when needed (e.g. a
	mesh will know if the "poly as subd" override is active or not).
*/
void NSIExport::RegisterDlSets()
{
	/* Iterate on dep nodes of type nsiSet */
	MItDependencyNodes iterator( MFn::kSet );
	for (; !iterator.isDone(); iterator.next())
	{
		MObject node = iterator.thisNode();
		MFnSet setFn( node );
		if( setFn.typeId() != MU_typeIds::DL_SET )
			continue;

		RegisterDlSetToOneDelegate( node, false /* not live */ );
	}
}

void NSIExport::RegisterDlSetToOneDelegate( MObject &setFn, bool i_edit)
{
	std::unordered_set<std::string> nsiset;
	MIntArray types; types.append(MFn::kGeometric); types.append(MFn::kPfxHair);
	NSIExportUtilities::FlattenMayaSet( setFn, types, IsLive(), nsiset);

	for( const auto &member : nsiset )
	{
		NSIExportDelegate *delegate = m_delegates.Find( member.c_str() );

		if( delegate )
		{
			delegate->MemberOfSet( setFn );
			if( i_edit )
			{
				delegate->SetAttributes();
				delegate->SetAttributesAtTime( 0, true );
			}
		}
	}
}

void NSIExport::ExportObjectAttributeNodes()
{
	/* These are implicit here. Not set and 0 is the default. */
	static_assert(NSI_MATTE_OBJ_PRIORITY == 0);
	static_assert(NSI_PRELIT_OBJ_PRIORITY == 0);
	static_assert(NSI_VISIBILITY_OBJ_PRIORITY == 0);

	NSI::Context nsi( m_context );
	for( int i = 0; i != e_invalidAttribute; i++ )
	{
		GeoAttribute type = (GeoAttribute)i;
		const char* handle = GeoAttributeNodeHandle( type );
		nsi.Create( handle, "attributes" );

		nsi.SetAttribute(
			handle,
			NSI::IntegerArg(
				GeoAttributeNSIName( type ),
				GeoAttributeNSIDefaultValue( type ) ? 0 : 1 ) );
	}
}

void NSIExport::ConnectToObjectAttributesNodes( MFnDagNode& i_dagNode )
{
	for( int i = 0; i != e_invalidAttribute; i++ )
	{
		GeoAttribute type = (GeoAttribute)i;
		AdjustGeoAttributeConnection( m_context, i_dagNode, type,
			/* first time = */ true, IsLive() );
	}
}

const char* NSIExport::GeoAttributeNodeHandle( GeoAttribute i_type )
{
	switch( i_type )
	{
		case e_matte: return "3delight_matte";
		case e_prelit: return "3delight_prelit";
		case e_visCamera: return "3delight_invisibleToCameraRays";
		case e_visShadow: return  "3delight_invisibleToShadowRays";
		case e_visDiffuse: return "3delight_invisibleToDiffuseRays";
		case e_visReflection: return  "3delight_invisibleToReflectionRays";
		case e_visRefraction: return "3delight_invisibleToRefractionRays";
		case e_invalidAttribute:
		default:
			assert( false );
	}

	return "";
}

const char* NSIExport::GeoAttributeNSIName( GeoAttribute i_type )
{
	switch( i_type )
	{
		case e_matte: return "matte";
		case e_prelit: return "prelit";
		case e_visCamera: return "visibility.camera";
		case e_visShadow: return "visibility.shadow";
		case e_visDiffuse: return "visibility.diffuse";
		case e_visReflection: return "visibility.reflection";
		case e_visRefraction: return "visibility.refraction";
		case e_invalidAttribute: default: assert( false );
	}

	return "";
}

const char* NSIExport::GeoAttributeMayaName( GeoAttribute i_type )
{
	switch( i_type )
	{
		case e_matte:
		case e_prelit:
			return "_3delight_compositingMode";
		case e_visCamera: return "primaryVisibility";
		case e_visShadow: return "castsShadows";
		case e_visDiffuse: return "_3delight_visibilityDiffuse";
		case e_visReflection: return "visibleInReflections";
		case e_visRefraction: return "visibleInRefractions";
		case e_invalidAttribute:	default: assert( false ); return "";
	}

	return "";
}

bool NSIExport::GeoAttributeNSIDefaultValue( GeoAttribute i_type )
{
	switch( i_type )
	{
		case e_matte:
		case e_prelit:
			return false;
		default:
			return true;
	}
}

bool NSIExport::GeoAttributeHasMayaDefaultValue(
	GeoAttribute i_type,
	MPlug i_attr )
{
	switch( i_type )
	{
		/*
			Matte and prelit require special processing because they're separate
			boolean attribute in NSI but are implemented by the same enum
			attribute in 3DFM, to make them mutually exclusive.
		*/
		case e_matte:
		{
			// Matte is off by default, which is all enum values except 1
			int value = 0;
			i_attr.getValue(value);
			return value != 1;
		}
		case e_prelit:
		{
			// Prelit is off by default, which is all enum values except 2
			int value = 0;
			i_attr.getValue(value);
			return value != 2;
		}
		default:
		{
			// Visibility attributes are all on by default
			bool value = false;
			i_attr.getValue(value);
			return value;
		}
	}
}

void NSIExport::AdjustGeoAttributeConnection(
	NSIContext_t i_nsi,
	MFnDagNode& i_dagNode,
	GeoAttribute i_type,
	bool i_first_time,
	bool i_live)
{
	MString maya_attr = GeoAttributeMayaName( i_type );

	MStatus status;
	MPlug plug = i_dagNode.findPlug( maya_attr, true, &status );

	if( status == MStatus::kSuccess )
	{
		MString dag_handle = NSIExportDelegate::NSIHandle( i_dagNode, i_live );
		NSI::Context nsi( i_nsi );

		if( !GeoAttributeHasMayaDefaultValue( i_type, plug ) )
		{
			nsi.Connect(
				GeoAttributeNodeHandle( i_type ), "",
				dag_handle.asChar(), "geometryattributes");
		}
		else if( !i_first_time )
		{
			// The disconnect operation is needed during live render.
			nsi.Disconnect(
				GeoAttributeNodeHandle( i_type ), "",
				dag_handle.asChar(), "geometryattributes");
		}
	}
}

const char* NSIExport::TransparentSurfaceHandle()
{
	return "3delight_transparent_surface";
}

void NSIExport::ExportTransparentSurface()
{
	NSI::Context nsi( m_context );

	MString shaderHandle = MString(TransparentSurfaceHandle()) + "|shader";
	nsi.Create(TransparentSurfaceHandle(), "attributes");
	nsi.Create(shaderHandle.asChar(), "shader");

	MString shader = MString(getenv("DELIGHT"));
	shader += "/osl/transparent.oso";

	const char* shader_cstr = shader.asChar();

	nsi.SetAttribute(
		shaderHandle.asChar(),
		NSI::CStringPArg("shaderfilename", shader_cstr));

	NSI::ArgumentList argList;
	argList.Add(new NSI::IntegerArg("priority", 60));

	nsi.Connect(
		shaderHandle.asChar(), "",
		TransparentSurfaceHandle(), "surfaceshader", argList );
}

MString NSIExport::AtmosphereAttributesNodeHandle()
{
	return MString("3delight_render_pass_atmosphere_attributes");
}

/**
	\brief Exports the shading network attached to the .atmosphere attribute of
	the RenderPassInterface as well as all the NSI mechnanics to render the
	atmosphere.

	Note that we add the Shader to the list of delegates so that all the
	Set/Connect/.. calls are performed at the same time as all the other
	delegates.
*/
NSIExportDelegate* NSIExport::ExportAtmosphere()
{
	NSI::Context nsi( m_context );

	MObject atm = m_render_pass->Atmosphere();

	if( !atm.hasFn(MFn::kDependencyNode) )
		return 0x0;

	MString atmHandle = NSIExportDelegate::NSIHandle(atm, IsLive());

	nsi.Create( atmHandle.asChar(), "environment" );
	nsi.Connect( atmHandle.asChar(), "", NSI_SCENE_ROOT, "objects" );

	nsi.Create( AtmosphereAttributesNodeHandle().asChar(), "attributes" );

	MFnDependencyNode dependency_node(atm);
	NSIExportDelegate::Context export_context(m_context, IsLive());
	NSIExportShader *shader = new NSIExportShader( atm, export_context );
	shader->Create();
	shader->SetAttributes();
	shader->SetAttributesAtTime( 0.0, true /* no motion*/ );
	shader->Connect( 0x0 );
	shader->Finalize();

	nsi.Connect(
		shader->Handle(), "",
		AtmosphereAttributesNodeHandle().asChar(), "volumeshader" );

	nsi.Connect(
		AtmosphereAttributesNodeHandle().asChar(), "",
		atmHandle.asChar(), "geometryattributes" );

	m_delegates.AddDelegate( shader );

	return shader;
}

/**
	\brief Exports matte related attributes in the layers sections.
	Do not confuse this with per-object matte attributes.
*/
void NSIExport::ExportMatteObjects( bool i_first_time ) const
{
	NSI::Context nsi( m_context );

	const char *k_set_matte = "3delight_set_matte";

	if( !i_first_time )
	{
		/* This will also remove connections */
		nsi.Delete( k_set_matte );
	}

	MObject matte_objects = m_render_pass->MatteObjects();

	if (!matte_objects.hasFn(MFn::kSet))
		return;

	int has_matte = 1;
	nsi.Create( k_set_matte, "attributes" );
	nsi.SetAttribute( k_set_matte,
		(
			NSI::IntegerArg("matte", has_matte),
			NSI::IntegerArg("matte.priority", NSI_MATTE_RENDERSETTINGS_PRIORITY)
		) );

	const MObject &object = matte_objects;
	assert( object.hasFn(MFn::kSet) );
	MString handle = NSIExportDelegate::NSIHandle( object, IsLive() );

	nsi.Connect(
		k_set_matte, "", handle.asChar(), "geometryattributes" );
}

/**
	\brief Export everything related to pass's Image Layers.

	\sa RenderPassInterface
*/
void NSIExport::ExportImageLayers( void )
{
	NSI::Context nsi( m_context );

	MIntArray layer_indices;
	m_render_pass->layerIndices( layer_indices );

	bool popup_render_view = false;

	RenderPassInterface::eFileOutputMode output_mode =
		RenderPassInterface::e_disabled;

	switch (m_export_mode)
	{
	case e_maya_batch:
	case e_maya_batch_sequence:
	case e_export_scene:
	case e_interactive_frame_sequence:
	case e_interactive:
		output_mode = RenderPassInterface::e_enabled;
		break;
	default:
		output_mode = RenderPassInterface::e_disabled;
	}


	/* Collect all cameras. */
	std::vector<MObject> cameras = m_render_pass->cameras();

	/* Collect the light catagories. */
	MStringArray light_categories;
	MObjectArray objLight_categories;
	m_render_pass->layerLightCategories( light_categories, objLight_categories );

	/*
		Make sure a NSI node exist for each light category. This will make a multi-
		light layer appear empty when the light / set is not visible for instance,
		which is more appropriate than the default "rgba" that would else be
		produced because the light set NSI connection would involve a non-existing
		node.
	*/
	for( unsigned int i = 0; i < objLight_categories.length(); i++ )
	{
		if( objLight_categories[i].isNull() )
		{
			continue;
		}

		MString handle =
			NSIExportDelegate::NSIHandle( objLight_categories[i], IsLive() );

		// Avoid producing pointless NSICreate statements
		if( m_delegates.Find( handle.asChar() ) == 0x0 )
		{
			nsi.Create( handle.asChar(), "transform" );
		}
	}

	unsigned sort_key = 0;

	for( int c = 0; c<cameras.size(); c ++ )
	{
	/*
		This is not indented so not to fuck up git history in the loop
		below.
	*/
	MFnDagNode cam( cameras[c] );
	MString camera = cam.fullPathName();

	MString screen = NSIExportDelegate::NSIHandle( cam, IsLive() ) + "|screen";

	assert( cameras[c].hasFn(MFn::kCamera) );
	MFnCamera mfn_cam(cameras[c]);

	NSIExportUtilities::ExportScreen(
		m_context, *m_render_pass, mfn_cam, screen, false, IsLive() );

	/* Record handles so we can connect the image_plane aov as background of
	   the RGB output once both are exported. */
	MString Ci_fb_handle, imageplane_fb_handle,
		Ci_file_handle, imageplane_file_handle;

	for( int i = 0; i < layer_indices.length(); i++ )
	{
		int index = layer_indices[ i ];

		//Skip if layer is not selected to be rendered
		bool render_layer = m_render_pass->layerOutput(index);
		if (!render_layer)
		{
			continue;
		}

		bool fb_output = m_render_pass->layerFramebufferOutput(IsLive());

		bool file_output, jpg_output;

		m_render_pass->LayerEffectiveFileOutput(
			index,
			output_mode,
			file_output,
			jpg_output,
			IsLive());

		if( IsBatch() || m_export_mode == e_export_scene )
		{
			// No frame buffer output in batch mode and export mode. Only file output.
			file_output = true;
			fb_output = false;
		}

		MString variable_source, layer_type, aov, aov_token, aov_label, description;
		int with_alpha;
		m_render_pass->layerVariable(
			index,
			variable_source,
			layer_type,
			with_alpha,
			aov,
			aov_token,
			aov_label,
			&description);
		MString file = m_render_pass->layerFilename( index );
		MString format = m_render_pass->layerScalarFormat( index );

		if( cameras.size() > 1 && file.rindexW("<camera>")==-1 )
		{
			MGlobal::displayError( "3Delight for Maya: layer " + aov +
				" doesn't contain the <camera> tag in its file name so "
				"it can't be rendered using a stereo camera." );
			continue;
		}

		MString fb_driver = FramebufferDriver();

		MString cmd = MString( "DL_expandLayerFilename(" );
		cmd += "\"" + file + "\", ";
		cmd += "\"" + m_render_pass->name() + "\", ";
		cmd += "\"\", ";  // no aov token for a shared driver
		cmd += "\"" + fb_driver + "\", ";
		cmd += "\"\", "; // no light token for a shared driver
		cmd += "\"" + camera + "\", ";
		cmd += "0, 0 )";
		MString fb_filename = MGlobal::executeCommandStringResult( cmd );

		/*
			If the AOV is not a shading component, limit the number of light
			categories to 1. This will use only the first category in the
			list, which is the "universal" category that includes all light
			sources.
		*/
		assert(light_categories.length() > 0);
		assert(light_categories[0] == "");
		int shading_component = 1;
		MGlobal::executeCommand(
			"DOV_isShadingComponent(\"" + description + "\")",
			shading_component );
		bool render_multi_light = m_render_pass->layerMultiLight(index);
		unsigned nb_light_categories =
			shading_component && render_multi_light
			? light_categories.length()
			: 1;

		for( int j = 0; j < nb_light_categories; j++ )
		{
			MString light( light_categories[ j ] );
			MString light_token = LightTokenFromLightCategory( light );

			if( light == "__incandescence" )
			{
				// incandescence light group is not currently supported
				continue;
			}

			MString layer_base_handle;
			layer_base_handle += index;
			layer_base_handle +=
				"-" + aov_token + "-" + light + "-" + cam.name() + "-";

			if( fb_output )
			{
				MString fb_format = format;

				/*
					Maya render view requires float data between 0 and 255.0.
					This replicates the RenderMan behaviour, where a quantize
					scales and clamps values in that range, and the display
					driver requires float data. Not super obvious. FIXME: this
					should be done in the display driver.
				*/
				if( fb_driver == "maya_render_view" )
				{
					fb_format = "uint8";
					popup_render_view = true;
				}

				bool create_node = false;
				MString framebuffer_handle = OutputDriverHandle(
					fb_filename, fb_driver, create_node );

				if( create_node )
				{
					nsi.Create( framebuffer_handle.asChar(), "outputdriver" );

					nsi.SetAttribute( framebuffer_handle.asChar(),
						(
							NSI::StringArg( "drivername", fb_driver.asChar() ),
							NSI::StringArg("imagefilename",fb_filename.asChar())
						) );

					if( fb_driver == "maya_render_view" )
					{
						nsi.SetAttribute( framebuffer_handle.asChar(),
							(
								NSI::StringArg( "cameraname", camera.asChar() )
							) );
					}
				}

				/*
					FIXME: Improve output to fb drivers that do not support
					multi layer
				*/
				MString layer_handle = layer_base_handle + "fb-" + fb_format;

				ExportOneOutputLayer(
					layer_handle,
					index,
					variable_source,
					layer_type,
					aov,
					with_alpha,
					fb_format,
					screen,
					light_categories[j],
					objLight_categories[j],
					framebuffer_handle,
					fb_driver,
					sort_key );

				if( fb_driver == "idisplay"  &&
					(m_export_mode == e_interactive || m_export_mode == e_live) &&
					(!m_render_pass->DisableExtraImageLayers() || aov == "Ci"))
				{
					ExportLayerFeedbackData( layer_handle, light );
				}

				if( aov == "Ci" )
					Ci_fb_handle = layer_handle;
				else if( aov == "image_plane")
					imageplane_fb_handle = layer_handle;
			}

			/*
				FIXME: Outputing multiple layers to a single file may not work if the
				selected driver does not support that. The last flag of
				DL_expandLayerFilename could be used to generate a unique file name.
				This should be done only if the layer is using the default filename.

				For now, not attempting to fiddle with this since this may change the
				filename even when there is a single layer set to produce a file, which
				would not necessarily require a filename change (assuming there is no
				multi-light selection).
			*/
			if( file_output )
			{
				MString driver = m_render_pass->layerDriver( index );

				int generate_unique_filename = 0;

				MString cmd = MString( "DL_expandLayerFilename(" );
				cmd += "\"" + file + "\", ";
				cmd += "\"" + m_render_pass->name() + "\", ";
				cmd += "\"" + aov_token + "\", ";
				cmd += "\"" + driver + "\", ";
				cmd += "\"" + light_token + "\", ";
				cmd += "\"" + camera + "\", ";
				cmd += "1, ";
				cmd += generate_unique_filename;
				cmd += " )";

				MString filename = MGlobal::executeCommandStringResult( cmd );

				bool create_node = false;
				MString driver_handle = OutputDriverHandle(
					filename, driver, create_node );

				if( create_node )
				{
					nsi.Create( driver_handle.asChar(), "outputdriver" );
					nsi.SetAttribute( driver_handle.asChar(),
						(
							NSI::StringArg( "drivername", driver.asChar() ),
							NSI::StringArg( "imagefilename", filename.asChar() )
						) );
				}

				MString layer_handle = layer_base_handle + "file-" + format;
				ExportOneOutputLayer(
					layer_handle,
					index,
					variable_source,
					layer_type,
					aov,
					with_alpha,
					format,
					screen,
					light_categories[j],
					objLight_categories[j],
					driver_handle,
					driver,
					sort_key );

				if( aov == "Ci" )
					Ci_file_handle = layer_handle;
				else if( aov == "image_plane")
					imageplane_file_handle = layer_handle;
			}

			//Output only beauty leayer on jpeg image file.
			if( jpg_output && aov == "Ci")
			{
				MString jpg_format( "uint8" );
				MString driver( "jpeg" );

				int generate_unique_filename = 0;

				if( !DriverSupportsMultipleLayers( driver ) &&
					m_render_pass->LayerUsesDefaultFilename( index ) )
				{
					generate_unique_filename = 1;
				}

				MString cmd = MString( "DL_expandLayerFilename(" );
				cmd += "\"" + file + "\", ";
				cmd += "\"" + m_render_pass->name() + "\", ";
				cmd += "\"" + aov_token + "\", ";
				cmd += "\"" + driver + "\", ";
				cmd += "\"" + light_token + "\", ";
				cmd += "\"" + camera + "\", ";
				cmd += "1, ";
				cmd += generate_unique_filename;
				cmd += " )";

				MString filename = MGlobal::executeCommandStringResult( cmd );

				bool create_node = false;
				MString driver_handle = OutputDriverHandle(
					filename, driver, create_node );

				if( create_node )
				{
					nsi.Create( driver_handle.asChar(), "outputdriver" );
					nsi.SetAttribute( driver_handle.asChar(),
						(
							NSI::StringArg( "drivername", driver.asChar() ),
							NSI::StringArg( "imagefilename", filename.asChar() )
						) );
				}

				MString layer_handle = layer_base_handle + "jpg-" + jpg_format;
				ExportOneOutputLayer(
					layer_handle,
					index,
					variable_source,
					layer_type,
					aov,
					0,
					jpg_format,
					screen,
					light_categories[j],
					objLight_categories[j],
					driver_handle,
					driver,
					sort_key );
			}
		}
	}
	/* Specify that "image_plane" is the background of the main layer. */
	if( Ci_fb_handle.length() != 0 && imageplane_fb_handle.length() != 0 )
	{
		nsi.Connect(
			imageplane_fb_handle.asChar(), "",
			Ci_fb_handle.asChar(), "backgroundlayer");
	}
	if( Ci_file_handle.length() != 0 && imageplane_file_handle.length() != 0 )
	{
		nsi.Connect(
			imageplane_file_handle.asChar(), "",
			Ci_file_handle.asChar(), "backgroundlayer");
	}

	}

	if( popup_render_view &&
			( m_export_mode == NSIExport::e_live ||
				m_export_mode == NSIExport::e_interactive ) )
	{
		MGlobal::executeCommand( MString( "DRV_popupMayaRenderViewWindow \"\"" ) );
	}
}

/**
	TODO: i_aov contains the type of the variable which is not so good. The
	best would be, maybe, to have it as a separate string ?
*/
void NSIExport::ExportOneOutputLayer(
		const MString& i_handle,
		int i_index,
		const MString& i_variable_source,
		const MString& i_layer_type,
		const MString& i_aov,
		int i_alpha,
		const MString& i_format,
		const MString& i_screen,
		const MString& i_lightCategory,
		const MObject& i_lightCategoryObject,
		const MString& i_driver_handle,
		const MString& i_driver_name,
		unsigned& io_sort_key )
{
	if (m_render_pass->DisableExtraImageLayers() && i_aov != "Ci")
	{
		return;
	}
	NSI::Context nsi( m_context );

	nsi.Create( i_handle.asChar(), "outputlayer" );

	nsi.SetAttribute( i_handle.asChar(),
		(
			NSI::StringArg( "variablename", i_aov.asChar() ),
			NSI::StringArg( "layertype", i_layer_type.asChar() ),
			NSI::StringArg( "scalarformat", i_format.asChar() ),
			NSI::IntegerArg( "withalpha", i_alpha ),
			NSI::IntegerArg( "sortkey", io_sort_key++ ),
			NSI::StringArg( "variablesource", i_variable_source.asChar() )
		) );

	if(i_aov == "relighting_multiplier")
	{
		nsi.SetAttribute( i_handle.asChar(),
			(
				NSI::CStringPArg( "filter", "box" ) ,
				NSI::DoubleArg( "filterwidth", 1.0 )
			) );
		// Setting "maximumvalue" is probably not a good idea in this case
	}
	else
	{
		nsi.SetAttribute( i_handle.asChar(),
			(
				NSI::CStringPArg( "filter", m_render_pass->PixelFilter() ) ,
				NSI::DoubleArg( "filterwidth", m_render_pass->FilterWidth() )
			) );

		bool enable_clamping = m_render_pass->EnableClamping();
		if(!enable_clamping && i_layer_type == "color")
		{
			/*
				Use 3Delight's fancy tonemapping technique. This allows
				rendering of very high dynamic range images with no ugly
				aliasing on the edges (think about area lights).
				The value here can really be very high and things will still
				work fine. Note that this also eliminates some fire flies.
			*/
			nsi.SetAttribute(
				i_handle.asChar(),
				NSI::DoubleArg( "maximumvalue", 50 ));
		}
	}

	if( i_aov == "Ci" || i_aov == "outlines" )
	{
		/* We draw outlines on "Ci" and the "outlines" AOV. */
		nsi.SetAttribute( i_handle.asChar(),
			NSI::IntegerArg( "drawoutlines", 1 ) );
	}

	if( i_format == "uint8" )
	{
		nsi.SetAttribute( i_handle.asChar(),
			NSI::StringArg("colorprofile", "srgb") );
	}

	// Decide whether to output ID AOVs in Cryptomatte format.
	unsigned cryptomatte_layers = 0;
	if(strncmp(i_aov.asChar(), "id.", 3) == 0 &&
		i_variable_source == "builtin" &&
		(i_driver_name == "exr" || i_driver_name == "deepalphaexr" ||
		 i_driver_name == "dwaaexr" || i_driver_name == "deepalphadwaaexr" ||
		 i_driver_name == "deepexr"))
	{
		cryptomatte_layers = 2;
	}

	nsi.Connect(
		i_handle.asChar(), "", i_screen.asChar(), "outputlayers" );

	if( !i_lightCategoryObject.isNull() )
	{
		/* Add component to connection, if present in category name. */
		NSI::ArgumentList ls_args;
		int sep_idx = i_lightCategory.rindexW('.');
		if( -1 != sep_idx )
		{
			MString c = i_lightCategory.substringW(
				sep_idx + 1, i_lightCategory.numChars() - 1);
			ls_args.Add(new NSI::StringArg("component", c.asChar()));
		}
		MString light_handle =
			NSIExportDelegate::NSIHandle(i_lightCategoryObject, IsLive());
		nsi.Connect(
			light_handle.asChar(), "", i_handle.asChar(), "lightset", ls_args);
	}

	if( i_lightCategory.length() != 0 )
	{
		/* Provide a nice name for the light set. */
		MString label = MGlobal::executeCommandStringResult(
			"RS_getLightGroupLabel(\"" + i_lightCategory + "\")");
		nsi.SetAttribute(i_handle.asChar(),
			NSI::StringArg("lightsetname", label.asChar()));
	}

	nsi.Connect(
		i_driver_handle.asChar(), "",
		i_handle.asChar(), "outputdrivers" );

	if(cryptomatte_layers > 0)
	{
		// Change the filter and output type to fit the Cryptomatte format
		nsi.SetAttribute(
			i_handle.asChar(),
			(
				NSI::StringArg( "layertype", "color" ),
				NSI::IntegerArg( "withalpha", 0 ),
				NSI::StringArg( "filter", "cryptomatteheader" )
			) );

		/*
			Export one additional layer per Cryptomatte level. Each will
			output 2 values from those present in each pixel's samples.
		*/
		for(unsigned cl = 0; cl < cryptomatte_layers; cl++)
		{
			MString cl_handle = i_handle + std::to_string(cl).c_str();
			nsi.Create(cl_handle.asChar(), "outputlayer");

			std::string filter("cryptomattelayer");
			filter += std::to_string(cl * 2);
			nsi.SetAttribute(
				cl_handle.asChar(),
				(
					NSI::StringArg( "variablename", i_aov.asChar() ),
					NSI::StringArg( "layertype", "quad" ),
					NSI::StringArg( "scalarformat", "float" ),
					NSI::IntegerArg( "withalpha", 0 ),
					NSI::StringArg( "filter", filter ),
					NSI::DoubleArg( "filterwidth", m_render_pass->FilterWidth() ),
					NSI::IntegerArg( "sortkey", io_sort_key++ ),
					NSI::StringArg( "variablesource", "builtin" )
				) );

			nsi.Connect(
				cl_handle.asChar(), "", i_screen.asChar(), "outputlayers" );

			nsi.Connect(
				i_driver_handle.asChar(), "",
				cl_handle.asChar(), "outputdrivers" );
		}
	}
}

void NSIExport::ExportLayerFeedbackData(
	const MString& i_handle,
	const MString& i_lightCategory ) const
{
	MString id = DL_externalCommandServer::GetServerID();

	if( i_lightCategory.length() == 0 )
	{
		NSI::Context nsi( m_context );

		nsi.SetAttribute( i_handle.asChar(),
			(
				NSI::StringArg( "sourceapp", "Maya" ),
				NSI::StringArg( "feedbackid", id.asChar() )
			));
		return;
	}

	MString cmd = "DIF_getLayerDescriptor( ";
	cmd += "\"" + i_lightCategory + "\", ";
	cmd += " { \"" + m_render_pass->name() + "\" } )";

	MStatus status;
	MString data =
		MGlobal::executeCommandStringResult( cmd, false, false, &status );

	if( status != MStatus::kSuccess )
	{
		return;
	}

	NSI::Context nsi( m_context );

	nsi.SetAttribute( i_handle.asChar(),
		(
			NSI::StringArg( "sourceapp", "Maya" ),
			NSI::StringArg( "feedbackid", id.asChar() ),
			NSI::StringArg( "feedbackdata", data.asChar() )
		));
}

MString NSIExport::LightTokenFromLightCategory(const MString& i_category) const
{
	MString cmd = MString("DL_getLightGroupToken(\"" ) + i_category + "\")";
	return MGlobal::executeCommandStringResult( cmd );
}

MString NSIExport::FramebufferDriver() const
{
	if( m_export_mode == e_live_in_vp2 )
	{
		return MString( "nsi_viewport" );
	}

	/* Replicate what is done by the DPF_getInteractiveRenderView() MEL proc. */
	bool var_exists = false;
	MString var_value =
		MGlobal::optionVarStringValue( "_3delight_render_view", &var_exists );

	if( !var_exists )
		var_value = MString("idisplay");

	return var_value;
}

bool NSIExport::DriverSupportsMultipleLayers(const MString& i_driver) const
{
	return ( i_driver == "idisplay" ||
		i_driver == "deepexr" ||
		i_driver == "exr" ||
		i_driver == "deepalphaexr" ||
		i_driver == "dwaaexr" ||
		i_driver == "deepalphadwaaexr" ||
		i_driver == "tiff" );
}

MString NSIExport::OutputDriverHandle(
	const MString& i_filename,
	const MString& i_drivername,
	bool& o_first_request )
{
	// This is about the same as what is done in 3dfk.
	o_first_request = false;
	MString key = i_drivername + "!@#$%" + i_filename;
	MString& handle = m_output_driver_map[key];

	if( handle.length() == 0 )
	{
		o_first_request = true;
		std::string buf{"driver"};
		buf += std::to_string(unsigned(m_output_driver_map.size()));
		handle = buf.c_str();
	}
	return handle;
}

/**
	IMPORTANT: we use getPath() instead of dagPath() because the later would
	fail when no function set is attached. This seems to render in IPR when we
	add objects to the DAG.
*/
void NSIExport::connectToParent(
	NSIContext_t nsi, MFnDagNode& i_dagNode, bool i_connect)
{
	MDagPath dagPath;
	/*
		Note: this rarely fails but does not always return the proper instance
		path. It returns *a* path to an instance.
	*/
	MStatus status = i_dagNode.getPath( dagPath);

	if( status != MStatus::kSuccess )
	{
		MGlobal::displayError( "3Delight for Maya: unable to get object's dag" \
				"path, live render is desynchronized." );
		return;
	}

	MString delegate( NSIExportDelegate::NSIHandle(i_dagNode, IsLive()) );
	MString instanceTransform =
		NSIExportDelegate::NSIInstanceHandle( i_dagNode, IsLive() );

	dagPath.pop();
	/*
		Underworld paths have a special node as the root of the underworld. We
		don't export an equivalent so we skip over it here.

		NSI also does not support connecting to a geometry node. So skip over
		the shape and connect to its parent transform.
	*/
	if( dagPath.apiType() == MFn::kUnderWorld )
	{
		dagPath.pop();
		if( !dagPath.hasFn(MFn::kTransform) )
		{
			dagPath.pop();
		}
	}

	MString DAGparent;

	if (dagPath.length() <= 0)
	{
		DAGparent = NSI_SCENE_ROOT;
	}
	else
	{
		MFnDagNode parentDagNode(dagPath.node());
		DAGparent = NSIExportDelegate::NSIHandle(parentDagNode, IsLive());
	}

	if( i_connect )
	{
		if( instanceTransform.length() > 0 )
		{
			NSIConnect(
				nsi,
				delegate.asChar(), "",
				instanceTransform.asChar(), "objects",
				0, 0x0);

			NSIConnect(
				nsi,
				instanceTransform.asChar(), "",
				DAGparent.asChar(), "objects",
				0, 0x0 );
		}
		else
		{
			NSIConnect(
				nsi,
				delegate.asChar(), "",
				DAGparent.asChar(), "objects",
				0, 0x0 );
		}
	}
	else
	{
		if( instanceTransform.length() > 0 )
		{
			NSIDisconnect(
				nsi, delegate.asChar(), "", instanceTransform.asChar(), "objects" );

			NSIDisconnect(
				nsi, instanceTransform.asChar(), "", DAGparent.asChar(), "objects" );
		}
		else
		{
			NSIDisconnect(
				nsi, delegate.asChar(), "", DAGparent.asChar(), "objects" );
		}
	}
}

/**
	\brief Clears a given flag on on all dag nodes
*/
void NSIExport::clearFlag(unsigned int i_flag)
{
	MItDag itr;
	itr.traverseUnderWorld(true);
	for(; !itr.isDone(); itr.next())
	{
		MFnDagNode dag_fn(itr.currentItem());
		dag_fn.setFlag(i_flag, 0);
	}
}

void NSIExport::GetArchivableObjectTypes(MIntArray& o_fnTypes)
{
	o_fnTypes.clear();
	o_fnTypes.append(MFn::kTransform);
	o_fnTypes.append(MFn::kMesh);
	o_fnTypes.append(MFn::kNurbsSurface);
	o_fnTypes.append(MFn::kInstancer);
	o_fnTypes.append(MFn::kParticle);
	o_fnTypes.append(MFn::kPfxGeometry);
	o_fnTypes.append(MFn::kPfxHair);
	o_fnTypes.append(MFn::kPluginLocatorNode);
	o_fnTypes.append(MFn::kPluginShape);
}

void NSIExport::GetRenderableObjectTypes(MIntArray& o_fnTypes)
{
	GetArchivableObjectTypes(o_fnTypes);
	o_fnTypes.append(MFn::kLight);
	o_fnTypes.append(MFn::kCamera);
	o_fnTypes.append(MFn::kImagePlane);
}

bool NSIExport::IsObjectVisible(MFnDependencyNode& i_depNode)
{
	bool visible =
		i_depNode.findPlug("visibility", true).asBool() &&
		i_depNode.findPlug("overrideVisibility", true).asBool() &&
		i_depNode.findPlug("lodVisibility", true).asBool();
	if( !visible )
	{
		/* Do this test second so we can skip it for visible nodes, which are
		   usually most nodes. */
		visible = m_forced_visible_nodes.Contains(i_depNode.object());
	}
	return visible;
}

/**
	- Intermediate objects are not renderable.
	- Texture references are not renderable.
*/
bool NSIExport::IsObjectRenderable(MFnDagNode& i_dagNode)
{
	// check DL_isObjectRenderable for opt - we could probably confine plug
	// searches to specific types / MFn
	//
	//	return DL_isObjectRenderable::isRenderable(i_dagNode);
	  /* Intermediate objects are not renderable. */
	if( i_dagNode.isIntermediateObject() )
	{
		return false;
	}

	/** reference object check */
	MStatus plug_exists;
	MPlug message_plug = i_dagNode.findPlug( "message", true, &plug_exists );
	if( plug_exists == MStatus::kSuccess )
	{
		MPlugArray plugs;
		message_plug.connectedTo( plugs, true, true );
		for( unsigned i=0; i<plugs.length(); i++ )
		{
			if( plugs[i].partialName() == "rob" )
				return false;
		}
	}

	return true;
}

/**
	\brief Returns true when the specified object is used by an instancer.

	This is not the same as a regular Maya instance, which are multiple DAG paths
	leading to the same shape, and which have proper API calls in MDagPath /
	MFnDagNode to handle.
*/
bool NSIExport::IsObjectUsedByInstancer(MFnDagNode& i_dagNode)
{
	if( !i_dagNode.object().hasFn( MFn::kTransform ) )
	{
		return false;
	}

	MStatus status;
	MPlug matrixPlug = i_dagNode.findPlug( MString( "matrix" ), true, &status );

	if( status != MStatus::kSuccess )
	{
		return false;
	}

	MPlugArray destPlugs;

#if MAYA_API_VERSION > 201600
	if( !matrixPlug.destinations( destPlugs ) )
	{
		return false;
	}
#else
	if( !matrixPlug.connectedTo( destPlugs, false, true ) )
	{
		return false;
	}
#endif

	for( unsigned i  = 0; i < destPlugs.length(); i++ )
	{
		if( destPlugs[i].node().hasFn( MFn::kInstancer ) )
		{
			return true;
		}
	}

	return false;
}

/**
	\sa RenderPassInterface
*/
void NSIExport::ExportQualityGroup( void ) const
{
	NSI::Context nsi( m_context );

	int shading_samples =
		std::max(1, (int)(m_render_pass->ShadingSamples() *
						m_render_pass->SamplingReduceFactor()+0.5));
	int volume_samples =
		std::max(1, (int)(m_render_pass->VolumeSamples() *
						m_render_pass->SamplingReduceFactor()+0.5));

	nsi.SetAttribute( NSI_SCENE_GLOBAL,
		(
			NSI::IntegerArg("quality.shadingsamples", shading_samples),
			NSI::IntegerArg("quality.volumesamples", volume_samples)
		) );

	double clamping_value = m_render_pass->ClampingValue();
	nsi.SetAttribute(NSI_SCENE_GLOBAL,
		(
			NSI::DoubleArg("clampindirect", clamping_value)
		) );

	MString k_diffuse( "Diffuse" );
	MString k_reflection( "Reflection" );
	MString k_refraction( "Refraction" );
	MString k_hair( "Hair" );

	int max_diffuse_depth = m_render_pass->MaxRayDepth( k_diffuse );
	int max_reflection_depth = m_render_pass->MaxRayDepth( k_reflection );
	int max_refraction_depth = m_render_pass->MaxRayDepth( k_refraction );
	int max_hair_depth = m_render_pass->MaxRayDepth( k_hair );

	nsi.SetAttribute( NSI_SCENE_GLOBAL,
		(
			 NSI::IntegerArg( "maximumraydepth.diffuse", max_diffuse_depth ),
			 NSI::IntegerArg( "maximumraydepth.reflection", max_reflection_depth),
			 NSI::IntegerArg( "maximumraydepth.refraction", max_refraction_depth),
			 NSI::IntegerArg("maximumraydepth.hair", max_hair_depth)
		) );

	double maximum_distance = m_render_pass->MaxDistance();
	nsi.SetAttribute( NSI_SCENE_GLOBAL,
		(
			 NSI::DoubleArg( "maximumraylength.specular", maximum_distance),
			 NSI::DoubleArg( "maximumraylength.diffuse", maximum_distance ),
			 NSI::DoubleArg( "maximumraylength.reflection", maximum_distance),
			 NSI::DoubleArg( "maximumraylength.refraction", maximum_distance),
			 NSI::DoubleArg( "maximumraylength.hair", maximum_distance)
		) );

}

void NSIExport::ExportFrameID( double i_time ) const
{
	NSI::Context nsi( m_context );
	nsi.SetAttribute( NSI_SCENE_GLOBAL, NSI::DoubleArg( "frame", i_time ) );
}

void NSIExport::ExportOverridesGroup( void ) const
{
	NSI::Context nsi( m_context );

	nsi.SetAttribute( NSI_SCENE_GLOBAL,
		(
		NSI::IntegerArg("show.displacement", !m_render_pass->DisableDisplacement()),
		NSI::IntegerArg("show.osl.subsurface", !m_render_pass->DisableSubsurface()),
		NSI::IntegerArg("show.atmosphere", !m_render_pass->DisableAtmosphere()),
		NSI::DoubleArg("show.multiplescattering", m_render_pass->DisableMultipleScattering() ? 0.0 : 1.0)
		));
}

void NSIExport::ExportCropGroup( const MString& i_screen_handle ) const
{
	assert( IsLive() );

	NSI::Context nsi( m_context );

	int resX = m_render_pass->ResolutionX();
	int resY = m_render_pass->ResolutionY();

	float crop[2][2];
	m_render_pass->Crop( crop[0], crop[1] );

	/* left, top, right, bottom. */
	int pw[4] =
	{
		static_cast<int>(crop[0][0] * resX + 0.5f),
		static_cast<int>(crop[0][1] * resY + 0.5f),
		static_cast<int>(crop[1][0] * resX + 0.5f),
		static_cast<int>(crop[1][1] * resY + 0.5f)
	};

	// prioritywindow
	nsi.SetAttribute(
		i_screen_handle.asChar(),
		*NSI::Argument( "prioritywindow" )
			.SetArrayType( NSITypeInteger, 2 )
			->SetCount( 2 )
			->SetValuePointer( pw ) );
}

void NSIExport::IsolateSelection()const
{
	NSI::Context nsi(m_context);

	nsi.Disconnect(NSI_ALL_NODES, "", NSI_SCENE_GLOBAL, "exclusiveshading");

	if(!m_render_pass->IsolateSelection())
	{
		return;
	}

	bool live = IsLive();
	if(!live && m_export_mode != e_interactive)
	{
		return;
	}

	MSelectionList selection;
	MGlobal::getActiveSelectionList(selection);
	for(MItSelectionList s(selection) ; !s.isDone(); s.next())
	{
		MObject obj;
		s.getDependNode(obj);

		MString handle;

		if(obj.hasFn(MFn::kTransform))
		{
			MDagPath path;
			s.getDagPath(path);
			path.extendToShape();
			handle = NSIExportDelegate::NSIHandle(MFnDagNode(path), live);
		}
		else if(NSIExportUtilities::IsShadingNode(obj))
		{
			handle = NSIExportDelegate::NSIShaderHandle(obj, live);
		}
		else
		{
			handle = NSIExportDelegate::NSIHandle(obj, live);
		}

		NSIExportDelegate* delegate = m_delegates.Find(handle.asChar());
		if(!delegate)
		{
			continue;
		}

		NSIExportShadingGroup* sg =	
			dynamic_cast<NSIExportShadingGroup*>(delegate);
		assert(!obj.hasFn(MFn::kShadingEngine) == !sg);
		if(sg)
		{
			handle = sg->SurfaceSGHandle();
		}

		nsi.Connect(
			handle.asChar(), "",
			NSI_SCENE_GLOBAL, "exclusiveshading");
	}
}

/**
	\brief Re-issue objects of a particular MFn::Type current NSI context.

	\param i_type
		Objects with this particular MFn::Type will be re-issued. Meant to be
		MFn::kLight or MFn::kGeometric (for use with Scene Elements feature)

	This is a far reaching operation and we have to get rid of the affected
	delegates and re-scan the scene for objects of the given type.

	We also need to re-issue dependency nodes because these will most probably
	contain connections to the scene (sets, light linker, etc..).  As I said, a
	far reaching operation that is one notch short of re-sending the entire
	scene (we can't do that because we don't want to destroy cameras,
	output drivers, etc ...).
*/
void NSIExport::ReIssueDAGObjects( MFn::Type i_type )
{
	std::vector<NSIExportDelegate *> new_delegates;

	m_delegates.DeleteDagDelegates( i_type );

	MIntArray filter; filter.append(i_type); filter.append(MFn::kTransform);
	ScanDAG( filter, false /* not first time */ );

	SetDelegateAttributes();

	/*
		Now pass through all sets, shading engines and light linker to establish
		connections. This might re-do already existing connections but that
		won't hurt anything.
	*/
	for( const auto &item : *m_delegates.DependencyHashTable() )
	{
		NSIExportDelegate *delegate = item.second.get();
		const MObject& object = delegate->Object();

		if( object.hasFn(MFn::kSet) ||
			object.hasFn(MFn::kShadingEngine) ||
			object.hasFn(MFn::kLightLink) )
		{
			delegate->Connect( m_delegates.DagHashTable() );
		}
	}
}

/**
	At this point in time we can flush all the delegates and remove callbacks.
	Callbacks must be removed first or crashes will happen (a callback might
	be called on an already deleted delegate and boom!).

	Note that deleting a delegate will also remove any associated callbacks
*/
void NSIExport::StopRender( void )
{
	assert(
		IsLive() ||
		m_export_mode == e_interactive ||
		m_export_mode == e_interactive_frame_sequence );

	if( m_export_mode == e_interactive_frame_sequence )
	{
		if( m_sequence_context )
		{
			m_sequence_context->RenderControl(
				NSI::CStringPArg( "action", "stop") );
		}
		m_sequence_context.reset();
		return;
	}

	NSI::Context nsi( m_context );
	nsi.RenderControl( NSI::CStringPArg( "action", "stop") );

	End();

	// FIXME :  what??
	MMessage::removeCallback( m_node_added_id );
	m_node_added_id = k_null_callback;
	MMessage::removeCallback( m_node_removed_id );
	m_node_removed_id = k_null_callback;
	MMessage::removeCallback( m_instance_added_id );
	m_instance_added_id = k_null_callback;
	MMessage::removeCallback( m_instance_removed_id );
	m_instance_removed_id = k_null_callback;
	MMessage::removeCallback( m_parent_added_id );
	m_parent_added_id = k_null_callback;
	MMessage::removeCallback( m_parent_removed_id );
	m_parent_removed_id = k_null_callback;
	MMessage::removeCallback( m_render_pass_id );
	m_render_pass_id = k_null_callback;
	for( MCallbackId id : m_object_sets_ids )
	{
		MMessage::removeCallback(id);
	}
	m_object_sets_ids.clear();

	if(m_selection_id != k_null_callback)
	{
		MMessage::removeCallback( m_selection_id );
		m_selection_id = k_null_callback;
	}
}

/**
	\brief Registers all necessary callbacks for a delegate node.

	As of now, the "dirty node" callback seems to be enough for all
	manipulations but we still need the attribute change because
	attributes are actually changed _after_ the dirty node callback
	is called ... It's a ham-fisted approach that works and covers
	all the cases.
*/
void NSIExport::RegisterDelegateCallbacks(
	NSIExportDelegate *i_delegate,
	NSIExport* i_exporter )
{
	if( i_delegate->RegisterCallbacks() )
		return;

	MObject object = i_delegate->Object();
	MCallbackId id;

	id = MNodeMessage::addAttributeChangedCallback(
		object,
		&AttributeChangedCallback, (void*) i_exporter );

	i_delegate->AddCallbackId( id );

	id = MNodeMessage::addNodeDirtyPlugCallback(
		object, &NodeDirtyPlugCB, (void *)i_delegate );

	i_delegate->AddCallbackId( id );
}

/**
	\brief A callback on the set that defines the ObjectsToRender

	Note that we don't do anything particular here and we simply re-issue the
	DAG objects using ReIssueDAGObjects which will issue everything as defined
	by the .objectsToRender render settings attributes.

	\see ReIssueDAGObjects
*/
void NSIExport::ObjectsToRenderCB(
	MNodeMessage::AttributeMessage i_msg,
	MPlug &/*i_plug*/,
	MPlug &/*i_otherPlug*/,
	void *i_data )
{
	if( !(i_msg & (
			MNodeMessage::kConnectionMade |
			MNodeMessage::kConnectionBroken)) )
	{
		NSIExport* nsi_export = (NSIExport*) i_data;
		nsi_export->ReIssueDAGObjects( MFn::kGeometric );

		NSI::Context nsi( nsi_export->m_context );
		nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
	}
}

/**
	\brief A callback on the set that defines the LightsToRender
	\see ObjectsToRenderCB
*/
void NSIExport::LightsToRenderCB(
	MNodeMessage::AttributeMessage i_msg,
	MPlug &/*i_plug*/,
	MPlug &/*i_otherPlug*/,
	void *i_data )
{
	if( !(i_msg & (
			MNodeMessage::kConnectionMade |
			MNodeMessage::kConnectionBroken)) )
	{
		NSIExport* nsi_export = (NSIExport*) i_data;
		nsi_export->ReIssueDAGObjects( MFn::kLight );

		NSI::Context nsi( nsi_export->m_context );
		nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
	}
}


void NSIExport::RenderPassAttributeChangedCallback(
	MNodeMessage::AttributeMessage i_msg,
	MPlug &i_plug,
	MPlug &i_otherPlug,
	void *i_data )
{
	NSIExport* nsi_export = (NSIExport*)i_data;
	nsi_export->RenderPassAttributeChanged(i_msg, i_plug, i_otherPlug);
}


void NSIExport::RenderPassAttributeChanged(
	MNodeMessage::AttributeMessage i_msg,
	MPlug &i_plug,
	MPlug &i_otherPlug )
{
	if( !(i_msg & (
		MNodeMessage::kAttributeSet |
		MNodeMessage::kConnectionMade |
		MNodeMessage::kConnectionBroken ) ) )
	{
		return;
	}

	NSI::Context nsi( m_context );

	MStatus status;
	MObject attrib = i_plug.attribute();
	MFnAttribute attribute_fn( attrib, &status );
	if( status != MS::kSuccess )
	{
		MGlobal::displayError( "3Delight for Maya: unable to get an attribute "
				"in an IPR session so render might be out of sync" );
		return;
	}

	if ( attribute_fn.name() == "disableMotionBlur" ||
		attribute_fn.name() == "disableDepthOfField" ||
		attribute_fn.name() == "enableSpeedBoost" )
	{
		std::vector<MObject> cameras = m_render_pass->cameras();

		for( int i=0; i<cameras.size(); i++ )
		{
			MString camera_handle =
				NSIExportDelegate::NSIHandle( cameras[i], IsLive() );

			NSIExportDelegate *delegate =
				m_delegates.Find( camera_handle.asChar() );

			assert( delegate );
			if( delegate )
			{
				delegate->SetAttributes();
			}
		}

		nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
		return;
	}

	int idx = 0;
	bool foundName = true;

	if(
		attribute_fn.name() == "disableDisplacement" ||
		attribute_fn.name() == "disableSubsurface" ||
		attribute_fn.name() == "disableAtmosphere" ||
		attribute_fn.name() == "enableSpeedBoost" )
	{
		ExportOverridesGroup();
	}
	else if(
		attribute_fn.name() == "samplingMultiplier" ||
		attribute_fn.name() == "maxReflectionDepth" ||
		attribute_fn.name() == "maxRefractionDepth" ||
		attribute_fn.name() == "maxDiffuseDepth" ||
		attribute_fn.name() == "maxSpecularDepth" ||
		attribute_fn.name() == "maxHairDepth" )
	{
		ExportQualityGroup();
	}
	else if( attribute_fn.name() == "environment" )
	{
		MObject environment = i_otherPlug.node();;
		assert( !environment.isNull() );
		MFnDagNode envdag( environment );

		if( i_msg & MNodeMessage::kConnectionMade )
		{
			connectToParent( m_context, envdag, true );
		}
		else if( i_msg & MNodeMessage::kConnectionBroken )
		{
			connectToParent( m_context, envdag, false );
		}
	}
	else if(
		(idx = m_render_pass->WhichObjectSet(attribute_fn.name())) != -1 &&
		idx < m_object_sets_ids.size() )
	{
		MMessage::removeCallback(m_object_sets_ids[idx]);
		m_object_sets_ids[idx] = k_null_callback;

		bool is_light;
		MObject new_set = m_render_pass->GetObjectSet(idx, is_light);

		ReIssueDAGObjects(is_light ? MFn::kLight : MFn::kGeometric);

		if( new_set.hasFn(MFn::kSet) )
		{
			m_object_sets_ids[idx] = MNodeMessage::addAttributeChangedCallback(
				new_set, is_light ? LightsToRenderCB : ObjectsToRenderCB, this);
		}
	}
	else if( attribute_fn.name() == "atmosphere" )
	{
		if( i_msg & MNodeMessage::kConnectionBroken )
		{
			MString handle =
				NSIExportDelegate::NSIHandle( i_otherPlug.node(), IsLive() );
			MString shader_handle = handle + "|shader";
			nsi.Disconnect(
				handle.asChar(), "", NSI_SCENE_ROOT, "objects" );
			nsi.Disconnect(
				shader_handle.asChar(), "",
				AtmosphereAttributesNodeHandle().asChar(), "volumeshader" );
		}
		else if( i_msg & MNodeMessage::kConnectionMade )
		{
			/*
				FIXME: we will be able to remove all this when shaders and
				shading networks are exported properly.
			*/
			MString handle =
				NSIExportDelegate::NSIHandle( i_otherPlug.node(), IsLive() );
			MString shader_handle = handle + "|shader";

			MObject atm = m_render_pass->Atmosphere();

			if( !atm.hasFn(MFn::kDependencyNode) )
				return;
			MFnDependencyNode dependency_node( atm );

			if( m_delegates.Find(handle.asChar()) )
			{
				nsi.Connect(
					dependency_node.name().asChar(), "",
					NSI_SCENE_ROOT, "objects" );
				nsi.Connect(
					shader_handle.asChar(), "",
					AtmosphereAttributesNodeHandle().asChar(), "volumeshader" );
			}
			else
			{
				NSIExportDelegate *delegate = ExportAtmosphere();
				if( delegate )
					RegisterDelegateCallbacks( delegate, this );
			}
		}
	}
	else if( attribute_fn.name() == "matteObjects" )
	{
		ExportMatteObjects( /* first time = */ false );
	}
	else if(
		attribute_fn.name() == "useCropWindow" ||
		attribute_fn.name() == "cropMinX" ||
		attribute_fn.name() == "cropMinY" ||
		attribute_fn.name() == "cropMaxX" ||
		attribute_fn.name() == "cropMaxY" )
	{
		assert(IsLive());
		std::vector<MObject> cameras = m_render_pass->cameras();

		for( int c = 0; c < cameras.size(); c++ )
		{
			MFnDagNode cam( cameras[c] );
			MString camera = cam.fullPathName();

			MString screen =
				NSIExportDelegate::NSIHandle( cam, IsLive() ) + "|screen";
			ExportCropGroup( screen );
		}
	}
	else if(attribute_fn.name() == "isolateSelection")
	{
		if(m_render_pass->IsolateSelection())
		{
			assert(m_selection_id == k_null_callback);
			m_selection_id =
				MEventMessage::addEventCallback(
					"SelectionChanged", SelectionChangedCB, this);
		}
		else
		{
			assert(m_selection_id != k_null_callback);
			MMessage::removeCallback(m_selection_id);
		}

		IsolateSelection();
	}
	else
	{
		foundName = false;
	}

	if( foundName )
	{
		nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
	}
}

/**
	\brief Calls NSIExportDelegate::SetAttribute[AtTime] methods.

	This will in turn issue the appropriate NSI calls to set the changed
	attributes. We then issue a synchronize command using nsi.RenderControl.

	The only attribute we deal with here is the Visibility attribute which
	necessitates a connect or a disconnect.
*/
void NSIExport::AttributeChangedCallback(
	MNodeMessage::AttributeMessage i_msg,
	MPlug &i_plug,
	MPlug &i_other_plug,
	void *i_exporter )
{
	if( !(i_msg & (
		MNodeMessage::kAttributeSet |
		MNodeMessage::kConnectionMade |
		MNodeMessage::kConnectionBroken ) ) )
	{
		return;
	}

	MStatus status;
	MObject node = i_plug.node();
	MObject attrib = i_plug.attribute();
	MFnAttribute attribute_fn( attrib, &status );
	if( status != MS::kSuccess )
	{
		MGlobal::displayError( "3Delight for Maya: unable to get and attribute "
				"in an IPR session so render might be out of sync" );
		return;
	}

	NSIExport* exporter = (NSIExport*) i_exporter;

	bool event_handled = false;
	MFnDependencyNode depFn( node );

	if( depFn.typeId() == MU_typeIds::DL_SET )
	{
		if( i_msg & MNodeMessage::kAttributeSet )
		{
			/*
				This is for set attributes which are actually exported on geo.
				It will cause all the members to update. Not very efficient so
				we should ideally do it only for attributes which need it.
			*/
			exporter->RegisterDlSetToOneDelegate( node, true );
		}
		else if( i_msg & (
			MNodeMessage::kConnectionMade | MNodeMessage::kConnectionBroken ))
		{
			/*
				Push the updated set info to the mesh member delegate that is
				concerned by this connection made / broken event.
			*/
			MString handle =
				NSIExportDelegate::NSIHandle( i_other_plug.node(), true );
			NSIExportMesh* delegate =
				(NSIExportMesh*) exporter->m_delegates.Find( handle.asChar() );

			if( delegate )
			{
				MObject set;
				if( i_msg & MNodeMessage::kConnectionMade )
					set = node;
				else
					/* will receive an empty set */;

				delegate->MemberOfSet( set );
				delegate->SetAttributes();
				delegate->SetAttributesAtTime( 0.0f, true );
			}
		}

		/*
			Let the set delegate handle events too.
		*/
		NSIExportDelegate* delegate = exporter->m_delegates.Find(
			NSIExportDelegate::NSIHandle( node, true ).asChar() );

		if( delegate )
		{
			static_cast<NSIExportSet*>(delegate)->AttributeChanged(
				exporter->m_delegates.DagHashTable(),
				i_msg, i_plug, i_other_plug );

			event_handled = true;
		}
	}
	else if( i_msg & MNodeMessage::kAttributeSet &&
		node.hasFn(MFn::kDagNode) &&
		(attribute_fn.name() == "visibility" ||
		attribute_fn.name() == "lodVisibility" ||
		attribute_fn.name() == "overrideVisibility") )
	{
		MFnDagNode dag_node(node);

		bool connect = exporter->IsObjectVisible( dag_node );
		if( connect )
		{
			/*
			   Check for objects which were not fully exported because
			   initially invisible.
			*/
			MItDag it;
			it.reset(node);
			it.traverseUnderWorld(true);
			for( ; !it.isDone(); it.next() )
			{
				NSIExportDelegate *delegate = exporter->GetDelegate(
					NSIExportDelegate::NSIHandle(it.currentItem(), true));
				if( delegate && !delegate->Visible() )
				{
					delegate->SetVisible(true);
					delegate->SetAttributes();
					delegate->SetAttributesAtTime(0.0, true);
					delegate->Connect(exporter->m_delegates.DagHashTable());
					delegate->Finalize();
				}
			}
		}
		exporter->connectToParent( exporter->m_context, dag_node, connect );
		event_handled = true;
	}
	else if( i_msg & MNodeMessage::kAttributeSet )
	{
		for( int i = 0; i != e_invalidAttribute; i++ )
		{
			GeoAttribute type = (GeoAttribute)i;
			if( attribute_fn.name() == GeoAttributeMayaName( type ) )
			{
				MFnDagNode dag_node(node);

				AdjustGeoAttributeConnection(
					exporter->m_context,
					dag_node,
					type,
					false,
					true);

				event_handled = true;
				break;
			}
		}
	}

	if( !event_handled && (i_msg & MNodeMessage::kAttributeSet))
	{
		NSIExportDelegate* delegate = exporter->GetDelegate(
			NSIExportDelegate::NSIHandle( node, true ) );

		if( delegate )
		{
			delegate->SetAttributes( );
			delegate->SetAttributesAtTime( 0.0f, true );

			/*
				When exporting the camera, we also need to export the screen. BUT, not
				in VP2 mode as that one handles the screens all by itself.
			*/
			if( node.hasFn(MFn::kCamera) && exporter->m_export_mode != e_live_in_vp2 )
			{
				MFnCamera mfn_cam(node);
				MString screen =
					NSIExportDelegate::NSIHandle( mfn_cam, exporter->IsLive() ) + "|screen";
				NSIExportUtilities::ExportScreen(
					exporter->m_context,
					*exporter->m_render_pass, mfn_cam, screen, false,
					exporter->IsLive() );
			}
		}
		else
		{
			MGlobal::displayError(
				"3Delight for Maya: unknown node " +
				NSIExportDelegate::NSIHandle(node, true) +
				"; live render is desynchronized." );
		}
	}

	NSI::Context nsi( exporter->m_context );
	nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
}

/**
	\brief Called when a dependency node is dirty
*/
void NSIExport::NodeDirtyPlugCB( MObject &i_node, MPlug& i_plug, void *i_delegate )
{
	NSIExportDelegate *delegate = (NSIExportDelegate *) i_delegate;

	/*
		Avoid re-entrant NodeDirty handling in a delegate.
	*/
	if( delegate->NodeDirtyBegin() )
	{
		/*
			In IPR, when a complex attribute, such as a transform, changes due
			to a time change, its value is sometimes not up-to-date at this
			point, which mean we're going to export its previous value again.
			Accessing the attribute directly through its plug seems to have the
			side effect of completing the update. Calling asString() does the
			trick even when the attribute is not a string, which is not the case
			of asFloat().
		*/
		i_plug.asString();

		delegate->SetAttributes();
		delegate->SetAttributesAtTime( 0.0, true /* no motion*/ );
		delegate->Connect( 0x0 /* FIXME */ );

		delegate->NodeDirtyEnd();

		NSI::Context nsi( delegate->NSIContext() );
		nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
	}
}

void NSIExport::NodeRemovedCB( MObject &i_node, void *i_exporter )
{
	if( !i_exporter )
	{
		assert( false );
		return;
	}

	MFnDependencyNode dep(i_node);

	MString handle = NSIExportDelegate::NSIHandle( i_node, true );
	NSIExport *exporter = (NSIExport *)i_exporter;

	if( i_node.hasFn( MFn::kCamera ) )
	{
		// Can't delete cameras during a live render
		NSI::Context nsi( exporter->m_context );
		nsi.RenderControl( NSI::CStringPArg( "action", "stop") );

		/*
			Recursively delete the camera node and its children, since some are
			built using some attributes of the camera.
		*/
		exporter->m_delegates.Delete( handle.asChar(), 1 );

		nsi.RenderControl( NSI::CStringPArg( "action", "start" ) );
	}
	else if( exporter->m_delegates.Delete(handle.asChar()) )
	{
		/*
			Not all node are registered in our delegates container. E.g., a
			creation node for some polygon would be deleted when deleting a
			polygon but it is not present in m_delegates.
		*/
		NSI::Context nsi( exporter->m_context );
		nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
	}
}

/**
	\brief Add or remove instance or parent.

	In NSI, this is simply a connect/disconnect.
	See connectToParent() for how things are usually connected.
*/
void NSIExport::ParentingCB(
	MDagMessage::DagMessage msgType,
	MDagPath &child, MDagPath &parent, void *clientData )
{
	NSIExport *exporter = (NSIExport *)clientData;

	NSI::Context nsi( exporter->m_context );

	MStatus status;
	MFnDagNode childDagFn( child,  &status );

	MString childHandle = NSIExportDelegate::NSIHandle(child, true);
	MString parentHandle;
	if( parent.length() > 0 )
	{
		parentHandle = NSIExportDelegate::NSIHandle(parent, true);
	}
	else
	{
		parentHandle = NSI_SCENE_ROOT;
	}
	MString instanceTransformHandle =
		NSIExportDelegate::NSIInstanceHandle(childDagFn.object(), parent, true);

	if( msgType == MDagMessage::kInstanceAdded ||
	    msgType == MDagMessage::kParentAdded )
	{
		/*
			Skip this is there's no delegate for the child. The reason is that
			when a new shape is created, Maya sends a kParentAdded message
			parenting it to the root before the transform and shape get their
			node added callback. Parenting the instanceTransformHandle to
			NSI_SCENE_ROOT later leaves us with two objects, one being stuck at
			the origin.
		*/
		if( !exporter->m_delegates.Find(childHandle.asChar()) )
			return;

		if( instanceTransformHandle.length() > 0 )
		{
			nsi.Create( instanceTransformHandle.asChar(), "transform" );

			nsi.Connect(
				childHandle.asChar(), "",
				instanceTransformHandle.asChar(), "objects");

			nsi.Connect(
				instanceTransformHandle.asChar(), "",
				parentHandle.asChar(), "objects");
		}
		else
		{
			nsi.Connect(
				childHandle.asChar(), "",
				parentHandle.asChar(), "objects");
		}
	}
	else if(
		msgType == MDagMessage::kInstanceRemoved ||
		msgType == MDagMessage::kParentRemoved )
	{
		/*
			When removing an instance, Maya will send a kParentRemoved from
			root for the transform after it has sent the node removed callback.
			As we've already removed the node, this causes NSI error messages
			(but no real harm otherwise). Filter this out here to keep quiet.

			When there are no instances involved, callbacks come in the
			expected order, with node removal last.
		*/
		if( !exporter->m_delegates.Find(childHandle.asChar()) )
			return;

		if( instanceTransformHandle.length() > 0 )
		{
			/*
				There's no need to disconnect to/from the instance transform as
				deleting the node will do this anyway. Skipping these will
				avoid harmless but annoying NSI error messages with instances
				because we get both kInstanceRemoved and kParentRemoved. We
				can't disconnect from the deleted node the second time.
			*/
			nsi.Delete( instanceTransformHandle.asChar() );
		}
		else
		{
			nsi.Disconnect(
				childHandle.asChar(), "",
				parentHandle.asChar(), "objects");
		}
	}
	else
	{
		assert( false );
	}

	nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );

}

void NSIExport::SelectionChangedCB(void* nsi_export)
{
	NSIExport* exporter = static_cast<NSIExport*>(nsi_export);
	assert(exporter->IsLive());
	exporter->IsolateSelection();
	NSI::Context nsi(exporter->m_context);
	nsi.RenderControl(NSI::CStringPArg( "action", "synchronize"));
}

/**
	\brief Called when a dependency node is added in Maya.

	Some DAG nodes, such as Gobo and Decay filters are not created in the
	DAG version of CreateDelegate. We trap this calling the dependecy node
	verion of the function instead.
*/
void NSIExport::NodeAddedCB( MObject &i_node, void *i_pass )
{
	NSIExport *exporter = (NSIExport *)i_pass;

	NSIExportDelegate *delegate = 0x0;
	MFnDependencyNode dep_node( i_node );

	MDagPath node_dag_path;
	bool is_dag = i_node.hasFn(MFn::kDagNode);
	if( is_dag )
	{
		/*
			Find one path to i_node. Since we are not in a instance added CB,
			this if this is a shape, it is the first instance of that shape.
			Thus getPath will return the only path leading to that instance.
		*/
		MFnDagNode temp(i_node);
		temp.getPath( node_dag_path );
	}

	/*
		Creating a MFnDagNode using a path ensures that dagPath() calls will
		work which is required to produce the per-instance NSI transform.
	*/
	MFnDagNode dag_node( node_dag_path );

	if( is_dag )
	{
		exporter->CreateDelegate(
			node_dag_path, dag_node, delegate, true /* live */ );

		/*
			Create a transform that is used as a connection anchor point for
			instance-specific connections.
		*/
		MString instanceTransformHandle =
			NSIExportDelegate::NSIInstanceHandle( dag_node, true );

		if( instanceTransformHandle.length() > 0 )
		{
			NSI::Context nsi( exporter->m_context );
			nsi.Create( instanceTransformHandle.asChar(), "transform" );
		}
	}

	if( delegate )
	{
		delegate->Create();
		delegate->SetNiceName();
		delegate->SetAttributes();
		delegate->SetAttributesAtTime( 0.0, true /* no motion*/ );
		delegate->Connect( exporter->m_delegates.DagHashTable() );
		delegate->Finalize();
		exporter->m_delegates.AddDelegate( delegate );
		RegisterDelegateCallbacks( delegate, exporter );

		/*
		   If this is a dag node, connect it to parent if:
		   - It's visible.
		   - It's not an environment light. Environment light is connected
		     to parent if selected in the render pass (we have only one
			 environment light per pass).
		   - It's not a decay/gobo filter, this one is always a child of some
			 light.  FIXME: this is not true.
		   - It's not an incandescence light, this one is a set and has its own
		     semantics.
        */

		if( exporter->IsObjectVisible(dag_node) &&
			dag_node.typeId() != MU_typeIds::DL_ENVIRONMENTNODE &&
			dag_node.typeId() != MU_typeIds::DL_GOBOFILTER &&
			dag_node.typeId() != MU_typeIds::DL_DECAYFILTER &&
			dag_node.typeId() != MU_typeIds::DL_INCANDESCENCELIGHT )
		{
			assert( delegate->NSIContext() == exporter->m_context );
			exporter->connectToParent( exporter->m_context, dag_node );
		}

	}

	/**
		This node, even if it was a DAG node, could have a Dependency node
		counterpart. For example, place3DTexture is a placement matrix but
		also a shader.
	*/
	if( i_node.hasFn(MFn::kDependencyNode) )
	{
		NSIExportDelegate *delegate = exporter->CreateDelegate( i_node );
		if( delegate )
		{
			delegate->Create();
			delegate->SetAttributes();
			delegate->SetAttributesAtTime( 0.0, true /* no motion*/ );
			delegate->Connect( nullptr );
			delegate->Finalize();
			exporter->m_delegates.AddDelegate( delegate );
			RegisterDelegateCallbacks( delegate, exporter );
		}
	}

	NSI::Context nsi( exporter->m_context );
	nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
}

/**
	\brief Register callback for each delegate and also on node
	addition/removal
*/
void NSIExport::RegisterIPRCallbacks( void )
{
	m_delegates.ResetIterator();
	NSIExportDelegate *delegate;
	while( (delegate=m_delegates.Next()) )
	{
		RegisterDelegateCallbacks( delegate, this );
	}

	m_node_added_id = MDGMessage::addNodeAddedCallback(
		NodeAddedCB, kDefaultNodeType, this );
	m_node_removed_id = MDGMessage::addNodeRemovedCallback(
		NodeRemovedCB, kDefaultNodeType, this );
	m_instance_added_id = MDagMessage::addDagCallback(
		MDagMessage::kInstanceAdded, ParentingCB, this );
	m_instance_removed_id = MDagMessage::addDagCallback(
		MDagMessage::kInstanceRemoved, ParentingCB, this );
	m_parent_added_id = MDagMessage::addDagCallback(
		MDagMessage::kParentAdded, ParentingCB, this );
	m_parent_removed_id = MDagMessage::addDagCallback(
		MDagMessage::kParentRemoved, ParentingCB, this );

	MObject object = m_render_pass->Object();

	m_render_pass_id = MNodeMessage::addAttributeChangedCallback(
		object, RenderPassAttributeChangedCallback, this );

	if(m_render_pass->IsolateSelection())
	{
		m_selection_id =
			MEventMessage::addEventCallback(
				"SelectionChanged", SelectionChangedCB, this);
	}

	/*
		If we use ObjectsToRender or LightsToRender features, we must track
		additions and removals of objects from the set.
	*/
	m_object_sets_ids.resize(m_render_pass->NumObjectSets(), k_null_callback);
	for( int i = 0; i < m_render_pass->NumObjectSets(); ++i )
	{
		bool is_light;
		MObject set = m_render_pass->GetObjectSet(i, is_light);
		if( set.hasFn(MFn::kSet) )
		{
			m_object_sets_ids[i] = MNodeMessage::addAttributeChangedCallback(
				set, is_light ? LightsToRenderCB : ObjectsToRenderCB, this );
		}
	}
}

/**
	FIXME: a sequence render on the cloud will stop as soon as the one of the
	frame finishes. BAD!

	FIXME: execution flow unclear as NSIExport -stop might try to stop and
	already finished render -> calling this again ? UNFAIR!
*/
void NSIExport::RenderStoppedCallback(
	void* i_data,
	NSIContext_t i_context,
	int i_status )
{
	if( i_status != NSIRenderCompleted && i_status != NSIRenderAborted )
		return;

	NSIExport* exporter = (NSIExport*) i_data;
	if( !exporter->m_render_pass->isValid() )
		return;

	/*
		e_live_in_vp2 is a special mode of IPR. The render might
		stop and start for several times until user switches the viewport
		render override. The *NSIExport -stop* will be emited from
		dlViewportRenderOverride::RenderOverrideChangedCallback().
	*/
	if ( exporter->GetMode() == e_live_in_vp2 )
		return;

	/*
		There is a possibility that the render settings node does not exist
		anymore once the MEL command is executed. This is likely to happen
		when a render settings node used for an ongoing rendering is deleted.
		Testing its existence in the MEL command avoids MEL errors.
	*/
	MString cmd = "if(objExists(\""  + exporter->m_render_pass->name() + "\")) ";
	cmd += "NSIExport -stop -rs\"" + exporter->m_render_pass->name() + "\";";

	MGlobal::executeCommandOnIdle( cmd );
}

bool NSIExport::IsLight( const MFnDependencyNode& i_object )
{
	MObject obj( i_object.object() );

	// Maya built-in light nodes will satisfy this check
	if( obj.hasFn( MFn::kLight ) )
		return true;

	// Plug-in-defined lights require a classification string check.
	if( obj.hasFn( MFn::kPluginShape ) ||
		obj.hasFn(MFn::kPluginLocatorNode ) ||
		obj.hasFn(MFn::kPluginDependNode) )
	{
		// Check the node classification for the "light" classification.
		MString classification =
			MFnDependencyNode::classification( i_object.typeName() );

		MStringArray tokens;
		classification.split(':', tokens);

		for(unsigned i = 0; i < tokens.length(); i++ )
		{
			if( tokens[i] == "light" )
				return true;
		}
	}

	return false;
}

bool NSIExport::IsGeo( const MFnDagNode& i_object )
{
	if( !i_object.object().hasFn( MFn::kGeometric ) )
		return false;

	/*
		Plug-in shapes will pass the kGeometric test. They could be lights, though.
	*/
	if( i_object.object().hasFn( MFn::kPluginShape ) && IsLight( i_object ) )
		return false;

	return true;
}

bool NSIExport::CancelRequested()
{
	if( m_computation == 0x0 )
	{
		return false;
	}

	return m_computation->isInterruptRequested();
}

void NSIExport::SetProgressValue( int i_value )
{
	if( m_computation != 0x0 )
	{
		m_computation->setProgress( i_value );
	}
}

int NSIExport::GetProgressValue()
{
	if( m_computation == 0x0 )
	{
		return 0;
	}

	return m_computation->progress();
}

void NSIExport::CreateSequenceContext()
{
	m_sequence_context.reset(new NSI::Context);
	m_sequence_context->Begin((
		NSI::IntegerArg("createsequencecontext", 1),
		NSI::StringArg("software", "MAYA_BATCH"),
		NSI::IntegerArg("readpreferences", IsBatch() ? 0 : 1) ));

	if( m_export_mode == e_maya_batch_sequence )
	{
		/* Batch mode will wait in the main thread, after export. */
		return;
	}

	/*
		Wait in a thread. Make sure to capture values here as *this could be
		deleted before the thread exits.
	*/
	std::thread wait_thread{
		[](std::shared_ptr<NSI::Context> ctx, MString pass_name)
		{
			ctx->RenderControl( NSI::StringArg("action", "wait") );
			/*
				If the NSIExport object is already gone, this will do the
				NSIEnd(). Otherwise, the mel below will call StopRender() which
				will free the other shared_ptr ref and do NSIEnd().
			*/
			ctx.reset();

			/*
				\ref RenderStoppedCallback
			*/
			MString cmd = "if(objExists(\""  + pass_name + "\")) ";
			cmd += "NSIExport -stop -rs\"" + pass_name + "\";";
			MGlobal::executeCommandOnIdle( cmd );
		},
		m_sequence_context, m_render_pass->name()};
	wait_thread.detach();
}

void
NSIExport::SetReferencesNiceName()
{
	for (int i = 0; i<m_references.length(); i++)
	{
		const MObject& node = m_references[i];
		MFnReference ref(node);
		MString file = ref.fileName(true, false, false);
		MObjectArray nodes;
		ref.nodes(nodes);
		for (int j = 0; j < nodes.length(); j++)
		{
			const MObject& obj = nodes[j];
			MString handle = NSIExportDelegate::NSIHandle(obj, IsLive());
			NSIExportDelegate* delegate = m_delegates.Find(handle.asChar());
			if (delegate)
			{
				delegate->SetNiceName(file);
			}
		}
	}
}

bool NSIExport::UseSeparateProcess()
{
	/* These are the only modes where the feature is relevant. */
	if( m_export_mode != e_interactive && m_export_mode != e_live )
	{
		return false;
	}

	/*
		Only built-in drivers are supported. This is a limitation of the
		current implementation.
	*/
	if( m_render_pass->layerFramebufferOutput(IsLive()) &&
	    FramebufferDriver() != "idisplay" )
	{
		return false;
	}

	/* Read the preference. */
	bool var_exists = false;
	int var_value =
		MGlobal::optionVarIntValue("_3delight_separate_process", &var_exists);

	return var_exists && var_value != 0;
}
