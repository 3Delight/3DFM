/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#ifndef __RENDERPASSINTERFACE_H__
#define __RENDERPASSINTERFACE_H__

#include <maya/MObject.h>
#include <maya/MObjectHandle.h>

#include <vector>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MObject;
class MDoubleArray;
class MIntArray;
class MObjectArray;
class MPlug;
class MString;
class MStringArray;
#endif

/**
	\brief Defines a pure virtual interface to access certain screen values in a
	render pass.

	This design allows to create a render pass without an actual Maya object
	for a small subset of attributes (in this case, some screen attributes)

	\sa NSIViewportExport, NSIExport::ExportScreen
*/
class RenderPassInterfaceForScreen
{
public:
	/**
		\brief Returns X res

		Part of the Image Resolution and Crop group
	*/
	virtual int ResolutionX( void ) const = 0;

	/**
		\brief Returns Y res

		Part of the Image Resolution and Crop group
	*/
	virtual int ResolutionY( void ) const = 0;

	/**
		\brief Returns the pixel aspect ratio of the rendered image

		Part of the Image Resolution and Crop group
	*/
	virtual double PixelAspectRatio( void ) const = 0;

	/**
		\brief Returns the crop window as defined in the pass.

		Part of the Image Resolution and Crop group
	*/
	virtual void Crop( float o_top_left[2], float o_bottom_right[2] ) const = 0;

	/**
		\brief Returns the number of pixel samples.

		Pixel samples are part of the Quality group
	*/
	virtual int PixelSamples( void ) const = 0;

	/**
		\brief Returns the index of the selected pixel filter

		Pixel filter is part of the Quality group.

		Used to determine if importance sample will be used
	*/
	virtual int PixelFilterIndex( void ) const = 0;
};

/**
	\brief Defines a pure virtual interface to access certain camera values in a
	render pass.

	This design allows to create a render pass without an actual Maya object
	for a small subset of attributes (in this case, some camera attributes)

	\sa MaterialViewer, NSIExportCamera
*/
class RenderPassInterfaceForCamera
{
public:
	/**
		\brief Returns the pixel filter for this pass.

		Pixel filter is part of the Quality group
	*/
	virtual const char *PixelFilter( void ) const = 0;

	/**
		\brief Returns the width, in pixels, of the filter

		Note that filters can cover a fraction of a pixels and that is why its
		defined using a real number
	*/
	virtual double FilterWidth( void ) const = 0;

	// @{
	/** Render boost method needed by the camera exporter */
	virtual bool DisableMotionBlur() const = 0;
	virtual bool DisableDepthOfField() const = 0;
	// @}
};

class RenderPassInterface :
	public RenderPassInterfaceForCamera, public RenderPassInterfaceForScreen
{
public:
	/**
		Make sure this is in sync with the MEL interface.

		NOTE: this is NOT the export mode (which can be IPR or batch). That one
		is specified in NSIExport. This is what the render pass specifies as
		the action to take when pressing the render button. These two different
		things are still related because an IPR render will override whatever
		render mode there is in the pass.
	*/
	enum eRenderMode
	{
		e_render = 0,
		e_export_scene = 1
	};

	/**
		Image layer (convoluted) file output modes.
	*/
	enum eFileOutputMode
	{
		e_disabled,
		e_enabled,
	};

	enum eSelectedOutputMode
	{
		e_idisplay = 1,
		e_image,
		e_imageAndDisplay,
		e_nsi
	};

public:
	// @{
	/** \sa RenderPassMaterialViewerInterface */
	virtual int ResolutionX( void ) const;
	virtual int ResolutionY( void ) const;
	virtual double PixelAspectRatio( void ) const;
	virtual void Crop( float o_top_left[2], float o_bottom_right[2] ) const;
	virtual const char *PixelFilter( void ) const;
	virtual double FilterWidth( void ) const;
	virtual bool DisableMotionBlur() const;
	virtual bool DisableDepthOfField() const;
	// @}

public:
	// Construct the interface object with the specified render pass node.
	RenderPassInterface(MObject& i_renderPassNode, bool i_canApplyOverrides);
	virtual ~RenderPassInterface();

	MObject Object( void ) const
	{
		return m_object_handle.object();
	}

	// The name of the render pass.
	//
	MString name() const;

	// Returns true if the object received at construction time was a valid 
	// RenderPass node.
	//
	bool isValid() const;

	// Returns the rendered cameras.
	virtual std::vector<MObject> cameras() const;

	// Returns the environment selected in Scene Elements
	virtual MObject Environment() const;

	// Returns the atmosphere selected in Scene Elements
	virtual MObject Atmosphere() const;

	/**
		\brief Returns the objects to render maya set
	*/
	virtual MObject ObjectsToRender( void ) const;

	/**
		\brief Returns the lights to render maya set
	*/
	virtual MObject LightsToRender( void ) const;

	/**
		\brief Returns the objects to render as mattes maya set
	*/
	virtual MObject MatteObjects(void) const;

	virtual MObject ForceObjects(void) const;
	virtual MObject ExcludeObjects(void) const;

	virtual MObject OverrideDlSets(void) const;

	virtual MObject ForceLights(void) const;
	virtual MObject ExcludeLights(void) const;

	/* Generic access fo ObjectsToRender, LightsToRender, etc. */
	int NumObjectSets() const;
	int WhichObjectSet(const MString &name) const;
	MObject GetObjectSet(int idx, bool &o_is_light) const;

	// Returns the list of frames to render, taking into account the various
	// frame range attributes.
	//
	void framesToRender(MDoubleArray& o_frames) const;

	/**
		\brief Returns the render mode of this pass

		This is part of the Render Engine group
	*/
	eRenderMode RenderMode( void ) const;

	/**
		\brief Returns true if there is no animation rendering.
	*/
	virtual bool RenderCurrentFrameOnly( void ) const;

	/**
	\brief Returns true if we are saving the current frame only as standin.
	*/
	virtual bool ArchiveCurrentFrameOnly(void) const;

	/**
		\brief Returns animation start/end/increment time
	*/
	double StartFrame( void ) const;
	double EndFrame( void ) const;
	double Increment( void ) const;


	/**
		\brief Returns true if motion blur is enabled
	*/
	virtual bool EnableMotionBlur() const;

	/**
		\brief Returns true if progressive render is enabled.
	*/
	virtual bool IsProgressive( void ) const;

	/**
		\brief Returns true if network caching is enabled.
	*/
	virtual bool UseNetworkCache( void ) const;

	/**
		\brief Returns the number of shading samples

		Shading samples are part of the Quality group
	*/
	virtual int ShadingSamples( void ) const;

	virtual int PixelSamples( void ) const;

	virtual int PixelFilterIndex(void) const;

	virtual int VolumeSamples( void ) const;

	virtual bool EnableClamping(void) const;

	virtual double ClampingValue(void) const;

	/**
		\brief Returns the max ray depth for the specified ray type

		\param type
			One of: Reflection, Refraction, Diffuse, Specular, Hair

		The ray depth attributes are part of the Quality group
	*/
	virtual int MaxRayDepth( MString &type ) const;

	/**
		\brief Returns the max distance a ray can travel

		This is part of the Quality group
	*/
	virtual double MaxDistance( void ) const;

	// Returns the expanded RIB filename
	virtual MString expandedNSIFilename() const;

	// The array of logical (sparse) layer indices.
	virtual void layerIndices(MIntArray& o_indices) const;

	//
	// Layers attribute accessors. Note that the specified index is expected to
	// be an element of the array returned by layersIndices. Using other indices
	// will not fail and will create new layer attribute elements.
	//
	// The filename of the specified layer.
	virtual MString layerFilename(int i_index) const;

	// Returns false when the filename is set as a per-layer override.
	virtual bool LayerUsesDefaultFilename(int i_index) const;

	// The display driver of the specified layer.
	virtual MString layerDriver(int i_index) const;

	// The variable (AOV) of the specified layer.

	/**
		Retrieves various fields from the description of an output variable.

		\param i_index
			Index of the variable from which the description is requested.
		\param o_variable_source
			Will be set to the NSI variable source (ie : attribute, shader or
			builtin)
		\param o_layer_type
			Will be set to the intended NSI layer type (ie : scalar, vector,
			color or quad)
		\param o_with_alpha
			Will be set to 0 when an alpha channel should be appended, and
			non-zero otherwise.
		\param o_nsi_name
			Will be set to the name of the variable as known to NSI.
		\param o_token
			Will be set to the token that should be used in place of the
			variable name in file and layer names.
		\param o_label
			Will be set to the name of the variable to be used in the user
			interface.
		\param o_description
			If not null, will be set to the string containing all of the above,
			which is the actual unique ID of the variable within 3DFM.
	*/
	virtual void layerVariable(
		int i_index,
		MString& o_variable_source,
		MString& o_layer_type,
		int& o_with_alpha,
		MString& o_nsi_name,
		MString& o_token,
		MString& o_label,
		MString* o_description = 0x0)const;

	// The light categories to be applied to "shading components" layers
	//
	// o_categories returns all strings; strings are needed in ExportImageLayers
	// in NSIExport; first string is empty and represents all lights
	//
	// o_objCategories returns all objects related to "shading components"
	// layers; this is used to retrieve the NSIHandle from an object when
	// connecting the light category to "lightset" (See ExportOneOutputLayer
	// in NSIExport); first object is null and represents all light
	virtual void layerLightCategories(
		MStringArray& o_categories,
		MObjectArray& o_objCategories) const;

	// The various output enable flags
	virtual bool layerOutput(int i_index) const;
	virtual bool layerMultiLight(int i_index) const;
	virtual bool layerFramebufferOutput(bool ipr) const;
	virtual bool layerFileOutput() const;
	virtual bool layerJpegOutput() const;
	virtual bool layerNSIOutput() const;

	/**
		\brief The effective layer file output mode, taking into account the
		Batch / InteractiveOutputMode, and the relevant output toggle states.

		\param i_index
			The layer index
		\param i_mode
			The return value of either BatchOutputMode or InteractiveOutputMode
		\param o_fileOutput
			Indicates if this layer should produce a file output
		\param o_jpegOutput
			Indicates if this layer should produce a jpeg output
	*/
	virtual void LayerEffectiveFileOutput(
		int i_index, 
		eFileOutputMode i_mode, 
		bool& o_fileOutput,
		bool& o_jpegOutput,
		bool ipr) const;

	// The pixel format, as a value expected by NSI outputlayer
	virtual MString layerScalarFormat(int i_index) const;

	// Overrides stuff
	virtual bool EnableSpeedBoost( void ) const;
	virtual bool DisableDisplacement() const;
	virtual bool DisableSubsurface() const;
	virtual bool DisableAtmosphere() const;
	virtual bool DisableMultipleScattering() const;
	virtual bool DisableExtraImageLayers() const;
	virtual int ResolutionReduceFactor() const;
	virtual double SamplingReduceFactor() const;

	virtual bool IsolateSelection()const;

protected:
	// Returns the name of the node connected to the specified attribute.
	MObject getConnectedNode(MString i_attrName) const;

	// Returns the name of the node connected to the specified plug.
	MObject getConnectedNode(MPlug& i_plug) const;

	// Finds the logical children at index i_index of the specified attribute.
	bool findPlugElement(MString i_name, int i_index, MPlug& o_plug) const;

	// If i_attrName is found, set its value in o_value and return true.
	bool getAttrValue(MString i_attrName, double& o_value) const;
	bool getAttrValue(MString i_attrName, int& o_value) const;
	bool getAttrValue(MString i_attrName, MString& o_value) const;

	// Same as above, but operates on array plugs using a *logical* index.
	bool getAttrValue(MString i_attrName, int i_index, double& o_value) const;
	bool getAttrValue(MString i_attrName, int i_index, int& o_value) const;
	bool getAttrValue(MString i_attrName, int i_index, MString& o_value ) const;

	// Splits a variable description into its 6 fields (see layerVariable)
	static void variableFields(
		const MString& i_description,
		MString& o_variable_source,
		MString& o_layer_type,
		int& o_with_alpha,
		MString& o_nsi_name,
		MString& o_token,
		MString& o_label);

private:
	RenderPassInterface();

	MObjectHandle m_object_handle;
	bool m_canApplyOverrides;
};

#endif
