#include "NSIExportDelegate.h"

#include "DL_utils.h"

#include <maya/MDagPath.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MMessage.h>
#include <maya/MPlug.h>
#include <maya/MSelectionList.h>
#include <maya/MStringArray.h>
#include <maya/MUuid.h>

#include "nsi.hpp"

namespace
{
	const MString k_separator = "|";
	const MString k_ns_separator = ":";
	const MString k_underworld_separator = "->";
	const MString k_file_separator = "/";

	/**
		\brief Returns a handle based on an object's UUID

		The handle consists of the UUID of the node, possibly prefixed with the
		node's namespace if it isn't void.

		A given object in a Maya scene file has a specific UUID saved with it.
		Creating many references to a single Maya scene file in another scene
		will result in having many times the same object, with the same UUID, in
		different namespaces.
	*/
	MString
	UUIDNSIHandle(const MObject &i_node)
	{
		MFnDependencyNode depFn( i_node );
		MString curr_namespace = depFn.parentNamespace();
		MString uuid = depFn.uuid().asString();

		if( curr_namespace != MString( "" ) )
		{
			curr_namespace += ":";
		}

		return curr_namespace + uuid;
	}

	/**
		\brief Returns a handle based on a object's path.

		We don't convert the path directly to a string because we want to use
		the same handle for all instance paths leading to the same node. This is
		why we base our handle on the name of the first parent (ie : parent(0))
		of each intermediate node.
	*/
	MString
	PathNSIHandle(const MFnDagNode& i_dagNode)
	{
		if( i_dagNode.inUnderWorld() )
		{
			/*
				Walking up with parent(0) all the way to the root does not work
				for underworld nodes because there is a MFn::kUnderWorld node
				in the path which does not have MFnDagNode.

				If we just call i_dagNode.fullPathName(), we'll get the wrong
				path if the shape to which underworld nodes are attached is
				instanced.

				Fortunately, underworld DAG paths are actually two paths which
				we can easily split and process separately. The way we do it
				here assumes no instancing in the underworld.
			*/
			MDagPath fullPath;
			i_dagNode.getPath(fullPath);
			if( fullPath.pathCount() == 2 )
			{
				MDagPath abovePath, underPath;
				fullPath.getPath(abovePath, 0);
				fullPath.getPath(underPath, 1);
				return
					PathNSIHandle(abovePath) +
					k_underworld_separator +
					underPath.fullPathName();
			}
		}

		MString handle = k_separator + i_dagNode.name();

		MObject parent = i_dagNode.parent(0);
		if(parent.isNull())
		{
			/*
				This is very unlikely, because it would mean that we're
				exporting the root node. But let's be safe.

				I managed to preproduce it by:
				1- Have a live render in the may viewport
				2- New scene
				3- Do not save
			*/
			return handle;
		}

		/*
			Work our way up the DAG path, adding node names to the beginning of
			the handle.
			We don't include the root node ("world") because it would appear in
			all handles, which would be redundant. This is why the stopping
			condition is based on the grandParent being null, which means the
			parent is the root node.
		*/
		MObject grandParent = MFnDagNode(parent).parent(0);
		while(!grandParent.isNull())
		{
			handle = k_separator + MFnDagNode(parent).name() + handle;
			parent = grandParent;
			grandParent = MFnDagNode(grandParent).parent(0);
		}

		/*
			This assertions ensures that we produce reasonable handles for
			non-instanced objects. It also checks that MFnDagNode::isInstanced
			works as advertised, in case we ever need to optimize this function
			and return i_dagNode.fullPathName() directly for non-instanced
			objects. (isInstanced should be able to detect both direct and
			indirect instances.)
		*/
		assert(i_dagNode.isInstanced() || handle == i_dagNode.fullPathName());

		return handle;
	}
}

NSIExportDelegate::NSIExportDelegate(
	MFnDagNode &i_dagNode, const Context& i_context )
:
	m_object_handle( i_dagNode.object() ),
	m_nsi(i_context.m_nsi),
	m_motion_samples(1),
	m_nodeDirtyInProgress(false),
	m_live(i_context.m_live_render)
{
	MString h = NSIHandle( i_dagNode, m_live );
	m_nsi_handle = ::strdup( h.asChar() );
}


NSIExportDelegate::NSIExportDelegate(
	MFnDagNode &i_dagNode,
	const Context& i_context,
	const MString &i_handle )
:
	m_object_handle( i_dagNode.object() ),
	m_nsi( i_context.m_nsi ),
	m_motion_samples(1 ),
	m_nodeDirtyInProgress(false),
	m_live(i_context.m_live_render)
{
	assert( i_handle.length() );
	m_nsi_handle = ::strdup( i_handle.asChar() );
}

NSIExportDelegate::NSIExportDelegate(
	MFnDependencyNode &i_dep_node,
	MObject &i_object,
	const Context& i_context )
:
	m_object_handle(i_object),
	m_nsi(i_context.m_nsi),
	m_motion_samples(1),
	m_nodeDirtyInProgress(false),
	m_live(i_context.m_live_render)
{
	// FIXME: this is the same as the first ctor
	m_nsi_handle = ::strdup( NSIHandle( i_dep_node.object(), m_live ).asChar() );
}

/**
	\brief Solely for NSINurbsSurface.
*/
NSIExportDelegate::NSIExportDelegate(
	MFnDagNode &i_dagNode,
	MObject &i_object,
	const Context& i_context )
:
	m_object_handle(i_object),
	m_nsi(i_context.m_nsi),
	m_motion_samples(1),
	m_nodeDirtyInProgress(false),
	m_live(i_context.m_live_render)
{
	MString h = NSIHandle( i_dagNode, m_live );
	m_nsi_handle = ::strdup( h.asChar() );
}

NSIExportDelegate::NSIExportDelegate( MObject& i_object, const Context& i_context )
:
	m_object_handle( i_object ),
	m_nsi( i_context.m_nsi ),
	m_motion_samples(1),
	m_nodeDirtyInProgress(false),
	m_live(i_context.m_live_render)
{
	MString h = NSIHandle( i_object, m_live );
	m_nsi_handle = ::strdup( h.asChar() );
}

void NSIExportDelegate::RemoveCallbacks( void )
{
	for( int i=0; i<m_ids.size(); i++ )
	{
		MMessage::removeCallback( m_ids[i] );
	}
}

NSIExportDelegate::~NSIExportDelegate()
{
	free( (void *)m_nsi_handle );

	RemoveCallbacks();
}

/**
	The default implementation deletes the NSI node non-recursively.
*/
void NSIExportDelegate::Delete( bool i_recursive )
{
	NSI::Context nsi( m_nsi );
	nsi.Delete( m_nsi_handle, NSI::IntegerArg("recursive", i_recursive ? 1 : 0) );
}

/**
	\brief Returns a valid NSI handle for a given dag node position
*/
MString NSIExportDelegate::NSIHandle( const MFnDagNode& i_dagNode, bool i_live )
{
	if(i_live)
	{
		return UUIDNSIHandle(i_dagNode.object());
	}

	return PathNSIHandle(i_dagNode);
}

/**
	\brief Returns a valid NSI handle from a Maya object
*/
MString NSIExportDelegate::NSIHandle( const MObject &i_node, bool i_live )
{
	if(i_live)
	{
		return UUIDNSIHandle(i_node);
	}

	if( i_node.hasFn(MFn::kDagNode) )
	{
		return PathNSIHandle(MFnDagNode(i_node));
	}

	MStatus status;
	MFnDependencyNode dep_node( i_node, &status );
	return dep_node.name();
}

void NSIExportDelegate::RenameHandle( const char* i_new_name )
{
	free( (void *)m_nsi_handle );
	m_nsi_handle = ::strdup( i_new_name );
}

bool NSIExportDelegate::NodeDirtyBegin()
{
	if( m_nodeDirtyInProgress )
	{
		return false;
	}

	m_nodeDirtyInProgress = true;
	return true;
}

void NSIExportDelegate::NodeDirtyEnd()
{
	m_nodeDirtyInProgress = false;
}

// FIXME: this seems unused (and is probably useless with UUIDs)
MString NSIExportDelegate::NSIAttributesHandle( const MFnDagNode &dag_node, bool i_live )
{
	return NSIHandle( dag_node, i_live ) + "|attributes";
}

MString NSIExportDelegate::NSIAttributeOverridesHandle( const MFnDagNode &dag_node, bool i_live )
{
	return NSIHandle( dag_node, i_live ) + "|attributeOverrides";
}

MString NSIExportDelegate::NSIAttributesHandle( const MObject &obj, bool i_live )
{
	return NSIHandle( obj, i_live ) + "|attributes";
}

MString NSIExportDelegate::NSIShaderHandle( MObject i_object, bool i_live )
{
	return NSIHandle( i_object, i_live ) + "|shader";
}

MString NSIExportDelegate::NSIInstanceHandle( const MFnDagNode& i_dag_node, bool i_live )
{
	MStatus status;
	MDagPath parentPath = i_dag_node.dagPath( &status );

	/*
		When MFnDagNode::dagPath() fails, it means that the received i_dagNode
		does not refer to a specific instance.
	*/
	//assert( status == MStatus::kSuccess );
	if( status != MStatus::kSuccess )
	{
		return "";
	}

	parentPath.pop();

	return NSIInstanceHandle( i_dag_node.object(), parentPath, i_live );
}

MString NSIExportDelegate::NSIInstanceHandle(
	MObject i_shape,
	const MDagPath& i_parent,
	bool i_live)
{
	/*
		Just about any DAGnode is instanceable, including transforms. At this
		time, the instance transform's purpose is to handle per-instance shader
		assignment, which is only possible on shapes. So, skip the instance
		transform when dealing with a transform DAG node.
	*/
	if( !i_shape.hasFn( MFn::kGeometric ) )
	{
		return "";
	}

	MStatus status = MStatus::kSuccess;

	MFnDagNode parentDagFn( i_parent, &status );
	if( status != MStatus::kSuccess )
		return "";

	MString handle =
		NSIHandle( parentDagFn, i_live )  + MString( "|" ) + NSIHandle( i_shape, i_live );

	return handle;
}

void NSIExportDelegate::SetNiceName(const MString& i_referenced_file)
{
	if(!Object().hasFn(MFn::kDagNode))
	{
		return;
	}

	MFnDagNode dag(Object());

	MString ns = dag.parentNamespace();

	// Remove all but first instance of namespace in full path name
	MString name = PathNSIHandle(dag);
	MString ns_block = k_separator + ns + k_ns_separator;
	const MString ns_placeholder = "!#$!";
	name.substituteFirst(ns_block, ns_placeholder);
	name.substitute(ns_block, k_separator);
	name.substituteFirst(ns_placeholder, ns_block);

	// Replace the namespace with the name of the referenced file when possible
	if(dag.isFromReferencedFile())
	{
		if(i_referenced_file.length() == 0)
		{
			/*
				Don't set the nicename incorrectly : wait until this function is
				called with a non-empty referenced file name before setting the
				nice name.
			*/
			return;
		}

		MStringArray components;
		getFixedPath(i_referenced_file).split('/', components);
		MString file = components[components.length()-1];

		/*
			Replace the substring ending with the first namespace with the
			referenced file name.
		*/
		int ns_pos = name.indexW(ns_block);
		if(ns_pos >= 0)
		{
			name =
				file + k_file_separator + k_separator +
				name.substringW(ns_pos + ns_block.length(), name.length()-1);
		}
	}

	// Set the nicename attribute
	NSI::Context ctx(m_nsi);
	ctx.SetAttribute(m_nsi_handle, NSI::StringArg("nicename", name.asChar()));
}
