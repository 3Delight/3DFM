#include "xgmSplineDescriptionExporter.h"

#include "XGenSplinesDelegate.h"

static void* CreateDelegate( 
	MObject i_obj, 
	NSIContext_t i_context, 
	const MString& i_handle,
	NSIUtils& i_utils,
	void* i_data )
{
	return new XGenSplinesDelegate( i_obj, i_context, i_handle, i_utils, i_data );
}

bool xgmSplineDescriptionExporter::isUndoable() const
{
	return false;
}

void* xgmSplineDescriptionExporter::creator()
{
	return new xgmSplineDescriptionExporter;
}

MSyntax xgmSplineDescriptionExporter::newSyntax()
{
	MSyntax syntax;
	return syntax;
}

MStatus xgmSplineDescriptionExporter::doIt( const MArgList& )
{
	NSICustomExporter* exporter = new NSICustomExporter;
	exporter->m_createDelegate = CreateDelegate;

  char strPointer[256];
  sprintf( strPointer, "%p", exporter );
  MString result ( strPointer );
  setResult( result );

  return MStatus::kSuccess;
}
