#include <maya/MGlobal.h>
#include <maya/MFnPlugin.h>

#include "dlInterface.h"
#include "xgmSplineDescriptionExporter.h"

_3DL_ALWAYS_EXPORT MStatus initializePlugin(MObject);
_3DL_ALWAYS_EXPORT MStatus uninitializePlugin(MObject);

MStatus
initializePlugin( MObject i_obj )
{
  MString versionString;
  versionString = 1.000;

  MFnPlugin plugin( i_obj, "DnA Research", versionString.asChar(), "Any");
  
  return plugin.registerCommand(
    EXPORT_CMD_STR, 
    xgmSplineDescriptionExporter::creator, 
    xgmSplineDescriptionExporter::newSyntax);
}

MStatus
uninitializePlugin( MObject i_obj )
{
  MFnPlugin plugin( i_obj );
  MStatus result = plugin.deregisterCommand( EXPORT_CMD_STR );
  return result;
}
