/*
	Copyright (c) 2008 soho vfx inc.
	Copyright (c) 2008 The 3Delight Team.
*/

// UI procedures that are not tied to a specific 3Delight window nor
// the Attribute Editor
//

global proc DL_buildChoiceMenuWithHeader(
  string $menu,
  string $menu_items_callback,
  string $default_menu_item,
  string $item_type,
  string $item_selected_cmd,
  string $header_cmd,
  string $header,
  string $no_items_label)
{
  //
  // Description:
  //  This procedure empties and populates the specified menu $menu with the
  //  list of strings returned by the menu_items_callback.
  //
  //  There will always be one menu item spefified by $default_menu_item,
  //  unless it is an empty string.
  //
  //  If there are too many items to display in a menu, the menu will have an
  //  item that, when selected, will bring up a dialog listing all the possible
  //  items returned by $menu_items_allback.
  //
  //  When an item is selected the $item_selected_cmd command will be evaluated.
  //  The selected menu item name string will be passed to this command as the
  //  last parameter.
  //
  //  If provided, the $header_cmd is evaluated to built items in the top
  //  part of the menu, it takes precedence over $header.
  //  When $header is used, an insensitive "menu header" item will be displayed
  //  at the top of the menu, followed by a separator.
  //
  //  When the $menu_items_callback return an empty list, the menu will display
  //  only one insensitive item labeled as specified by $no_items_label - unless
  //  this parameter is set to an empty string; in this case the menu will only
  //  display the $default_menu_item.
  //

  setParent -menu $menu;
  menu -edit -deleteAllItems $menu;

  // Create the header
  //
  if ($header_cmd != "")
  {
    eval($header_cmd);
  }
  else if ($header != "")
  {
    menuItem -enable false -label $header;
    menuItem -divider true;
  }

  // Create the default item
  //
  if ($default_menu_item != "")
  {
    menuItem
      -enableCommandRepeat false
      -label $default_menu_item
      -command ($item_selected_cmd + " \"\"");
  }

  // Create the remaining items in the menu.
  //
  string $menu_items[] = eval($menu_items_callback);
  global int $gMaxNumOptionMenuItems;

  int $num_menu_items = size($menu_items);
  if ($num_menu_items == 0 && $no_items_label != "")
  {
    menuItem -enable false -label $no_items_label;
  }
  else if (size($menu_items) < $gMaxNumOptionMenuItems)
  {
    for ($item in $menu_items)
    {
      menuItem
        -enableCommandRepeat false
        -label $item
        -command ($item_selected_cmd + " \"" + $item + "\"");
    }
  }
  else
  {
    // There are too many items in the scene to put in one menu.  Instead, we
    // offer the user the option of opening a dialog from which they can
    // choose one.
    //
    menuItem
      -enableCommandRepeat false
      -label "Too many items. Choose from dialog..."
      -command
        ("DL_optionMenuChoiceDlg "
          + DL_stringArrayAsString($menu_items)
          + " \"Choose " + $item_type + "\""
          + " \"" + encodeString($item_selected_cmd) + "\"");
  }
}

global proc
DL_buildChoiceMenu(
  string $menu,
  string $menu_items_callback,
  string $default_menu_item,
  string $item_type,
  string $item_selected_cmd)
{
  DL_buildChoiceMenuWithHeader(
    $menu,
    $menu_items_callback,
    $default_menu_item,
    $item_type,
    $item_selected_cmd,
    "",
    "",
    "");
}

global proc string
DL_showNameValidateDlg(
  string $initialName,
  string $windowTitle,
  string $initialMsg,
  string $buttonText,
  string $name_set_cmd)
{
  //
  // Description:
  //  This procedure opens a modal dialog to rename a node.
  //  The dialog asks the user to specify the new name of the node.
  //  The name will be checked to ensure it is valid and unique
  //  before the node is renamed. If the name is not valid or is
  //  not unique, the user will be told why the name cannot be used and will
  //  be asked to specify a different name. This iterative process will
  //  continue until the user specifies a valid, unique name or decides to
  //  cancel the rename operation.
  //

  string $name;
  string $choice;
  int $is_valid_name;
  int $is_existing_name;

  $choice =
    `promptDialog
      -title $windowTitle
      -message $initialMsg
      -text $initialName
      -button $buttonText
      -button "Cancel"
      -defaultButton $buttonText`;

  if ($choice == $buttonText)
  {
    // Get the name the user has specified for the new shader collection.
    //
    $name = `promptDialog -query -text`;

    // Check to see if the name is valid and unique.
    //
    $is_valid_name = isValidObjectName($name);
    $is_existing_name = (size(`ls $name`) > 0);

    while (
         ($choice == $buttonText)
      && (   (!$is_valid_name)
          || ($is_existing_name)))
    {
      //
      // The name specified by the user is either invalid, or not unique.
      // We will tell them what's wrong with their name and ask them to choose
      // a new one.
      //

      string $message;

      if (!$is_valid_name)
      {
        $message =
          ("\""
            + $name
            + "\" is an invalid name.\n"
            + "The name must begin with a letter and may contain only\n"
            + "letters, numbers, and underscores.\n"
            + "Please choose a different name:");
      }
      else if ($is_existing_name)
      {
        $message =
          ("A node called \""
            + $name
            + "\" already exists.\n"
            + "Please choose a different name:");
      }

      $choice =
        `promptDialog
          -title $windowTitle
          -message $message
          -text $name
          -button $buttonText
          -button "Cancel"
          -defaultButton $buttonText`;

      if ($choice == $buttonText)
      {
        // Get the new name the user has specified for the new shader
        // collection.
        //
        $name = `promptDialog -query -text`;

        // Check to see if the name is valid and unique.
        //
        $is_valid_name = isValidObjectName($name);
        $is_existing_name = (size(`ls $name`) > 0);
      }
    }
  }

  if ($choice == $buttonText)
  {
    // The user eventually chose a valid, unique name and pressed the Create
    // button in the dialog. We will return the user's chosen, valid name.
    //
    eval($name_set_cmd + " " + $name);
    return $name;
  }
  else
  {
    return "";
  }
}

global proc
DL_changeSensitivity(string $ui_element, int $enable)
{
  // Wrapper around the DL_doChangeSensitivity proc to deffer its call on
  // maya pre 2011 linux
  //
  float $maya_version = getApplicationVersionAsFloat();
  string $os = `about -os`;
  if ($maya_version < 2011 && match("linux", $os) != "")
  {
    evalDeferred -lowestPriority
      ("evalDeferred -lowestPriority(DL_doChangeSensitivity(\"" + $ui_element +
        "\", " + $enable + "))");
  }
  else
    DL_doChangeSensitivity($ui_element, $enable);
}

global proc
DL_doChangeSensitivity(string $ui_element, int $enable)
{
	if (`layout -q -exists $ui_element`)
	{
    layout -edit -enable $enable $ui_element;

    // We need to change the current parent as short children name rely on
    // the current parent to be resolved properly.
    //
    string $prev_parent = `setParent -q`;
    setParent $ui_element;

    string $children[] = `layout -q -childArray $ui_element`;
    for($curr_child in $children)
    {
      DL_changeSensitivity($curr_child, $enable);
    }

    setParent $prev_parent;
  }
  else if (`control -q -exists $ui_element`)
  {
    control -edit -enable $enable $ui_element;
  }
}

global proc string
DL_separator(string $name)
{
  // Constructs a separator that will span for 80% of the parent layout width,
  // with blanks for the first and last 10%, and a height of 22.
  // The actual separator will be named after the received name. It is placed in
  // a form layout, whose name is returned.
  //
  return DL_customSeparator( $name, 22, 0, 0, 0, 0, 10, 90 );
}

//
// Constructs a separator with the specified height, offsets and positions.
//
// The actual separator will be named after the received name. It is placed in
// a form layout, whose name is returned.
//
// The posititions & offsets are passed to a formLayout -attachPosition call.
//
global proc string DL_customSeparator(
  string $name,
  int $height,
  int $offset_top,
  int $offset_bottom,
  int $offset_left,
  int $offset_right,
  int $position_left,
  int $position_right )
{
  string $sep_name = $name;
  string $layout = $name == "" ? "" : $name + "formLayout";

  string $separator_cmd = "separator -height 1 -st none -bgc 0.4 0.4 0.4 ";

  $separator_cmd += $sep_name;
  $layout = $layout == "" ? `formLayout` : `formLayout $layout`;
  $sep_name = eval($separator_cmd);
  setParent ..;

  if( $height == 0 )
    $height = 1;

  formLayout
    -edit
    -height ( $height + $offset_top + $offset_bottom )
    -attachForm $sep_name "top" ($height / 2 + $offset_top)
    -attachPosition $sep_name "left" $offset_left $position_left
    -attachPosition $sep_name "right" $offset_right $position_right
    -attachForm $sep_name "bottom" ($height / 2 + $offset_bottom)
    $layout;

  return $layout;
}

global proc string
DL_separatorWithLabel(string $base_name, string $label, int $label_width)
{
  return DL_customSeparatorWithLabel(
    $base_name,
    $label,
    $label_width,
    22,
    13, 5, 0, 0,
    10, 90 );
}

//
// Constructs a labeled separator with the specified height, offsets and
// positions.
//
// The actual separator will be named after the received name. It is placed in
// a form layout, whose name is returned.
//
// The posititions & offsets are passed to a formLayout -attachPosition call.
//
global proc string DL_customSeparatorWithLabel(
  string $base_name,
  string $label,
  int $label_width,
  int $height,
  int $offset_top,
  int $offset_bottom,
  int $offset_left,
  int $offset_right,
  int $position_left,
  int $position_right )
{
  if ($label == "" || $label_width <= 0)
  {
    return DL_customSeparator(
      $base_name,
      $height,
      $offset_top, $offset_bottom, $offset_left, $offset_right,
      $position_left, $position_right);
  }

  // The attributeEditor template draws odd separators in 2016
  setUITemplate -pushTemplate NONE;

  // AE_separatorManage assumes specific layout name construct; change with
  // caution.
  string $layout = $base_name + "Layout";
  string $sep1 = $base_name + "Separator1";
  string $sep2 = $base_name + "Separator2";
  string $text = $base_name + "Label";

  if ($base_name == "")
  {
    $layout = `formLayout`;
    $base_name = $layout;
  }
  else
  {
    $layout = `formLayout ($base_name + "Layout")`;
  }

  // 2016 changes:
  // -style "single" -hr true is identical to the default
  // -hr works only with "single" style, and if not specified, the separator
  //  is vertical with the "single" style
  // -enable false does nothing visually
  //
  // pre-2016 command was:
  // separator -height 2 -st single -hr true -enable false
  //
  // $enable_background was true.
  //
  string $separator_cmd = "separator -height 1 -st none -bgc 0.4 0.4 0.4 ";
  int $sep_v_offset = $height / 2;

  $sep1 = eval($separator_cmd + $base_name + "Separator1");

  $text = `text
    -label ("<span style=\"color:#909090;\"><i>" + $label + "</i></span>")
    ($base_name + "Label")`;

  $sep2 = eval($separator_cmd + $base_name + "Separator2");

  setParent ..;

  formLayout
    -edit
    -height ( $height + $offset_top + $offset_bottom )
    -attachForm $text "top" $offset_top
    -attachForm $text "bottom" $offset_bottom
    -attachPosition $text "left" ($label_width / -2) 50
    -attachNone $text "right"

    -attachForm $sep1 "top" ( $sep_v_offset + $offset_top )
    -attachPosition $sep1 "left" $offset_left $position_left
    -attachControl $sep1 "right" 3 $text
    -attachForm $sep1 "bottom" ( $sep_v_offset + $offset_bottom )

    -attachForm $sep2 "top" ( $sep_v_offset + $offset_top )
    -attachControl $sep2 "left" 4 $text
    -attachPosition $sep2 "right" $offset_right $position_right
    -attachForm $sep2 "bottom" ( $sep_v_offset + $offset_bottom )
    $layout;

  setUITemplate -popTemplate;

  return $layout;
}

/*
  Creates a frame layout with a header looking similar to DL_separatorWithLabel.

  The separator has almost no margin; it is aligned with its content's slightly
  lighter background region.

  Note that this is not currently collapsible.

  The name of the returned layout is the name of the parent layout of this
  compound gadget. It differs from the parent that is current at the time
  this procedure returns; the current parent is the layout where to add
  elements of the frame layout. To go back to the parent that was current
  before this procedure was called, you need to call `setParent ..` 3 times.
*/
global proc string DL_frameLayout(
  string $base_name,
  string $label,
  int $label_width)
{
  string $base_form = `formLayout $base_name`;

  string $frame_tweak_form = `formLayout ($base_name + "Tweak")`;
  string $frame_layout = `frameLayout
    -label $label
    -collapsable true
    -collapse false
    ($base_name + "Frame")`;

  string $column =
    `columnLayout -adjustableColumn true ($frame_layout + "Column")`;

  // spacer
  text -label "" -height 18;

  setParent ..;  // from column layout
  setParent ..;  // from frame layout
  setParent ..;  // from form layout

  string $sep = DL_separatorWithLabel($base_name + "Sep", $label, $label_width);
  string $sep_components[] = `layout -q -childArray $sep`;

  // Adjust the separator with label layout  to remove its margins
  formLayout
    -edit
    -attachPosition $sep_components[0] "left" 2 0
    -attachPosition $sep_components[2] "right" 2 100
    $sep;

  setParent ..; // from form layout

  // Hide the bar from the frame layout
  formLayout
    -edit
    -attachForm $frame_layout "top" -20
    -attachForm $frame_layout "left" 0
    -attachForm $frame_layout "right" 0
    $frame_tweak_form;

  // Align the separator with the top of the frame layout
  formLayout
    -edit
    -attachForm $sep "top" 0
    -attachForm $sep "left" 0
    -attachForm $sep "right" 0

    -attachControl $frame_tweak_form "top" -21 $sep
    -attachForm $frame_tweak_form "left" 0
    -attachForm $frame_tweak_form "right" 0
    -attachForm $frame_tweak_form "bottom" 0
    $base_form;

  setParent $column;

  return $base_form;
}

// Selects one of three integers according to the current OS.
global proc int
DL_switchOsInt(int $linux, int $mac, int $windows)
{
  if(`about -linux`) return $linux;
  if(`about -mac`) return $mac;
  return $windows;
}

