/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

//
// Called by AEgetRelatedNodes in showEditor.mel; this procedure returns the
// list of nodes that can appear in the tab layout of the Attribute Editor,
// next to $node.
//
global proc string[] AEdlRenderSettingsRelated( string $node )
{
  string $related_nodes[] = { };
  $related_nodes[ 0 ] = $node;

  /*
    List all connected nodes as related, and add the assigned shaders too.
  */
  string $connected_nodes[] = `listConnections $node`;
  string $secondary_nodes[];

  for( $curr_node in $connected_nodes )
  {
    /*
      Do not show resolution & render global nodes as related nodes; 
      simply displaying a resolution node them in the AE corrupts some of its
      values. They both are internal nodes that the user has no reason to 
      interact with directly.
    */
    string $type = `nodeType $curr_node`;

    if( $type == "resolution" || $type == "dlRenderGlobals" )
      continue;

    $related_nodes[ size( $related_nodes ) ] = $curr_node;
  }
  
  /*
    The last node of this list is expected to be a duplicate of one of the 
    list items; its purpose is to indicate the node the AE will focus on by
    default (i.e. the selected tab by default)
  */
  $related_nodes[ size( $related_nodes ) ] = $node;
  return $related_nodes;
}

