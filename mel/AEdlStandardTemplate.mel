/*
   Copyright (c) 2012 soho vfx inc.
   Copyright (c) 2018 The 3Delight Team.
   */

global proc AE_STD_TransmissionChanged(string $node)
{
  int $dim = getAttr( $node + ".transmission" ) == 0;
  editorTemplate -dimControl $node "transmission_color" $dim;
  editorTemplate -dimControl $node "transmission_scatter" $dim;
  editorTemplate -dimControl $node "transmission_depth" $dim;
}

global proc
AEdlStandardTemplate(string $node)
{
  editorTemplate -beginScrollLayout;

  editorTemplate -callCustom AEshaderTypeNew AEshaderTypeReplace "message";

  // Base
  AE_addSeparatorWithLabel( "baseSep", "Base", 40 );
  editorTemplate -label "Base" -addControl "base";
  editorTemplate -label "Color" -addControl "base_color";
  editorTemplate -label "Roughness" -addControl "diffuse_roughness";

  // Specular
  AE_addSeparatorWithLabel( "specSep", "Specular", 50 ); 
  editorTemplate -label "Specular" -addControl "specular";
  editorTemplate -label "Color" -addControl "specular_color";
  editorTemplate -label "Roughness" -addControl "specular_roughness";
  editorTemplate -label "Metalness" -addControl "metalness";
  editorTemplate -label "IOR" -addControl "specular_IOR";
  editorTemplate -label "Anisotropy" -addControl "specular_anisotropy";
  editorTemplate -label "Rotation" -addControl "specular_rotation";

  // Coating
  AE_addSeparatorWithLabel( "coatingSep", "Coating", 50 );
  editorTemplate -label "Coat" -addDynamicControl "coat";
  editorTemplate -label "Color" -addDynamicControl "coat_color";
  editorTemplate -label "Roughness" -addDynamicControl "coat_roughness";
  editorTemplate -label "IOR" -addDynamicControl "coat_IOR";
  editorTemplate -label "Affects Color" -addDynamicControl "coat_affect_color";
  editorTemplate -label "Affects Roughness" -addDynamicControl "coat_affect_roughness";

  // Thin Film
  AE_addSeparatorWithLabel( "thinFilmSep", "Thin Film", 60 );
  editorTemplate -label "Thickness" -addDynamicControl "thin_film_thickness";
  editorTemplate -label "IOR" -addDynamicControl "thin_film_IOR";

  // Subsurface
  AE_addSeparatorWithLabel( "subsurfaceSep", "Subsurface", 60 );
  editorTemplate -label "Subsurface" -addDynamicControl "subsurface";
  editorTemplate -label "Color" -addDynamicControl "subsurface_color";
  editorTemplate -label "Radius" -addDynamicControl "subsurface_radius";
  editorTemplate -label "Scale" -addDynamicControl "subsurface_scale";
  editorTemplate -label "Anisotropy" -addDynamicControl "subsurface_anisotropy";
  editorTemplate -label "Merge Set" -addDynamicControl "subsurface_merge_set";
  editorTemplate -label "Dominant Material" -addDynamicControl "subsurface_dominant_material";
  editorTemplate -label "Double-Sided" -addDynamicControl "subsurface_double_sided";

  // Transmission
  AE_addSeparatorWithLabel( "transmissionSep", "Transmission", 60 );
  editorTemplate 
    -label "Transmission"
    -addDynamicControl "transmission" "AE_STD_TransmissionChanged";
  editorTemplate -label "Color" -addDynamicControl "transmission_color";
  editorTemplate -label "Scattering" -addDynamicControl "transmission_scatter";
  editorTemplate -label "Depth" -addDynamicControl "transmission_depth";
  editorTemplate -label "Transmit Aovs" -addDynamicControl "transmitAovs";

  // Emission
  AE_addSeparatorWithLabel( "emissionSep", "Emission", 80 );
  editorTemplate -label "Emission" -addControl "emission_w";
  editorTemplate -label "Color" -addControl "emission_color";

  // Geometry
  AE_addSeparatorWithLabel( "geoSep", "Geometry", 60 );
	editorTemplate -label "Opacity" -addDynamicControl "opacity";
  AE_addNavigationControlWithLabel("input_normal", "Base Normal");
  AE_addNavigationControlWithLabel("coat_normal", "Coat Normal");
  editorTemplate -label "Thin Walled" -addDynamicControl "thin_walled";

  AE_addAOVGroup("aovGroup");

  editorTemplate -suppress "volumetric_enable";

  editorTemplate -addExtraControls -extraControlsLabel "";
  editorTemplate -suppress caching;
  editorTemplate -suppress nodeState;
  editorTemplate -suppress "uvCoord";
  editorTemplate -suppress "incandescence_multiplier";

  editorTemplate -suppress "usedBy3dfm";
  editorTemplate -suppress "version";
  editorTemplate -suppress "frozen";

  editorTemplate -endScrollLayout;
}
