import maya.app.general.fileTexturePathResolver as resolver

findAllFilesForPatternOriginal = resolver.findAllFilesForPattern

def findAllFilesForPatternNew(pattern, frameNumber):
    # First try matching to the end of the file name to avoid matching our
    # converted textures.
    result = findAllFilesForPatternOriginal(pattern + '$', frameNumber)
    if result:
        return result
    # Fallback in case pattern does not contain full file name.
    return findAllFilesForPatternOriginal(pattern, frameNumber)

resolver.findAllFilesForPattern = findAllFilesForPatternNew

# vim: set softtabstop=4 expandtab shiftwidth=4:                                        

