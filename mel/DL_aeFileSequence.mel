/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

proc string[]
getOutsideRangeModeMenuItemLabels()
{
  return { "Hold first / last file index", "Repeat sequence" };
}

global proc
AE_fileSequenceNew(string $plug)
{
  string $node = plugNode($plug);

  text -label "" -h 4;

  checkBoxGrp -label1 "Use File Sequence" "fileSequenceModeCheckBoxGrp";
  intFieldGrp
    -label "Index Range"
    -numberOfFields 2
    "fileSequenceIndexRangeGrp";

  string $labels[] = getOutsideRangeModeMenuItemLabels();

  optionMenuGrp -label "Outside Range" "fileSequenceOutsideRangeModeGrp";
    for ($curr_label in $labels)
    {
      menuItem -label $curr_label;
    }
    setParent ..;

  attrControlGrp
    -label "Offset"
    -attribute ($node + ".fileSequenceOffset")
    -hideMapButton true
    "fileSequenceOffsetGrp";

  attrControlGrp
    -label "Increment"
    -attribute ($node + ".fileSequenceIncrement")
    -hideMapButton true
    "fileSequenceIncrementGrp";

  text -label "" -h 8;
  checkBoxGrp
    -label1 "Confine to Frame Range"
    "fileSequenceFrameRangeModeGrp";

  intFieldGrp
    -label "Frame Range"
    -numberOfFields 2
    "fileSequenceFrameRangeGrp";

  AE_fileSequenceReplace($plug);
}

global proc
AE_fileSequenceReplace(string $plug)
{
  string $node = plugNode($plug);

  connectControl
    -index 2 "fileSequenceModeCheckBoxGrp"
    ($node + ".fileSequenceMode");
  connectControl
    -index 2 "fileSequenceIndexRangeGrp"
    ($node + ".fileSequenceFirstIndex");
  connectControl
    -index 3 "fileSequenceIndexRangeGrp"
    ($node + ".fileSequenceLastIndex");

  connectControl
    -index 2 "fileSequenceFrameRangeModeGrp"
    ($node + ".fileSequenceFrameRangeMode");
  connectControl
    -index 2 "fileSequenceFrameRangeGrp"
    ($node + ".fileSequenceFrameRangeStart");
  connectControl
    -index 3 "fileSequenceFrameRangeGrp"
    ($node + ".fileSequenceFrameRangeEnd");

  attrControlGrp
    -edit
    -attribute ($node + ".fileSequenceOffset")
    "fileSequenceOffsetGrp";

  attrControlGrp
    -edit
    -attribute ($node + ".fileSequenceIncrement")
    "fileSequenceIncrementGrp";

  string $parent = `setParent -q`;
  string $cmd_params = "\"" + $node + "\" " + "\"" + $parent + "\"";
  string $cmd = "AE_fileSequenceModeChangedCB " + $cmd_params;

  string $range_mode_cb =
    "AE_fileSequenceFrameRangeModeChangedCB " + $cmd_params;

  $cmd += " \"" + encodeString($range_mode_cb) + "\"";

  scriptJob
    -attributeChange ($node + ".fileSequenceMode") $cmd
    -compressUndo true
    -replacePrevious
    -parent "fileSequenceModeCheckBoxGrp";

  eval($cmd);

  scriptJob
    -attributeChange ($node + ".fileSequenceFrameRangeMode") $range_mode_cb
    -compressUndo true
    -replacePrevious
    -parent "fileSequenceFrameRangeModeGrp";

  eval($cmd);

  $cmd = "AE_fileSequenceOutsideRangeModeMenuChangedCB " + $cmd_params;
  optionMenuGrp -edit -changeCommand $cmd "fileSequenceOutsideRangeModeGrp";

  $cmd = "AE_fileSequenceOutsideRangeModeChangedCB " + $cmd_params;

  scriptJob
    -attributeChange ($node + ".fileSequenceOutsideRangeMode") $cmd
    -compressUndo true
    -replacePrevious
    -parent "fileSequenceOutsideRangeModeGrp";

  eval($cmd);
}

global proc
AE_fileSequenceModeChangedCB(
  string $node,
  string $parent,
  string $frameRangeModeChangedCB )
{
  string $curr_parent = `setParent -q`;
  setParent $parent;

  int $fs_mode = getAttr($node + ".fileSequenceMode");
  int $enable = $fs_mode != 0;

  DL_changeSensitivity("fileSequenceIndexRangeGrp", $enable);
  DL_changeSensitivity("fileSequenceOutsideRangeModeGrp", $enable);
  DL_changeSensitivity("fileSequenceOffsetGrp", $enable);
  DL_changeSensitivity("fileSequenceIncrementGrp", $enable);
  DL_changeSensitivity("fileSequenceFrameRangeModeGrp", $enable);
  eval( $frameRangeModeChangedCB );

  setParent $curr_parent;
}

global proc
AE_fileSequenceFrameRangeModeChangedCB(string $node, string $parent)
{
  string $curr_parent = `setParent -q`;
  setParent $parent;

  int $fs_mode = getAttr($node + ".fileSequenceMode");
  int $fs_frame_mode = getAttr($node + ".fileSequenceFrameRangeMode");

  int $enable = $fs_mode != 0 && $fs_frame_mode != 0;

  DL_changeSensitivity("fileSequenceFrameRangeGrp", $enable);

  setParent $curr_parent;
}

global proc
AE_fileSequenceOutsideRangeModeChangedCB(string $node, string $parent)
{
  string $curr_parent = `setParent -q`;
  setParent $parent;

  int $mode = getAttr ($node + ".fileSequenceOutsideRangeMode");
  string $labels[] = getOutsideRangeModeMenuItemLabels();

  if ($mode >= 0 && $mode < size($labels))
  {
    optionMenuGrp -edit -value $labels[$mode] "fileSequenceOutsideRangeModeGrp";
  }

  setParent $curr_parent;
}

global proc
AE_fileSequenceOutsideRangeModeMenuChangedCB(string $node, string $parent)
{
  string $curr_parent = `setParent -q`;
  setParent $parent;

  string $item = `optionMenuGrp -q -value "fileSequenceOutsideRangeModeGrp"`;
  string $labels[] = getOutsideRangeModeMenuItemLabels();
  int $mode = 0;
  for ($i = 0; $i < size($labels); $i++)
  {
    if ($item == $labels[$i])
    {
      $mode = $i;
      break;
    }
  }

  setAttr ($node + ".fileSequenceOutsideRangeMode") $mode;

  setParent $curr_parent;
}