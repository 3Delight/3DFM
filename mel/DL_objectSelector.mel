/*
	Copyright (c) 2006 soho vfx inc.
	Copyright (c) 2006 The 3Delight Team.
*/

global proc
DOS_applyChanges( string $message_attrib, string $filter )
{
  DL_disconnectNode( $message_attrib );

  string $selected_sets[] = `selectionConnection -q -object DOSselectionConn`;
  for( $set in $selected_sets )
  {
    DL_connectNodeToMultiMessagePlug( $set, $message_attrib );
  }

  deleteUI delightObjectSelector;
  deleteUI DOSselectionConn;
  delete $filter;
}

global proc
DOS_cancelChanges( string $filter )
{
  deleteUI delightObjectSelector;
  deleteUI DOSselectionConn;
  delete $filter;
}

/*
  Filter used in the outliner control to show only user sets.
*/
global proc int
DOS_isObjSet( string $name )
{
  if (   `nodeType $name` != "objectSet"
      || `sets -q -vertices $name`
      || `sets -q -edges $name`
      || `sets -q -facets $name`
      || `sets -q -editPoints $name`
      || `sets -q -renderable $name`
      || $name == "defaultObjectSet"
      || $name == "defaultLightSet" )
  {
    return 0;
  }
  else
    return 1;
}

//
// Item filter script to keep user-defined sets and geometric objects.
//
global proc int
DOS_isGeoOrSet( string $name )
{
  string $type = `nodeType $name`;

  switch( $type )
  {
    case "objectSet":
      return $name != "defaultObjectSet" && $name != "defaultLightSet";

    case "delightCSG":
    case "dlSet":
    case "transform":
    case "nurbsSurface":
    case "mesh":
    case "subdiv":
    case "instancer":
    case "particle":
    case "nParticle":
      return 1;
  }

  return 0;
}

global proc
openDelightSetSelector( string $message_attrib )
{
  // Starting with Maya 2017, the itemFilter -uniqueNodeNames flag is buggy.
  // It becomes effective only as a "edit" operation.
  //
  string $filter = `itemFilter -byScript DOS_isObjSet`;
  itemFilter -edit -uniqueNodeNames true $filter;

  openDelightObjectSelector(
    $message_attrib, 
    $filter,
    "3Delight Set Selector",
    false );
}

global proc
openDelightGeoAndSetSelector( string $message_attrib, string $title )
{
  // Starting with Maya 2017, the itemFilter -uniqueNodeNames flag is buggy.
  // It becomes effective only as a "edit" operation.
  //
  string $filter = `itemFilter -byScript DOS_isGeoOrSet`;
  itemFilter -edit -uniqueNodeNames true $filter;

  openDelightObjectSelector(
    $message_attrib, 
    $filter,
    $title,
    true );
}

//
// Open an outliner editor inside a small dialog to edit connections between
// some Maya objects and the specified message attribute.
//
// The item filter passed as $filter will be deleted when the dialog is closed.
//
global proc
openDelightObjectSelector(
  string $message_attrib,
  string $filter,
  string $title,
  int $show_set_members)
{
  string $window_name = "delightObjectSelector";

  if( `window -exists $window_name` )
    deleteUI $window_name;

  int $width = 275;
  int $height = 275;
  
  if ( `windowPref -exists $window_name` )
  {
    $width = `windowPref -q -width $window_name`;
    $height = `windowPref -q -height $window_name`;
  }

  window 
    -title $title
    -ip
    -mb on
    -rtf false
    -width $width
    -height $height
    delightObjectSelector;

  if( `selectionConnection -exists DOSselectionConn` )
    deleteUI DOSselectionConn;

  selectionConnection DOSselectionConn;

  formLayout DOSmainForm;
  {
    outlinerEditor
      -mainListConnection "worldList"
      -selectionConnection DOSselectionConn
      -filter $filter
      -showShapes false
      -showAttributes false
      -showDagOnly false
      -autoSelectNewObjects false
      -doNotSelectNewObjects false
      -showSelected false
      -showSetMembers $show_set_members
      -allowMultiSelection true
      -alwaysToggleSelect true
      -directSelect true
      -expandObjects false
      DOSoutlinerEditor;

    formLayout DOSbottomButtonsForm;
    {
      button 
        -l "Apply"
        -h 26
        -c ("DOS_applyChanges " + $message_attrib + " " + $filter)
        DOSapplyButton;

      button 
        -l "Cancel"
        -h 26
        -c ("DOS_cancelChanges " + $filter)
        DOScancelButton;

      formLayout 
        -edit
        -attachForm DOSapplyButton top 5
        -attachForm DOSapplyButton bottom 5
        -attachForm DOSapplyButton left 5
        -attachControl DOSapplyButton right 5 DOScancelButton
        -attachForm DOScancelButton top 5
        -attachForm DOScancelButton bottom 5
        -attachForm DOScancelButton right 5
        DOSbottomButtonsForm;

      setParent ..;
    }

    formLayout 
      -edit
      -attachForm DOSoutlinerEditor top 5
      -attachForm DOSoutlinerEditor left 5
      -attachForm DOSoutlinerEditor right 5
      -attachControl DOSoutlinerEditor bottom 5 DOSbottomButtonsForm
      -attachNone DOSbottomButtonsForm top
      -attachForm DOSbottomButtonsForm right 5
      -attachForm DOSbottomButtonsForm right 5
      -attachForm DOSbottomButtonsForm bottom 5
      DOSmainForm;

    setParent ..;
  }

  showWindow;

  /*
    Setting the previous selection does not work for whatever stupid reason.
    The window needs to become visible first so we'll wait until the next idle
    event.
  */
  string $start_connections[] = `listConnections $message_attrib`;
  string $select_cmd = "";

  for( $conn in $start_connections )
  {
    $select_cmd += "selectionConnection -e -select " + $conn + 
      " DOSselectionConn;";
  }

  if( $select_cmd != "" )
  {
    evalDeferred $select_cmd;
  }
}
