/*
	Copyright (c) 2006 soho vfx inc.
	Copyright (c) 2006 The 3Delight Team.
*/

//
// This patch counter purpose is to create a unique dummy attribs name.
// Things seem to work well when we create compound attributes if we create
// them with a unique temporary name and then rename the attribute to the
// desired name; this is mandatory when creating an attribs which has the same
// name as one that was recently deleted.
//
global int $gCompoundAttrPatchCounter = 0;


global proc string[]
getEnumerands(string $attrib_name)
{
  string $all_enumerands;
  string $enumerand_tokens[];

  $all_enumerands = `addAttr -q -enumName $attrib_name`;

  tokenize($all_enumerands, ":", $enumerand_tokens);

  return $enumerand_tokens;
}

global proc string
getEnumAttribValueAsString(int $value, string $attrib_name)
{
  string $all_enumerands[];

  $all_enumerands = getEnumerands($attrib_name);

  return $all_enumerands[$value];
}

global proc int
getNumMultiAttribElements(string $attrib_name)
{
  return `getAttr -size $attrib_name`;
}

global proc string[]
getMultiComplexAttribElementNames(string $attrib_name)
{
  // Returns the list of attributes that are elements of the specified
  // attribute. For simple attribute types, `listAttr -multi` is enough. For
  // more complex types (like a color attr), it also returns each element's
  // components. This procedure filter these components out.
  //
  string $elements[];
  clear($elements);

  string $all_components[] = `listAttr -multi $attrib_name`;
  for($curr_component in $all_components)
  {
    if (match("\[[0-9]+\]$", $curr_component) != "")
      $elements[size($elements)] = $curr_component;
  }

  return $elements;
}

global proc int
getNumMultiComplexAttribElements(string $attrib_name)
{
  // Same as getNumMultiAttribElements but works with multi attributes of
  // complex types, like colors. The other version is kept since it is probably
  // faster.
  //
  return size(getMultiComplexAttribElementNames($attrib_name));
}

proc int[]
getElementIndices(string $elements[])
{
  int $indices[];

  for ($curr_element in $elements)
  {
    string $element_tokens[];

    tokenize($curr_element, "[]", $element_tokens);

    $indices[size($indices)] = int($element_tokens[size($element_tokens)-1]);
  }

  return $indices;
}

global proc int[]
getMultiComplexAttribElementIndices(string $attrib_name)
{
  string $elements[] = getMultiComplexAttribElementNames($attrib_name);
  return getElementIndices($elements);
}

global proc int[]
getMultiAttribElementIndices(string $attrib_name)
{
  string $elements[] = `listAttr -multi $attrib_name`;
  return getElementIndices($elements);
}

proc int
findNextElementIndex(string $elements[])
{
  if (size($elements) > 0)
  {
    string $last_element = $elements[size($elements)-1];

    string $element_tokens[];

    int $num_tokens = tokenize($last_element, "[]", $element_tokens);
    return int($element_tokens[size($element_tokens)-1]) + 1;
  }
  else
    return 0;
}

global proc int
getNextMultiComplexAttribElementIndex(string $attrib_name)
{
  string $elements[] = getMultiComplexAttribElementNames($attrib_name);
  return findNextElementIndex($elements);
}

global proc int
getNextMultiAttribElementIndex(string $attrib_name)
{
  string $elements[];
  $elements = `listAttr -multi $attrib_name`;
  return findNextElementIndex($elements);
}

global proc string[]
getAllMultiAttributeStringValues(string $attrib_name)
{
  return `getAllMultiAttributeValues -str $attrib_name`;
}

global proc string
getMultiAttributeStringValue(string $attrib_name, int $which)
{
  string $ret_val = getAttr("-asString", $attrib_name + "[" + $which + "]");
  return $ret_val;
}

global proc float[]
getAllMultiAttributeFloatValues(string $attrib_name)
{
  return `getAllMultiAttributeValues -flt $attrib_name`;
}

global proc float
getMultiAttributeFloatValue(string $attrib_name, int $which)
{
  return getAttr($attrib_name + "[" + $which + "]");
}

global proc int[]
getAllMultiAttributeIntValues(string $attrib_name)
{
  return `getAllMultiAttributeValues -int $attrib_name`;
}

global proc int
getMultiAttributeIntValue(string $attrib_name, int $which)
{
  return getAttr($attrib_name + "[" + $which + "]");
}

proc
getAllAttributeChildren(
  string $node,
  string $attr,
  string $children[],
  int $skipDestination,
  int $skipSource)
{
  $children[size($children)] = $attr;

  if($skipDestination && `connectionInfo -isDestination ($node + "." + $attr)`)
  {
    return;
  }

  if($skipSource && `connectionInfo -isSource ($node + "." + $attr)`)
  {
    return;
  }

  string $direct_children[] = `attributeQuery -listChildren -node $node $attr`;

  if (size($direct_children) > 0)
  {
    for($curr_child in $direct_children)
    {
      getAllAttributeChildren(
        $node, $curr_child, $children, $skipDestination, $skipSource);
    }
  }
}

global proc
DL_createStringAttr(string $node, string $attr, string $default)
{
  addAttr -ln $attr -dt "string" $node;
  setAttr ($node + "." + $attr) -type "string" "dummy_string";
  setAttr ($node + "." + $attr) -type "string" $default;
}

global proc
DL_createFloat3Attr(
  string $node,
  string $attr,
  int $use_as_color,
  float $red,
  float $green,
  float $blue)
{
  global int $gCompoundAttrPatchCounter;
  string $temp_name = "dummy" + $gCompoundAttrPatchCounter;

  if ($use_as_color != 0)
  {
    addAttr -ln $temp_name -at "float3" -usedAsColor $node;
  }
  else
  {
    addAttr -ln $temp_name -at "float3" $node;
  }

  addAttr
    -ln ($temp_name + "R")
    -at "float"
    -dv $red
    -parent $temp_name $node;

  addAttr
    -ln ($temp_name + "G")
    -at "float"
    -dv $green
    -parent $temp_name $node;

  addAttr
    -ln ($temp_name + "B")
    -at "float"
    -dv $blue
    -parent $temp_name $node;

  renameAttr ($node + "." + $temp_name) $attr;
  renameAttr ($node + "." + $temp_name + "R") ($attr + "R");
  renameAttr ($node + "." + $temp_name + "G") ($attr + "G");
  renameAttr ($node + "." + $temp_name + "B") ($attr + "B");

  $gCompoundAttrPatchCounter ++;
}

global proc
DL_createLong2Attr(
  string $node,
  string $attr,
  int $x,
  int $y)
{
  global int $gCompoundAttrPatchCounter;
  string $temp_name = "dummy" + $gCompoundAttrPatchCounter;

  addAttr -ln $temp_name -at "long2" $node;

  addAttr
    -ln ($temp_name + "X")
    -at "long"
    -dv $x
    -parent $temp_name $node;

  addAttr
    -ln ($temp_name + "Y")
    -at "long"
    -dv $y
    -parent $temp_name $node;

  renameAttr ($node + "." + $temp_name) $attr;
  renameAttr ($node + "." + $temp_name + "X") ($attr + "X");
  renameAttr ($node + "." + $temp_name + "Y") ($attr + "Y");

  $gCompoundAttrPatchCounter ++;
}

global proc
DL_createShort2Attr(
  string $node,
  string $attr,
  int $x,
  int $y)
{
  global int $gCompoundAttrPatchCounter;
  string $temp_name = "dummy" + $gCompoundAttrPatchCounter;

  addAttr -ln $temp_name -at "short2" $node;

  addAttr
    -ln ($temp_name + "X")
    -at "short"
    -dv $x
    -parent $temp_name $node;

  addAttr
    -ln ($temp_name + "Y")
    -at "short"
    -dv $y
    -parent $temp_name $node;

  renameAttr ($node + "." + $temp_name) $attr;
  renameAttr ($node + "." + $temp_name + "X") ($attr + "X");
  renameAttr ($node + "." + $temp_name + "Y") ($attr + "Y");

  $gCompoundAttrPatchCounter ++;
}

global proc
DL_createFloat2Attr(string $node, string $attr, float $x, float $y)
{
  global int $gCompoundAttrPatchCounter;
  string $temp_name = "dummy" + $gCompoundAttrPatchCounter;

  addAttr -ln $temp_name -at "float2" $node;

  addAttr
    -ln ($temp_name + "X")
    -at "float"
    -dv $x
    -parent $temp_name $node;

  addAttr
    -ln ($temp_name + "Y")
    -at "float"
    -dv $y
    -parent $temp_name $node;

  renameAttr ($node + "." + $temp_name) $attr;
  renameAttr ($node + "." + $temp_name + "X") ($attr + "X");
  renameAttr ($node + "." + $temp_name + "Y") ($attr + "Y");

  $gCompoundAttrPatchCounter ++;
}

global proc
DL_copyAttribute(string $src_plug, string $dst_plug)
{
  // Copy an attribute, taking into account its keys, connections and values.
  // Works on simple and compound attributes.
  //

  string $all_src_attrs[];
  string $src_node = plugNode($src_plug);
  string $src_attr = plugAttr($src_plug);
  string $src_children[];

  string $all_dst_attrs[];
  string $dst_node = plugNode($dst_plug);
  string $dst_attr = plugAttr($dst_plug);
  string $dst_children[];

  int $copy_multi_elements = 0;

  // This is to avoid calling attributeQuery with an attribute that contains
  // [x] since this form is not supported by the command.
  //
  int $src_is_array_element = size(match("\\[.*\\]", $src_attr));

  // Add the multi elements, if any
  if (!$src_is_array_element &&
    `attributeQuery -node $src_node -multi $src_attr`)
  {
    $all_src_attrs = `listAttr -multi $src_plug`;
    $copy_multi_elements = (size($all_src_attrs) != 1 ||
      (size($all_src_attrs) == 1 && $src_attr != $all_src_attrs[0]));
  }

  if ($copy_multi_elements)
  {
    for ($curr_attr in $all_src_attrs)
    {
      // Possible complex src names may be formed like this:
      // pointArray[0].pointArrayX
      // So split the source attribute name based on ".", and substitute the
      // source attribute name in each token with the destination attribute.
      //
      string $tokens[];
      int $num_tokens = tokenize($curr_attr, ".", $tokens);
      string $curr_dst_attr = "";

      int $i;
      for($i = 0; $i < $num_tokens; $i++)
      {
        if ($i > 0)
          $curr_dst_attr += ".";

        $curr_dst_attr += substitute($src_attr, $tokens[$i], $dst_attr);
      }

      $all_dst_attrs[size($all_dst_attrs)] = $curr_dst_attr;
    }
  }
  else
  {
    // attributeQuery doesn't work with attributes that are array elements.
    //
    if (match("\\[.*\\]", $src_attr) == "")
      getAllAttributeChildren($src_node, $src_attr, $src_children, 1, 0);
    else
      $all_src_attrs[0] = $src_attr;

    if (match("\\[.*\\]", $dst_attr) == "")
      getAllAttributeChildren($dst_node, $dst_attr, $dst_children, 0, 1);
    else
      $all_dst_attrs[0] = $dst_attr;

    DL_stringArrayAppend($all_src_attrs, $src_children);
    DL_stringArrayAppend($all_dst_attrs, $dst_children);

    if (size($all_src_attrs) != size($all_dst_attrs))
    {
      // The src and dst attributes don't have the same number of children.
      // I guess we can work on the attr themselves
      //
      clear($all_src_attrs);
      clear($all_dst_attrs);

      $all_src_attrs[0] = $src_attr;
      $all_dst_attrs[0] = $dst_attr;
    }
  }

  int $i;
  for ($i = 0; $i < size($all_src_attrs); $i++)
  {
    string $curr_src_attr = $all_src_attrs[$i];
    string $curr_src_plug = $src_node + "." + $curr_src_attr;

    string $curr_dst_attr = $all_dst_attrs[$i];
    string $curr_dst_plug = $dst_node + "." + $curr_dst_attr;

    int $keyedAttr = 0;

    string $connections[] =
      `listConnections -p true -s true -d false $curr_src_plug`;

    // Duplicate keys, if any
    //
    if (size($connections) > 0)
    {
      $keyedAttr = isAnimCurve(`plugNode($connections[0])`);

      if ($keyedAttr > 0 && `copyKey -t ":" -at $curr_src_attr	$src_node` != 0)
      {
        pasteKey -option replaceCompletely -at $curr_dst_attr $dst_node;
        $keyedAttr = 1;
      }
    }

    // If the attr isn't keyed, then duplicate connections, or copy the value
    // if the attribute doesn't receive a value via a connection.
    //
    if ($keyedAttr == 0)
    {
      int $connections_restored = 0;

      $connections = `listConnections -p true -s false -d true $curr_src_plug`;
      if (size($connections) > 0)
      {
        // Attempt to duplicate any connections involving this attribute
        for($attr in $connections)
          connectAttr -f $curr_dst_plug $attr;
      }

      $connections = `listConnections -p true -s true -d false $curr_src_plug`;
      if (size($connections) > 0)
      {
        for($attr in $connections)
          connectAttr -f $attr $curr_dst_plug;

        $connections_restored = 1;
      }

      // If there was no connection to duplicate, attempt to copy the attribute's
      // value if the attribute is a simple data type.
      //
      if ($connections_restored)
        continue;

      if (size(`listAttr -userDefined $src_plug`) == 0)
      {
        continue;
      }

      string $attr_type = `addAttr -q -at $curr_src_plug`;
      if (   $attr_type == "bool"
          || $attr_type == "long"
          || $attr_type == "short"
          || $attr_type == "byte"
          || $attr_type == "enum"
          || $attr_type == "float"
          || $attr_type == "double"
          || $attr_type == "doubleAngle"
          || $attr_type == "doubleLinear")
      {
        setAttr $curr_dst_plug `getAttr $curr_src_plug`;
      }
      else if ($attr_type == "typed")
      {
        $attr_type = `getAttr -type $curr_src_plug`;

        if ($attr_type == "string" || $attr_type == "")
        {
          setAttr -type "string" $curr_dst_plug `getAttr $curr_src_plug`;
        }
      }
    }
  }
}

global proc
DL_duplicate(string $src_node, string $dst_node)
{
  // For all dynamic attributes on the source node, create a similar
  // attribute on dst_node and duplicate the source attribute's value or
  // connections.
  //

  string $src_attrs[] = `listAttr -userDefined $src_node`;

  // Create all attributes
  //
  for($curr_src_attr in $src_attrs)
  {
    string $src_plug = $src_node + "." + $curr_src_attr;

    string $cmd = "addAttr -ln \"" + $curr_src_attr + "\"";

    string $attr_type = `addAttr -q -at $src_plug`;
    string $data_type[] = `addAttr -q -dt $src_plug`;

    if ($attr_type == "typed")
      $cmd += " -dt \"" + $data_type[0] + "\"";
    else
      $cmd += " -at \"" + $attr_type + "\"";

    if ($attr_type == "compound")
      $cmd += " -nc " + `addAttr -q -nc $src_plug`;

    if ($attr_type == "enum")
      $cmd += " -enumName \"" + `addAttr -q -enumName $src_plug` + "\"";

    string $parent = `addAttr -q -parent $src_plug`;
    if ($parent != $curr_src_attr)
      $cmd += " -parent " + $parent;

    if (`addAttr -q -multi $src_plug`)
      $cmd += " -multi";

    switch($attr_type)
    {
      case "bool":
        $cmd += " -defaultValue " + `addAttr -q -defaultValue $src_plug`;
        break;

      case "long":
      case "short":
      case "float":
      case "double":
        $cmd += " -defaultValue " + `addAttr -q -defaultValue $src_plug`;

        if (`addAttr -q -hasMinValue $src_plug`)
          $cmd += " -minValue " + `addAttr -q -minValue $src_plug`;

        if (`addAttr -q -hasMaxValue $src_plug`)
          $cmd += " -maxValue " + `addAttr -q -maxValue $src_plug`;

        if (`addAttr -q -hasSoftMinValue $src_plug`)
          $cmd += " -softMinValue " + `addAttr -q -softMinValue $src_plug`;

        if (`addAttr -q -hasSoftMaxValue $src_plug`)
          $cmd += " -softMaxValue " + `addAttr -q -softMaxValue $src_plug`;
        break;

      case "float3":
        if (`addAttr -q -usedAsColor $src_plug`)
          $cmd += " -usedAsColor ";
        break;
    }

    if (`addAttr -q -keyable $src_plug`)
      $cmd += " -keyable true";

    if (`addAttr -q -hidden $src_plug`)
      $cmd += " -hidden true";

    $cmd += " " + $dst_node;
    eval($cmd);
  }

  // Duplicate the attribute values / keys / connections
  //
  for($curr_attr in $src_attrs)
  {
    // Don't bother with children attributes as DL_copyAttributes deals with
    // them
    //
    if (size(`attributeQuery -listParent -node $src_node $curr_attr`) > 0)
      continue;

    string $src_plug = $src_node + "." + $curr_attr;
    string $attr_type = `addAttr -q -at $src_plug`;
    DL_copyAttribute($src_plug, ($dst_node + "." + $curr_attr));
  }

  // Lock the attributes when relevant
  //
  for($curr_att in $src_attrs)
  {
    if (`getAttr -lock ($src_node + "." + $curr_attr)`)
      setAttr -lock true ($dst_node + "." + $curr_attr);
  }
}

global proc
DL_updateEnumAttr(string $plug, string $enum_names)
{
  // This procedures updates existing enum attributes to offer specified enum
  // names.
  //
  string $tokens[];
  clear($tokens);

  int $num_tokens = tokenize($enum_names, ":", $tokens);

  int $multi_values[];
  clear($multi_values);

  int $multi_indices[];
  clear($multi_values);

  string $node = plugNode($plug);
  string $attr = plugAttr($plug);

  int $is_multi = 0;

  if(objExists($plug))
  {
    $is_multi = `addAttr -q -multi $plug`;
    int $locked[] = `lockNode -q $node`;
    float $max = `addAttr -q -max $plug`;

    if ( $max != ($num_tokens - 1) && $locked[0] == 0 )
    {
      // Number of possible enum values is different in existing node.
      // Store its current value(s).
      if ($is_multi)
      {
        $multi_indices = getMultiAttribElementIndices($plug);

        for ($i in $multi_indices)
        {
          $multi_values[$i] = getAttr($plug + "[" + $i + "]");
        }
      }
      else
      {
        $multi_values[0] = getAttr($plug);
      }

      // Delete the existing attribute and create a fresh new one.
      //
      deleteAttr -at $attr $node;

      string $cmd = DL_getNodeTypePrefix($node);
      $cmd += $attr + "CreateAttr " + $node;
      eval($cmd);

      // Restore the previous values in the new attribute.
      //
      if($is_multi)
      {
        for($i in $multi_indices)
        {
          setAttr ($plug + "[" + $i + "]") $multi_values[$i];
        }
      }
      else
        setAttr $plug $multi_values[0];
    }
  }
}

// Returns true if the attribute is a float3 color attribute, or a child of such
// an attribute.
//
proc int
isColorAttribute(string $node, string $attr)
{
  string $parents[] = `attributeQuery -node $node -listParent $attr`;
  if(size($parents) > 0)
  {
    $attr = $parents[0];
  }

  return `attributeQuery -node $node -usedAsColor $attr`;
}

global proc int
DL_isColorAttribute(string $plug)
{
  // Returns true if $plug is a color attribute. $plug must be float3 or double3
  // (requirement from attributeQuery).
  //
  // For dynamic attributes, it is probably more efficient to use:
  // addAttr -q -usedAsColor
  // But when it is not garanteed that $plug is a dynamic attribute, this tries
  // to make "attributeQuery" happy with regular or compound attributes, such
  // as: ramp1.colorEntryList[1].color
  //
  string $node = plugNode($plug);
  string $attr = plugAttr($plug);
  string $compound_element = plugAttr($attr);

  if ($compound_element == "")
  {
    return isColorAttribute($node, $attr);
  }

  string $compound_attr = plugNode($attr);
  $compound_attr = match("[^\[]*", $compound_attr);

  return isColorAttribute(($node + "." + $compound_attr), $compound_element);
}

// Returns a string like "node.attr[i].attr_"
proc string GetRampAttributePrefix( string $node, string $attr, int $index )
{
  return $node + "." + $attr + "[" + $index + "]." + $attr + "_";
}

// Sets one float ramp point at the specified index.
global proc DL_setOneFloatRampPoint(
  string $node,
  string $attr,
  int $index,
  float $position,
  int $interpolation,
  float $value )
{
  string $attr_prefix = GetRampAttributePrefix( $node, $attr, $index );
  setAttr ( $attr_prefix + "Position" ) $position;
  setAttr ( $attr_prefix + "FloatValue" ) $value;
  setAttr ($attr_prefix + "Interp" ) $interpolation;
}

// Sets one color ramp point at the specified index
global proc DL_setOneColorRampPoint(
  string $node,
  string $attr,
  int $index,
  float $position,
  int $interpolation,
  float $r,
  float $g,
  float $b )
{
  string $attr_prefix = GetRampAttributePrefix( $node, $attr, $index );
  setAttr ( $attr_prefix + "Position" ) $position;
  setAttr ( $attr_prefix + "Color" ) $r $g $b;
  setAttr ($attr_prefix + "Interp" ) $interpolation;
}

