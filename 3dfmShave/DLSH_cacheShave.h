#ifndef __DLSH_CACHESHAVE_H__
#define __DLSH_CACHESHAVE_H__

#include <maya/MPxCommand.h>

class DLSH_cacheShave : public MPxCommand
{
public:
  virtual MStatus       doIt(const MArgList& args);

  virtual bool          isUndoable() const
                        { return false; }

  static void*          creator()
                        { return new DLSH_cacheShave; }

  static MSyntax        newSyntax();
private:

  MStatus               doAdd(MDagPath& selected_path, float sample_time);
  MStatus               doRemove(MDagPath& selected_path);
  MStatus               doEmit(MDagPath& selected_path, float sample_time);
  MStatus               doEmit(MDagPath& selected_path);
};

#endif
